<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'Laravel'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services the application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    'asset_url' => env('ASSET_URL', null),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'Asia/Jakarta',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'id',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Faker Locale
    |--------------------------------------------------------------------------
    |
    | This locale will be used by the Faker PHP library when generating fake
    | data for your database seeds. For example, this will be used to get
    | localized telephone numbers, street address information and more.
    |
    */

    'faker_locale' => 'en_US',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Package Service Providers...
         */

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

        #datatables
        Yajra\DataTables\DataTablesServiceProvider::class,
        #excel maatwebsite/excel
        Maatwebsite\Excel\ExcelServiceProvider::class,
        App\Providers\MacroServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Arr' => Illuminate\Support\Arr::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'Str' => Illuminate\Support\Str::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,
        'DataTables' => Yajra\DataTables\Facades\DataTables::class,
        'Excel' => Maatwebsite\Excel\Facades\Excel::class,
    ], 
    'crawler_url' => env('CRAWLER_URL', 'whatever.test/api/test'),
    'roles' => [
        'super_admin' => ['role_name' => 'Super Admin', 'role_code' => 1000],
        'operator' => ['role_name' => 'Operator', 'role_code' => 1100],
        'direktur' => ['role_name' => 'Direktur', 'role_code' => 1200],
        'koordinator' => ['role_name' => 'Koordinator', 'role_code' => 1300],
        'petugas' => ['role_name' => 'Petugas', 'role_code' => 1400],
        'petugas_balai' => ['role_name' => 'Petugas Balai', 'role_code' => 1500],
        'koordinator_balai' => ['role_name' => 'Koordinator Balai', 'role_code' => 1700],
        'kepala_balai' => ['role_name' => 'Kepala Balai', 'role_code' => 1800],
        'stakeholder' => ['role_name' => 'Stakeholder', 'role_code' => 1600],
        'petugas_loka' => ['role_name' => 'Petugas Loka', 'role_code' => 1900],
        'kepala_loka' => ['role_name' => 'Kepala Loka', 'role_code' => 2000],
    ],
    'nologo' => 'nologo.png',
    'commodities' => [
        'cosmetic' => ['id_commodity' => 3100, 'commodity' => 'Kosmetika'],
        'medicine' => ['id_commodity' => 3200, 'commodity' => 'Obat'],
        'suplemen' => ['id_commodity' => 3300, 'commodity' => 'Suplemen Kesehatan'],
        'traditional_midicine' => ['id_commodity' => 3400, 'commodity' => 'Obat Tradisional'],
        'nappza' => ['id_commodity' => 3500, 'commodity' => 'Nappza'],
        'food' => ['id_commodity' => 3600, 'commodity' => 'Pangan Olahan'],
        'local_traditional_midicine' => ['id_commodity' => 3700, 'commodity' => 'Obat Tradisional Lokal'],
        'import_traditional_midicine' => ['id_commodity' => 3800, 'commodity' => 'Obat Tradisional Import'],
        'local_suplemen' => ['id_commodity' => 3900, 'commodity' => 'Suplemen Kesehatan Lokal'],
        'import_suplemen' => ['id_commodity' => 3110, 'commodity' => 'Suplemen Kesehatan Import'],
    ],
    'status' => [
        'active' => ['id_status' => 1, 'status' => 'Aktif'],
        'takedown' => ['id_status' => 2, 'status' => 'Takedown'],
        'tllapangan' => ['id_status' => 3, 'status' => 'TL Lapangan'],
        'arsip' => ['id_status' => 4, 'status' => 'Arsip'],
        'inactive' => ['id_status' => 5, 'status' => 'Tidak Aktif'],
        'guidance' => ['id_status' => 6, 'status' => 'Pembinaan'],
        'profilling' => ['id_status' => 7, 'status' => 'Profilling'],
        'drafttakedown' => ['id_status' => 8, 'status' => 'Draft Takedown'],
        'draftprofiling' => ['id_status' => 9, 'status' => 'Draft Profilling'],
        'draftpembinaan' => ['id_status' => 10, 'status' => 'Draft Pembinaan']
    ],
    'offenses' => [
        ['id_offense' => 1, 'offense' => 'Tanpa Izin Edar', 'acronym' => 'TIE'],
        ['id_offense' => 2, 'offense' => 'Penjualan Tanpa Kewenangan', 'acronym' => 'PTK'],
        ['id_offense' => 3, 'offense' => 'Pemalsuan Produk', 'acronym' => 'PP'],
        ['id_offense' => 4, 'offense' => 'Tanpa Izin Edar & Mengandung Bahan Berbahaya', 'acronym' => 'TIEBB'],
        ['id_offense' => 5, 'offense' => 'Tanpa Izin Edar & Mengandung Bahan Kimia Obat', 'acronym' => 'TIEBKO'],
        ['id_offense' => 6, 'offense' => 'Repacking', 'acronym' => 'RPKG'],
        ['id_offense' => 7, 'offense' => 'Pelanggaran Lain - lain', 'acronym' => 'PLL'],
    ],
    'crawling_type' => [
        ['id_crawling_type' => 1, 'type' => 'Patber Obat Covid19'],
        ['id_crawling_type' => 2, 'type' => 'Patber NPP, OOT, Misoprostol'],
        ['id_crawling_type' => 3, 'type' => 'Patber dari Laporan ULPK'],
        ['id_crawling_type' => 4, 'type' => 'Patber dari Laporan Kedeputian I,II,III'],
        ['id_crawling_type' => 5, 'type' => 'Patber Semua Komoditas'],
        ['id_crawling_type' => 6, 'type' => 'Patber Bulanan dari UPT'],
        ['id_crawling_type' => 7, 'type' => 'Patber Nodin Rahasia']
    ],
    "takedown_import_file" => "xlsx/bpom_takedown_import_template_v2.xlsx",
    "marketplaces" => [

        [
            'id_marketplace' => 1,
            'name' => 'blibli',
            'description' => 'Blibli terbaik',
        ],

        [
            'id_marketplace' => 2,
            'name' => 'bukalapak',
            'description' => 'bukalapak terbaik',

        ],

        [
            'id_marketplace' => 3,
            'name' => 'elevenia',
            'description' => 'elevenia terbaik',

        ],

        [
            'id_marketplace' => 4,
            'name' => 'olx',
            'description' =>
            'olx terbaik',

        ],

        [
            'id_marketplace' => 5,
            'name' => 'jdid',
            'description' => 'jdid terbaik',

        ],

        [
            'id_marketplace' => 6,
            'name' => 'shopee',
            'description' => 'shopee terbaik',

        ],


        [
            'id_marketplace' => 7,
            'name' => 'tokopedia',
            'description' => 'tokopedia terbaik',

        ],
        [
            'id_marketplace' => 8,
            'name' => 'lazada',
            'description' => 'lazada terbaik',

        ],
        // [
        //     'id_marketplace' => 9,
        //     'name' => 'blanja',
        //     'description' => 'blanja terbaik',

        // ],
        [
            'id_marketplace' => 10,
            'name' => 'twitter',
            'description' => 'twitter',

        ],
        [
            'id_marketplace' => 11,
            'name' => 'facebook',
            'description' => 'facebook',

        ],
        [
            'id_marketplace' => 12,
            'name' => 'instagram',
            'description' => 'instagram',

        ],
        [
            'id_marketplace' => 13,
            'name' => 'youtube',
            'description' => 'youtube',

        ],
        [
            'id_marketplace' => 8,
            'name' => 'tiktok',
            'description' => 'tiktok',

        ],

    ],
    "upt" => [
        [
            'name' => 'Nasional',
            'location' => '*',
            'working_area' => 'Aceh, Sumatera Utara, Sumatera Barat, Riau, Jambi, Sumatera Selatan, Bengkulu, Lampung, Kepulauan Bangka Belitung, Kepulauan Riau, DKI Jakarta, Jawa Barat, Jawa Tengah, DI Yogyakarta, Jawa Timur, Banten, Bali, Nusa Tenggara Barat, Nusa Tenggara Timur, Kalimantan Barat, Kalimantan Tengah, Kalimantan Selatan, Kalimantan Timur, Kalimantan Utara, Sulawesi Utara, Sulawesi Tengah, Sulawesi Selatan, Sulawesi Tenggara, Gorontalo, Sulawesi Barat, Maluku, Maluku Utara, Papua Barat, Papua'
        ],
        [
            'name' => 'Balai Besar POM Banda Aceh',
            'location' => 'Kota Banda Aceh',
            'working_area' => 'Kota Banda Aceh, Kabupaten Aceh Barat, Kabupaten Aceh Barat Daya, Kabupaten Aceh Besar, Kabupaten Aceh Jaya, Kabupaten Aceh Tamiang, Kabupaten Aceh Timur, Kabupaten Aceh Utara, Kabupaten Bireuen, Kabupaten Nagan Raya, Kabupaten Pidie, Kabupaten Pidie Jaya, Kabupaten Simeulue, Kota Langsa, Kota Lhokseumawe, Kota Sabang'
        ],
        [
            'name' => 'Balai Besar POM Medan',
            'location' => 'Kota Medan',
            'working_area' => 'Kota Medan, Kabupaten Deli Serdang, Kabupaten Nias Selatan, Kabupaten Karo, Kabupaten Serdang Bedagai, Kabupaten Mandailing Natal, Kota Pematang Siantar, Kabupaten Tapanuli Selatan, Kota Binjai, Kabupaten Dairi, Kabupaten Padang Lawas, Kabupaten Tapanuli Tengah, Kabupaten Padang Lawas Utara, Kabupaten Batu Bara, Kabupaten Nias Utara, Kota Tebing Tinggi, Kabupaten Humbang Hasundutan, Kabupaten Nias, Kota Gunungsitoli, Kota Padangsidimpuan, Kota Sibolga, Kabupaten Nias Barat, Kabupaten Pakpak Bharat, Kabupaten Langkat'
        ],
        [
            'name' => 'Balai Besar POM Padang',
            'location' => 'Kota Padang',
            'working_area' => 'Kota Padang, Kabupaten Pasaman Barat, Kabupaten Pesisir Selatan, Kabupaten Tanah Datar, Kabupaten Pasaman, Kabupaten Padang Pariaman, Kabupaten Kepulauan Mentawai, Kota Padang Panjang, Kota Pariaman, Kota Sawah Lunto, Kabupaten Solok, Kabupaten Solok Selatan, Kota Solok'
        ],
        [
            'name' => 'Balai Besar POM Pekanbaru',
            'location' => 'Kota Pekanbaru',
            'working_area' => 'Kota Pekanbaru, Kabupaten Kampar, Kabupaten Pelalawan, Kabupaten Rokan Hilir, Kabupaten Kuantan Singingi, Kabupaten Rokan Hulu, Kabupaten Siak, Kabupaten Kepulauan Meranti'
        ],
        [
            'name' => 'Balai Besar POM Palembang',
            'location' => 'Kota Palembang',
            'working_area' => 'Kota Palembang, Kabupaten Banyu Asin, Kabupaten Muara Enim, Kabupaten Musi Banyuasin, Kabupaten Ogan Ilir, Kabupaten Ogan Komering Ilir, Kabupaten Ogan Komering Ulu, Kabupaten Ogan Komering Ulu Selatan, Kabupaten Ogan Komering Ulu Timur, Kabupaten Penukal Abab Lematang Ilir, Kota Prabumulih, Kota Pagar Alam'
        ],
        [
            'name' => 'Balai Besar POM Bandar Lampung',
            'location' => 'Kota Bandar Lampung',
            'working_area' => 'Kota Bandar Lampung, Kabupaten Lampung Selatan, Kabupaten Lampung Tengah, Kabupaten Lampung Timur, Kabupaten Lampung Utara, Kabupaten Tanggamus, Kabupaten Pringsewu, Kabupaten Pesawaran, Kabupaten Way Kanan, Kota Metro, Kabupaten Lampung Barat, Kabupaten Pesisir Barat'
        ],
        [
            'name' => 'Balai Besar POM Jakarta',
            'location' => 'Kota Jakarta',
            'working_area' => 'Kota Jakarta Timur, Kabupaten Kepulauan Seribu, Kota Jakarta Barat, Kota Jakarta Pusat, Kota Jakarta Selatan, Kota Jakarta Utara'
        ],
        [
            'name' => 'Balai Besar POM Bandung',
            'location' => 'Kota Bandung',
            'working_area' => 'Kota Bandung, Kabupaten Subang, Kabupaten Cianjur, Kabupaten Garut, Kabupaten Bandung Barat, Kabupaten Majalengka, Kabupaten Sumedang, Kabupaten Bandung,  Kabupaten Karawang, Kabupaten Bekasi, Kota Bekasi, Kabupaten Sukabumi, Kota Cimahi, Kota Sukabumi, Kabupaten Purwakarta, Kabupaten Cirebon, Kota Cirebon, Kabupaten Indramayu, Kabupaten Kuningan'
        ],
        [
            'name' => 'Balai Besar POM Semarang',
            'location' => 'Kota Semarang',
            'working_area' => 'Kota Semarang, Kabupaten Grobogan, Kabupaten Demak, Kabupaten Kebumen, Kabupaten Klaten, Kabupaten Pati, Kabupaten Jepara, Kabupaten Kendal, Kabupaten Magelang, Kabupaten Semarang, Kabupaten Batang, Kabupaten Boyolali, Kabupaten Purworejo, Kabupaten Rembang, Kabupaten Temanggung, Kabupaten Wonosobo, Kabupaten Pekalongan, Kabupaten Kudus, Kabupaten Blora, Kota Magelang, Kota Pekalongan, Kota Salatiga, Kabupaten Tegal, Kota Tegal, Kabupaten Brebes, Kabupaten Pemalang'
        ],
        [
            'name' => 'Balai Besar POM Yogyakarta',
            'location' => 'Kota Yogyakarta',
            'working_area' => 'Kota Yogyakarta, Kabupaten Bantul, Kabupaten Gunung Kidul, Kabupaten Kulon Progo, Kabupaten Sleman'
        ],
        [
            'name' => 'Balai Besar POM Surabaya',
            'location' => 'Kota Surabaya',
            'working_area' => 'Kota Surabaya, Kabupaten Bojonegoro, Kabupaten Malang, Kabupaten Mojokerto, Kabupaten Sidoarjo, Kabupaten Jombang, Kabupaten Nganjuk, Kabupaten Gresik, Kabupaten Pasuruan, Kabupaten Probolinggo, Kabupaten Tuban, Kabupaten Ponorogo, Kabupaten Pacitan, Kabupaten Bangkalan, Kota Malang, Kabupaten Sampang, Kabupaten Sumenep, Kabupaten Lamongan, Kabupaten Pamekasan, Kabupaten Madiun, Kabupaten Ngawi, Kota Pasuruan, Kota Batu, Kabupaten Magetan, Kota Mojokerto, Kota Madiun, Kota Probolinggo'
        ],
        [
            'name' => 'Balai Besar POM Serang',
            'location' => 'Kota Serang',
            'working_area' => 'Kota Serang, Kabupaten Lebak, Kabupaten Serang, Kota Cilegon, Kota Tangerang, Kota Tangerang Selatan, Kabupaten Pandeglang'
        ],
        [
            'name' => 'Balai Besar POM Denpasar',
            'location' => 'Kota Denpasar',
            'working_area' => 'Kota Denpasar, Kabupaten Tabanan, Kabupaten Badung, Kabupaten Gianyar, Kabupaten Karang Asem, Kabupaten Bangli, Kabupaten Klungkung'
        ],
        [
            'name' => 'Balai Besar POM Mataram',
            'location' => 'Kota Mataram',
            'working_area' => 'Kota Mataram, Kabupaten Lombok Tengah, Kabupaten Lombok Timur, Kabupaten Sumbawa, Kabupaten Lombok Barat, Kabupaten Lombok Utara, Kabupaten Sumbawa Barat'
        ],
        [
            'name' => 'Balai Besar POM Pontianak',
            'location' => 'Kota Pontianak',
            'working_area' => 'Kota Pontianak, Kabupaten Sambas, Kabupaten Sintang, Kabupaten Kubu Raya, Kabupaten Kapuas Hulu, Kabupaten Ketapang, Kota Singkawang, Kabupaten Mempawah, Kabupaten Melawi, Kabupaten Bengkayang, Kabupaten Landak, Kabupaten Kayong Utara'
        ],
        [
            'name' => 'Balai Besar POM Palangka Raya',
            'location' => 'Kota Palangka Raya',
            'working_area' => 'Kota Palangka Raya, Kabupaten Kapuas, Kabupaten Katingan, Kabupaten Barito Selatan, Kabupaten Barito Timur, Kabupaten Barito Utara, Kabupaten Gunung Mas, Kabupaten Murung Raya, Kabupaten Pulang Pisau, Kabupaten Kotawaringin Timur'
        ],
        [
            'name' => 'Balai Besar POM Banjarmasin',
            'location' => 'Kota Banjarmasin',
            'working_area' => 'Kota Banjarmasin, Kota Banjarbaru, Kabupaten Banjar, Kabupaten Tanah Laut, Kabupaten Barito Kuala, Kabupaten Tapin, Kabupaten Hulu Sungai Selatan, Kabupaten Hulu Sungai Tengah'
        ],
        [
            'name' => 'Balai Besar POM Samarinda',
            'location' => 'Kota Samarinda',
            'working_area' => 'Kota Samarinda, Kabupaten Kutai Kartanegara, Kabupaten Kutai Timur, Kabupaten Paser, Kabupaten Berau, Kabupaten Kutai Barat, Kabupaten Penajam Paser Utara, Kota Bontang, Kabupaten Mahakam Ulu'
        ],
        [
            'name' => 'Balai Besar POM Manado',
            'location' => 'Kota Manado',
            'working_area' => 'Kota Manado, Kabupaten Minahasa, Kota Bitung, Kabupaten Minahasa Utara, Kabupaten Bolaang Mongondow, Kabupaten Minahasa Selatan, Kota Tomohon, Kabupaten Minahasa Tenggara, Kota Kotamobagu, Kabupaten Bolaang Mongondow Timur, Kabupaten Bolaang Mongondow Utara, Kabupaten Bolaang Mongondow Selatan'
        ],
        [
            'name' => 'Balai Besar POM Makassar',
            'location' => 'Kota Makassar',
            'working_area' => 'Kota Makassar, Kabupaten Bone, Kabupaten Gowa, Kabupaten Pangkajene dan Kepulauan, Kabupaten Bulukumba, Kabupaten Maros, Kabupaten Sidenreng Rappang, Kabupaten Soppeng, Kabupaten Wajo, Kabupaten Selayar, Kabupaten Pinrang, Kabupaten Sinjai, Kabupaten Bantaeng, Kabupaten Jeneponto, Kabupaten Barru, Kabupaten Takalar, Kota Pare-Pare'
        ],
        [
            'name' => 'Balai Besar POM Jayapura',
            'location' => 'Kota Jayapura',
            'working_area' => 'Kota Jayapura, Kabupaten Jayapura, Kabupaten Nabire, Kabupaten Biak Numfor, Kabupaten Kepulauan Yapen, Kabupaten Keerom, Kabupaten Paniai, Kabupaten Pegunungan Bintang, Kabupaten Sarmi, Kabupaten Waropen, Kabupaten Dogiyai, Kabupaten Yahukimo, Kabupaten Mamberamo Raya, Kabupaten Supiori, Kabupaten Yalimo, Kabupaten Deiyai, Kabupaten Jayawijaya, Kabupaten Puncak Jaya, Kabupaten Tolikara, Kabupaten Memberamo Tengah'
        ]
        // ULPK Loka POM Kabupaten Aceh Selatan
        // ULPK Loka POM Kabupaten Aceh Tengah
        // ULPK Loka POM Kabupaten Banggai
        // ULPK Loka POM Kabupaten Banyumas
        // ULPK Loka POM Kabupaten Belitung
        // ULPK Loka POM Kabupaten Bima
        // ULPK Loka POM Kabupaten Bogor
        // ULPK Loka POM Kabupaten Buleleng
        // ULPK Loka POM Kabupaten Dharmasraya
        // ULPK Loka POM Kabupaten Ende
        // ULPK Loka POM Kabupaten Hulu Sungai Utara
        // ULPK Loka POM Kabupaten Indragiri Hilir
        // ULPK Loka POM Kabupaten Jember
        // ULPK Loka POM Kabupaten Kediri
        // ULPK Loka POM Kabupaten Kepulauan Sangihe
        // ULPK Loka POM Kabupaten Kotawaringin Barat
        // ULPK Loka POM Kabupaten Maluku Tenggara Barat
        // ULPK Loka POM Kabupaten Manggarai Barat
        // ULPK Loka POM Kabupaten Merauke
        // ULPK Loka POM Kabupaten Mimika
        // ULPK Loka POM Kabupaten Pulau Morotai
        // ULPK Loka POM Kabupaten Rejang Lebong
        // ULPK Loka POM Kabupaten Sanggau
        // ULPK Loka POM Kabupaten Sorong
        // ULPK Loka POM Kabupaten Tanah Bumbu
        // ULPK Loka POM Kabupaten Tangerang
        // ULPK Loka POM Kabupaten Toba Samosir
        // ULPK Loka POM Kabupaten Tulang Bawang
        // ULPK Loka POM Kota Balikpapan
        // ULPK Loka POM Kota Baubau
        // ULPK Loka POM Kota Dumai
        // ULPK Loka POM Kota Lubuklinggau
        // ULPK Loka POM Kota Palopo
        // ULPK Loka POM Kota Payakumbuh
        // ULPK Loka POM Kota Sungai Penuh
        // ULPK Loka POM Kota Surakarta
        // ULPK Loka POM Kota Tanjungbalai
        // ULPK Loka POM Kota Tanjungpinang
        // ULPK Loka POM Kota Tarakan
        // ULPK Loka POM Kota Tasikmalaya
    ]
];
