(function ($, DataTable) {
    "use strict";

    var _buildParams = function (dt, action, onlyVisibles) {
        var params = dt.ajax.params();
        params.action = action;
        params._token = $('meta[name="csrf-token"]').attr('content');

        if (onlyVisibles) {
            params.visible_columns = _getVisibleColumns();
        } else {
            params.visible_columns = null;
        }
        
        return params;
    };
    
    var _getVisibleColumns = function () {

        var visible_columns = [];
        $.each(DataTable.settings[0].aoColumns, function (key, col) {
            if (col.bVisible) {
                visible_columns.push(col.name);
            }
        });

        return visible_columns;
    };

    var _downloadFromUrl = function (url, params) {
        var postUrl = url + '/export';
        var xhr = new XMLHttpRequest();
        xhr.open('POST', postUrl, true);
        xhr.responseType = 'arraybuffer';
        xhr.onload = function () {
            if (this.status === 200) {
                var filename = "";
                var disposition = xhr.getResponseHeader('Content-Disposition');
                if (disposition && disposition.indexOf('attachment') !== -1) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                }
                var type = xhr.getResponseHeader('Content-Type');

                var blob = new Blob([this.response], {type: type});
                if (typeof window.navigator.msSaveBlob !== 'undefined') {
                    // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                    window.navigator.msSaveBlob(blob, filename);
                } else {
                    var URL = window.URL || window.webkitURL;
                    var downloadUrl = URL.createObjectURL(blob);

                    if (filename) {
                        // use HTML5 a[download] attribute to specify filename
                        var a = document.createElement("a");
                        // safari doesn't support this yet
                        if (typeof a.download === 'undefined') {
                            window.location = downloadUrl;
                        } else {
                            a.href = downloadUrl;
                            a.download = filename;
                            document.body.appendChild(a);
                            a.click();
                        }
                    } else {
                        window.location = downloadUrl;
                    }

                    setTimeout(function () {
                        URL.revokeObjectURL(downloadUrl);
                    }, 100); // cleanup
                }
            }
        };
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.send($.param(params));
    };

    var _buildUrl = function(dt, action) {
        var url = dt.ajax.url() || '';
        var params = dt.ajax.params();
        params.action = action;

        if (url.indexOf('?') > -1) {
            return url + '&' + $.param(params);
        }
        
        return url + '?' + $.param(params);
    };

    DataTable.ext.buttons.excel = {
        className: 'btn btn-default btn-sm mr-2',

        text: function (dt) {
            return '<i class="fas fa-file-export"></i> ' + dt.i18n('buttons.excel', 'Export');
        },

        action: function (e, dt, button, config) {
            var url = _buildUrl(dt, 'excel');

            const selected = [];
            const checkboxChecked = $('tbody tr').find('td:first input[type=checkbox]:checked');
            if (checkboxChecked.length > 0) {
                checkboxChecked.each(function() {
                    selected.push($(this).data('id'));
                })
            } else {
                $('tbody tr').find('td:first input[type=checkbox]').each(function() {
                    selected.push($(this).data('id'));
                });
            }

            window.location = url + "&selected="+ selected;
        }
    };

    DataTable.ext.buttons.postExcel = {
        className: 'buttons-excel',

        text: function (dt) {
            return '<i class="fa fa-file-excel-o"></i> ' + dt.i18n('buttons.excel', 'Excel');
        },

        action: function (e, dt, button, config) {
            var url = dt.ajax.url() || window.location.href;
            var params = _buildParams(dt, 'excel');

            _downloadFromUrl(url, params);
        }
    };
    
    DataTable.ext.buttons.postExcelVisibleColumns = {
        className: 'buttons-excel',

        text: function (dt) {
            return '<i class="fa fa-file-excel-o"></i> ' + dt.i18n('buttons.excel', 'Excel (only visible columns)');
        },

        action: function (e, dt, button, config) {
            var url = dt.ajax.url() || window.location.href;
            var params = _buildParams(dt, 'excel', true);

            _downloadFromUrl(url, params);
        }
    };

    DataTable.ext.buttons.export = {
        extend: 'collection',

        className: 'buttons-export',

        text: function (dt) {
            return '<i class="fa fa-download"></i> ' + dt.i18n('buttons.export', 'Export') + '&nbsp;<span class="caret"/>';
        },

        buttons: ['csv', 'excel', 'pdf']
    };

    DataTable.ext.buttons.csv = {
        className: 'buttons-csv',

        text: function (dt) {
            return '<i class="fa fa-file-excel-o"></i> ' + dt.i18n('buttons.csv', 'CSV');
        },

        action: function (e, dt, button, config) {
            var url = _buildUrl(dt, 'csv');
            window.location = url;
        }
    };

    DataTable.ext.buttons.postCsvVisibleColumns = {
        className: 'buttons-csv',

        text: function (dt) {
            return '<i class="fa fa-file-excel-o"></i> ' + dt.i18n('buttons.csv', 'CSV (only visible columns)');
        },

        action: function (e, dt, button, config) {
            var url = dt.ajax.url() || window.location.href;
            var params = _buildParams(dt, 'csv', true);

            _downloadFromUrl(url, params);
        }
    };
    
    DataTable.ext.buttons.postCsv = {
        className: 'buttons-csv',

        text: function (dt) {
            return '<i class="fa fa-file-excel-o"></i> ' + dt.i18n('buttons.csv', 'CSV');
        },

        action: function (e, dt, button, config) {
            var url = dt.ajax.url() || window.location.href;
            var params = _buildParams(dt, 'csv');

            _downloadFromUrl(url, params);
        }
    };

    DataTable.ext.buttons.pdf = {
        className: 'buttons-pdf',

        text: function (dt) {
            return '<i class="fa fa-file-pdf-o"></i> ' + dt.i18n('buttons.pdf', 'PDF');
        },

        action: function (e, dt, button, config) {
            var url = _buildUrl(dt, 'pdf');
            window.location = url;
        }
    };

    DataTable.ext.buttons.postPdf = {
        className: 'buttons-pdf',

        text: function (dt) {
            return '<i class="fa fa-file-pdf-o"></i> ' + dt.i18n('buttons.pdf', 'PDF');
        },

        action: function (e, dt, button, config) {
            var url = dt.ajax.url() || window.location.href;
            var params = _buildParams(dt, 'pdf');

            _downloadFromUrl(url, params);
        }
    };

    DataTable.ext.buttons.print = {
        className: 'buttons-print',

        text: function (dt) {
            return  '<i class="fa fa-print"></i> ' + dt.i18n('buttons.print', 'Print');
        },

        action: function (e, dt, button, config) {
            var url = _buildUrl(dt, 'print');
            window.location = url;
        }
    };

    DataTable.ext.buttons.reset = {
        className: 'buttons-reset',

        text: function (dt) {
            return '<i class="fa fa-undo"></i> ' + dt.i18n('buttons.reset', 'Reset');
        },

        action: function (e, dt, button, config) {
            dt.search('');
            dt.columns().search('');
            dt.draw();
        }
    };

    DataTable.ext.buttons.reload = {
        className: 'buttons-reload',

        text: function (dt) {
            return '<i class="fa fa-refresh"></i> ' + dt.i18n('buttons.reload', 'Reload');
        },

        action: function (e, dt, button, config) {
            dt.draw(false);
        }
    };

    DataTable.ext.buttons.create = {
        className: 'btn btn-default btn-sm mr-2 btn-add',

        text: function (dt) {
            return '<i class="fas fa-plus"></i> ' + dt.i18n('buttons.create', 'Create');
        },

        action: function (e, dt, button, config) {
            const url = new URL(window.location.href.replace(/\/+$/, ""))
            window.location = `${url.origin}${url.pathname}/create`
        }
    };

    DataTable.ext.buttons.import = {
        className: 'btn btn-default btn-sm mr-2 btn-add',

        text: function (dt) {
            return '<i class="fas fa-file-import"></i> ' + dt.i18n('buttons.import', 'Import');
        },

        action: function (e, dt, button, config) {
            const url = new URL(window.location.href.replace(/\/+$/, ""))
            window.location = `${url.origin}${url.pathname}/import`
        }
    };

    DataTable.ext.buttons.submit = {
        className: 'btn btn-primary btn-submit',

        text: function (dt) {
            return dt.i18n('buttons.submit', 'Submit');
        },

        action: function (e, dt, button, config) {
            var url = _buildUrl(dt, 'submit');
            const checkboxes = dt.column(0).checkboxes.selected();
            const selected = [];
            $.each(checkboxes, function(_rowIndex, value) {
                selected.push(value);
            });

            $.ajax({
                type: "GET",
                url: url + "&selected=" + selected,
                success: function (response) {
                    dt.draw(false);
                },
                error: (xhr, status, error) => {
                }
            });
        }
    };

    DataTable.ext.buttons.reject = {
        className: 'btn btn-danger',

        text: function (dt) {
            return dt.i18n('buttons.reject', 'Tolak');
        },

        action: function (e, dt, button, config) {
            const rejectForm = $('#reject-form');
            rejectForm.on('submit', function(e) {
                e.preventDefault();
                const message = rejectForm.serialize();

                var url = _buildUrl(dt, 'reject');

                const checkboxes = dt.column(0).checkboxes.selected();
                const selected = [];
                $.each(checkboxes, function(_rowIndex, value) {
                    selected.push(value);
                });

                $.ajax({
                    type: "GET",
                    url: url + "&selected=" + selected + "&" + message,
                    success: function (response) {
                        $('#reject-message').val('');
                        $('#reject-modal').modal('hide');
                        dt.draw(false);
                    },
                    error: (xhr, status, error) => {
                    }
                });
            })
        }
    };

    DataTable.ext.buttons.delete = {
        className: 'btn btn-default btn-sm mr-2',

        text: function (dt) {
            return '<i class="far fa-trash-alt"></i> ' + dt.i18n('buttons.delete', 'Delete');
        },

        action: function (e, dt, button, config) {
            var url = _buildUrl(dt, 'delete');
            
            const checkboxes = dt.column(-1).checkboxes.selected();
            const selected = [];
            $.each(checkboxes, function(_rowIndex, value) {
                selected.push(value);
            });

            $.ajax({
                type: "GET",
                url: url + "&selected=" + selected,
                success: function (response) {
                    $.each(checkboxes, function(rowIndex, value) {
                        dt.row(rowIndex).remove();
                    });

                    if (dt.data().length === 0) {
                        const url = window.location.href;
                        const pathname = window.location.pathname;

                        window.location = url.slice(0, url.lastIndexOf('/'))
                    } else {
                        dt.draw(false);
                    }
                },
                error: (xhr, status, error) => {
                }
            });
        }
    };

    DataTable.ext.buttons.filter = {
        className: 'btn btn-default btn-sm mr-2',

        text: function (dt) {
            return '<i class="fas fa-filter"></i> ' + dt.i18n('buttons.filter', 'Filter Data');
        },

        action: function (e, dt, button, config) {
            // var url = _buildUrl(dt, 'delete');
            
            // const checkboxes = dt.column(-1).checkboxes.selected();
            // const selected = [];
            // $.each(checkboxes, function(_rowIndex, value) {
            //     selected.push(value);
            // });

            // $.ajax({
            //     type: "GET",
            //     url: url + "&selected=" + selected,
            //     success: function (response) {
            //         $.each(checkboxes, function(rowIndex, value) {
            //             dt.row(rowIndex).remove();
            //         });

            //         if (dt.data().length === 0) {
            //             const url = window.location.href;
            //             const pathname = window.location.pathname;

            //             window.location = url.slice(0, url.lastIndexOf('/'))
            //         } else {
            //             dt.draw(false);
            //         }
            //     },
            //     error: (xhr, status, error) => {
            //     }
            // });
        }
    };

    if (typeof DataTable.ext.buttons.copyHtml5 !== 'undefined') {
        $.extend(DataTable.ext.buttons.copyHtml5, {
            text: function (dt) {
                return '<i class="fa fa-copy"></i> ' + dt.i18n('buttons.copy', 'Copy');
            }
        });
    }

    if (typeof DataTable.ext.buttons.colvis !== 'undefined') {
        $.extend(DataTable.ext.buttons.colvis, {
            text: function (dt) {
                return '<i class="fa fa-eye"></i> ' + dt.i18n('buttons.colvis', 'Column visibility');
            }
        });
    }
})(jQuery, jQuery.fn.dataTable);
