$(() => {
    let datatable;
    datatable = $('.datatable').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "order": [
            [1, 'asc']
        ],
        "info": true,
        "autoWidth": true,
        "serverSide": true,
        "ajax": {
            type: 'GET',
            url: DATATABLE_URL
        },
        "language": {
            "processing": loading()
        },
        "processing": true,
        "columns": [
            {
                searchable: false,
                orderable: false,
                width: 50,
                data: 'name',
                name: 'marketplace.name',
                className: 'align-middle text-center',
            },
            {
                searchable: false,
                data: 'name',
                name: 'marketplace.name',
                className: 'align-middle text-center',
            },
            {
                data: 'crawling',
                searchable: false,
                className: 'align-middle text-wrap'
            },
            {
                searchable: false,
                data: 'active',
                className: 'align-middle text-center'
            },
            {
                searchable: false,
                data: 'takedown',
                className: 'align-middle',
                width: '50px',
            },
            {
                searchable: false,
                data: 'percentage',
                className: 'align-middle',
            },
        ],
        "pageLength": 10,
        "dom": `<"row"<"col-2 mt-2"l><"col-10 mt-1 d-flex flex-row-reverse"<p><"mt-1 mr-3"f>>>t<"bottom"ip>r`,
        "drawCallback": function() {

            // $('#DataTables_Table_0_filter').prepend(`
            //     <button data-toggle="modal" data-target="#advance-search-modal" class='btn btn-default btn-sm mr-2' id="advance-search-button"> <i class='fas fa-search'></i> Advance Search</button>
            // `);
        }
    });

    datatable.on( 'draw.dt', function () {
        datatable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $('#date-filter, #reported-product-status').remove();
    $('#periode-filter').removeClass('d-none').addClass('d-block');
});

$('#btn-terapkan').click((e) => {
    $('#home-filter-modal').modal('hide');
    reloadDatatableWithFilter();
});

function reloadDatatableWithFilter() {

    let commodities = [];
    let searchAdvance = $(`#form-advance-search`).serialize();
    let filter_periode = $('input[name=filter_periode]').val();

    $(`input.commodities:checked`).map((i, val) => {
        commodities.push(parseInt($(val).val()));
    });

    if (filter_periode) {
        const currentMonth = moment(filter_periode, 'YYYY-MM-DD hh:mm:ss');
        const startOfMonth = currentMonth.startOf('month').format('DD MMMM YYYY');
        const endOfMonth = currentMonth.endOf('month').format('DD MMMM YYYY');
        
        $('#firstMonth').text(startOfMonth);
        $('#endMonth').text(endOfMonth);
    } else {
        const currentMonth = moment();
        const startOfMonth = currentMonth.startOf('month').format('DD MMMM YYYY');
        const endOfMonth = currentMonth.endOf('month').format('DD MMMM YYYY');
        
        $('#firstMonth').text(startOfMonth);
        $('#endMonth').text(endOfMonth);
    }

    let data = {
        commodities: commodities,
        filter_periode: filter_periode
    };

    let tbl = $('.datatable').DataTable();
    let url = new URL(DATATABLE_URL, window.location.origin);
    let params = new URLSearchParams(data);

    tbl.ajax.url(`${url}?${params.toString()}&${searchAdvance}`).load();
}

$('#form-advance-search').submit(e => {
    $('#advance-search-modal').modal('hide');
    reloadDatatableWithFilter();
});
