$(() => {
    let table = $('.datatable').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "ajax": $.fn.dataTable.pipeline( {
            url: 'big-seller/data',
            pages: 5 // number of pages to cache
        } ),
        "processing": true,
        "deferRender": true,
        "language": {
            "processing": loading()
        },
        "dom": `<"row"<"col-2 mt-2"l><"col-10 mt-1 d-flex flex-row-reverse"<p><"mt-1 mr-3"f>>>t<"bottom"ip><"bg-transparent"r>`,
        "columns": [
            {
                data: 'keywords',
                className: 'align-middle',
                cache: 'search',
                width: 50
            },
            {
                data: 'store',
                name: 'stores.name',
                className: 'align-middle',
                fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                    $(nTd).html("<a href='/grab/product-list?store="+oData.id_store+"'>"+oData.store+"</a>");
                }    
            },
            {
                data: 'marketplace',
                name: 'marketplace.name',
                className: 'align-middle',
                cache: 'search',
                width: 50
            },
            {
                searchable: false,
                data: 'product_total',
                name: 'product_total',
                className: 'align-middle',
                cache: 'search',
            },
            {
                searchable: false,
                data: 'sold_count',
                name: 'sold_count',
                className: 'align-middle',
                cache: 'search',
            },
            {
                searchable: false,
                data: 'view_count',
                name: 'view_count',
                className: 'align-middle',
            },
            {
                searchable: false,
                data: 'followers_count',
                name: 'followers_count',
                className: 'align-middle text-center',
            }
        ],
        "pageLength": 10,
        "serverSide": true,
        order: [
            [0, "desc"]
        ],
        drawCallback: async (settings, json) => {
            $('[data-toggle="tooltip"]').tooltip();
            $('img.d-block').on("error", function () {
                $(this).attr('src', './img/nologo.png');
            });
            if (settings.json.mp) {
                setButton(false, settings.json.mp);
            }
            $.ajax({
                type: "GET",
                url: "big-seller/data/mp-product-total",
                data: {
                    getTotalMarketplace: true
                },
                dataType: "json",
                success: function (response) {
                    setButton(response.data, settings.json.mp);
                }
            });
        }

    });

    $('#btn-arsip').click(() => submitForm('arsip', table))

    $('.btn-refresh').click((e) => {
        table.ajax.reload();
    });
});

function submitForm(action, table) {
    $('#overlayLoading').css('display', 'block');
    let rowsSelected = table.column(6).checkboxes.selected();
    if (rowsSelected.length > 0) {
        let ids = [];
        $('input.id-checkbox').remove();
        $.each(rowsSelected, (index, id) => {
            ids[index] = id;
            $('#form-grab').append(`
                    <input type="hidden" class="id-checkbox" value="${id}" name='id[]' />
                `);
        });

        let url = $('#form-grab').attr('action') + '/' + action;

        $.ajax({
            type: "POST",
            url: url,
            data: $('#form-grab').serialize(),
            dataType: "json",
            success: function (response) {
                $('input.id-checkbox').remove();
                table.column(6).checkboxes.deselectAll();
                $('#btn-tllapangan').html(`TL Lapangan(0)`)
                $('#btn-arsip').html(`Arsipkan(0)`)
                $('#btn-takedown').html(`Takedown(0)`)
                $('#btn-guidance').html(`Pembinaan(0)`)
                table.ajax.reload();

                showInfo('success', response.message);

            },
            error: (xhr, status, error) => {
                showInfo('error', error)
            }
        });

    } else {
        showInfo('warning', "Anda belum memilih produk. \n\r Silahkan pilih minimal satu produk");
    }
    $('#overlayLoading').css('display', 'none');
};

$('#btn-terapkan').click((e) => {
    $('#home-filter-modal').modal('hide');
    reloadDatatableWithFilter();
});

function reloadDatatableWithFilter(mp = null) {
    let tbl = $('.datatable').DataTable();
    let url = new URL('/big-seller/data', window.location.origin);
    let params = new URLSearchParams({ mp });

    tbl.ajax.url(`${url}?${params.toString()}`).load();
}

function setButton(data, mp = false) {
    if (Array.isArray(data)) {
        $('.btn-store').empty();
        data.map((val, i) => {
            $('.btn-store').append(`
                <a id="id-mp-${val.id_marketplace}" onclick="showOnly('${val.id_marketplace}', this)" class="btn btn-primary mx-1 mt-2 text-light mp-button">${val.marketplace.toUpperCase()} (${val.total}) </a>
            `);
        })
    }
    if (mp) {
        $('a.mp-button').removeClass('active');
        $(`a#id-mp-${mp}`).addClass('active');
    }
}

function showOnly(marketplace, element) {
    $('.btn, .btn-primary, .mx-1').removeClass('active');
    $(element.currentTarget).addClass('active');
    reloadDatatableWithFilter(marketplace);
};

// function showDetail(id) {
//     $.ajax({
//         type: "get",
//         url: `big-seller/show/${id}`,
//         data: {
//             'XXXXXXXX-XXXX-XXX': 'XXX-xxxX-xX-XX-X'
//         },
//         dataType: "json",
//         success: function (response, status, xhr) {
//             showModal(response)
//         },
//         error: (xhr, status, error) => {
//             showInfo('error', status + ' ' + error)
//         },
//     });
// }

// function showModal(data) {
//     $('#crawled-at').html(data.crawled ? moment(data.crawled).format('DD/MM/YYYY hh:mm') : "-");
//     $('#status').html(data.report_status ? data.report_status : "-");
//     $('#product-name').html(data.product_name);
//     $('#product-price').html(new Intl.NumberFormat('id-ID', {
//         style: 'currency',
//         currency: 'IDR',

//     }).format(data.product_price).replace(',00', ''));
//     $("#product-sold-count").html(data.sold_count ? data.sold_count : "-");
//     $("#product-sold-count").html(data.sold_count ? data.sold_count : "-");
//     $("#product-stock").html(data.item_count ? data.item_count : "-");
//     $("#product-rating").html(data.rating ? data.rating : "-");
//     $("#product-link").html(data.product_link ? `<a target="_blank" href='${data.product_link}'>Kunjungi</a>` : "-");
//     $("#product-desc").html(data.product_desc ? data.product_desc : "-");
//     $("#store-name").html(data.id_store && data.store_name ? `<a href="/grab/product-list?store=${data.id_store}">${data.store_name}</a>` : "-");
//     $("#store-link").html(data.store_url ? `<a target="_blank" href='${data.store_url}'>Kunjungi</a>` : "-");
//     $("#store-location").html(data.location ? data.location : "-");
//     $("#store-desc").html(data.store_desc ? data.store_desc : "-");
//     $("#marketplace_name").html(data.marketplace_name ? data.marketplace_name : "-");
//     $("#product-picture").attr('src', null != data.product_picture ? data.product_picture : 'img/noimage.png');
//     $("#product-picture").attr('onerror', `this.onerror=null;this.src='{{url('img/noimage.png')}}`);
//     $('#detail-modal').modal('show');


// }