$(() => {

    let table = $('.datatable').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "ajax": {
            url: 'grab/data',
            cache: true,
            dataType: 'json'
        },
        "processing": true,
        "deferRender": true,
        "language": {
            "processing": loading()
        },
        "dom": `<"row"<"col-2 mt-2"l><"col-10 mt-1 d-flex flex-row-reverse"<p><"mt-1 mr-3"f>>>t<"bottom"ip><"bg-transparent"r>`,
        "columns": [
            {
                searchable: false,
                orderable: false,
                data: 'null',
                render: function (_data, _type, _row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'crawled',
                name: 'crawled',
                className: 'align-middle',
                searchable: false,
                cache: 'search',
                render: (value) => {
                    if (value != null) {
                        return moment(value, 'YYYY-MM-DD hh:mm:ss').format('DD-MM-YYYY');
                    } else {
                        return "-";
                    }
                }
            },
            {
                searchable: false,
                data: 'keywords',
                name: 'keywords.keywords',
                className: 'align-middle',
                cache: 'search',
            },
            {
                searchable: false,
                data: 'logo',
                name: 'mp',
                className: 'align-middle',
                cache: 'search',
                render: (value, x, data) => {
                    let marketplaceName = data.mp != null ? data.mp : '-';
                    return `
                        <img src='./img/${value}' class='rounded mx-auto d-block' style="max-width:45px" onerror="replcaceImageWithHTML(this, '${marketplaceName}')"/>
                    `;
                }
            },
            {

                data: 'product',
                name: 'products.name',
                className: 'align-middle',
                cache: 'search',
            },
            {
                searchable: false,
                data: 'commodity',
                name: 'commodity',
                className: 'align-middle',
                cache: 'search',
            },
            {
                searchable: false,
                data: 'store_location',
                name: 'store_location',
                className: 'align-middle',
                cache: 'search',
            },
            {
                searchable: false,
                data: 'sold_count',
                name: 'sold_count',
                className: 'align-middle',
                cache: 'search',
            },
            {
                searchable: false,
                data: 'price',
                name: 'price',
                render: (value) => {
                    return new Intl.NumberFormat('id-ID', {
                        style: 'currency',
                        currency: 'IDR',

                    }).format(value).replace(',00', '')
                },
                className: 'align-middle text-center',
            },
            {
                searchable: false,
                data: 'link_id',
                name: 'links.id',
                className: 'align-middle',
                cache: 'search',
                orderable: false,
                // render: (value, b, c, d) => {
                //     return `<button onclick="reportProduct(${value})" class='btn btn-xs btn-primary'>Laporkan</button>`
                // }
            },
        ],
        "pageLength": 10,
        "serverSide": true,
        "columnDefs": [
            {
                targets: -1,
                'checkboxes': {
                    'selectRow': true
                }
            },
        ],
        order: [
            [1, "desc"]
        ],
        drawCallback: async (settings, json) => {
            $('[data-toggle="tooltip"]').tooltip();
            $('img.d-block').on("error", function () {
                $(this).attr('src', './img/nologo.png');
            });
            $('#advance-search-button').remove();
            $('#DataTables_Table_0_filter').prepend(`
                <button data-toggle="modal" data-target="#advance-search-modal" class='btn btn-default btn-sm mr-2' id="advance-search-button"> <i class='fas fa-search'></i> Advance Search</button>
            `);

            $('.product-list').click(function(e) {
                e.preventDefault();
                $.ajax({
                    type: "get",
                    url: e.target.href,
                    data: {
                        'XXXXXXXX-XXXX-XXX': 'XXX-xxxX-xX-XX-X'
                    },
                    dataType: "json",
                    success: function (response, status, xhr) {
                        showModal(response)
                    },
                    error: (xhr, status, error) => {
                        showInfo('error', status + ' ' + error)
                    },
                });
            });

            $('.btn-filter-marketplace').click(function(e) {
                $('.btn-filter-marketplace').removeClass('active')
                $(this).toggleClass('active');
                
                const marketplace = $(this).data('marketplace');
                reloadDatatableWithFilter(marketplace);
            });

            generate_marketplace_button();
        },
        'select': {
            'style': 'multi'
        },
    });

    function generate_marketplace_button()
    {
        const token = $('[name=csrf-token]').attr('content');
        $.ajax({
            url: `/grab/data/mp-product-total?${token}=${token}&getTotalMarketplace=true`,
            success: function(e) {
                const data = e.data;

                for (const item of data) {
                    const elem = $(`[data-marketplace=${item.marketplace}]`)
                    if (elem) {
                        $(elem).html(`${item.marketplace.toUpperCase()} (${item.total})`)
                    }
                }
            }
        })
    }

    $('.takedown-tllapangan').click((e) => {
        let action = $(e.currentTarget).attr('data-btn');
        let rowsSelected = table.column(-1).checkboxes.selected();
        if (rowsSelected.length <= 0) {
            showInfo('warning', "Anda belum memilih produk. \n\r Silahkan pilih minimal satu produk");
            return false;
        }

        let key = $('meta[name=csrf-token]').attr('content');
        let html = `
            <div class='form-group'>
            <select id="id_crawling_type" class='form-control' required>
                <option disabled selected>Jenis Laporan</option>
                    ${crawling_type.map(({type, id_crawling_type}) => `<option value='${id_crawling_type}'>${type}</option>`)}
            </select> 
            </div>
            <div class='form-group'>
                <select id="id_offense" class='form-control' required>
                    <option disabled selected>Pilih Pelanggaran</option>
                    ${offenses.map(({offense, id_offense}) => `<option value='${id_offense}'>${offense}</option>`)}
                </select>
            </div>
            <div class="form-group">
                <label class="float-left">Pilih Tanggal Takedown</label>
                <input  id="datetk" data-date-format="Ymd" type="date" class="form-control" required>
            </div>
            <div class="form-group">
                <label class="float-left">Keterangan (optional)</label>
                <textarea id="note"  class="form-control"></textarea>
            </div>
            `;

        Swal.fire({
            title: "Takedown",
            html: html,
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            preConfirm: (value) => {
                if (value) {
                    if ($('#id_offense').val() == null) {
                        swal.showValidationMessage(
                            "Silahkan pilih pelanggaran!"
                        )
                    }else if($('#id_crawling_type').val() == null) {
                        swal.showValidationMessage(
                            "Silahkan pilih Jenis Laporan!"
                        )
                    } else {
                        Swal.resetValidationMessage();
                    }

                    return {
                        id_crawling_type: $('#id_crawling_type').val(),
                        id_offense: $('#id_offense').val(),
                        datetk: $('#datetk').val(),
                        note: $('#note').val()
                    }
                }
            }
        }).then(({
            value
        }) => {
            $('input[name=id_crawling_type]').val(value.id_crawling_type);
            $('input[name=id_offense]').val(value.id_offense);
            $('input[name=datetk]').val(value.datetk);
            $('input[name=note]').val(value.note);
            submitForm(action);
        });
    })

    table.on('change', 'input[type=checkbox]', (e) => {
        let rowsSelected = table.column(-1).checkboxes.selected();
        $('#btn-takedown').html(`Takedown(${rowsSelected.length})`);
    });

    $('.btn-refresh').click((e) => {
        table.ajax.reload();
    });

    $('#field-pelanggaran').html(`
        <label for="keywords">Keywords</label>
        <input type="text" name="s_keywords" id="s_keywords" class="form-control" placeholder="Masukan keywords">
    `);

    $('#periode-filter, #date-filter, #reported-product-status').remove()

    function submitForm(action) {
        $('#overlayLoading').css('display', 'block');
        let rowsSelected = table.column(-1).checkboxes.selected();
        if (rowsSelected.length > 0) {
            let ids = [];
            $('input.id-checkbox').remove();
            $.each(rowsSelected, (index, id) => {
                ids[index] = id;
                $('#form-grab').append(`
                    <input type="hidden" class="id-checkbox" value="${id}" name='id[]' />
                `);
            });

            let url = $('#form-grab').attr('action') + '/' + action;

            $.ajax({
                type: "POST",
                url: url,
                data: $('#form-grab').serialize(),
                dataType: "json",
                success: function (response) {
                    $('input.id-checkbox').remove();
                    table.column(-1).checkboxes.deselectAll();
                    $('#btn-tllapangan').html(`TL Lapangan(0)`)
                    $('#btn-arsip').html(`Arsipkan(0)`)
                    $('#btn-takedown').html(`Takedown(0)`)
                    $('#btn-guidance').html(`Pembinaan(0)`)
                    table.ajax.reload();

                    showInfo('success', response.message);

                },
                error: (xhr, status, error) => {
                    showInfo('error', error)
                }
            });

        } else {
            showInfo('warning', "Anda belum memilih produk. \n\r Silahkan pilih minimal satu produk");
        }
        $('#overlayLoading').css('display', 'none');
    };

    $('#btn-terapkan').click((e) => {
        $('#home-filter-modal').modal('hide');
        reloadDatatableWithFilter();
    });

    function reloadDatatableWithFilter(mp = '') {
        let searchAdvance = $(`#form-advance-search`).serialize();

        const data = $('#home-filter-form').serialize() + "&mp=" + mp;
        let tbl = $('.datatable').DataTable();
        let url = new URL('/grab/data', window.location.origin);
        let params = new URLSearchParams(data);
        tbl.ajax.url(`${url}?${params.toString()}&${searchAdvance}`).load();
    }

    function setButton(data, mp = false) {
        if (Array.isArray(data)) {
            $('.btn-store').empty();
            data.map((val, i) => {
                $('.btn-store').append(`
                    <a id="id-mp-${val.id_marketplace}" onclick="showOnly('${val.id_marketplace}', this)" class="btn btn-primary mx-1 mt-2 text-light mp-button">${val.marketplace.toUpperCase()} (${val.total}) </a>
                `);
            })
        }
        if (mp) {
            $('a.mp-button').removeClass('active');
            $(`a#id-mp-${mp}`).addClass('active');
        }
    }

    function showModal(data) {
        $('#crawled-at').html(data.crawled ? moment(data.crawled).format('DD/MM/YYYY hh:mm') : "-");
        $('#status').html(data.report_status ? data.report_status : "-");
        $('#product-name').html(data.product_name);
        $('#product-price').html(new Intl.NumberFormat('id-ID', {
            style: 'currency',
            currency: 'IDR',

        }).format(data.product_price).replace(',00', ''));
        $("#product-sold-count").html(data.sold_count ? data.sold_count : "-");
        $("#product-sold-count").html(data.sold_count ? data.sold_count : "-");
        $("#product-stock").html(data.item_count ? data.item_count : "-");
        $("#product-rating").html(data.rating ? data.rating : "-");
        $("#product-link").html(data.product_link ? `<a target="_blank" href='${data.product_link}'>Kunjungi</a>` : "-");
        $("#product-desc").html(data.product_desc ? data.product_desc : "-");
        $("#store-name").html(data.id_store && data.store_name ? `<a href="/grab/product-list?store=${data.id_store}">${data.store_name}</a>` : "-");
        $("#store-link").html(data.store_url ? `<a target="_blank" href='${data.store_url}'>Kunjungi</a>` : "-");
        $("#store-location").html(data.location ? data.location : "-");
        $("#store-desc").html(data.store_desc ? data.store_desc : "-");
        $("#marketplace_name").html(data.marketplace_name ? data.marketplace_name : "-");
        $("#product-picture").attr('src', null != data.product_picture ? data.product_picture : 'img/noimage.png');
        $("#product-picture").attr('onerror', `this.onerror=null;this.src='{{url('img/noimage.png')}}`);
        $('#detail-modal').modal('show');
    }

    $('#form-advance-search').submit(e => {
        $('#advance-search-modal').modal('hide');
        reloadDatatableWithFilter();
    });
});
