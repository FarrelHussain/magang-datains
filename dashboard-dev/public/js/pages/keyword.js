/*******global variable in local file********/

let datatable;

/*********************************** */


$(() => {
    datatable = $('.datatable').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "ajax": 'keyword/data',
        "dom": `<"row"<"col-2 mt-2"l><"col-10 mt-1 d-flex flex-row-reverse"<p><"mt-1 mr-3"f>>>t<"bottom"ip>r`,
        "language": {
            "processing": loading()
        },
        "processing": true,
        "columns": [{
                //0
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                searchable: false,
                orderable: false,
                className: 'align-middle'
            },
            {
                //1
                data: 'words',
                name: 'keywords',
                className: 'align-middle',
                render: (value, a, data) => {
                    return `<a href="" data-id="${data.id}" onclick="return false" class="keyword-name">${value}</a>`
                }
            },
            {
                //2
                data: 'commodity',
                name: 'commodities.commodity',
                className: 'align-middle'
            },
            {
                //3
                data: 'mp_logo',
                name: 'marketplace.name',
                className: '',
                render: (value, x, data) => {
                    return `
                        <img src='./img/${value}' class='rounded mx-auto d-block' style="width:45px"  onerror="replcaceImageWithHTML(this, '${data.marketplace_name}')"/>
                    `;
                }
            },
            {
                //4
                data: 'crawling_type',
                name: 'crawling_type.type',
                className: 'align-middle'
            },
            {
                //5
                data: 'product_total',
                name: 'keywords.product_total',
                className: 'text-center align-middle',
                render: (value) => {
                    return value != null ? value : 0;
                }
            },
            {
                //6
                data: 'max_product',
                name: 'keywords.max_product',
                searchable: false,
                className: 'text-center align-middle',
            },
            {
                //7
                data: 'total',
                data: 'total',
                searchable: false,
                className: 'text-center align-middle',
            },
            {
                //8
                data: 'active',
                name: 'active',
                searchable: false,
                render: (value, a, b, c) => {
                    value = (Boolean(value));
                    return `<span class='badge ${value ? 'badge-success' : 'badge-grey'} text-sm'><i class='text-xs'>${value ? 'active' : 'inactive'}</i></span>`;

                },
                className: 'text-center align-middle'
            },
            {
                //9
                data: 'active',
                name: 'status',
                searchable: false,
                render: (value, a, b, c) => {
                    value = Boolean(value);
                    return `<button data-index=${b.DT_RowIndex} data-id=${b.id} onclick='activation(this)' class='activate btn btn-xs ${value ? 'btn-danger' : 'btn-info'}'><i class='text-xs'>${value ? 'deactivate' : 'activate'}</i></button>`

                },
                className: 'text-center align-middle'
            },
        ],
        "pageLength": 11,
        "serverSide": true,
        drawCallback: () => {
            setFilter();
            $('a.keyword-name').on('click', e => {
                let id = $(e.currentTarget).attr('data-id');
                getDetailKeywords(id);
            });
        }
    });

    $('#modal-detail').on('hide.bs.modal', e => {
        $('#table-history > tbody').html(`
            <tr>
                <td>-</td>
                <td class="text-center">-</td>
                <td class="text-center">-%</td>
            </tr>
        `);
        $('#top-stores > tbody').html(`
            <tr>
                <td>-</td>
                <td class="text-center">-</td>
                <td class="text-center">-</td>
                <td class="text-center">-</td>
            </tr>
        `);
    });

});

function setFilter() {
    let dt = $('#DataTables_Table_0_wrapper').children()[0];
    dt = $(dt).children()[1];
    $('#filter-button').remove();
    $('#DataTables_Table_0_filter').prepend(`
        <button data-toggle="modal" data-target="#home-filter-modal" class='btn btn-default btn-sm mr-2' id="filter-button"> <i class='fas fa-filter'></i> Filter Data</button>
    `);
}

$('#btn-terapkan').click((e) => {
    $('#home-filter-modal').modal('hide');
    reloadDatatableWithFilter();
});

function reloadDatatableWithFilter() {

    let commodities = [];
    let data = $('#home-filter-form').serialize();
    $(`input.commodities:checked`).map((i, val) => {
        commodities.push($(val).val())
    });

    // let data = {
    //     commodities: commodities
    // };

    let tbl = $('.datatable').DataTable();
    let url = new URL('/keyword/data', window.location.origin);
    let params = new URLSearchParams(data);
    tbl.ajax.url(`${url}?${params.toString()}`).load();
}

function activation(element) {
    $('#overlayLoading').css('display', 'block');
    element = $(element);
    let row = $(element.closest('tr'));
    let href = $(location).attr('href') + '/activation/';
    let path = element.find('i').html() + '/';
    let index = element.attr('data-index');
    let id = element.attr('data-id') + '/';
    let url = href + path + id + index;

    $.ajax({
        type: "put",
        url: url,
        dataType: "json",
        data: {
            _token: $('meta[name=csrf-token]').attr('content')
        },
        success: function (response, status, xhr) {
            showInfo('success', response.message);
            datatable.row(row).data(response.data).invalidate();

        },
        error: (xhr, status, error) => {
            showInfo('error', `${error} <br> ${xhr.responseJSON.message}`);
        },
    });
    $('#overlayLoading').css('display', 'none');
}

function getDetailKeywords(id) {
    $.ajax({
        type: "POST",
        url: `/keyword/detail/${id}`,
        data: {
            _token: $('meta[name=csrf-token]').attr('content')
        },
        dataType: "json",
        success: function (response) {
            let totalTarget = response.history.target_total ? response.history.target_total : '-';
            if (totalTarget == '-1' || totalTarget == 'semua') {
                totalTarget = 'Semua';
            }
            if ((response.history != null) && (response.history.length != 0)) {
                $('#table-history > tbody > tr').empty();
                $('#table-history > tbody > tr').append(`
                    <td>${response.history.content ? response.history.content : "-"}</td>
                    <td>${response.history.group ? response.history.group : "-"}</td>
                    <td>${response.history.therapy_class ? response.history.therapy_class : "-"}</td>
                    <td>${response.history.product_total ? response.history.product_total : "-"}</td>
                    <td class="text-center">${totalTarget}</td>
                    <td class="text-center">${response.history.result_total ? response.history.result_total : "-"}</td>
                    <td class="text-center">${response.history.progress > 0 ? response.history.progress + '%' : "-"}</td>
                `);
            }

            if (response.top_stores.length > 0) {
                $('#top-stores > tbody').empty();
                response.top_stores.map((val, i) => {
                    if ((val.name != null) && (val.count != null) && val.count > 0) {
                        $('#top-stores > tbody').append(`
                            <tr>
                                <td>${val.name}</td>
                                <td class="text-center">${val.count ? val.count : '0'}</td>
                                <td class="text-center">${val.seller_rate ? val.seller_rate : '-'}</td>
                                <td class="text-center">${val.last_update ? val.last_update : '-'}</td>
                            </tr>
                        `);
                    } else {
                        $('#top-stores > tbody').html(`
                            <tr>
                                <td colspan="4" class="text-center">Tidak ada data</td>
                            </tr>
                        `);
                    }
                });
            }

            $('#modal-detail').modal('show');
        },
        error: (xhr, status, error) => {
            showInfo('error', error);
        },
        beforeSend: () => {
            $('#overlayLoading').css('display', 'block');
        },
        complete: () => {
            $('#overlayLoading').css('display', 'none');
        }
    });
}
