$(() => {
    $('.datatable').DataTable({
        searching: false,
        processing: true,
        language: {
            processing: loading()
        },
        serverSide: true,
        "ajax": {
            "url": "home/getdata?mp=tokopedia",
            "data": {
                "user_id": 451
            }
        },
        columns: [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                searchable: false,
                orderable: false
            },
            {
                name: 'commodity',
                data: 'commodity',
            },
            {
                name: 'total',
                data: 'total'
            },
            {
                name: 'crawled_date',
                data: 'crawled_date',
            },
            {
                name: 'color',
                data: 'color'
            }

        ],
        columnDefs: [{
                targets: 0,
                searchable: false,
                orderable: false
            },
            {
                targets: 3,
                render: (value, a, b, c) => moment(value).format('DD-MM-YYYY')
            },
            {
                targets: 4,
                render: (value, a, b, c) => `<p class="text-white" style="background-color:${value}; font-size:10px; width: '1%'">&nbsp;</p>`,
                // width: "1%"
            },
        ],
        drawCallback: (settings, json) => {
            setChart(settings.json.data);
            // setFilter();
        }
    });
    $('#reported-product-status').remove();
});

$('#btn-terapkan').click((e) => {

    $('#home-filter-modal').modal('hide');

    let mp = $('.table-title').html().toLowerCase().trim();
    let dateFrom = $('input[name=start]').val();
    let dateEnd = $('input[name=end]').val();
    let commodities = [];
    $(`input.commodities:checked`).map((i, val) => {
        commodities.push($(val).val())
    });

    reloadDatatable(mp, dateFrom, dateEnd, commodities);

});


$('button.mr-2').on('click', (e) => {

    let mp = $(e.currentTarget).html().toLowerCase().trim();
    let dateFrom = $('input[name=start]').val();
    let dateEnd = $('input[name=end]').val();
    let commodities = [];
    $(`input.commodities:checked`).map((i, val) => {
        commodities.push($(val).val())
    });

    $('.btn, .mr-2').removeClass('active');
    $(e.currentTarget).addClass('active');
    $('.table-title').html(mp.charAt(0).toUpperCase() + mp.slice(1))

    reloadDatatable(mp, dateFrom, dateEnd, commodities);

});

function reloadDatatable(mp, dateFrom, dateEnd, commodities) {

    let data = {
        mp: mp,
        dateFrom: dateFrom,
        dateEnd: dateEnd,
        commodities: commodities
    };

    let tbl = $('.datatable').DataTable();
    let url = new URL('/home/getdata', window.location.origin);
    let params = new URLSearchParams(data);

    tbl.ajax.url(`${url}?${params.toString()}`).load();
}

function setFilter() {
    let dt = $('#DataTables_Table_0_wrapper').children()[0];
    dt = $(dt).children()[1];
    $(dt).empty();
    $(dt).addClass('text-right').append(`
        <button data-toggle="modal" data-target="#home-filter-modal" class='btn btn-default btn-sm'> <i class='fas fa-filter'></i> Filter Data</button>
    `);
}


function setChart(original) {
    let data = [];
    let categories = [];
    original.map((val, i) => {
        data[i] = {
            name: val.commodity,
            y: parseFloat(val.total),
            color: val.color
        };
        categories[i] = val.commodity
    });
    let title = $('button.mr-2.active').html();
    title = title.charAt(0).toUpperCase() + title.slice(1);
    //console.log(data);

    Highcharts.chart('chart', {
        chart: {
            type: 'bar',
        },
        title: {
            text: `${title}`
        },
        subtitle: {
            text: "Total Keseluruhan Produk Berdasarkan Komoditas"
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true
                }
            },

        },
        tooltip: {
            pointFormat: 'Total: <span style="color:{series.color}">{point.y}</span><br/>'
        },
        yAxis: {
            title: {
                text: "Total"
            }
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: categories
        },
        series: [{

            data: data
        }]
    });
}
