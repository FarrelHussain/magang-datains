let commodities = '';
let base_url = '';
let _token = '';
$(document).ready(function () {
    base_url = $('meta[name=base-url]').attr('content');
    _token = $('meta[name=csrf-token]').attr('content');

    $.post(`${base_url}/get-config-commodity`, {
        _token: _token
    }, (res, s) => {
        commodities = res;
    });

    $.validator.setDefaults({
        submitHandler: function (form) {
            form.submit();
        }
    });
    $('#formCrawl').validate({
        rules: {
            keywords: {
                required: true,
            },
            'marketplace[]': {
                required: true,
            },
        },
        messages: {
            keywords: {
                required: "Silahkan isikan keywords yang anda inginkan",
            },
            commodity: {
                required: "Silahkan pilih komoditas",
            },
            'marketplace[]': "Silahkan pilih salah satu marketplace"
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    $('#select-commodity').on('change', e => {
        let commodity = $(e.currentTarget).val();
        if ((commodity == commodities.nappza.id_commodity) || (commodity == commodities.medicine.id_commodity)) {
            $('#content').css('display', '');
            $('#therapy_class').css('display', '');
            $('#group').css('display', '');
        } else {
            $('#content').css('display', 'none');
            $('#therapy_class').css('display', 'none');
            $('#group').css('display', 'none');
        }
    });
});
