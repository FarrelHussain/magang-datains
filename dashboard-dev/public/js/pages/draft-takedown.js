$(() => {
    let datatable;
    datatable = $('.datatable').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "order": [
            [0, 'desc']
        ],
        "info": true,
        "autoWidth": true,
        "serverSide": true,
        "ajax": {
            type: 'GET',
            url: DATASOURCE_URL,
            data: {
                data: 'drafttakedown'
            }
        },
        "language": {
            "processing": loading()
        },
        "processing": true,
        "columns": [
            {
                searchable: false,
                data: 'periode',
                className: 'align-middle text-center',
            },
            {
                searchable: false,
                data: 'report_type',
                className: 'align-middle text-center',
            },
            {
                searchable: false,
                data: 'proposed_by',
                className: 'align-middle text-center',
            },
            {
                //1
                data: 'id_report',
                name: 'reports.id_report',
                searchable: false,
                render: (value, a, data, c) => {
                    //console.log(data)url('reported-product/takedown')}
                    if (value != null) {
                        return `<a href='/reported-product/draft-takedown?id=${value}'><button data-index=${data.DT_RowIndex} data-id=${data.id} class='activate btn btn-xs btn-info'><i class='text-xs'>Detail</i></button></a>`
                    } else {
                        return `-`;
                    }
                },
                className: 'text-center align-middle'
            }
        ],
        
        "pageLength": 12,
        "dom": `<"row"<"col-2 mt-2"l><"col-10 mt-1 d-flex flex-row-reverse"<p><"mt-1 mr-3"f>>>t<"bottom"ip>r`,
        drawCallback: () => {
            $('[data-toggle="tooltip"]').tooltip();
            // setFilter();
        }
    });

    datatable.on('change', 'input[type=checkbox]', (e) => {
        let rowsSelected = datatable.column(-1).checkboxes.selected();
        let link_id = [];
        $('#pilih').html(`Pilih (${rowsSelected.length})`)

    });

    $('#date-filter').remove();
    $('#periode-filter').removeClass('d-none').addClass('d-block');
    $('#custom-export-periode').removeClass('d-none');

});

function showDetail(id) {
    $.ajax({
        type: "get",
        url: `show/${id}`,
        data: {
            'XXXXXXXX-XXXX-XXX': 'XXX-xxxX-xX-XX-X',
            'data': 'takedown'
        },
        dataType: "json",
        success: function (response, status, xhr) {
            showModal(response)
        },
        error: (xhr, status, error) => {
            showInfo('error', status + ' ' + error)
        },
    });
}

function showModal(data) {
    $('#crawled-at').html(data.crawled ? moment(data.crawled).format('DD/MM/YYYY hh:mm') : "-");
    $('#status').html(data.report_status ? data.report_status : "-");
    $('#product-name').html(data.product_name);
    $('#product-price').html(new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',

    }).format(data.product_price).replace(',00', ''));
    $("#product-sold-count").html(data.sold_count ? data.sold_count : "-");
    $("#product-sold-count").html(data.sold_count ? data.sold_count : "-");
    $("#product-stock").html(data.item_count ? data.item_count : "-");
    $("#product-rating").html(data.rating ? data.rating : "-");
    $("#product-link").html(data.product_link ? `<a target="_blank" href='${data.product_link}'>Kunjungi</a>` : "-");
    $("#product-desc").html(data.product_desc ? data.product_desc : "-");
    $("#store-name").html(data.id_store && data.store_name ? `<a href="/grab/product-list?store=${data.id_store}">${data.store_name}</a>` : "-");
    $("#store-link").html(data.store_url ? `<a target="_blank" href='${data.store_url}'>Kunjungi</a>` : "-");
    $("#store-location").html(data.location ? data.location : "-");
    $("#store-desc").html(data.store_desc ? data.store_desc : "-");
    $("#marketplace_name").html(data.marketplace_name ? data.marketplace_name : "-");
    $("#product-picture").attr('src', null != data.product_picture ? data.product_picture : '../img/noimage.png');
    $('#detail-modal').modal('show');
}

function setFilter() {
    let dt = $('#DataTables_Table_0_wrapper').children()[0];
    dt = $(dt).children()[1];
    $('#filter-button').remove();
    $('#advance-search-button').remove();

    $('#DataTables_Table_0_filter').prepend(`
    <button data-toggle="modal" data-target="#advance-search-modal" class='btn btn-default btn-sm mr-2' id="advance-search-button"> <i class='fas fa-search'></i> Advance Search</button>
    `);

    $('#DataTables_Table_0_filter').prepend(`
        <button data-toggle="modal" data-target="#home-filter-modal" class='btn btn-default btn-sm mr-2' id="filter-button"> <i class='fas fa-filter'></i> Filter Data</button>
    `);
}

$('#btn-terapkan').click((e) => {
    $('#home-filter-modal').modal('hide');
    reloadDatatableWithFilter();
});

function reloadDatatableWithFilter() {

    let commodities = [];
    let searchAdvance = $(`#form-advance-search`).serialize();
    let linkStatus = $('input[name="reported_product_status"]:checked').val();
    let filter_periode = $('input[name=filter_periode]').val();

    $(`input.commodities:checked`).map((i, val) => {
        commodities.push(parseInt($(val).val()));
    });

    let data = {
        commodities: commodities,
        link_status: linkStatus,
        filter_periode: filter_periode
    };

    let tbl = $('.datatable').DataTable();
    let url = new URL('/reported-product/draft', window.location.origin);
    let params = new URLSearchParams(data);

    tbl.ajax.url(`${url}?${params.toString()}&${searchAdvance}`).load();
}

// $('#pilih').on('click', (e) => {
//     if ($(`input[name="mp[]"]`).length) {
//         $('#form-export').submit();
//     } else {
//         showInfo('warning', 'Silahkan pilih minimal satu data')

//     }
// });

// $('#custom-export').click((e) => {
//     $('input[name="mp[]"]').remove();
//     $('#custom-export-modal').modal('show');
// });

$('#form-export').on('submit', e => {
    $('#custom-export-modal').modal('hide');
})

$('#custom-export-modal').on('hidden.bs.modal', e => {
    $(`input[name=product_name]`).val('');
    $(`input[name=content]`).val('');
    $(`input[name=therapy_class]`).val('');
    $(`input[name=group]`).val('');
    $(`input[name=store]`).val('');
    $(`select.form-control`).val(null);
});

// $('#form-advance-search').submit(e => {
//     $('#advance-search-modal').modal('hide');
//     reloadDatatableWithFilter();
// });
