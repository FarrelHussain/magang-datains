/*******global variable in local file********/

let datatable;

/*********************************** */


$(() => {
    datatable = $('.datatable').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "ajax": 'commodity/data',
        "dom": `<"row"<"col-2 mt-2"l><"col-10 mt-1 d-flex flex-row-reverse"<p><"mt-1 mr-3"f>>>t<"bottom"ip>r`,
        "language": {
            "processing": loading()
        },
        "processing": true,
        "columns": [{
                //0
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                searchable: false,
                orderable: false,
                className: 'align-middle'
            },
            {
                //1
                data: 'commodity',
                name: 'commodity',
                className: 'align-middle'
            },
            {
                //2
                data: 'id_commodity',
                name: 'id_commodity',
                className: 'text-center',
                render: (value, x, data) => {
                    // return `
                    //     <div class='row text-center d-flex justify-content-center'>
                    //     <a id="a-commodity-${value}" data-commodity="${data.commodity}" data-id="${value}" href="" onclick="return false" class="btn-edit"><button class='btn btn-xs btn-warning mr-2'><i class='fas fa-cog text-light'></i></button></a>
                    //     <form action='commodity/${value}' method="POST" onsubmit="return confirm('Apakah anda yakin menghapus data ini?');">
                    //         <input type='hidden' name='_token' value='${$('meta[name="csrf-token"]').attr('content')}' />
                    //         <input type='hidden' name='_method' value='DELETE' />
                    //         <button class='btn btn-xs btn-danger'><i class='fas fa-trash text-light'></i></button>
                    //     </form>
                    //     </div>

                    // `;
                    return `<a id="a-commodity-${value}" data-commodity="${data.commodity}" data-id="${value}" href="" onclick="return false" class="btn-edit"><button class='btn btn-xs btn-warning mr-2'><i class='fas fa-cog text-light'></i></button></a>`;
                }
            },
        ],
        "pageLength": 10,
        "serverSide": true,
        drawCallback: () => {
            setAddButton();
            $('.btn-edit').click((e) => editCommodity($(e.currentTarget).attr('data-id')))
        }
    });

    $('#date-filter').remove();
    $('#reported-product-status').remove();

});

function editCommodity(id) {
    $('#edit-equipment').remove();
    $('#form-commodity').append(`
        <div id="edit-equipment">
            <input type="hidden" name="_method" value="PUT" />
        </div>
    `)
    $('#modal-add-commodity-title').html("Edit Komoditas");
    $('#form-commodity').attr('action', $('#form-commodity').attr('data-store') + `/${id}`);
    $('input[name=commodity]').val($(`#a-commodity-${id}`).attr('data-commodity'));
    $('#modal-add-commodity').modal('show');
}

function setAddButton() {
    let dt = $('#DataTables_Table_0_wrapper').children()[0];
    dt = $(dt).children()[1];
    $('#add-button').remove();
    $('#DataTables_Table_0_filter').prepend(`
        <a data-toggle="modal" data-target="#modal-add-commodity" class='btn btn-success btn-sm mr-2 text-light' id="add-button"> <i class='fas fa-plus'></i> Tambah</button>
    `);
}

$('#btn-terapkan').click((e) => {
    $('#home-filter-modal').modal('hide');
    reloadDatatableWithFilter();
});

function reloadDatatableWithFilter() {

    let commodities = [];

    $(`input.commodities:checked`).map((i, val) => {
        commodities.push($(val).val())
    });

    let data = {
        commodities: commodities
    };

    let tbl = $('.datatable').DataTable();
    let url = new URL('/keyword/data', window.location.origin);
    let params = new URLSearchParams(data);

    tbl.ajax.url(`${url}?${params.toString()}`).load();
}
