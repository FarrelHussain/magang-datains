let table = null;
let store = null;
$(() => {
    store = window.location.search;
    store = new URLSearchParams(store);

    if (store = store.get('store')) {

        table = $('.table').DataTable({
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "ajax": `/grab/product-list/data/${store}`,
            "processing": true,
            "language": {
                "processing": loading()
            },
            "dom": `<"row"<"col-2 mt-2"l><"col-10 mt-1 d-flex flex-row-reverse"<p><"mt-1 mr-3"f>>>t<"bottom"ip>r`,
            columns: [{
                    data: 'name',
                    name: 'products.name',
                    className: 'align-middle',
                    render: (value, a, data) => {
                        if (data.link != null) {
                            return `<a target="_blank" data-toggle="tooltip" title="${value}" href="${data.link}">${value.substring(0, 100)}</a>`
                        } else {
                            return value
                        }
                    }
                },
                {
                    data: 'price',
                    name: 'data_product.price',
                    className: 'align-middle',
                },
                {
                    data: 'item_count',
                    name: 'data_product.item_count',
                    className: 'align-middle',
                    render: (value, a, data) => {
                        if (value) {
                            return value;
                        } else {
                            return 0;
                        }
                    }
                },
                {
                    data: 'commodity',
                    name: 'commodities.commodity',
                    className: 'align-middle',
                },
                {
                    data: 'keywords',
                    name: 'keywords.keywords',
                    className: 'align-middle',
                },
                {
                    data: 'status',
                    name: 'status.status',
                    className: 'align-middle',
                },
            ],
            drawCallback: () => {
                $('[data-toggle="tooltip"]').tooltip();
            }
        });
    }
})
