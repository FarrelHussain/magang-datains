$(() => {
    let datatable;
    datatable = $('.datatable').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "dom": `<"row"<"col-2 mt-2"l><"col-10 mt-1 d-flex flex-row-reverse"<p><"mt-1 mr-3"f>>>t<"bottom"ip>r`,
        "ajax": {
            type: 'GET',
            url: 'data',
            data: {
                data: 'arsip'
            }
        },
        "language": {
            "processing": loading()
        },
        "processing": true,
        "columns": [{
                //0
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                orderable: false,
                searchable: false,
                className: 'align-middle'
            },
            {
                //1
                data: 'commodity',
                name: 'commodities.commodity',
                searchable: false,
                className: 'align-middle'
            },
            {
                //1
                data: 'product_name',
                name: 'products.name',
                render: (value, a, data, c) => {
                    return `<a href='###' title="${value}" data-toggle="tooltip" onclick='showDetail(${data.id_product})' class='product-list' title='Click untuk detail'>${value.substring(0, 100)}</a>`
                },
                className: 'align-middle'
            },

            {
                //2
                data: 'product_price',
                name: 'data_product.price',
                searchable: false,
                className: 'text-right align-middle',
                render: (value) => {
                    return new Intl.NumberFormat('id-ID', {
                        style: 'currency',
                        currency: 'IDR',

                    }).format(value).replace(',00', '')
                },
                className: 'align-middle'
            },
            {
                //3
                data: 'store_name',
                name: 'stores.name',
                searchable: false,
                render: (value, a, data, c) => {
                    if (null != data.store_url) {
                        return `<a href='${data.store_url}' target='__blank' title='Click untuk kunjungi'>${data.store_name}</a>`
                    } else if (value != null) {
                        return value;
                    } else {
                        return "-";
                    }
                },
                className: 'align-middle text-center'
            },
            {
                data: 'mp_logo',
                name: 'marketplace.name',
                searchable: false,
                width: '50px',
                className: 'align-middle',
                render: (value, x, data) => {
                    return `
                        <img src='${value}' class='rounded mx-auto d-block' style="width:45px" onerror="replcaceImageWithHTML(this, '${data.marketplace_name}')"/>
                    `;
                }
            },
            {
                //5
                data: 'report_status',
                name: 'status.status',
                searchable: false,
                className: 'text-center align-middle'
            },
            {
                //6
                data: 'acronym',
                name: 'offenses.acronym',
                searchable: false,
                className: 'text-center align-middle',
                render: (value, x, data) => {
                    if (null != value) {
                        return `<text class="align-self-middle" title="${data.offense}">${value}</text>`
                    } else {
                        return `<text class="align-self-middle" title="Tidak ada pelanggaran">-</text>`
                    }
                }
            },
            {
                //7
                data: 'link_id',
                name: 'status.link_id',
                searchable: false,
                className: 'align-middle',
                orderable: false,

            },
            {
                //7
                data: 'link_date_update',
                name: 'links.updated_at',
                searchable: false,
                className: 'd-none',
                searchable: false,

            },
        ],
        "columnDefs": [{
                targets: 8,
                'checkboxes': {
                    'selectRow': true
                },
            }

        ],
        'select': {
            'style': 'multi'
        },
        "pageLength": 10,
        "serverSide": true,
        drawCallback: () => {
            $('[data-toggle="tooltip"]').tooltip();
            setFilter();
        },
        order: [
            [9, 'desc']
        ],
    });

    datatable.on('change', 'thead input[type=checkbox]', (e) => {
        let rowsSelected = datatable.column(8).checkboxes.selected();
        $('#btn-arsip').html(`Arsipkan(${rowsSelected.length})`);
        let link_id = [];
        $('#pilih').html(`Pilih (${rowsSelected.length})`)
        if (rowsSelected.length == 0) {
            $('input[name="mp[]"]').remove();
            return false;
        }
        $.each(rowsSelected, function (idx, val) {
            link_id[idx] = val;
            $('#form-export').append(`<input name="mp[]" type="hidden" value="${val}" />`);
        });

    });

    datatable.on('change', 'tbody input[type=checkbox]', (e) => {
        let rowsSelected = datatable.column(8).checkboxes.selected();
        $('#btn-arsip').html(`Arsipkan (${rowsSelected.length})`);

        let link_id = [];
        $('#pilih').html(`Pilih (${rowsSelected.length})`);
        if (rowsSelected.length == 0) {
            $('input[name="mp[]"]').remove();
            return false;
        }
        $.each(rowsSelected, function (idx, val) {
            link_id[idx] = val;
            $('#form-export').append(`<input name="mp[]" type="hidden" value="${val}" />`);
        });
    });

    $('#date-filter').remove();
    $('#reported-product-status').remove();

});

function showDetail(id) {
    $.ajax({
        type: "get",
        url: `show/${id}`,
        data: {
            'XXXXXXXX-XXXX-XXX': 'XXX-xxxX-xX-XX-X',
            'data': 'arsip'
        },
        dataType: "json",
        success: function (response, status, xhr) {
            showModal(response)
        },
        error: (xhr, status, error) => {
            showInfo('error', status + ' ' + error)
        },
    });
}

function showModal(data) {
    $('#crawled-at').html(data.link_date_create ? moment(data.link_date_create).format('DD/MM/YYYY hh:mm') : "-");
    $('#status').html(data.report_status ? data.report_status : "-");
    $('#product-name').html(data.product_name);
    $('#product-price').html(new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',

    }).format(data.product_price).replace(',00', ''));
    $("#product-sold-count").html(data.sold_count ? data.sold_count : "-");
    $("#product-sold-count").html(data.sold_count ? data.sold_count : "-");
    $("#product-stock").html(data.item_count ? data.item_count : "-");
    $("#product-rating").html(data.rating ? data.rating : "-");
    $("#product-link").html(data.product_link ? `<a target="_blank" href='${data.product_link}'>Kunjungi</a>` : "-");
    $("#product-desc").html(data.product_desc ? data.product_desc : "-");
    $("#store-name").html(data.id_store && data.store_name ? `<a href="/grab/product-list?store=${data.id_store}">${data.store_name}</a>` : "-");
    $("#store-link").html(data.store_url ? `<a target="_blank" href='${data.store_url}'>Kunjungi</a>` : "-");
    $("#store-location").html(data.location ? data.location : "-");
    $("#store-desc").html(data.store_desc ? data.store_desc : "-");
    $("#marketplace_name").html(data.marketplace_name ? data.marketplace_name : "-");
    $("#product-picture").attr('src', null != data.product_picture ? data.product_picture : '../img/noimage.png');
    $('#detail-modal').modal('show');

}

function setFilter() {
    let dt = $('#DataTables_Table_0_wrapper').children()[0];
    dt = $(dt).children()[1];
    $('#filter-button').remove();
    $('#advance-search-button').remove();

    $('#DataTables_Table_0_filter').prepend(`
    <button data-toggle="modal" data-target="#advance-search-modal" class='btn btn-default btn-sm mr-2' id="advance-search-button"> <i class='fas fa-search'></i> Advance Search</button>
    `);
    $('#DataTables_Table_0_filter').prepend(`
        <button data-toggle="modal" data-target="#home-filter-modal" class='btn btn-default btn-sm mr-2' id="filter-button"> <i class='fas fa-filter'></i> Filter Data</button>
    `);
}

$('#btn-terapkan').click((e) => {
    $('#home-filter-modal').modal('hide');
    reloadDatatableWithFilter();
});

function reloadDatatableWithFilter() {

    let commodities = [];
    let searchAdvance = $(`#form-advance-search`).serialize();

    $(`input.commodities:checked`).map((i, val) => {
        commodities.push(parseInt($(val).val()));
    });

    // console.log(commodities);
    // return;

    let data = {
        commodities: commodities
    };

    let tbl = $('.datatable').DataTable();
    let url = new URL('/reported-product/data', window.location.origin);
    let params = new URLSearchParams(data);

    tbl.ajax.url(`${url}?${params.toString()}&${searchAdvance}`).load();
}

$('#pilih').on('click', (e) => {
    if ($(`input[name="mp[]"]`).length) {
        $('#form-export').submit();
    } else {
        showInfo('warning', 'Silahkan pilih minimal satu data')

    }
});

$('#custom-export').click((e) => {
    $('input[name="mp[]"]').remove();
    $('#custom-export-modal').modal('show');
});

$('#form-export').on('submit', e => {
    $('#custom-export-modal').modal('hide');
})

$('#custom-export-modal').on('hidden.bs.modal', e => {
    $(`input[name=product_name]`).val('');
    $(`input[name=content]`).val('');
    $(`input[name=therapy_class]`).val('');
    $(`input[name=group]`).val('');
    $(`input[name=store]`).val('');
    $(`select.form-control`).val(null);
});

$('#form-advance-search').submit(e => {
    $('#advance-search-modal').modal('hide');
    reloadDatatableWithFilter();
});
