/*******global variable in local file********/

let datatable;

/*********************************** */

$(() => {
    datatable = $('.datatable').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "dom": `<"row"<"col-2 mt-2"l><"col-10 mt-1 d-flex flex-row-reverse"<p><"mt-1 mr-3"f>>>t<"bottom"ip>r`,
        "pageLength": 10,
        "order": [["1", 'asc']],
        "columnDefs": [
            {
                "targets": [0,1],
                "orderable": false,
                "searchable": false
            },
            {
                "targets": 1,
                render: (value, x, data) => {
                    return `
                        <img src='/img/${value}.png' class='rounded mx-auto d-block' style="width:45px"  onerror="replcaceImageWithHTML(this, '${data.marketplace_name}')"/>
                    `;
                }
            }
        ]
    });

    datatable.on( 'order.dt search.dt', function () {
        datatable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

});
