$(() => {
    let datatable;
    datatable = $('.datatable').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "ajax": 'reported-product/data',
        "language": {
            "processing": loading()
        },
        "processing": true,
        "columns": [{
                //0
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                orderable: false,
                searchable: false,
                className: 'align-middle'
            },
            {
                //1
                data: 'commodity',
                name: 'commodities.commodity',
                className: 'align-middle'
            },
            {
                //1
                data: 'product_name',
                name: 'products.name',
                render: (value, a, data, c) => {
                    //console.log(data)
                    return `<a href='###' onclick='showDetail(${data.id_reported_product})' class='product-list' title='Click untuk kunjungi'>${value}</a>`
                },
                className: 'align-middle'
            },

            {
                //2
                data: 'product_price',
                name: 'data_product.price',
                className: 'text-right align-middle',
                render: (value) => {
                    return new Intl.NumberFormat('id-ID', {
                        style: 'currency',
                        currency: 'IDR',

                    }).format(value).replace(',00', '')
                },
                className: 'align-middle'
            },
            {
                //3
                data: 'store_name',
                data: 'stores.name',
                render: (value, a, data, c) => {
                    return `<a href='${data.store_url}' target='__blank' title='Click untuk kunjungi'>${data.store_name}</a>`
                },
                className: 'align-middle'
            },
            {
                data: 'mp_logo',
                name: 'marketplace.name',
                className: 'align-middle',
                render: (value, x, data) => {
                    return `
                        <img src='./img/${value}' class='rounded mx-auto d-block' style="width:45px" onerror="replcaceImageWithHTML(this, '${data.marketplace_name}')"/>
                    `;
                }
            },
            {
                //5
                data: 'report_status',
                name: 'status.status',
                className: 'text-center align-middle'
            },
            {
                //6
                data: 'link_id',
                name: 'status.link_id',
                className: 'align-middle',
                orderable: false,

            },
        ],
        "columnDefs": [{
                targets: 7,
                'checkboxes': {
                    'selectRow': true
                },
            }

        ],
        'select': {
            'style': 'multi'
        },
        "pageLength": 10,
        "serverSide": true,
    });

    datatable.on('change', 'thead input[type=checkbox]', (e) => {
        let rowsSelected = datatable.column(7).checkboxes.selected();
        let link_id = [];
        $('#pilih').html(`Pilih (${rowsSelected.length})`)
        if (rowsSelected.length == 0) {
            $('#pilih').attr('href', `###`);
            $('#pilih').attr('target', '');
            return false;
        }
        $.each(rowsSelected, function (idx, val) {
            link_id[idx] = val;
        });
        $('#pilih').attr('href', `export/reported-product?mp=${link_id}&fv=lkaw993j88939&mv=99388iejjkw002889`);
        $('#pilih').attr('target', `__blank`);
    })
    datatable.on('change', 'tbody input[type=checkbox]', (e) => {
        let rowsSelected = datatable.column(7).checkboxes.selected();
        let link_id = [];
        $('#pilih').html(`Pilih (${rowsSelected.length})`);
        if (rowsSelected.length == 0) {
            $('#pilih').attr('href', `###`);
            $('#pilih').attr('target', '');
            return false;
        }
        $.each(rowsSelected, function (idx, val) {
            link_id[idx] = val;
        });
        $('#pilih').attr('href', `export/reported-product?mp=${link_id}&fv=lkaw993j88939&mv=99388iejjkw002889`);
        $('#pilih').attr('target', `__blank`);
    });

    $('#date-filter').remove();

});

$('#btn-terapkan').click((e) => {
    $('#home-filter-modal').modal('hide');
    reloadDatatableWithFilter();
});

function reloadDatatableWithFilter() {

    let commodities = [];

    $(`input.commodities:checked`).map((i, val) => {
        commodities.push($(val).val())
    });

    let data = {
        commodities: commodities
    };

    let tbl = $('.datatable').DataTable();
    let url = new URL('/reported-product/data', window.location.origin);
    let params = new URLSearchParams(data);

    tbl.ajax.url(`${url}?${params.toString()}`).load();
}


function showDetail(id) {
    $.ajax({
        type: "get",
        url: `reported-product/show/${id}`,
        data: {
            'XXXXXXXX-XXXX-XXX': 'XXX-xxxX-xX-XX-X'
        },
        dataType: "json",
        success: function (response, status, xhr) {
            showModal(response)
        },
        error: (xhr, status, error) => {
            showInfo('error', status + ' ' + error)
        },
    });
}

function showModal(data) {
    $('#crawled-at').html(moment(data.crawled).format('DD/MM/YYYY hh:mm'));
    $('#status').html(data.report_status);
    $('#product-name').html(data.product_name);
    $('#product-price').html(new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',

    }).format(data.product_price).replace(',00', ''));
    $("#product-sold-count").html(data.sold_count);
    $("#product-sold-count").html(data.sold_count);
    $("#product-stock").html(data.item_count);
    $("#product-rating").html(data.rating);
    $("#product-link").html(`<a target="_blank" href='${data.product_link}'>Kunjungi</a>`);
    $("#product-desc").html(data.product_desc);
    $("#store-name").html(data.store_name);
    $("#store-link").html(`<a target="_blank" href='${data.store_url}'>Kunjungi</a>`);
    $("#store-location").html(data.location);
    $("#store-desc").html(data.store_desc);
    $("#marketplace").html(data.marketplace_name);
    $("#product-picture").attr('src', null != data.product_picture ? data.product_picture : 'img/noimage.png');
    $("#product-picture").attr('onerror', `this.onerror=null;this.src='{{url('img/noimage.png')}}`);
    $('#detail-modal').modal('show');
}
