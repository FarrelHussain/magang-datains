{
    let datatable;
    datatable = $('.datatable').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "dom": `<"row"<"col-2 mt-2"l><"col-10 mt-1 d-flex flex-row-reverse"<p><"mt-1 mr-3"f>>>t<"bottom"ip>r`,
        "ajax": {
            type: 'GET',
            url: 'data',
            data: {
                data: 'pengajuan-takedown'
            }
        },
        "language": {
            "processing": loading()
        },
        "processing": true,
        "columns": [{
                //0
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                orderable: false,
                searchable: false,
                className: 'align-middle'
            },
            {
                //1
                data: 'tgl_submit',
                name: 'reports.tgl_submit',
                searchable: false,
                className: 'align-middle',
                render: (value, a, data) => {
                    if (value != null) {
                        return value;
                    } else {
                        return `-`;
                    }
                }
            },
            {
                //2
                data: 'jenis_laporan',
                name: 'reports.jenis_laporan',
                render: (value, a, data, c) => {
                    if (value != null) {
                        return value;
                    } else {
                        return `-`;
                    }
                },
                className: 'align-middle'
            },

            {
                //3
                data: 'status',
                name: 'reports.status',
                render: (value, a, data, c) => {
                    if (value != null) {
                        return value;
                    } else {
                        return `-`;
                    }
                },
                className: 'align-middle'
            },

            {
                //4
                data: 'tgl_submit',
                name: 'reports.tgl_submit',
                searchable: false,
                className: 'text-center align-middle'
            },
            {
                //5
                data: 'tgl_submit',
                name: 'reports.tgl-submit',
                searchable: false,
                className: 'text-center align-middle'
            },
    
            {
                //6
                data: 'kode_report',
                name: 'reports.kode-report',
                render: (value, a, data, c) => {
                    if (value != null) {
                        return value;
                    } else {
                        return `-`;
                    }
                },
                className: 'align-middle'
            },
        ],
        "columnDefs": [{
                targets: 7,
                'checkboxes': {
                    'selectRow': true
                },
            }

        ],
        'select': {
            'style': 'multi'
        },
        "pageLength": 10,
        "serverSide": true,
        drawCallback: () => {
            $('[data-toggle="tooltip"]').tooltip();
            setFilter();
        },
        order: [
            [9, 'desc']
        ]
    });

    datatable.on('change', 'thead input[type=checkbox]', (e) => {

        let rowsSelected = datatable.column(8).checkboxes.selected();
        $('#btn-feedback').html(`Feedback(${rowsSelected.length})`);
        $('#btn-arsip').html(`Arsipkan(${rowsSelected.length})`);
        $('#btn-takedown').html(`Takedown (${rowsSelected.length})`);

        let link_id = [];
        $('#pilih').html(`Pilih (${rowsSelected.length})`)
        if (rowsSelected.length == 0) {
            $('input[name="mp[]"]').remove();
            return false;
        }

        $.each(rowsSelected, function (idx, val) {
            link_id[idx] = val;
            $('#form-export').append(`<input name="mp[]" type="hidden" value="${val}" />`);
        });

    });

    datatable.on('change', 'tbody input[type=checkbox]', (e) => {
        let rowsSelected = datatable.column(8).checkboxes.selected();
        $('#btn-feedback').html(`Feedback (${rowsSelected.length})`);
        $('#btn-arsip').html(`Arsipkan (${rowsSelected.length})`);
        $('#btn-takedown').html(`Takedown (${rowsSelected.length})`);

        let link_id = [];
        $('#pilih').html(`Pilih (${rowsSelected.length})`);
        if (rowsSelected.length == 0) {
            $('input[name="mp[]"]').remove();
            return false;
        }
        $.each(rowsSelected, function (idx, val) {
            link_id[idx] = val;
            $('#form-export').append(`<input name="mp[]" type="hidden" value="${val}" />`);
        });
    });

    $('#date-filter').remove();

    $('.takedown-feedback').click((e) => {

        let action = $(e.currentTarget).attr('data-btn');
        let rowsSelected = table.column(6).checkboxes.selected();
        if (rowsSelected.length <= 0) {
            showInfo('warning', "Anda belum memilih produk. \n\r Silahkan pilih minimal satu produk");
            return false;
        }

        let key = $('meta[name=csrf-token]').attr('content');
        $.get(`grab/offense?${key}=${key}`, {}, async (res, status) => {
            let html = `<div class='form-group'><select id="id_offense" class='form-control' required><option disabled selected>Pilih Pelanggaran</option>`;
            if (res.success) {
                res.message.map((val, i) => {
                    html += `<option value='${val.id_offense}'>${val.offense}</option>`
                });
            } else {
                showInfo('error', res.message);
                return false;
            }
            html += `</select></div>`;
            html += `<div class="form-group"><label class="float-left">Keterangan (optional)</label><textarea id="note"  class="form-control"></textarea></div>`
            await Swal.fire({
                title: "Silahkan Pilih Feedback",
                html: html,
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                preConfirm: (value) => {
                    if (value) {
                        return new Promise((resolve, project) => {
                            if ($('#id_offense').val() == null) {
                                swal.showValidationMessage(
                                    "Silahkan pilih Feedback!"
                                )
                            } else {
                                Swal.resetValidationMessage();
                            }
                            resolve({
                                id_offense: $('#id_offense').val(),
                                note: $('#note').val()
                            });
                        })
                    }
                }
            }).then(({
                value
            }) => {
                $('input[name=id_offense]').val(value.id_offense);
                $('input[name=note]').val(value.note);
                submitForm(action, table);

            });

        });
        //submitForm('takedown', table)
    })

    $('#btn-arsip').click(() => {
        submitForm('arsip', datatable);
    });

    $('#btn-takedown').click(() => {
        submitForm('takedown', datatable);
    });

    $('#date-filter').remove();
    $('#reported-product-status').remove();
});
