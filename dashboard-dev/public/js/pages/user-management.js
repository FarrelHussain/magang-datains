/*******global variable in local file********/

let datatable;

/*********************************** */


$(() => {
    datatable = $('.datatable').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "dom": `<"row"<"col-2 mt-2"l><"col-10 mt-1 d-flex flex-row-reverse"<p><"mt-1 mr-3"f>>>t<"bottom"ip>r`,
        "ajax": {
            'url': 'user-management',
            'dataType': 'json',
            'method': 'GET',
            'data': {
                'operator_data': $('meta[name="csrf-token"]').attr('content')
            }
        },
        "language": {
            "processing": loading()
        },
        "processing": true,
        "columns": [{
                //0
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                searchable: false,
                orderable: false,
            },
            {
                //1
                data: 'name',
                name: 'name'
            },
            {
                //1
                data: 'username',
                name: 'username'
            },
            {
                //2
                data: 'email',
                name: 'email',
            },
            {
                //3
                data: 'location',
                data: 'location',
            },
            { 
                //4
                data: 'role.role_name',
                data: 'role.role_name',
            },
            {
                //5
                data: 'id',
                name: 'id',
                searchable: false,
                render: (value, a, b, c) => {
                    return `
                        <div class='row'>
                            <div class='col-6 pr-0 mr-0'>
                                <a href="user-management/${value}"><button class='btn btn-xs btn-warning'><i class='fas fa-cog text-light'></i></button></a>
                            </div>
                            <div class='col-6 pl-0 ml-0'>
                                <form action='user-management/${value}' method="POST" onsubmit="return confirm('Apakah anda yakin menghapus data ini?');">
                                    <input type='hidden' name='_token' value='${$('meta[name="csrf-token"]').attr('content')}' />
                                    <input type='hidden' name='_method' value='DELETE' />
                                    <button class='btn btn-xs btn-danger'><i class='fas fa-trash text-light'></i></button>
                                </form>
                            </div>
                        </div>

                    `;
                },
                className: 'text-center'
            },
        ],
        "pageLength": 10,
        "serverSide": true,
    });

    $.validator.setDefaults({
        submitHandler: function (form) {
            form.submit();
        }
    });
    $('#formAddUser').validate({
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            username: {
                required: true,
                minlength: 4
            },
            location_id: {
                required: true,

            },
            role_code: {
                required: true,

            },
            password_confirmation: {
                equalTo: '#password'
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

});
