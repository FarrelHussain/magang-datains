$(() => {
    $('.datatable').DataTable({
        "order": [[ 4, 'desc' ]],
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "dom": `<"row"<"col-2 mt-2"l><"col-10 mt-1 d-flex flex-row-reverse"<p><"mt-1 mr-3">>>t<"bottom"ip>r`,
        "ajax": {
            'url': 'user-activity/get-data',
            'dataType': 'json',
            'method': 'GET',
            'data': {
                'operator_data': $('meta[name="csrf-token"]').attr('content')
            }
        },
        "language": {
            "processing": loading()
        },
        "processing": true,
        "columns": [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                searchable: false,
                orderable: false,
            },
            {
                data: 'username',
                name: 'username',
                searchable: false,
            },
            {
                data: 'description',
                name: 'description',
                searchable: false,
            },
            {
                data: 'ip_address',
                name: 'ip_address',
                searchable: false,
                orderable: false,
            },
            {
                data: 'timestamp',
                name: 'timestamp',
                searchable: false,
            },
        ],
        "pageLength": 10,
        "serverSide": true,
    });
});
