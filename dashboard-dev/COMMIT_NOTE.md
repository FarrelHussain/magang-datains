# BPOM Dashboard

### Commit Note 12/07/2020
- update config and autoload psr-4
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```
- Migration add total_product and target columns to stores table
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_07_12_191908_add_total_product_and_target__to_stores_table.php
```
- clear cache

### COmmit Note 10/07/2020
- update config and autoload psr-4
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```
- Migration add crawled_from column to stores table
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_07_10_002624_add_crawled_from_to_stores_table.php
```
- clear cache

### COmmit Note 05/07/2020
- update config and autoload psr-4
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```
- Migration create keywords table view
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_07_05_151545_create_keywords_record_view.php
```
### COmmit Note 01/07/2020
- update config and autoload psr-4
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```
- Migration add columns to stores table
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_06_30_230943_add_columns_to_stores_table.php
```

- Migration add total_product to keywords table
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_07_01_002853_add_total_product_column_to_keywords_table.php
```

### Commit Note 29/06/2020
- update config and autoload psr-4
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```

- crawling histories migration
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_06_29_232711_create_crawling_histories_table.php
```

### Commit Note 23/06/2020 (20:07)

- update config and autoload psr-4
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```
- seeding marketplace logo (update marketplace logo)
```sh
$ cd [project dir]
$ php artisan db:seed --class=MarketplaceLogoSeeder
```

### Commit Note 23/06/2020
- update config
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```
- seeding new status
```sh
$ cd [project dir]
$ php artisan db:seed --class=StatusSeeder 
```
- clear browser cache

### Commit Note 17/06/2020

- Update config

```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```
- add offense: 'pelanggaran lain - lain', run command below

```sh
$ cd [project dir]
$ php artisan db:seed --class=OffensesSeeder
```
- To add commodities look at commit note 07/06/2020

### Commit Note 16/06/2020
- run following command
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```

### Commit Note 08/06/2020

- Before going any further, please make sure everything is ok by running command as follow
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```

- Clear browser cache

### Commit Note 07/06/2020

- Before going any further, please make sure everything is ok by running command as follow
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```

- To add marketplace you can run following command (run marketplace seeder specifically)
```sh
$ cd [project dir]
$ php artisan db:seed --class=MarketplaceSeeder
```
- To add commodities (run commodities seeder specifically)
```sh
$ cd [project dir]
$ php artisan db:seed --class=CommoditiesSeeder
```

### Commit Note 01/06/2020
- Before going any further, please make sure everything is ok by running command as follow
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```
- migration for adding produsen / manufacturer
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_06_01_120416_add_manufacturer_column_to_data_product_table.php
```


### commit Note 06/05/2020
- Just run following commands
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```

### commit Note 29/04/2020
- Before going any further, please make sure everything is ok by running command as follow
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```

- migration for adding inserted_by column in links table
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_04_29_153901_add_from_column_to_links_table.php
```
- create folder "imports" inside "storage/app/public" and don't forget the permission

### commit Note 27/04/2020

- this commit only migration. here are the migrations

```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_04_27_155816_modify_link_column_data_type.php
$ php artisan migrate --path=database/migrations/2020_04_27_161522_add_default_value_timestamp_to_links.php
$ php artisan migrate --path=database/migrations/2020_04_27_163110_add_default_value_timestamp_to_commodities.php
$ php artisan migrate --path=database/migrations/2020_04_27_163319_add_default_value_timestamp_to_data_product.php
$ php artisan migrate --path=database/migrations/2020_04_27_163442_add_default_value_timestamp_to_keywords.php
$ php artisan migrate --path=database/migrations/2020_04_27_164038_add_default_value_timestamp_to_marketplace.php
$ php artisan migrate --path=database/migrations/2020_04_27_164149_add_default_value_timestamp_to_offenses.php
$ php artisan migrate --path=database/migrations/2020_04_27_164337_add_default_value_timestamp_to_products.php
$ php artisan migrate --path=database/migrations/2020_04_27_164456_add_default_value_timestamp_to_reported_products.php
$ php artisan migrate --path=database/migrations/2020_04_27_164556_add_default_value_timestamp_to_roles.php
$ php artisan migrate --path=database/migrations/2020_04_27_164641_add_default_value_timestamp_to_status.php
$ php artisan migrate --path=database/migrations/2020_04_27_164747_add_default_value_timestamp_to_stores.php
```



### Commit Note 22/04/2020

- Before going any further, please make sure everything is ok by running command as follow
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```

- migrations
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_04_21_211145_delete_note_id_offense_tbl_reported_product.php
$ php artisan migrate --path=database/migrations/2020_04_21_211918_add_columns_to_links_table.php
$ php artisan migrate --path=database/migrations/2020_04_21_212415_add_columns_to_keywords_table.php
$ php artisan migrate --path=database/migrations/2020_04_21_213439_add_status_column_to_reported_products_table.php
$ php artisan migrate --path=database/migrations/2020_04_21_214638_add_id_user_to_links_table.php
```
- seeder
```sh
$ cd [project dir]
$ php artisan db:seed --class=StatusSeeder
```

### Commit Note 17/04/2020 (1)
- Before going any further, please make sure everything is ok by running command as follow
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
$ php artisan storage:link
```
- Migration: add screenshot column to product
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_04_17_193859_add_screenshot_column_to_products_table.php
```

- Migration: add id_user column to reported product
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_04_17_194701_add_id_user_column_to_reported_products.php
```
- API Endpoint: Update product screenshot

| Method | Endpoint                                | body (form data)            | Response                                             |
|:------:|:---------------------------------------:|:---------------------------:|:----------------------------------------------------:|
| POST   | domain.com/api/product/{id_product}     | { screenshot: file.image }  | {code: HTTP_CODE, success: BOOLEAN, message: STRING} |


### Commit Note 15/04/2020 (1)
- Before going any further, please make sure everything is ok by running command as follow
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```
- over and over again As alyaws, js made with love so just clear your cache browser

### Commit Note 15/04/2020
- Before going any further, please make sure everything is ok by running command as follow
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```
- over and over again As alyaws, js made with love so just clear your cache browser


### Commit Note 13/04/2020
- Before going any further, please make sure everything is ok by running command as follow
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```

- Offenses migration
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_04_13_214152_create_offenses_table.php
```

- Add columns (id_offense, note) to reported_products migration
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_04_13_215744_add_columns_to_reported_product_table.php
```
- Seeder all (InsyaAloh no error - tested) but it will replace all constant data with default one (only: status, offenses, commodities, admin, operator, roles) and it'll be ok if you don't mind.

```sh
$ cd [project dir]
$ php artisan db:seed
```
- if there is still error occurs just run by the class. here are seeders you need to run for this commit

| Seeder            | Command                                               |
| ----------------- |:-----------------------------------------------------:|
| status            | php artisan db:seed --class=StatusSeeder              |
| offenses          | php artisan db:seed --class=OffensesSeeder            |
| commodities       | php artisan db:seed --class=CommoditiesSeeder         |

- the last but not the least, just clear your browser cache cz you know? JS made with LOVE <3

### Commit Note 11/04/2020 (1)

- There is no special note for this commit if you've read and followed commit note one prev before this.
- just in case, clear your cache browser

### Commit Note 11/04/2020

- Before going any further, please make sure everything is ok by running command as follow
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```

- Commodities migration
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_04_10_195655_create_commodities_table.php
```

- Commodities Seeder. Btw, if you wanna change the commodities data (for seeding needs) just go to config/app.php, you can find it in "commodities" index.
```sh
$ cd [project dir]
php artisan db:seed --class=CommoditiesSeeder
```

- Migration for adding id_commodity column to keywords table
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_04_10_202830_add_id_commodity_to_keywords_table.php
```

- Migration for adding id_commodity column to products table
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_04_10_203358_add_id_commodity_to_products_table.php
```

- For the first run, data may not be seen when commodities filter is used, it could be caused by id_commodity value is still null in products table
- clear browser cache may be important :)

### Commit Note 01/04/2020 

- Before going any further, please make sure everything is ok by running command as follow
```sh
$ cd [project dir]
$ php artisan config:cache
$ composer dump-autoload
```
- migrate logo (column name: logo) migration by running
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_04_01_200752_add_logo_column_to_marketplace_table.php
```

- Run logo seeder if necessary
```sh
$ cd [project dir]
$ php artisan db:seed --class=LogoMarketplaceSeeder
```
- Clear browser cache


### Commit Note 27/03/2020 17:47
- Add migrations and seeders
- Add user level
- Before going any further, please make sure everything is ok by running command as follow

```sh
$ cd [project dir]
$ composer install
$ php artisan config:cache
```

- Change data_product.price data type. in order to be able run the migration run command below

```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_03_27_093945_drop_column_price.php
$ php artisan migrate --path=database/migrations/2020_03_27_094143_add_column_price_after_delete.php
```
- Add roles table.  to run the migration hit command below

```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_03_27_095211_create_roles_table.php
```

- Add role_code column to users. Run the migration command as follow
```sh
$ cd [project dir]
$ php artisan migrate --path=database/migrations/2020_03_27_100751_add_role_code_column_to_users_table.php
```

- Roles Seeder
```sh
$ cd [project dir]
$ php artisan db:seed --class=RolesSeeder
```

- Super Admin Level Seeder
```sh
$ cd [project dir]
$ php artisan db:seed --class=AdminSeeder
```

- Operator Level Seeder
```sh
$ cd [project dir]
$ php artisan db:seed --class=OperatorSeeder
```

### Commit Note 27/03/2020 00:00
- Add migrations
- Before migrating don't forget to run

```sh
$ cd [project dir]
$ composer install
```
