import glob
import os
import mysql.connector
from mysql.connector import Error

def getDuplicateLink():
    try:
        sql_select_Query = """SELECT  link,  COUNT(link)
                            FROM links
                            GROUP BY link
                            HAVING COUNT(link) > 1
                            LIMIT 500;"""
        cursor = connection.cursor()
        cursor.execute(sql_select_Query)
        records = cursor.fetchall()
        print("Total number of rows in links is: ", cursor.rowcount)

        print("\nPrinting each links record")
        for row in records:
            print("Link = ", row[0], )
            print("Total = ", row[1])
            # new_cursor = connection.cursor()
            cursor.execute('SELECT id_product FROM `links` WHERE `link` = "{}"'.format(str(row[0])))
            links = cursor.fetchall()
            
            for i, link in enumerate(links):
                id_product = link[0]
                if i != 0:
                    print(i)
                    print(id_product)

                    # delete product
                    cursor.execute("SET FOREIGN_KEY_CHECKS=0;")
                    cursor.execute("DELETE FROM `products` WHERE (`id_product` = '{}')".format(id_product))
                    cursor.execute("DELETE FROM `data_product` WHERE (`id_product` = '{}')".format(id_product))
                    cursor.execute("DELETE FROM `links` WHERE (`id_product` = '{}')".format(id_product))
                    cursor.execute("SET FOREIGN_KEY_CHECKS=1;")
                    connection.commit()

            # exit()
    except Error as e:
        print("Error reading data from MySQL table", e)
    finally:
        if (connection.is_connected()):
            connection.close()
            cursor.close()
            print("MySQL connection is closed")

def removeDuplicateReported():
    try:
        sql_select_Query = """
        SELECT 
            id_link, COUNT(id_link)
        FROM reported_products
        GROUP BY
            id_link, id_link
        HAVING 
            COUNT(id_link) > 1;"""

        cursor = connection.cursor()
        cursor.execute(sql_select_Query)
        records = cursor.fetchall()
        print("Total number of rows in reported products is: ", cursor.rowcount)

        print("\nPrinting each reported record")
        for row in records:
            print("Link ID = "+ str(row[0]) )
            print("Total = "+ str(row[1]))
            # new_cursor = connection.cursor()
            cursor.execute('SELECT id_reported_product, id_status FROM `reported_products` WHERE `id_link` = "{}"'.format(str(row[0])))
            reported_products = cursor.fetchall()
            
            for i, reported_product in enumerate(reported_products):
                id_reported_product = reported_product[0]
                if '1' == str(reported_product[1]):
                    print(i)
                    print(id_reported_product)

                    cursor.execute("DELETE FROM `reported_products` WHERE (`id_reported_product` = '{}')".format(id_reported_product))
                    connection.commit()

    except Error as e:
        print("Error reading data from MySQL table", e)
    finally:
        if (connection.is_connected()):
            connection.close()
            cursor.close()
            print("MySQL connection is closed")

def updateMarketplaceID():
    try:
        import tldextract
        sql_select_Query = """select * from stores where id_marketplace is null;"""

        cursor = connection.cursor()
        cursor.execute(sql_select_Query)
        records = cursor.fetchall()
        print("Total number of rows in stores is: ", cursor.rowcount)

        print("\nPrinting each store record")
        for row in records:
            url = str(tldextract.extract(row[2]).domain)
            id_store = str(row[0])
            print("Store ID = "+ id_store )
            print("URL = "+ url)
            
            cursor.execute('SELECT id_marketplace FROM `marketplace` WHERE `name` LIKE  "%{}%"'.format(url))
            marketplace = cursor.fetchall()
            
            for i, marketplace in enumerate(marketplace):
                id_marketplace = marketplace[0]
                
                cursor.execute("UPDATE `stores` SET `id_marketplace` = '{}' WHERE id_store = {}".format(id_marketplace, id_store))
                connection.commit()
            # exit()

    except Error as e:
        print("Error reading data from MySQL table", e)
    finally:
        if (connection.is_connected()):
            connection.close()
            cursor.close()
            print("MySQL connection is closed")

def remove_unnecessary_files():
    try:
        sql_select_Query = """SELECT screenshot FROM `products` WHERE `screenshot` IS NOT NULL;"""

        cursor = connection.cursor()
        cursor.execute(sql_select_Query)
        records = [item[0] for item in cursor.fetchall()]

        for file in glob.glob("/home/mplace/dashboard/public/storage/screenshot/*.png"):
            if os.path.basename(file) not in records:
                print(file)
                print(os.path.basename(file))
                if os.path.exists(file):
                    os.remove(file)
                    print("The file has been removed")
                else:
                    print("The file does not exist")

    except Error as e:
        print("Error reading data from MySQL table", e)
    finally:
        if (connection.is_connected()):
            connection.close()
            cursor.close()
            print("MySQL connection is closed")

def removeDuplicateLinkandReported():
    try:
        sql_select_Query = """SELECT  link,  COUNT(link)
                            FROM links
                            GROUP BY link
                            HAVING COUNT(link) > 1
                            """
        cursor = connection.cursor()
        cursor.execute(sql_select_Query)
        records = cursor.fetchall()
        print("Total number of rows in links is: ", cursor.rowcount)

        print("\nPrinting each links record")
        for row in records:
            print("Link = ", row[0], )
            print("Total = ", row[1])
            # new_cursor = connection.cursor()
            cursor.execute('''SELECT links.id_product, reported_products.id_status, reported_products.id_reported_product FROM `links` 
                            JOIN reported_products on reported_products.id_link = links.id
                            WHERE links.`link` = "{}"'''.format(str(row[0])))
            links = cursor.fetchall()
            
            for i, link in enumerate(links):
                id_product = link[0]
                id_status = link[1]
                id_reported_product = link[2]
                if i != 0:
                    print(i)
                    print(id_product)

                    # delete product
                    cursor.execute("SET FOREIGN_KEY_CHECKS=0;")
                    cursor.execute("DELETE FROM `products` WHERE (`id_product` = '{}')".format(id_product))
                    cursor.execute("DELETE FROM `data_product` WHERE (`id_product` = '{}')".format(id_product))
                    cursor.execute("DELETE FROM `links` WHERE (`id_product` = '{}')".format(id_product))
                    cursor.execute("DELETE FROM `reported_products` WHERE (`id_reported_product` = '{}')".format(id_reported_product))
                    cursor.execute("SET FOREIGN_KEY_CHECKS=1;")
                    connection.commit()

            # exit()
    except Error as e:
        print("Error reading data from MySQL table", e)
    finally:
        if (connection.is_connected()):
            connection.close()
            cursor.close()
            print("MySQL connection is closed")

def removeDuplicateStores():
    try:
        sql_select_Query = """select url, count(url) as count from stores
            group by url having count(url) > 1;"""

        cursor = connection.cursor(dictionary=True)
        cursor.execute(sql_select_Query)
        records = cursor.fetchall()
        print("Total number of rows in links is: ", cursor.rowcount)

        print("\nPrinting each links record")
        for row in records:
            print("URL = ", row['url'], )
            print("Total = ", row['count'])
            # new_cursor = connection.cursor()
            cursor.execute('SELECT id_store FROM stores WHERE `url` = "{}"'.format(str(row['url'])))
            stores = cursor.fetchall()
            
            for i, store in enumerate(stores):
                id_store = store['id_store']
                # if i != 0:
                #     print(i)
                # print(id_store)
                cursor.execute("SELECT * FROM `products` WHERE `id_store` IN ({}) LIMIT 50".format(id_store))
                products = cursor.fetchall()

                if len(products) == 0:  
                    # delete product
                    cursor.execute("SET FOREIGN_KEY_CHECKS=0;")
                    cursor.execute("DELETE FROM `stores` WHERE (`id_store` = '{}')".format(id_store))
                    cursor.execute("SET FOREIGN_KEY_CHECKS=1;")
                    connection.commit()

            # exit()
    except Error as e:
        print("Error reading data from MySQL table", e)
    finally:
        if (connection.is_connected()):
            connection.close()
            cursor.close()
            print("MySQL connection is closed")

if __name__ == "__main__":
    connection = mysql.connector.connect(host='192.168.10.115',
                                            database='esmart_d4bpom_new_new',
                                            user='mplace',
                                            password='MpLAce@123!')

    # getDuplicateLink()
    # removeDuplicateReported()
    # updateMarketplaceID()
    # remove_unnecessary_files()
    # removeDuplicateLinkandReported()
    removeDuplicateStores()
