### Commit Note 
[Commit Note](https://gitlab.com/datains/bpom/esmart/dashboard/-/blob/dashboard-dev/COMMIT_NOTE.md) update: 23/06/2020

### Installation
Install the dependencies and devDependencies.
```sh
$ cd [project dir]
$ composer install
$ composer dumpautoload
$ php artisan storage:link
```
### Setup ENV
- Set CRAWLER_URL
- Set APP_NAME
- Set APP_URL
- Set DB Connection
- Set ASSET_URL (if necessary) 
- Set .htaccess (as needed)

### Migration
```sh
$ cd [project dir]
$ php artisan migrate
```

### Seeder (Constant Seeder)
```sh
$ cd [project dir]
$ php artisan db:seed
```

### API Endpoints
- Update product screenshot

| Method | Endpoint                                | body (form data)            | Response                                             |
|:------:|:---------------------------------------:|:---------------------------:|:----------------------------------------------------:|
| POST   | domain.com/api/product/{id_product}     | { screenshot: file.image }  | {code: HTTP_CODE, success: BOOLEAN, message: STRING} |
