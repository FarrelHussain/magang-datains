<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddDefaultValueTimestampToDataProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('data_product')) {
            if (Schema::hasColumns('data_product', ['created_at', 'updated_at'])) {
                Schema::table('data_product', function (Blueprint $table) {
                    DB::statement("ALTER TABLE data_product CHANGE created_at created_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP");
                    DB::statement("ALTER TABLE data_product CHANGE updated_at updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP");
                });
            } else {
                echo "No created_at or updated_at column";
            }
        } else {
            echo "No data_product table";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('data_product')) {
            if (Schema::hasColumns('data_product', ['created_at', 'updated_at'])) {
                Schema::table('data_product', function (Blueprint $table) {
                    DB::statement("ALTER TABLE data_product CHANGE created_at created_at TIMESTAMP NULL");
                    DB::statement("ALTER TABLE data_product CHANGE updated_at updated_at TIMESTAMP NULL");
                });
            } else {
                echo "No created_at or updated_at column";
            }
        } else {
            echo "No data_product table";
        }
    }
}
