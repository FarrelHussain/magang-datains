<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stores', function (Blueprint $table) {
            $table->string('joined_since', 255)->nullable()->after('description');
            $table->text('last_update')->nullable()->after('joined_since');
            $table->double('seller_rate', 3, 2)->nullable()->after('last_update');
            $table->text('seller_type')->nullable()->after('seller_rate');
            $table->string('brand', 255)->nullable()->after('seller_type');
            $table->integer('followers', false)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            $table->dropColumn('joined_since');
            $table->dropColumn('last_update');
            $table->dropColumn('seller_rate');
            $table->dropColumn('seller_type');
            $table->dropColumn('brand');
            $table->dropColumn('followers');
        });
    }
}
