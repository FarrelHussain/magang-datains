<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableLinksChangeIdProdukToIdProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('links')) {
            if (Schema::hasColumn('links', 'id_produk')) {
                Schema::table('links', function (Blueprint $table) {
                    $table->renameColumn('id_produk', 'id_product');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('links')) {
            if (Schema::hasColumn('links', 'id_product')) {
                Schema::table('links', function (Blueprint $table) {
                    $table->renameColumn('id_product', 'id_produk');
                });
            }
        }
    }
}
