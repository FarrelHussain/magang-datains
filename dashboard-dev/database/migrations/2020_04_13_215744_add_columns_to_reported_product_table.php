<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToReportedProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('reported_products')) {
            Schema::table('reported_products', function (Blueprint $table) {
                $table->integer('id_offense', false)->length(11)->nullable()->after('id_link');
                $table->text('note')->nullable()->after('id_offense');

                $table->foreign('id_offense', 'reported_product_id_offense')->references('id_offense')->on('offenses')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reported_products', function (Blueprint $table) {
            $table->dropForeign('reported_product_id_offense');
            $table->dropColumn(['id_offense', 'note']);
        });
    }
}
