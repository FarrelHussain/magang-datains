<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_product', function (Blueprint $table) {
            $table->integer('id_data_product', true)->length(11);
            $table->integer('id_product', false)->length(11)->nullable()->index();
            $table->dateTime('crawled')->nullable();
            $table->integer('rating', false)->length(11)->nullable();
            $table->integer('review_count', false)->length(11)->nullable();
            $table->float('discount')->nullable();
            $table->double('price', 15, 8)->nullable();
            $table->integer('item_count', false)->length(11)->nullable();
            $table->integer('sold_count', false)->length(11)->nullable();
            $table->integer('view_count', false)->length(11)->nullable();
            $table->integer('favorited_count', false)->length()->nullable();
            $table->string('processing_time', 255)->nullable();
            $table->enum('item_condition', ['baru', 'bekas'])->nullable();
            $table->string('keywords', 255)->nullable();
            $table->timestamps();

            $table->foreign('id_product')->references('id_product')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_products');
    }
}
