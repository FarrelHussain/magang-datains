<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlteruseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('users')) {
            if (!Schema::hasColumns('users', ['location_id'])) {
                Schema::table('users', function (Blueprint $table) {
                    $table->string('location_id', 225)->nullable()->after('username');
                });
            }
        } else {
            echo "No products table";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('users')) {
            if (Schema::hasColumn('users', 'location_id')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->dropColumn('location_id');
                });
            }
        }
    }
}
