<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUptLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upt_locations', function (Blueprint $table) {
            $table->bigIncrements('id_upt_location');
            $table->string('name', 255)->nullable();
            $table->string('location', 255)->nullable();
            $table->mediumText('working_area')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upt_locations');
    }
}
