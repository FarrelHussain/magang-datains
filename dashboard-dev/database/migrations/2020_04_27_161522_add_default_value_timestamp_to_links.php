<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddDefaultValueTimestampToLinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('links')) {
            if (Schema::hasColumns('links', ['created_at', 'updated_at'])) {
                Schema::table('links', function (Blueprint $table) {
                    DB::statement("ALTER TABLE links CHANGE created_at created_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP");
                    DB::statement("ALTER TABLE links CHANGE updated_at updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP");
                });
            } else {
                echo "No created_at or updated_at column";
            }
        } else {
            echo "No links table";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('links')) {
            if (Schema::hasColumns('links', ['created_at', 'updated_at'])) {
                Schema::table('links', function (Blueprint $table) {
                    DB::statement("ALTER TABLE links CHANGE created_at created_at TIMESTAMP NULL");
                    DB::statement("ALTER TABLE links CHANGE updated_at updated_at TIMESTAMP NULL");
                });
            } else {
                echo "No created_at or updated_at column";
            }
        } else {
            echo "No links table";
        }
    }
}
