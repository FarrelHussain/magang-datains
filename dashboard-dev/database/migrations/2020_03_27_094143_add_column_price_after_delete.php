<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPriceAfterDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('data_product')) {
            if (!Schema::hasColumn('data_product', 'price')) {
                Schema::table('data_product', function (Blueprint $table) {
                    $table->string('price', 10)->nullable()->after('id_product');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('data_product')) {
            if (Schema::hasColumn('data_product', 'price')) {
                Schema::table('data_product', function (Blueprint $table) {
                    $table->dropColumn('price');
                });
            }
        }
    }
}
