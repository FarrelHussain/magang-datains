<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropContactStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('stores')) {
            if (Schema::hasColumn('stores', 'contact')) {
                Schema::table('stores', function (Blueprint $table) {
                    $table->dropColumn('contact');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('stores')) {
            if (!Schema::hasColumn('stores', 'contact')) {
                Schema::table('stores', function (Blueprint $table) {
                    $table->string('contact', 100)->nullable();
                });
            }
        }
    }
}
