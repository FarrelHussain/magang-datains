<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNotesToReportedProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('reported_products')) {
            if (!Schema::hasColumns('reported_products', ['notes'])) {
                Schema::table('reported_products', function (Blueprint $table) {
                    $table->string('notes')->nullable()->after('filename');
                });
            }
        } else {
            echo "No reported products table";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('reported_products')) {
            if (Schema::hasColumn('reported_products', 'notes')) {
                Schema::table('reported_products', function (Blueprint $table) {
                    $table->dropColumn('notes');
                });
            }
        }
    }
}
