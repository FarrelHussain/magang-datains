<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class May20AddColumnsToKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('keywords', function (Blueprint $table) {
            $table->string('content', 255)->nullable()->after('note');
            $table->string('therapy_class', 255)->nullable()->after('content');
            $table->string('group', 255)->nullable()->after('therapy_class');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('keywords', function (Blueprint $table) {
            $table->dropColumn(['content', 'therapy_class', 'group']);
        });
    }
}
