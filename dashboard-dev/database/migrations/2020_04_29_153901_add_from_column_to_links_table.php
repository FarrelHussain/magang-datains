<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFromColumnToLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('links')) {
            if (!Schema::hasColumn('links', 'inserted_by')) {
                Schema::table('links', function (Blueprint $table) {
                    $table->string('inserted_by', 100)->after('note')->nullable()->default('System');
                });
            } else {
                echo "Column already exist";
            }
        } else {
            echo "table doesn't exist";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('links')) {
            if (Schema::hasColumn('links', 'inserted_by')) {
                Schema::table('links', function (Blueprint $table) {
                    $table->dropColumn('inserted_by');
                });
            } else {
                echo "Column does not exist";
            }
        } else {
            echo "table doesn't exist";
        }
    }
}
