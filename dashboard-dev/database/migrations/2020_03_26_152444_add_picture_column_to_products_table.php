<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPictureColumnToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('products')) {
            if (!Schema::hasColumn('products', 'picture')) {
                Schema::table('products', function (Blueprint $table) {
                    $table->text('picture')->nullable()->after('description');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('products')) {
            if (Schema::hasColumn('products', 'picture')) {
                Schema::table('products', function (Blueprint $table) {
                    $table->dropColumn('picture');
                });
            }
        }
    }
}
