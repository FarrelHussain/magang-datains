<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class May20AddColumnsToReportedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reported_products', function (Blueprint $table) {
            $table->string('initial', 100)->nullable()->after('id_status');
            $table->dateTime('periode')->nullable()->after('initial');
            $table->string('filename', 255)->nullable()->after('periode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reported_products', function (Blueprint $table) {
            $table->dropColumn(['initial', 'periode', 'filename']);
        });
    }
}
