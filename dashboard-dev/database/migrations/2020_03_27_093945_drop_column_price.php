<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('data_product')) {
            if (Schema::hasColumn('data_product', 'price')) {
                Schema::table('data_product', function (Blueprint $table) {
                    $table->dropColumn('price');
                });
            } else {
                echo "No column price";
            }
        } else {
            echo "no data_product table";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('data_product')) {
            if (!Schema::hasColumn('data_product', 'price')) {
                Schema::table('data_product', function (Blueprint $table) {
                    $table->text('price')->nullable()->after('discount');
                });
            }
        }
    }
}
