<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddScreenshotColumnToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('products')) {
            if (!Schema::hasColumn('products', 'screenshot')) {
                Schema::table('products', function (Blueprint $table) {
                    $table->string('screenshot', 255)->nullable()->after('picture');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('products')) {
            if (Schema::hasColumn('products', 'screenshot')) {
                Schema::table('products', function (Blueprint $table) {
                    $table->dropColumn('screenshot');
                });
            }
        }
    }
}
