<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusColumnToReportedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('reported_products')) {
            if (!Schema::hasColumn('reported_products', 'id_status')) {
                Schema::table('reported_products', function (Blueprint $table) {
                    $table->integer('id_status', false)->length(11)->nullable()->after('id_link');

                    $table->foreign('id_status', 'reported_products_id_status_foreign')->references('id_status')->on('status')->onDelete('cascade');
                });
            } else {
                echo "id_status column already exists";
            }
        } else {
            echo "there is no reported_products table";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('reported_products')) {
            if (Schema::hasColumn('reported_products', 'id_status')) {
                Schema::table('reported_products', function (Blueprint $table) {
                    $table->dropForeign('reported_products_id_status_foreign');
                    $table->dropColumn('id_status');
                });
            } else {
                echo "id_status column does not exist";
            }
        } else {
            echo "there is no reported_products table";
        }
    }
}
