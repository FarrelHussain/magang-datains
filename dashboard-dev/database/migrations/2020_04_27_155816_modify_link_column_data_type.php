<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyLinkColumnDataType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('links')) {
            if (Schema::hasColumn('links', 'link')) {
                Schema::table('links', function (Blueprint $table) {
                    $table->text('link')->nullable()->change();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('links')) {
            if (Schema::hasColumn('links', 'link')) {
                Schema::table('links', function (Blueprint $table) {
                    $table->string('link', 255)->nullable()->change();
                });
            }
        }
    }
}
