<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->integer('id_store', true)->length(11);
            $table->integer('id_marketplace', false)->length(11)->nullable()->index();
            $table->string('url', 255)->nullable();
            $table->string('name', 255)->nullable();
            $table->string('location', 255)->nullable();
            $table->mediumText('description')->nullable();
            $table->string('contact', 255)->nullable();
            $table->timestamps();

            $table->foreign('id_marketplace')->references('id_marketplace')->on('marketplace');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
