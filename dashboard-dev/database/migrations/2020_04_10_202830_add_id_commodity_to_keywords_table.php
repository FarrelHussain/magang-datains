<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdCommodityToKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('keywords', function (Blueprint $table) {
            if (Schema::hasTable('keywords')) {
                if (!Schema::hasColumn('keywords', 'id_commodity')) {
                    Schema::table('keywords', function (Blueprint $table) {
                        $table->integer('id_commodity', false)->length(11)->nullable()->after('id_marketplace');
                        $table->foreign('id_commodity')->references('id_commodity')->on('commodities')->onDelete('cascade');
                    });
                }
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('keywords')) {
            if (Schema::hasColumn('keywords', 'id_commodity')) {
                Schema::table('keywords', function (Blueprint $table) {
                    $table->dropForeign('keywords_id_commodity_foreign');
                    $table->dropColumn('id_commodity');
                });
            }
        }
    }
}
