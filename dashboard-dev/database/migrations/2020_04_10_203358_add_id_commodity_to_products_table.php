<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdCommodityToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            if (Schema::hasTable('products')) {
                if (!Schema::hasColumn('products', 'id_commodity')) {
                    Schema::table('products', function (Blueprint $table) {
                        $table->integer('id_commodity', false)->length(11)->nullable()->after('id_keywords');
                        $table->foreign('id_commodity')->references('id_commodity')->on('commodities')->onDelete('cascade');
                    });
                }
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('products')) {
            if (Schema::hasColumn('products', 'id_commodity')) {
                Schema::table('products', function (Blueprint $table) {
                    $table->dropForeign('products_id_commodity_foreign');
                    $table->dropColumn('id_commodity');
                });
            }
        }
    }
}
