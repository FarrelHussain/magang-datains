<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlterAddKeywordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('keywords')) {
            if (!Schema::hasColumns('keywords', ['id_crawling_type'])) {
                Schema::table('keywords', function (Blueprint $table) {
                    $table->bigInteger('id_crawling_type')->nullable()->after('id_user');
                });

            }
        } else {
            echo "No reported products table";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('keywords')) {
            if (Schema::hasColumn('keywords', 'id_crawling_type')) {
                Schema::table('keywords', function (Blueprint $table) {
                    $table->dropColumn('id_crawling_type');
                });
            }
        }
    }
}
