<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlterReportBr11Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    if (!Schema::hasTable('reports')) {
        Schema::create('reports', function (Blueprint $table) {
            $table->bigIncrements('id_report');
            $table->string('kode_report');
            $table->unsignedBigInteger('id_user');
            $table->unsignedInteger('id_status');
            $table->string('jenis_laporan');
            $table->timestamp('tgl_submit')->nullable();
            $table->string('notadinas')->nullable();
            $table->timestamp('tgl_approve')->nullable();
            $table->text('feedback')->nullable();
            $table->timestamp('tgl_feedback')->nullable();
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users');
	});
	    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports');
    }
}
