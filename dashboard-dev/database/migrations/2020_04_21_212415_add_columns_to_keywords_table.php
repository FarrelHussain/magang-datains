<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('keywords')) {
            if (!Schema::hasColumns('keywords', ['note', 'product_origin'])) {
                Schema::table('keywords', function (Blueprint $table) {
                    $table->text('note')->nullable()->after('max_product');
                    $table->string('product_origin', 255)->nullable()->after('note');
                });
            } else {
                echo "note and product_origin columns already exists";
            }
        } else {
            echo "keywords table does not exists";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('keywords')) {
            if (Schema::hasColumns('keywords', ['note', 'product_origin'])) {
                Schema::table('keywords', function (Blueprint $table) {
                    $table->dropColumn('note');
                    $table->dropColumn('product_origin');
                });
            } else {
                echo "note and product_origin columns does not exist";
            }
        } else {
            echo "keywords table does not exists";
        }
    }
}
