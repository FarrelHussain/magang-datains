<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddDefaultValueTimestampToRoles extends Migration
{
    public function up()
    {
        if (Schema::hasTable('roles')) {
            if (Schema::hasColumns('roles', ['created_at', 'updated_at'])) {
                Schema::table('roles', function (Blueprint $table) {
                    DB::statement("ALTER TABLE roles CHANGE created_at created_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP");
                    DB::statement("ALTER TABLE roles CHANGE updated_at updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP");
                });
            } else {
                echo "No created_at or updated_at column";
            }
        } else {
            echo "No roles table";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('roles')) {
            if (Schema::hasColumns('roles', ['created_at', 'updated_at'])) {
                Schema::table('roles', function (Blueprint $table) {
                    DB::statement("ALTER TABLE roles CHANGE created_at created_at TIMESTAMP NULL");
                    DB::statement("ALTER TABLE roles CHANGE updated_at updated_at TIMESTAMP NULL");
                });
            } else {
                echo "No created_at or updated_at column";
            }
        } else {
            echo "No roles table";
        }
    }
}
