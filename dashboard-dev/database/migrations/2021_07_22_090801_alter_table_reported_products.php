<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableReportedProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('reports');

        Schema::table('reported_products', function (Blueprint $table) {
            $table->string('report_code')->after('notes')->nullable();
            $table->unsignedBigInteger('report_type')->after('report_code')->nullable()->index();
            $table->text('report_feedback')->after('report_type')->nullable();
            $table->string('official_memo')->after('report_feedback')->nullable();

            $table->timestamp('feedbacked_at')->nullable();
            $table->timestamp('approved_at')->nullable();
            $table->unsignedBigInteger('approved_by')->nullable()->index();
            
            $table->foreign('report_type')->references('id_crawling_type')->on('crawling_type');
            $table->foreign('approved_by')->references('id')->on('users')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reported_products', function(Blueprint $table) {
            $table->dropColumn('report_code');

            $table->dropForeign(['report_type', 'approved_by']);
            $table->dropIndex(['report_type', 'approved_by']);
            $table->dropColumn('report_type');
            $table->dropColumn('approved_by');

            $table->dropColumn('report_feedback');
            $table->dropColumn('official_memo');
            $table->dropColumn('feedbacked_at');
            $table->dropColumn('approved_at');
        });

        Schema::create('reports', function (Blueprint $table) {
            $table->bigIncrements('id_report');
            $table->string('kode_report');
            $table->unsignedBigInteger('id_user');
            $table->unsignedInteger('id_status');
            $table->string('jenis_laporan');
            $table->timestamp('tgl_submit')->nullable();
            $table->string('notadinas')->nullable();
            $table->timestamp('tgl_approve')->nullable();
            $table->text('feedback')->nullable();
            $table->timestamp('tgl_feedback')->nullable();
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users');
        });

        Schema::table('links', function (Blueprint $table) {
            if (!Schema::hasColumn('links', 'id_report')) {
                $table->unsignedBigInteger('id_report')->nullable()->after('id_user');
                $table->foreign('id_report')->references('id_report')->on('reports');
            }
        });
    }
}
