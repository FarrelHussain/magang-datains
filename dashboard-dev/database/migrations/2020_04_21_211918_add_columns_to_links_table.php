<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('links')) {
            if (!Schema::hasColumns('links', ['note', 'id_offense'])) {
                Schema::table('links', function (Blueprint $table) {
                    $table->text('note')->nullable()->after('link');
                    $table->integer('id_offense', false)->length(11)->nullable()->after('id_status');

                    $table->foreign('id_offense', 'links_id_offense_foreign')->references('id_offense')->on('offenses')->onDelete('cascade');
                });
            } else {
                echo "note and id_offense columns already exists";
            }
        } else {
            echo "there is no links table";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('links')) {
            if (Schema::hasColumns('links', ['note', 'id_offense'])) {
                Schema::table('links', function (Blueprint $table) {
                    $table->dropForeign('links_id_offense_foreign');
                    $table->dropColumn('note');
                    $table->dropColumn('id_offense');
                });
            } else {
                echo "there is no note and id_offense columns";
            }
        } else {
            echo "there is no links table";
        }
    }
}
