<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->integer('id_product', true)->length(11);
            $table->integer('id_store', false)->length(11)->nullable()->index();
            $table->integer('id_keywords', false)->length(11)->nullable()->index();
            $table->string('name', 255)->nullable();
            $table->mediumText('description')->nullable();
            $table->mediumText('note')->nullable();
            $table->timestamps();

            $table->foreign('id_store')->references('id_store')->on('stores');
            $table->foreign('id_keywords')->references('id_keywords')->on('keywords');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
