<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reported_products', function (Blueprint $table) {
            $table->integer('id_reported_product', true)->length(11);
            $table->integer('id_link', false)->length(11)->nullable()->index();
            $table->timestamps();

            $table->foreign('id_link')->references('id')->on('links');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reported_products');
    }
}
