<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('offenses')) {
            Schema::create('offenses', function (Blueprint $table) {
                $table->integer('id_offense', true)->length(11);
                $table->string('offense', 100)->nullable();
                $table->string('acronym', 100)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        } else {
            echo "Table already exsist!";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offenses');
    }
}
