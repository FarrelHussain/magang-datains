<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddDefaultValueTimestampToReportedProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('reported_products')) {
            if (Schema::hasColumns('reported_products', ['created_at', 'updated_at'])) {
                Schema::table('reported_products', function (Blueprint $table) {
                    DB::statement("ALTER TABLE reported_products CHANGE created_at created_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP");
                    DB::statement("ALTER TABLE reported_products CHANGE updated_at updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP");
                });
            } else {
                echo "No created_at or updated_at column";
            }
        } else {
            echo "No reported_products table";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('reported_products')) {
            if (Schema::hasColumns('reported_products', ['created_at', 'updated_at'])) {
                Schema::table('reported_products', function (Blueprint $table) {
                    DB::statement("ALTER TABLE reported_products CHANGE created_at created_at TIMESTAMP NULL");
                    DB::statement("ALTER TABLE reported_products CHANGE updated_at updated_at TIMESTAMP NULL");
                });
            } else {
                echo "No created_at or updated_at column";
            }
        } else {
            echo "No reported_products table";
        }
    }
}
