<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropReviewCountColumnDataProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('data_product')) {
            if (Schema::hasColumn('data_product', 'review_count')) {
                Schema::table('data_product', function (Blueprint $table) {
                    $table->dropColumn('review_count');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('data_product')) {
            if (!Schema::hasColumn('data_product', 'review_count')) {
                Schema::table('data_product', function (Blueprint $table) {
                    $table->integer('review_count', false)->length(11)->nullable();
                });
            }
        }
    }
}
