<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteNoteIdOffenseTblReportedProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('reported_products')) {
            if (Schema::hasColumns('reported_products', ['note', 'id_offense'])) {
                Schema::table('reported_products', function (Blueprint $table) {
                    $table->dropForeign('reported_product_id_offense');
                    $table->dropColumn('note');
                    $table->dropColumn('id_offense');
                });
            } else {
                echo "there is no note and id_offense columns";
            }
        } else {
            echo "there is no reported_product table";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('reported_products')) {
            if (!Schema::hasColumns('reported_products', ['note', 'id_offense'])) {
                Schema::table('reported_products', function (Blueprint $table) {
                    $table->text('note')->nullable();
                    $table->integer('id_offense', false)->length(11)->nullable()->after('id_link');
                    $table->foreign('id_offense', 'reported_product_id_offense')->references('id_offense')->on('offenses')->onDelete('cascade');
                });
            } else {
                echo "note and id_offense columns already exists";
            }
        } else {
            echo "there is no reported_product table";
        }
    }
}
