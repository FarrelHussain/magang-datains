<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keywords', function (Blueprint $table) {
            $table->integer('id_keywords', true)->length(11);
            $table->integer('id_marketplace', false)->length(11)->index();
            $table->string('keywords', 255)->nullable();
            $table->tinyInteger('status', false)->length(1)->nullable();
            $table->timestamps();

            $table->foreign('id_marketplace')->references('id_marketplace')->on('marketplace');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keywords');
    }
}
