<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdUserToLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('links')) {
            if (!Schema::hasColumn('links', 'id_user')) {
                Schema::table('links', function (Blueprint $table) {
                    $table->bigInteger('id_user')->unsigned()->nullable()->after('id_offense')->index();
                    $table->foreign('id_user', 'links_id_user_foreign')->references('id')->on('users')->onDelete('cascade');
                });
            } else {
                echo "Column already exists";
            }
        } else {
            echo "No links table";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('links')) {
            if (Schema::hasColumn('links', 'id_user')) {
                Schema::table('links', function (Blueprint $table) {
                    $table->dropForeign('links_id_user_foreign');
                    $table->dropColumn('id_user');
                });
            } else {
                echo "id_status column does not exist";
            }
        } else {
            echo "there is no links table";
        }
    }
}
