<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdUserColumnToReportedProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('reported_products')) {
            if (!Schema::hasColumn('reported_products', 'id_user')) {
                Schema::table('reported_products', function (Blueprint $table) {
                    $table->bigInteger('id_user')->unsigned()->nullable()->after('id_reported_product')->index();
                    $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
                });
            } else {
                echo "Column already exists";
            }
        } else {
            echo "No reported_products table";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('reported_products')) {
            if (Schema::hasColumn('reported_products', 'id_user')) {
                Schema::table('reported_products', function (Blueprint $table) {
                    $table->dropForeign('reported_products_id_user_foreign');
                    $table->dropColumn('id_user');
                });
            } else {
                echo "Column doesn't exists";
            }
        } else {
            echo "No reported_products table";
        }
    }
}
