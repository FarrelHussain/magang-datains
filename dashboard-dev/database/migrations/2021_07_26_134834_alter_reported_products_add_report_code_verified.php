<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterReportedProductsAddReportCodeVerified extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reported_products', function(Blueprint $table) {
            $table->string('report_code_verified')->after('report_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reported_products', function(Blueprint $table) {
            $table->dropColumn('report_code_verified');
        });
    }
}
