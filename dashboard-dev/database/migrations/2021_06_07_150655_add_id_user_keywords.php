<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdUserKeywords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('keywords')) {
            if (!Schema::hasColumn('keywords', 'id_user')) {
                Schema::table('keywords', function (Blueprint $table) {
                    $table->bigInteger('id_user')->unsigned()->nullable()->after('id_commodity')->index();
                    $table->foreign('id_user', 'keywords_id_user_foreign')->references('id')->on('users')->onDelete('cascade');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('keywords')) {
            if (Schema::hasColumn('keywords', 'id_user')) {
                Schema::table('keywords', function (Blueprint $table) {
                    $table->dropForeign('keywords_id_user_foreign');
                    $table->dropColumn('id_user');
                });
            } else {
                echo "id_user column does not exist";
            }
        } else {
            echo "there is no keywords table";
        }
    }
}
