<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRoleCodeColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('users')) {
            if (!Schema::hasColumn('users', 'role_code')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->integer('role_code', false)->nullable()->after('password');
                    $table->foreign('role_code')->references('role_code')->on('roles');
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('users')) {
            if (Schema::hasColumn('users', 'role_code')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->dropColumn('role_code');
                });
            }
        }
    }
}
