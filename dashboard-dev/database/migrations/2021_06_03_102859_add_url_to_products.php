<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUrlToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('products', 'url')) {
            Schema::table('products', function (Blueprint $table) {
                $table->string('url', 700)->nullable()->unique()->change();
            });
        } else {
            Schema::table('products', function (Blueprint $table) {
                $table->string('url', 700)->after('name')->unique()->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('url');
        });
    }
}
