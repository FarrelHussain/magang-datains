<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLogoColumnToMarketplaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('marketplace')) {
            if (!Schema::hasColumn('marketplace', 'logo')) {
                Schema::table('marketplace', function (Blueprint $table) {
                    $table->string('logo', 255)->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('marketplace')) {
            if (Schema::hasColumn('marketplace', 'logo')) {
                Schema::table('marketplace', function (Blueprint $table) {
                    $table->dropColumn('logo');
                });
            }
        }
    }
}
