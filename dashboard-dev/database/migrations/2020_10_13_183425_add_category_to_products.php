<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCategoryToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('products')) {
            if (!Schema::hasColumns('products', ['category'])) {
                Schema::table('products', function (Blueprint $table) {
                    $table->text('category')->nullable()->after('description');
                });
            }
            if (!Schema::hasColumns('products', ['category_url'])) {
                Schema::table('products', function (Blueprint $table) {
                    $table->text('category_url')->nullable()->after('category');
                });
            }
        } else {
            echo "No products table";
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
        if (Schema::hasTable('products')) {
            if (Schema::hasColumn('products', 'category')) {
                Schema::table('products', function (Blueprint $table) {
                    $table->dropColumn('category');
                });
            }
            if (Schema::hasColumn('products', 'category_url')) {
                Schema::table('products', function (Blueprint $table) {
                    $table->dropColumn('category_url');
                });
            }
        }
    }
}
