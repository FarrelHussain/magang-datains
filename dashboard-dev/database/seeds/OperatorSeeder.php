<?php

use App\Models\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OperatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hasher = app()->make('hash');

        $operator = [
            'name' => 'M Operator',
            'username' => 'operator-default',
            'email' => "operator@op.min",
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => $hasher->make('operator-default333'),
            'created_at' => date('Y-m-d H:i:s'),
        ];

        $roles = config('app.roles');
        $operator_role = $roles['operator'];
        $operator['role_code'] = $operator_role['role_code'];

        Role::firstOrCreate(
            ['role_code' => $operator_role['role_code']],
            ['role_name' => $operator_role['role_name']]
        );

        User::where('username', $operator['username'])->orWhere('email', $operator['email'])->delete();
        User::create($operator);
    }
}
