<?php

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class ReportedProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Schema::hasTable('reported_products') and Schema::hasTable('links')) {
            $links = DB::table('links')->get();
            if (sizeof($links) > 0) {
                foreach ($links as $key => $value) {
                    DB::table('reported_products')->insert([
                        'id_link' => $value->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                }
            } else {
                Log::critical('no data in links table');
            }
        } else {
            Log::critical('no reported_products table');
        }
    }
}
