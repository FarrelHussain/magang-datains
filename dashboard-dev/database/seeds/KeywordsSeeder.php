<?php


use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;


class KeywordsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Schema::hasTable('keywords')) {
            $faker = Factory::create('id_ID');
            for ($i = 0; $i < 10000; $i++) {
                DB::table('keywords')->insert([
                    'id_marketplace' => $faker->numberBetween(1, 7),
                    'keywords' => $faker->words(3, true),
                    'status' => $faker->numberBetween(0, 1),
                    'created_at' => $faker->dateTimeBetween('-3 years')
                ]);
            }
        } else {
            \Log::warning('no keywords table');
        }
    }
}
