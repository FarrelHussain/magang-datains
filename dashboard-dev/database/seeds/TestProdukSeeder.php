<?php

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class TestProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create('id_ID');

        if (Schema::hasTable('test_produk')) {
            for ($i = 0; $i < 10000; $i++) {
                DB::table('test_produk')->insert([
                    'tanggal' => $faker->dateTimeBetween('-3 years'),
                    'keyword' => $faker->words(3, true),
                    'produk' => $faker->words(3, true),
                    'dugaan' => $faker->words(5, true),
                    'melanggar' => $faker->words(4, true),
                    'toko' => $faker->company,
                    'pemilik' => $faker->name,
                    'harga' => $faker->numberBetween(10000, 100000),

                    'created_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
    }
}
