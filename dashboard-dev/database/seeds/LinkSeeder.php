<?php

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class LinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Schema::hasTable('links') and Schema::hasTable('products') and Schema::hasTable('status')) {
            $products = DB::table('products')->get();
            $faker = Factory::create();
            if (sizeof($products) > 0) {
                $status = DB::table('status')->get();
                if (sizeof($status) > 0) {
                    foreach ($products as $key => $value) {
                        DB::table('links')->insert([
                            'id_produk' => $value->id_product,
                            'id_status' => $status[$faker->numberBetween(0, sizeof($status) - 1)]->id_status,
                            'link' => $faker->url,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ]);
                    }
                } else {
                    Log::critical('no data in status table');
                }
            } else {
                Log::critical('no data in products table');
            }
        } else {
            Log::critical('check! do you have links, products and status stable?');
        }
    }
}
