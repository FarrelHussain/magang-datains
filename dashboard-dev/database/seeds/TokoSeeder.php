<?php

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class TokoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create('id_ID');

        if (Schema::hasTable('stores')) {
            for ($i = 0; $i < 1000; $i++) {
                DB::table('stores')->insert([
                    'id_store' => $i + 1,
                    'id_marketplace' =>  $faker->numberBetween(1, 7),
                    'url' => $faker->url,
                    'name' => $faker->name(),
                    'location' => $faker->city,
                    'description' => $faker->sentences(6, true),
                    'contact' => $faker->phoneNumber,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
            }
        }
    }
}
