<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = config('app.roles');

        foreach ($roles as $key => $value) {
            Role::updateOrCreate(
                ['role_code' => $value['role_code']],
                $value
            );
        }
    }
}
