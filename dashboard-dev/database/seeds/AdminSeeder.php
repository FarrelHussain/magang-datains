<?php

use App\Models\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hasher = app()->make('hash');
        $roles = config('app.roles');
        $super_admin = $roles['super_admin'];

        $admin = [
            'name' => "Admin",
            'email' => "admin@admin.min",
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => $hasher->make('admin'),
            'username' => 'admin',
            'created_at' => date('Y-m-d H:i:s'),
            'role_code' => $super_admin['role_code']
        ];

        Role::firstOrCreate(
            ['role_code' => $super_admin['role_code']],
            ['role_name' => $super_admin['role_name']]
        );

        User::updateOrCreate(
            ['username' => $admin['username'], 'email' => $admin['email']],
            $admin
        );
    }
}
