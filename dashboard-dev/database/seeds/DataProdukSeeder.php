<?php

use App\Models\Product;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DataProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kondisi = ['baru', 'bekas'];

        $faker = Factory::create();
        if (Schema::hasTable('data_product')) {
            $products = Product::all();
            if (sizeof($products) > 0) {
                foreach ($products as $key => $value) {
                    DB::table('data_product')->insert([
                        'id_product' => $value->id_product,
                        'crawled' => $faker->dateTimeBetween('-3 years', 'now'),
                        'rating'  => $faker->numberBetween(1, 5),
                        'review_count'  => $faker->numberBetween(1, 50),
                        'discount' => $faker->numberBetween(10, 87),
                        'price' => $faker->numberBetween(10000, 170000),
                        'item_condition' => $kondisi[$faker->numberBetween(0, 1)],
                        'sold_count' => $faker->numberBetween(100, 1000),
                        'view_count' => $faker->numberBetween(100, 10000),
                        'processing_time' => $faker->numberBetween(1, 5),
                        'favorited_count' => $faker->numberBetween(10, 1000),
                        'item_count' => $faker->numberBetween(100, 10000),
                        'keywords' => "Keywords " . $faker->numberBetween(1, 30),
                        'created_at' => $faker->dateTimeBetween('-3 years')
                    ]);
                }
            } else {
                echo "No data product";
            }
        } else {
            echo "Table data_produk doesn't exist!";
        }
    }
}
