<?php

use App\Models\commodity;
use Illuminate\Database\Seeder;

class CommoditiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $commodities = config('app.commodities');
        foreach ($commodities as $key => $value) {
            commodity::UpdateOrCreate(
                ['id_commodity' => $value['id_commodity']],
                $value
            );
        }
    }
}
