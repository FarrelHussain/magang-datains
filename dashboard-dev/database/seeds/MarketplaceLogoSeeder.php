<?php

use App\Models\Marketplace;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class MarketplaceLogoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $logos = [
            // 'blanja' => 'blanja.png',
            'blibli' => 'blibli.png',
            'bukalapak' => 'bukalapak.png',
            'elevenia' => 'elevenia.png',
            'jdid' => 'jdid.png',
            'lazada' => 'lazada.png',
            'olx' => 'olx.png',
            'shopee' => 'shopee.png',
            'tokopedia' => 'tokopedia.png',
        ];
        $marketplaces = Marketplace::OnlyMarketplace()->get();

        foreach ($marketplaces as $mp) {
            if (isset($logos[$mp->name])) {
                $mp->logo = $logos[$mp->name];
                $mp->save();
                $this->command->info("berhasil mengupdate logo. {$mp->name} => {$logos[$mp->name]}");
            } else {
                $this->command->error("nama marketplace tidak ada di dalam daftar logo: {$mp->name}");
            }
        }
    }
}
