<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesSeeder::class,
            AdminSeeder::class,
            OperatorSeeder::class,
            OffensesSeeder::class,
            StatusSeeder::class,
            CommoditiesSeeder::class,
            MarketplaceSeeder::class,
            CrawlingTypeSeeder::class,
            UptSeeder::class,
            MarketplaceLogoSeeder::class,
        ]);
    }
}
