<?php

use App\Models\CrawlingType;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CrawlingTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = config('app.crawling_type');
        if (Schema::hasTable('crawling_type')) {
            foreach ($status as $key => $value) {
                CrawlingType::updateOrCreate(
                    ['id_crawling_type' => $value['id_crawling_type']],
                    $value
                );
            }
        }
    }
}
