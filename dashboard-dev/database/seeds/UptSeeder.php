<?php

use App\Models\Upt;
use Illuminate\Database\Seeder;

class UptSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $upts = config('app.upt');
        foreach ($upts as $key => $value) {
            upt::UpdateOrCreate(
                $value
            );
        }
    }
}
