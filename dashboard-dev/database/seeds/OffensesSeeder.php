<?php

use App\Models\Offense;
use Illuminate\Database\Seeder;

class OffensesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $offenses = config('app.offenses');
        foreach ($offenses as $key => $value) {
            Offense::updateOrCreate(
                ['id_offense' => $value['id_offense']],
                $value
            );
        }
    }
}
