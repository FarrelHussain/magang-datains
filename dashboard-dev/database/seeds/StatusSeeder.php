<?php

use App\Models\Status;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = config('app.status');
        if (Schema::hasTable('status')) {
            foreach ($status as $key => $value) {
                Status::updateOrCreate(
                    ['id_status' => $value['id_status']],
                    $value
                );
            }
        }
    }
}
