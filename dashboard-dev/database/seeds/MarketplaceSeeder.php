<?php

use App\Models\Marketplace;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class MarketplaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Schema::hasTable('marketplace')) {

            $marketplaces = config('app.marketplaces');

            foreach ($marketplaces as $key => $value) {
                Marketplace::firstOrCreate(
                    ['id_marketplace' => $value['id_marketplace']],
                    $value
                );
            }
        }
    }
}
