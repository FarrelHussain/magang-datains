<?php

use App\Models\Keyword;
use App\Models\Store;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create('id_ID');

        if (Schema::hasTable('products')) {
            $keywords = Keyword::all();
            $stores = Store::all();
            if (sizeof($keywords) > 0 and sizeof($stores) > 0) {
                for ($i = 0; $i < 10000; $i++) {
                    DB::table('products')->insert([
                        'id_product' => $i + 1,
                        'id_store' => $faker->numberBetween(1, sizeof($stores)),
                        'id_keywords' => $faker->numberBetween(1, sizeof($keywords)),
                        'name' => $faker->words(3, true),
                        'description' => $faker->words(10, true),
                        'note' => $faker->words(10, true),
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }
        }
    }
}
