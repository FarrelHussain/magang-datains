<?php

use App\Models\Marketplace;
use Illuminate\Database\Seeder;

class LogoMarketplaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Schema::hasTable('marketplace')) {
            $marketplaces = Marketplace::all();

            foreach ($marketplaces as $key => $value) {
                Marketplace::where('id_marketplace', $value->id_marketplace)->update(['logo' => strtolower(str_replace(' ', '', $value->name)) . '.png']);
            }
        }
    }
}
