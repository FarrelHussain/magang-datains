<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logged_in' => 'Pengguna masuk',
    'logged_out' => 'Pengguna keluar',
    'created' => ':user menambahkan item baru :id',
    'updated' => ':user mengubah item :id',
    'deleted' => ':user menghapus item :id',
    'navigate' => 'menuju ke halaman :name',

    'export' => 'export :task',
    'activate' => 'mengaktifkan kata kunci :keyword',
    'deactivate' => 'menonaktifkan kata kunci :keyword',
    'crawl' => 'menambahkan :keyword ke dalam list crawling'
];
