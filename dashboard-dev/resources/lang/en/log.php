<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logged_in' => 'user logged in',
    'logged_out' => 'user logged out',
    'created' => ':user added new item :id',
    'updated' => ':user updated item :id',
    'deleted' => ':user removed item :id',
    'navigate' => 'go to page :name',

    'export' => 'export :task',
    'activate' => 'activated keyword :keyword',
    'deactivate' => 'deactivated keyword :keyword',
    'crawl' => 'added :keyword to crawl queue'

];
