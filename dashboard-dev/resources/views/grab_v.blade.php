@extends('layouts.layout')

@section('title')
    Grab
@endsection

@section('pageTitle')
    Grab
@endsection

@section('js')
    <script>
        const offenses = {!! $offenses !!};
        const crawling_type = {!! $crawling_type !!};
    </script>
    <script src="{{pageJS('grab.js')}}"></script>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h3>Grab</h3>
            <hr>
        </div> 
        <div class="col-12">
            <div class="card">
                
				<form class="row" action="{{url('grab')}}" method="post" id="form-grab" style="display: none" onsubmit="return false;">
            @csrf
            <input type="hidden" name="id_crawling_type">
            <input type="hidden" name="id_offense">
            <input type="hidden" name="datetk">
            <input type="hidden" name="note">
            <input type="hidden" name="id_report">
        </form>
        <div class="row">
            <div class="col-12 col-md-12 col-sm-12 mb-3">
                <div class="row justify-content-between items-center">
                    <div class="col-md-12 mb-3 btn-store">
                        <button data-marketplace='all' class="btn btn-primary mx-1 mt-3 active btn-filter-marketplace">Semua</button>
                        @foreach($product_count as $count)
                            <button data-marketplace='{{ $count->name }}' class="btn btn-primary mx-1 mt-3 btn-filter-marketplace">{{ strtoupper($count->name) }} ({{ $count->total }})</button>
                        @endforeach
                    </div>
                    <div class="col-md-12">
                        <div class="d-flex">
                            <a href="###" onclick="return false" class="btn btn-default btn-refresh mr-2 mt-2"><i class="fas fa-spinner"></i> Refresh</a>
                            <a href="###" data-toggle="modal" data-target="#home-filter-modal" class="btn btn-default mr-2 mt-2 btn-filter"><i class="fas fa-filter"></i> Filter Data</a>
                            <button class="btn btn-danger text-light mr-2 mt-2 takedown-tllapangan" data-btn="drafttakedown" id="btn-takedown">Takedown(0)</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive"> 
                            <table class="table table-bordered table-striped datatable">
                                <thead class="bg-primary">
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="5%">Tanggal</th>
                                        <th width="5%">Keyword</th>
                                        <th width="5%">Marketplace</th>
                                        <th width="40%">Produk</th>
                                        <th width="5%">Komoditas</th>
                                        <th width="20%">Lokasi</th>
                                        <th width="5%">Total Terjual</th>
                                        <th width="5%">Harga</th>
                                        <th width="5%">#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td id="loading" colspan="9" class="text-center">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    @include('forms.form_advance_search') 
    @include('utilities.detailModal')
    @include('utilities.filterHomeModal')
				
            </div>
        </div>
    </div>
</div>
@endsection
