@extends('layouts.layout')

@section('title')
    Arsip Products
@endsection

@section('pageTitle')
    Arsip Products
@endsection

@section('js')
    <script src="{{pageJS('arsip-product.js')}}"></script>
@endsection

@section('content')
 <div class="row">
       <div class="col-md-12">
           <h3>Arsip Products List</h3>
           <hr>
       </div>
       <div class="col-md-12 ml-2">
           <div class="row">
                <div class="col-md-1 mt-2 mr-5">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <i class="fas fa-download"></i> Export Data
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" id="pilih" onclick="return false" href="">Pilih (0)</a>
                                <a class="dropdown-item" target="__blank" data-count="{{$arsipCount}}" id="btn-export-takedown-data" href="{{$linkExport}}">Arsip Products({{$arsipCount}})</a>
                                <a class="dropdown-item" id="custom-export" onclick="return false" href="">Custom Export</a>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
       </div>
       <div class="col-md-12 mt-3">
           <div class="card">
               <div class="card-body">
                   <table class="table table-striped datatable">
                        <thead class="bg-primary">
                            <tr>
                                <th width="5%">No.</th>
                                <th width="15%">Komoditas</th>
                                <th>Product</th>
                                <th width="7%">Harga</th>
                                <th width="16%">Toko</th>
                                <th width="5%">Marketplace</th>
                                <th width="10%">Status</th>
                                <th width="7%">PLGN</th>
                                <th width="5%">#</th>
                                <th class="d-none"></th>
                            </tr>
                        </thead>
                        <tbody id="product-list">
                            <tr>
                                <td id="loading" colspan="9" class="text-center">
                                </td>
                            </tr>
                        </tbody>
                    </table>
               </div>
           </div>
       </div>
 </div>
  @include('forms.form_advance_search')
  @include('forms.form_export_modal')
  @include('utilities.detailModal')
  @include('utilities.filterHomeModal')
@endsection
