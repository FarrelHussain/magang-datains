@extends('layouts.layout')

@section('title')
    Crawling
@endsection

@section('pageTitle')
    Crawling
@endsection

@section('head-script')
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    <script src="{{asset('plugins/select2/select2.full.min.js')}}"></script>
@endsection
 
@section('js')
    <script src="{{pageJS('crawling.js')}}"></script>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h3>Crawling</h3>
            <hr>
        </div>
        <div class="col-12">

            <div class="card">
                <form action="{{url('crawling')}} " method="post" id="formCrawl">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group clearfix">
                                    <div class="form-group">
                                        <label for="">Pilih Crawling</label>
                                    </div>
                                    <div class="icheck-primary d-inline mr-3">
                                        <input type="radio" value="keywords" id="radioPrimary1" name="crawl_by" checked>
                                        <label for="radioPrimary1" class="font-weight-normal">
                                            Crawl by keywords
                                        </label>
                                    </div>
                                    <div class="icheck-primary d-inline">
                                        <input type="radio" value="stores" id="radioPrimary2" name="crawl_by">
                                        <label for="radioPrimary2" class="font-weight-normal">
                                            Crawl by store
                                        </label>
                                        <a href="()" data-toggle="modal" data-target="#modalNote"
                                            title="Petunjuk crawling by store" onclick="return false"><i
                                                class="fas fa-question-circle text-primary"></i></a>
                                    </div>
                                </div>
                                <div class="form-group query-field">

                                    <input type="text" name="query" value="{{old('query')}}" class="form-control "
                                        placeholder="Masukan keyword yang akan dicari.. Contoh: cytotec, citotek, citotec, sitotek" required>
                                    {{-- <select name="query" class="form-control select2"
                                        style="width: 100%;"></select> --}}
                                </div>

                                <div class="form-group">
                                    <label for="">Asal Produk</label>
                                    <input type="text" name="product_origin" placeholder="Masukan asal produk"
                                        value="{{old('product_origin')}}" required class="form-control">
                                </div>

                                <div class="form-group">
                                    <label for="">Jenis Crawling</label>
                                    <select name="crawling_type" id="select-crawling" class="form-control" required>
                                        <option class="text-muted" value="" selected disabled><span
                                                class="text-muted">Pilih Jenis Crawling</span></option>
                                        <?php
                                            $crawling_type = \DB::table('crawling_type')
                                            ->select('*')
                                            ->get();
                                        ?>
                                        @foreach ($crawling_type as $cwr)
                                        <option value="{{$cwr->id_crawling_type}}">{{$cwr->type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Komoditas</label>
                                    <select name="commodity" id="select-commodity" class="form-control" required>
                                        <option class="text-muted" value="" selected disabled><span
                                                class="text-muted">Pilih Komoditas</span></option>
                                        @foreach ($commodities as $item)
                                        <option value="{{$item->id_commodity}}">{{$item->commodity}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group" id="content" style="display: none">
                                    <label for="">Kandungan</label>
                                    <input type="text" name="content" id="content_input"
                                        class="form-control @error('content') is-invalid @enderror">
                                    @error('content')
                                    {{$message}}
                                    @enderror
                                </div>

                                <div class="form-group" id="therapy_class" style="display: none">
                                    <label for="">Kelas Terapi</label>
                                    <input type="text" name="therapy_class" id="therapy_class_input"
                                        class="form-control @error('therapy_class') is-invalid @enderror">
                                    @error('therapy_class')
                                    {{$message}}
                                    @enderror
                                </div>

                                <div class="form-group" id="group" style="display: none">
                                    <label for="">Golongan</label>
                                    <input type="text" name="group" id="group_input"
                                        class="form-control @error('group') is-invalid @enderror">
                                    @error('group')
                                    {{$message}}
                                    @enderror

                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="">Jumlah Produk</label>
                                    <input type="number" name="quantity" value="{{old('quantity')}}"
                                        class="form-control" placeholder="Kosongkan untuk crawling tak terbatas">
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="" class="form-label mb-2">Marketplace</label>
                                            <div class="form-group">
                                                @foreach ($marketplaces as $key => $marketplace)
                                                <div class="custom-control custom-checkbox">
                                                    <input class="custom-control-input" type="checkbox"
                                                        name="marketplace[]" id="customCheckbox{{$key}}"
                                                        value="{{$marketplace->name}}" @if(is_array(old('marketplace'))
                                                        && in_array("{{$marketplace->name}}",
                                                    old('marketplace')))
                                                    checked @endif>
                                                    <label for="customCheckbox{{$key}}"
                                                        class="custom-control-label">{{ucwords($marketplace->name)}}</label>
                                                </div>
                                                @endforeach
                                                @error('marketplace')
                                                {{$message}}
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-8">
                                        <div class="form-group">
                                            <label for="" class="form-label mb-2">Location</label>
                                            <div class="row">

                                                <div class="col-6">
                                                    <div class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"
                                                            name="locations[]" id="locationCheckbox[indonesia]"
                                                            value="*">
                                                        <label for="locationCheckbox[indonesia]"
                                                            class="custom-control-label">Indonesia</label>
                                                    </div>
                                                </div>
                                                
                                                @foreach($upt_location as $key => $a)
                                                <div class="col-6">
                                                    <div class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox"
                                                            name="locations[]" id="locationCheckbox{{$key}}"
                                                            value="{{$a}}" @if(is_array(old('locations')) &&
                                                            in_array("{{$a}}", old('locations'))) checked @endif>
                                                        <label for="locationCheckbox{{$key}}"
                                                            class="custom-control-label">{{$a}}</label>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @error('locations')
                                                {{$message}}
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-block btn-primary"> CRAWL </button>
                    </div>
                    {{-- <div class="card-footer bg-white">
                    </div> --}}
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalNote" tabindex="-1" role="dialog" aria-labelledby="modalNoteTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalNoteTitle">CRAWLER BY STORE</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-wrap">
        <div class="row">
            <div class="col-12 text-wrap">
                <h4>TOKOPEDIA</h4>
                <p class="text-wrap">Contoh: <br>
                    https://www.tokopedia.com/<b>orico</b><br> Yang diambil: <b>orico</b></p>
            </div>
            <div class="col-12 text-wrap">
                <h4>SHOPEE</h4>
                <p class="text-wrap">Contoh: <br>
                    https://shopee.co.id/<b>grosirmurah_kudus</b><br> Yang diambil: <b>grosirmurah_kudus</b></p>
            </div>
            <div class="col-12 text-wrap">
                <h4>BUKALAPAK</h4>
                <p class="text-wrap">Contoh: <br>
                 https://www.bukalapak.com/u/<b>faizal_azhari</b><br> Yang diambil: <b>faizal_azhari</b></p>
            </div>
            <div class="col-12 text-wrap">
                <h4>JDID</h4>
                <p class="text-wrap">Contoh: <br>
                   https://www.jd.id/shop/<b>33398</b>.html<br> Yang diambil: <b>33398</b></p>
            </div>
            <!-- <div class="col-12">
                <h4>LAZADA</h4>
                <p class="text-wrap">Contoh: <br>
                https://www.blanja.com/store/<b>vouchercenter</b> <br> Yang diambil: <b>vouchercenter</b></p>
            </div> -->
            <div class="col-12 text-wrap">
                <h4>BLIBLI</h4>
                <p class="text-wrap">Contoh: <br>
                 https://www.blibli.com/merchant/syaf/<b>SYF-60029</b>?page=1&start=0&pickupPointCode=&cnc=&multiCategory=true&excludeProductList=true&sort=7 <br> Yang diambil: <b>SYF-60029</b></p>
            </div>
            <div class="col-12 text-wrap">
                <h4>OLX</h4>
                <p class="text-wrap">Contoh: <br>
                   https://www.olx.co.id/profile/<b>59956699</b><br> Yang diambil: <b>59956699</b></p>
            </div>
            <div class="col-12">
                <h4>BLANJA</h4>
                <p class="text-wrap">Contoh: <br>
                https://www.blanja.com/store/<b>vouchercenter</b><br> Yang diambil: <b>vouchercenter</b></p>
            </div>
            <div class="col-12 text-wrap">
                <h4>ELEVENIA</h4>
                <p class="text-wrap">Contoh: <br>
                https://www.elevenia.co.id/store/<b>csshopsby</b><br> Yang diambil: <b>csshopsby</b></p>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    $(() => {
        $(`input[name=crawl_by]`).on('change', e => {
            let val = $(e.currentTarget).val();
            $('.query-field').empty();
            if (val == 'stores') {
                $('#keyowrd-label').html('Nama Toko');
                $(`input[name=query]`).attr('placeholder', "Masukan nama toko yang akan di-crawling");
                $('#buttonNote').removeClass('d-none');
                $('.query-field').append(`
                    <select name="query" class="form-control select2" style="width: 100%;"></select>
                `)
                let select = $('.select2').select2({
                    selectionCssClass: 'form-control',
                    theme: 'bootstrap4',
                    placeholder: 'Cari nama toko',
                    minimumInputLength: 3,
                    tags: true,
                    ajax: {
                        cache: true,
                        type: 'POST',
                        url: `{{route('search-store')}}`,
                        data: (params) => {
                            let query = {
                                search: params.term,
                                _token: $('meta[name=csrf-token]').attr('content'),
                            }
                            return query;
                        },
                        processResults: (response) => {
                            return {
                                results: response.data,
                            }
                        },
                    }
                });
                $('.select2').on('change', e => {
                    let val = $(`option[value='${$(e.currentTarget).val()}']`).html();
                    val = typeof val.split("|")[1] != 'undefined' ? val.split("|")[1] : "  ";
                    let marketplaces = JSON.parse(`<?php echo json_encode(collect(config('app.marketplaces'))->pluck('name')->toArray()) ?>`).slice();

                    if (marketplaces.indexOf(val.trim()) > -1) {
                        $(`input[name="marketplace[]"]`).not(`input[value=${val}]`).attr('disabled', 'disabled').attr('checked', null);
                        $(`input[value=${val}]`).attr('checked', true).attr('disabled', null);
                    }else{
                        $(`input[name="marketplace[]"]`).attr('disabled', null).attr('checked', false);
                    }
                })

            }else{
                $('#keyowrd-label').html('Keywords');
                $(`input[name="marketplace[]"]`).attr('disabled', null).attr('checked', null);
                $(`input[name=query]`).attr('placeholder', "Masukan keywords yang akan di-crawling");
                $('#buttonNote').addClass('d-none');
                $('.query-field').append(`
                    <input type="text" name="query" value="{{old('query')}}" class="form-control" placeholder="Masukan keyword yang akan dicari.. Contoh: cytotec, citotek, citotec, sitotek" required>
                `)
            }
        });
    })
</script>
@endsection
