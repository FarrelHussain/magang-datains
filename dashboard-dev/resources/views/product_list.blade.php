@extends('layouts.layout')

@section('title')
    {{$store->name ? $store->name : "noname"}}
@endsection

@section('js')
    <script src="{{asset('js/pages/product-list.js')}}"></script>
@endsection

@section('content')
    <div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h3>Store Detail</h3>
            <hr>
        </div>
            <div class="col-12 mt-2">
			<div class="row">
				<div class="col-sm-4">
				<div class="card">
						<div class="card-body">
						<?php
                $roles = \DB::table('stores')
                ->select('*')
                ->where('id_store','=',$_GET['store'])
                ->get();
                ?>
				@foreach ($roles as $name)
				<div class="col-12">
					<strong><h4><a href='{{$name->url}}'>{{$store->name ? $store->name : "noname"}}</a></h4></strong>
					<hr>
				</div>
                
                    {{$name->description}}
                    <br/><br/>
                    <p class="font-weight-lighter">Lokasi <br/><strong>{{$name->location}}</strong></p>
                    <p class="font-weight-lighter">Followers <br/><strong>{{$name->followers}}</strong></p>
                    <p class="font-weight-lighter">Marketplace <br/><strong>
                    <?php
                    $mp = \DB::table('marketplace')
                    ->select('name')
                    ->where('id_marketplace','=',$name->id_marketplace)
                    ->get();
                    ?>                
                    @foreach ($mp as $nm)
                        {{$nm->name}}
                    @endforeach
                    </strong></p>
                @endforeach
					</div>
					</div>
				</div>
				<div class="col-sm-8">
						<div class="card">
						<div class="card-body">
                        <table class="table table-striped datatable">
                            <thead class="bg-primary">
                                <tr>
                                    <td>Produk</td>
                                    <td>Harga</td>
                                    <td>Stok</td>
                                    <td>Komoditas</td>
                                    <td>keyword</td>
                                    <td>Status</td>
                                </tr>
                            </thead>
                        </table>
						</div>
						</div>
				</div>
			</div>
            </div>
    </div>
    </div>
@endsection
