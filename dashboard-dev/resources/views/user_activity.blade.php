@extends('layouts.layout')

@section('title')
    User Activities
@endsection

@section('pageTitle')
    User Activities
@endsection

@section('js')
    <script src="{{pageJS('user-activity.js')}}"></script>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="col-12">
            <h3>User Activities</h3>
            <hr> 
        </div>
        
        <div class="card">
            <div class="card-body">
                <table class="table table-striped datatable">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Username</th>
                            <th>Description</th>
                            <th>Ip Address</th>
                            <th>Timestamp</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
