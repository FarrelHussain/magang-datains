<div class="modal fade" id="detail-modal">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Informasi Produk</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
              <div class="col-6">
                <table class="table table-striped" width="100%">
                    <tbody>
                         <tr>
                            <td>Marketplace</td>
                            <td>:</td>
                            <td id="marketplace_name"></td>
                        </tr>
                        <tr>
                            <td>Crawled</td>
                            <td>:</td>
                            <td id="crawled-at">Get Up Stand Up | Playing For Cdange | Song Around The World</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td id="status">Get Up Stand Up | Playing For Change | Song Around Tde World</td>
                        </tr>
                        <tr>
                            <th colspan="3">TOKO</th>
                        </tr>
                        <tr>
                            <td>Toko</td>
                            <td>:</td>
                            <td id="store-name">Toko Lancar Maju Jalan</td>
                        </tr>
                        <tr>
                            <td>Link</td>
                            <td>:</td>
                            <td id="store-link">https://www.youtube.com/watch?v=d6szT5NnwTY</td>
                        </tr>
                        <tr>
                            <td>Toko Desc</td>
                            <td>:</td>
                            <td id="store-desc">https://www.youtube.com/watch?v=d6szT5NnwTY</td>
                        </tr>
                    </tbody>
                </table>
              </div>
              <div class="col-6">
                  <table class="table table-striped table-responsive">
                        <tr>
                            <th colspan="3">PRODUK</th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div style="max-width: 250px">
                                    <img 
                                        id="product-picture" 
                                        class="d-block" 
                                        width="250"
                                        height="250"
                                        src="{{asset('img/noimage.png')}}" 
                                        onerror="this.onerror=null;this.src='{{url('img/noimage.png')}}';" alt="Photo">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td id="product-name">Get Up Stand Up | Playing For Change | Song Around The World</td>
                        </tr>
                        <tr>
                            <td>Harga</td>
                            <td>:</td>
                            <td id="product-price">Get Up Stand Up | Playing For Change | Song Around The World</td>
                        </tr>
                        <tr>
                            <td>Terjual</td>
                            <td>:</td>
                            <td id="product-sold-count">Get Up Stand Up | Playing For Change | Song Around The World</td>
                        </tr>
                        <tr>
                            <td>Stok</td>
                            <td>:</td>
                            <td id="product-stock">Get Up Stand Up | Playing For Change | Song Around The World</td>
                        </tr>
                        <tr>
                            <td>Rating</td>
                            <td>:</td>
                            <td id="product-rating">Get Up Stand Up | Playing For Change | Song Around The World</td>
                        </tr>
                        <tr>
                            <td>Link</td>
                            <td>:</td>
                            <td id="product-link"></td>
                        </tr>
                        <tr>
                            <td>Deskripsi</td>
                            <td>:</td>
                            <td id="product-desc">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Totam dolorem soluta recusandae commodi vero corrupti eligendi, perspiciatis adipisci aliquid dolore consectetur autem eius aperiam vel a corporis, distinctio quisquam impedit! </td>
                        </tr>
                        <tr>
                            <td>Screenshot</td>
                            <td>:</td>
                            <td id="product-desc">-</td>
                        </tr>
                  </table>
              </div>
          </div>
        </div>
        {{-- <div class="modal-footer float-right">
          <button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
          <button type="button" class="btn btn-primary">KEPASTIAN PELANGGAN</button>
        </div> --}}
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
      <!-- /.modal -->
