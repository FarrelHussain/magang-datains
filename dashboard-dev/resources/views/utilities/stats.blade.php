@php
    $bg_color = 'bg-info';

    switch ($status) {
        case 'info':
            $bg_color = 'bg-info';
            break;
        case 'success':
            $bg_color = 'bg-success';
            break;
        case 'warning':
            $bg_color = 'bg-warning';
            break;
        case 'danger':
            $bg_color = 'bg-danger';
            break;
    }
@endphp
<div class="small-box {{ $bg_color }}">
    <div class="inner">
    <h3>{{ $value }}</h3>

    <p>{{ $message }}</p>
    </div>
    <div class="icon">
        <i class="ion ion-bag"></i>
    </div>
    <!-- <a href="{{ $url }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
</div>