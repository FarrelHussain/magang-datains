@if (isset($message))
    <script>
        $(() => {
            showInfo("{{isset($alert) ? $alert : "info"}}",  "{{$message}}");
        });
    </script>
@endif

@if (session('message'))
    <script>
        $(() => {
            showInfo("{{session('alert') !== null ? session('alert') : "info"}}",  "{{session('message')}}");
        });
    </script>
@endif

