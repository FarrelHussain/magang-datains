
        {{-- CSS --}}
        <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
        <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
        <link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">

        <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">
        <link rel="stylesheet" href="{{asset('plugins/pace-progress/themes/blue/pace-theme-center-atom.css')}}">
        <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('plugins/jquery-datatables-checkboxes/css/dataTables.checkboxes.css')}}">
        
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
        <!-- <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css"> -->
        <!-- <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.3/css/select.bootstrap4.min.css"> -->
        
        <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        {{-- END OF CSS --}}

        <script>
            window.paceOptions = {
                document: false
            }
        </script>

        {{-- JAVASCRIPT --}}
        <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('plugins/jquery-validation/jquery.validate.min.js')}}"></script>
        <script src="{{asset('plugins/jquery-validation/additional-methods.min.js')}}"></script>
        <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
        <script data-pace-options='{ "startOnPageLoad": false, "ajax": false }' src="{{asset('plugins/pace-progress/pace.min.js')}}"></script>
        <script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
        <script src="{{asset('plugins/toastr/toastr.min.js')}}"></script>
        <script src="{{asset('plugins/moment/moment.min.js')}}"></script>
        <script src="{{asset('plugins/jquery-datatables-checkboxes/js/dataTables.checkboxes.min.js')}}"></script>

        <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.bootstrap4.min.js"></script>
        <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
        <script src="https://cdn.jsdelivr.net/g/mark.js(jquery.mark.min.js),datatables.mark.js"></script>
        <script src="/vendor/datatables/buttons.server-side.js"></script>

        <script src="{{asset('plugins/highcharts/highcharts.js')}}"></script>
        <script src="{{asset('js/adminlte.min.js')}}"></script>
        <script src="{{asset('js/global.js')}}"></script>
        {{-- END OF JAVASCRIPT --}}
