<div class="modal fade" id="modal-import-takedown" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="{{route('import-takedown-data')}}" method="post" id="import-modal-form" enctype="multipart/form-data">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Impor File Excel</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
                @csrf
                <div class="form-group">
                    <label for="file">File Excel</label>
                    <input type="file" name="takedown_file" id="file" accept=".xls, .xlsx" class="form-control pb-3">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Import</button>
            </div>
        </div>
    </form>
  </div>
</div>

<script>
    $(() => {

        $('form#import-modal-form').submit(e => {
            $('#modal-import-takedown').modal('hide');
            $('#overlayLoading').css('display', 'block');
        });

    })
</script>
