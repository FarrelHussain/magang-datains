<div class="modal fade" id="home-filter-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="home-filter-form" action="" method="post">
                <div class="modal-header">
                    <h4 class="modal-title">Filter Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div id="periode-filter" class="col-12 row d-none">
                            <div class="col-12 mb-3">
                                <span class="mb-2" for="">Filter Berdasarkan Periode</span class="mb-1">
                            </div>
                            <div class="col-6 mb-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                        <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="month" name="filter_periode" class="form-control float-right ">
                                </div>
                            </div>
                        </div>
                        <div id="date-filter" class="col-12 row">
                            <div class="col-12 mb-3">
                                <span class="mb-2" for="">Filter Berdasarkan rentang tanggal</span class="mb-1">
                            </div>
                            <div class="col-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                        <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="date" name="start_time" placeholder="dd/mm/yyyy" class="form-control float-right ">
                                </div>
                            </div>
                            <div class="col-6 mb-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                        <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="date" name="end_time" placeholder="dd/mm/yyyy" class="form-control float-right" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row col-12" id="reported-product-status">
                            <div class="col-12 mb-4">
                                <span class="mb-2" for="">Filter Berdasarkan Status Link</span class="mb-1">
                            </div>
                            <div class="col-12 mt-n3">
                                <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline mr-3">
                                        <input type="radio" id="radioPrimary1" name="reported_product_status" value="all" checked>
                                        <label for="radioPrimary1">
                                            Semua
                                        </label>
                                    </div>
                                    <div class="icheck-primary d-inline mr-3">
                                        <input type="radio" id="radioPrimary2" name="reported_product_status" value="active">
                                        <label for="radioPrimary2">
                                            Aktif
                                        </label>
                                    </div>
                                    <div class="icheck-primary d-inline mr-3">
                                        <input type="radio" id="radioPrimary3" name="reported_product_status" value="inactive">
                                        <label for="radioPrimary3">
                                            Tidak Aktif
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="commodities-filter" class="col-12 row">
                            <div class="col-12 mb-4">
                                <span class="mb-2" for="">Filter Berdasarkan komoditas</span class="mb-1">
                            </div>
                            <div class="col-12 row">
                                @forelse ($commodities as $item)
                                    <div class="col-6 mt-n3">
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input commodities" name="commodities[]" type="checkbox" id="{{$item->id_commodity}}" value="{{$item->id_commodity}}">
                                                <label for="{{$item->id_commodity}}" class="custom-control-label text-weight-normal">{{$item->commodity}}</label>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                @endforelse
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-row justify-content-end">
                        <input type="reset" class="btn btn-default mr-3" id="reset-filter" value="RESET" />
                        <button type="button" class="btn btn-primary" id="btn-terapkan">TERAPKAN</button>
                    </div>
                </div>
            </form>
        </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
      <!-- /.modal -->
<script defer>
    window.addEventListener('DOMContentLoaded', function() {
        $('input[name=start_time]').change((e) => {
            let start = $(e.currentTarget).val();
            $('input[name=end_time]').removeAttr('disabled');
            $('input[name=end_time]').attr('min', start);
        });
        $('input[type=date]').attr('max', () => {
            let date = new Date();
            date.setDate(date.getDate() + 1);
            date = date.toISOString().substring(0, 10);
            return date;
        });
    })
    
</script>
