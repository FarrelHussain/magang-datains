@if(isset($show))
    <a href='{{ $show["url"] }}' class="btn btn-xs btn-info">
        <button class='activate btn btn-xs btn-info'><i class='text-xs'>{{ isset($show['text']) ? $show['text'] : 'Detail' }}</i>
        </button>
    </a>
@endif