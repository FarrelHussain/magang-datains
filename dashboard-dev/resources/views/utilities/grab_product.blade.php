<a 
    href='{{ route("grab.show", $link_id) }}' 
    title='{{ $title }}' 
    data-toggle='tooltip' 
    class='product-list d-flex align-items-center'
>
    <div style="width: 70px;" class="mr-3">
        <img src='{{ $picture }}' width="70" height="70" class="d-block" />
    </div>
    {{ substr($title, 0, 100) }}
</a>