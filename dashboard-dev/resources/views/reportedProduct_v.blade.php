@extends('layouts.layout')

@section('title')
    Reported Products
@endsection

@section('pageTitle')
    Reported Products
@endsection

@section('js')
    <script src="{{pageJS('reported-product.js')}}"></script>
@endsection

@section('content')
   <div class="row">
       <div class="col-12">
           <h3>Reported Products List</h3>
           <hr>
       </div>
       <div class="col-md-1">
           <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                        <i class="fas fa-download"></i> Export Data
                    </button>
                    <div class="dropdown-menu">
                        @if (sizeof($export) > 0)
                            @foreach ($export as $item)
                                @if ($item->id == 1)
                                    <a class="dropdown-item" id="pilih" href="###">Pilih (0)</a>
                                    <div class="dropdown-divider"></div>
                                    @php continue; @endphp
                                @endif
                                <a class="dropdown-item" target="__blank" href="{{$item->link}}">{{$item->label}}</a>
                            @endforeach
                        @else
                            <a class="dropdown-item disabled text-muted" aria-disabled="true" id="xxx" href="##">No lin can be executed</a>
                        @endif
                    </div>
                </div>
            </div>
       </div>
       <div class="col-md-2 ml-5">
           <button type="button" class="btn btn-default" data-toggle="modal" data-target="#home-filter-modal">
                <i class="fas fa-filter"></i> Filter Data
            </button>
       </div>
       <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered table-striped datatable">
                        <thead class="bg-primary">
                            <tr>
                                <th width="5%">No.</th>
                                <th width="15%">Komoditas</th>
                                <th>Product</th>
                                <th width="7%">Price</th>
                                <th width="16%">Store</th>
                                <th width="5%">Marketplace</th>
                                <th width="10%">Status</th>
                                <th width="5%">#</th>
                            </tr>
                        </thead>
                        <tbody id="product-list">
                            <tr>
                                <td id="loading" colspan="9" class="text-center">

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
       </div>
    </div>
    @include('utilities.detailModal')
    @include('utilities.filterHomeModal')
@endsection
