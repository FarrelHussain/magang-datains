@extends('layouts.layout')

@section('title')
    User Management
@endsection

@section('pageTitle')
    User Management
@endsection

@section('js')
    <script src="{{pageJS('user-management.js')}}"></script>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="col-12">
            <h3>User Management</h3>
            <hr> 
        </div>
        <div class="col-12 mb-3">
            <a href="{{url('user-management/create')}}"><button class="btn btn-success"><i class="fas fa-plus"></i> Tambah </button></a>
        </div>
        <div class="card">
            <div class="card-body">
                <table class="table table-striped datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Lokasi</th>
                            <th>Role</th>
                            <th width="6%">#</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
