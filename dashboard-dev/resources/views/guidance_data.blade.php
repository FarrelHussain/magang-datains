@extends('layouts.layout')

@section('title')
    Pembinaan
@endsection

@section('pageTitle')
    Pembinaan
@endsection

@section('js')
    <!-- <script src="{{pageJS('guidance-product.js')}}"></script> -->
    <script>$('table').DataTable();</script>
@endsection 

@section('content')
 <div class="row">
       <div class="col-md-12">
           <h3>Pembinaan</h3>
           <hr>
       </div>
       <div class="col-md-12 ml-2">
           <div class="row">
               <div class="input-group col-12">
                    <!-- <div class="input-group-prepend mr-2 py-0">
                        <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-download"></i> Export Data
                        </button> 
                        <div class="dropdown-menu">
                            <a class="dropdown-item" id="pilih" onclick="return false" href="">Pilih (0)</a>
                            <a class="dropdown-item" target="_blank" data-count="" id="btn-export-tllapangan" href="#">Pembinaan(#)</a>
                            <a class="dropdown-item" id="custom-export" onclick="return false" href="">Custom Export</a>
                        </div> 
                    </div>
                    <button class="btn btn-success mr-2 mt-2" id="btn-arsip">Arsipkan (0)</button> -->
                    <!-- <button class="btn btn-danger mr-2 mt-2" id="btn-takedown">Takedown (0)</button> -->
                </div>
           </div>
       </div>
       <div class="col-md-12 mt-3">
           <div class="card">
               <div class="card-body">
                   {{-- <div class="table-responsive"> --}}
                       <table class="table table-striped datatable">
                            <thead class="bg-primary">
                                <tr>
                                    <th width="5%">No.</th>
                                    <th width="20%">Kode</th>
                                    <th width="15%">Periode</th>
                                    <th width="25%">Jenis Laporan</th>
                                    <th width="15%">Status</th>
                                    <th width="15%">Dilaporkan Oleh</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody id="product-list">
                                <?php $no=0; ?>
                                @foreach($feedback as $fb)
                                <?php $no++; ?>
                                <tr>
                                    <td>
                                        {{$no}}
                                    </td>
                                    <td>
                                        {{$fb->rcv}}
                                    </td>
                                    <td>
                                        {{date('m-Y', strtotime($fb->periode))}}
                                    </td>
                                    <td>
                                        {{$fb->crawling_type}}
                                    </td>
                                    <td>
                                        Sudah Disetujui
                                    </td>
                                    <td>
                                        {{$fb->name}}
                                    </td>
                                    <td>
                                        <a href='/reported-product/drekomendasi-takedown/{{$fb->rcv}}' class="btn btn-xs btn-info">Detail</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                   {{-- </div> --}}
               </div>
           </div>
       </div>
 </div>

@endsection
