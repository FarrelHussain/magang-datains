@extends('layouts.layout')

@section('title')
    Takedown Products
@endsection

@section('pageTitle')
    Takedown Products
@endsection

@section('js')
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.jsdelivr.net/g/mark.js(jquery.mark.min.js),datatables.mark.js"></script>
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
@endsection

@section('content')
 <div class="row">
       <div class="col-md-12">
           <h3>Draft Takedown</h3>
           <hr>
       </div>
       <div class="col-md-12">
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="nav-link {{ pill_active('status', 'semua', true) }}" href="{{ route('reported-product.draft-takedown.index', ['status' => 'semua']) }}">Semua</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ pill_active('status', 'belum-disubmit') }}" href="{{ route('reported-product.draft-takedown.index', ['status' => 'belum-disubmit']) }}">Belum Disubmit</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ pill_active('status', 'menunggu') }}" href="{{ route('reported-product.draft-takedown.index', ['status' => 'menunggu']) }}">Menunggu Persetujuan</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ pill_active('status', 'disetujui') }}" href="{{ route('reported-product.draft-takedown.index', ['status' => 'disetujui']) }}">Disetujui</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ pill_active('status', 'ditolak') }}" href="{{ route('reported-product.draft-takedown.index', ['status' => 'ditolak']) }}">Ditolak</a>
            </li>
        </ul>
       </div>
       <div class="col-md-12 mt-3">
           <div class="card">
               <div class="card-body">
                  <div class="table-responsive"> 
                    {!! $dataTable->table() !!}
                   </div> 
               </div>
           </div>
       </div>
 </div>

@include('draft-takedown.filter')
@endsection
