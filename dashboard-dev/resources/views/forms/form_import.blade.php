@extends('layouts.layout')

@section('title')
    Import Data Takedown
@endsection

@section('js')
    {{-- <script src="{{pageJS('keyword.js')}}"></script> --}}
@endsection

@section('content')
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h3>Import Takedown Data</h3>
                <hr>
            </div>

            <div class="col-lg-12">
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                
                <div class="card overflow-auto"  style="height: 100%">
                    <div class="card-body" >
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                    <form action="{{ route('reported-product.draft-takedown.import' )}}" method="post" id="import-modal-form" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="file">File Excel</label>
                                            <input required type="file" name="takedown_file" id="file" accept=".xls, .xlsx" class="form-control-file">

                                            <a href="{{ route('download', ['q' => 'xlsx/bpom_takedown_import_template_v2.xlsx']) }}" class="">Unduh template</a>
                                            @error('takedown_file')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="">Periode</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                    <i class="far fa-calendar-alt"></i>
                                                    </span>
                                                </div>
                                                <input type="date" name="periode" placeholder="dd/mm/yyyy" class="form-control float-right ">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-primary">Import</button>
                                        </div>
                                    </form>
                            </div>
                            
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="card" style="height: 100%">
                                    <div class="card-body rounded pb-5 overflow-auto" style="background-color: rgba(143, 143, 143, 0.219); height: 100%;" >
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <p class="font-weight-bold">Result:</p>
                                                <hr>
                                            </div>
                                            @if (isset($result))
                                                <div class="col-12 col-md-12 col-sm-12">
                                                    <a data-toggle="collapse" role="button" aria-expanded="false" aria-controls="success-import" class="font-weight-bold text-dark" href="#success-import">Successfully Imported ({{ isset($result['success']->total) ? $result['success']->total : 0}}) <i class="fas fa-caret-down"></i></a>
                                                    <div class="collapse" id="success-import">
                                                        <div class="card-body rounded bg-light mt-2">
                                                            <ul>
                                                                @foreach ($result['success']->data as $item)
                                                                    <li><i>Index row {{$item->index}}: {{$item->message}}</i></li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 mt-3 col-md-12 col-sm-12">
                                                    <a data-toggle="collapse" role="button" aria-expanded="false" aria-controls="failed-import" class="font-weight-bold text-dark" href="#failed-import">Failed Imported ({{isset($result['error']->total) ? $result['error']->total : 0}}) <i class="fas fa-caret-down"></i></a>
                                                    <div class="collapse" id="failed-import">
                                                        <div class="card-body rounded bg-light mt-2">
                                                            <ul>
                                                                @foreach ($result['error']->data as $item)
                                                                    <li><i>Index row {{$item->index}}: {{$item->message}}</i></li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    $(() => {

        $('form#import-modal-form').submit(e => {
            $('#modal-import-takedown').modal('hide');
            $('#overlayLoading').css('display', 'block');
        });

        $('input[name=start]').change((e) => {
            let start = $(e.currentTarget).val();
            $('input[name=end]').removeAttr('disabled');
            $('input[name=end]').attr('min', start);
        })

        $('input[type=date]').attr('max', () => {
            let date = new Date();
            date.setDate(date.getDate() + 1);
            date = date.toISOString().substring(0, 10);
            return date;
        });

    })
</script>
@endsection
