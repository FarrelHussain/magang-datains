@extends('layouts.layout')

@section('title')
    Tambah Keywords
@endsection

@section('js')
    {{-- <script src="{{pageJS('keyword.js')}}"></script> --}}
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Tambah Keywords</h3>
            </div>
            <div class="card-body">
                <form action="" method="post">
                    <div class="form-group">
                        <label for="">Keywords</label>
                        <input type="text" placeholder="Masukan keywords" name="keywords" id="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Keterangan</label>
                        <input type="text" placeholder="Masukan keywords" name="keywords" id="" class="form-control">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
