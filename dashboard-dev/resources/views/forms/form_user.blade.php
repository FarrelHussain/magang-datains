@extends('layouts.layout')

@section('title')
    Tambah User
@endsection

@section('js')
    <script src="{{pageJS('user-management.js')}}"></script>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Tambah Pengguna</h3>
            </div>
            <div class="card-body">
               @if ($errors->any())
                    <div class="alert alert-danger " role="alert">
                         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{url('user-management')}}{{isset($data->id) ? '/' . $data->id : ''}}" method="post" id="formAddUser">
                    @csrf 
                    @if (isset($data->id))
                        @method('PUT')
                    @endif
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input id="validationCustom01" aria-describedby="inputGroupPrepend" type="text" placeholder="Masukan nama pengguna" name="name" id="" class="form-control" aria-invalid="true" required value="{{old('name') ? old('name') : (isset($data->name) ? $data->name : '')}}">
                        <div class="invalid-feedback">
                            Please choose a username.
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" placeholder="Masukan email pengguna" name="email" id="" class="form-control" required value="{{old('email') ? old('email') : (isset($data->email) ? $data->email : '')}}">
                    </div>
                    <div class="form-group">
                        <label for="">Username</label>
                        <input type="text" placeholder="Masukan username pengguna" name="username" id="" class="form-control" required value="{{old('username') ? old('username') : (isset($data->username) ? $data->username : '')}}">
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password"  placeholder="{{isset($data->password) ? "Kosongkan jika tidak akan merubah password" : 'Masukan password pengguna'}}" name="password" id="password" class="form-control" @if(!isset($data->id))  minlength="6" required @endif>
                    </div>
                    <div class="form-group">
                        <label for="">Confirm Password</label>
                        <input type="password" name="password_confirmation" placeholder="{{isset($data->password) ? "Kosongkan konfirmasi jika tidak akan merubah password" : 'Konfirmasi password'}}" class="form-control" @if(!isset($data->id))  minlength="6" required @endif>
                    </div>
                    <div class="form-group">
                        <label for="">Role</label>
                        <select name="role_code" id="" class="form-control" required>
                            @foreach ($roles as $item)
                                <option value="{{$item->role_code}}" {{(old('role_code') == $item->role_code) ? "selected" : ((isset($data->role_code) AND $data->role_code == $item->role_code) ? "selected" : '')}}>{{$item->role_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Location</label>
                        <select name="id_upt_location" id="" class="form-control" required>
                            @foreach ($upt_locations as $upt)
                                <option value="{{$upt->id_upt_location}}" {{(old('id') == $upt->id_upt_location) ? "selected" : ((isset($data->id_upt_location) AND $data->id_upt_location == $upt->id_upt_location) ? "selected" : '')}}>{{$upt->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success"> <i class="fas fa-save"></i> Simpan </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
