<div class="modal fade" id="custom-export-modal">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Custom Export</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ $linkExport }}" id="form-export" method="post">
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="product_name">Nama Produk</label>
                                <input type="text" name="product_name" id="product_name" class="form-control" placeholder="Masukan nama produk">
                            </div>
                            <div class="form-group">
                                <label for="content">Kandungan</label>
                                <input type="text" name="content" id="content" class="form-control" placeholder="Masukan kandungan produk">
                            </div>
                            <div class="form-group">

                                <label for="therapy_class">Kelas Terapi</label>
                                <input type="text" name="therapy_class" id="therapy_class" class="form-control" placeholder="Masukan kelas terapi produk">
                            </div>
                            <div class="form-group">
                                <label for="group">Golongan</label>
                                <input type="text" name="group" id="group" class="form-control" placeholder="Masukan golongan product">
                            </div>
                            <div class="form-group">
                                <label for="">Periode</label>
                                <div class="input-group d-none" id="custom-export-periode">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                        <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="month" name="periode" class="form-control float-right ">
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="store">Toko</label>
                                <input type="text" name="store" id="store" class="form-control" placeholder="Masukan nama toko">
                            </div>
                            <div class="form-group">
                                <label for="">Marketplace</label>
                                <select name="marketplace" id="marketplace" class="form-control">
                                    <option value class="opt-null" selected>None</option>
                                    @foreach ($marketplaces as $item)
                                        <option value="{{$item->id_marketplace}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Pelanggaran</label>
                                <select name="offense" id="offense" class="form-control">
                                    <option value class="opt-null" selected>None</option>
                                    @foreach ($offenses as $item)
                                        <option value="{{$item->id_offense}}">{{$item->offense}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="commodity">Komoditas</label>
                                <select name="commodity" id="commodity" class="form-control">
                                    <option value class="opt-null" selected >None</option>
                                    @foreach ($commodities as $item)
                                        <option value="{{$item->id_commodity}}">{{$item->commodity}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-download"></i> Export</button>
                </div>
            </form>
        </div>
    </div>
</div>
