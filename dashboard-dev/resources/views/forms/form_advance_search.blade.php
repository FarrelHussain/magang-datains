<div class="modal fade" id="advance-search-modal">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Advance Search</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form onsubmit="return false" id="form-advance-search" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="product_name">Nama Produk</label>
                                <input type="text" name="s_product_name" id="s_product_name" class="form-control" placeholder="Masukan nama produk">
                            </div>
                            <div class="form-group">
                                <label for="content">Kandungan</label>
                                <input type="text" name="s_content" id="s_content" class="form-control" placeholder="Masukan kandungan produk">
                            </div>
                            <div class="form-group">

                                <label for="therapy_class">Kelas Terapi</label>
                                <input type="text" name="s_therapy_class" id="s_therapy_class" class="form-control" placeholder="Masukan kelas terapi produk">
                            </div>
                            <div class="form-group">
                                <label for="group">Golongan</label>
                                <input type="text" name="s_group" id="s_group" class="form-control" placeholder="Masukan golongan product">
                            </div>
                            <div class="form-group">
                                <label for="group">Harga</label>
                                <div class="d-flex">
                                    <input type="number" name="s_price_min" id="s_price_min" class="form-control mr-2" placeholder="Masukan harga min">
                                    <input type="number" name="s_price_max" id="s_price_max" class="form-control" placeholder="Masukan harga max">
                                </div>
                            </div>

                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="store">Toko</label>
                                <input type="text" name="s_store" id="s_store" class="form-control" placeholder="Masukan nama toko">
                            </div>
                            <div class="form-group">
                                <label for="">Marketplace</label>
                                <select name="s_marketplace" id="s_marketplace" class="form-control s-select">
                                    <option value class="s-opt-null" selected>None</option>
                                    @foreach ($marketplaces as $item)
                                        <option value="{{$item->id_marketplace}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" id="field-pelanggaran">
                                <label for="">Pelanggaran</label>
                                <select name="s_offense" id="s_offense" class="form-control s-select">
                                    <option value class="s-opt-null" selected>None</option>
                                    @foreach ($offenses as $item)
                                        <option value="{{$item->id_offense}}">{{$item->offense}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="commodity">Komoditas</label>
                                <select name="s_commodity" id="s_commodity" class="form-control s-select">
                                    <option value class="s-opt-null" selected >None</option>
                                    @foreach ($commodities as $item)
                                        <option value="{{$item->id_commodity}}">{{$item->commodity}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="s_location">Wilayah</label>
                                <select name="s_location" id="s_location" class="form-control s-select">
                                    <option value class="s-opt-null" selected >None</option>
                                    @foreach ($locations as $item)
                                        <option value="{{$item->location}}">{{$item->location}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default">Reset</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> Cari</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script defer>
    (function() {
        $(`button[type=reset]`).click(e => {
            $('select.s-select').val(null);
        })
    })()
</script>
