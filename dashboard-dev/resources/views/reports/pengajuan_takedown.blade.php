@extends('layouts.layout')

@section('title')
Pengajuan Takedown
@endsection

@section('pageTitle')
Pengajuan Takedown
@endsection

@section('js')
    <!-- <script src="{{pageJS('pengajuan_takedown.js')}}"></script> -->
    <!-- <script>$('table').DataTable();</script> -->
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.jsdelivr.net/g/mark.js(jquery.mark.min.js),datatables.mark.js"></script>
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    {!! $dataTable->scripts() !!}
@endsection

@section('content')
 <div class="row">
       <div class="col-md-12">
           <h3>Report Pengajuan Takedown</h3>
           <hr>
       </div>
       
       <div class="col-md-12 mt-3">
           <div class="card">
               <div class="card-body">
                   <div class="table-responsive"> 
                        {!! $dataTable->table() !!}
                       <!-- <table class="table table-striped datatable">
                            <thead class="bg-primary">
                                <tr>
                                    <th width="5%">No.</th>
                                    <th width="15%">Periode Pengajuan</th>
                                    <th width="15%">Jenis Crawling</th>
                                    <th width="5%">Keywords</th>
                                    <th width="5%">Status</th>
                                    <th width="10%">Tanggal dibuat</th>
                                    <th width="10%">Tanggal Surat Pengajuan</th>
                                    <th width="10%">Nomor Surat Pengajuan</th>
                                    <th width="5%">#</th>
                                </tr>
                            </thead>
                            <tbody id="pengajuan-takedown">
                            
                            </tbody>
                        </table> -->
                    </div> 
               </div>
           </div>
       </div>
 </div>

<div class="modal fade" id="filter-data-reports">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Filter Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="row">
                    @if(isset($filterData['status']))
                        <div class="col-12 form-group">
                            <label for="filter-status" class="mb-2" for="">Status</label class="mb-1">
                            <select id="filter-status" name="filter-status" class="custom-select">
                                <option value="" selected disabled>Silahkan pilih</option>
                                @foreach($filterData['status'] as $status)
                                    <option value="{{ $status['id_status'] }}">{{ $status['status'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif

                    <div class="col-12 form-group">
                        <label for="daterange" class="mb-2" for="">Periode</label class="mb-1">
                        <input type="text" name="daterange" class="form-control" />
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Terapkan</button>
                <button type="reset" class="btn btn-default">Reset Filter</button>
            </div>
        </div>
    </div>
</div>
@endsection
