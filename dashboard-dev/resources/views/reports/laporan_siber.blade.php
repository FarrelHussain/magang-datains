@extends('layouts.layout')

@section('title')
{{$title}}
@endsection

@section('pageTitle')
{{$title}}
@endsection

@section('js')
    <!-- <script src="{{pageJS('tllapangan-product.js')}}"></script> 
    <script>$('table').DataTable();</script> -->
@endsection

@section('content')
 <div class="row">
       <div class="col-md-12">
           <h3>{{$title}}</h3>
           <hr>
       </div>
       
       <div class="col-md-12 mt-12">
           <div class="card">
               <div class="card-body">
                   {{-- <div class="table-responsive"> --}}
                       <table class="table table-striped datatable">
                            <thead class="bg-primary">
                                <tr>
                                    <th width="5%">No.</th>
                                    <th width="25%">Kandungan</th>
                                    <th width="25%">Kelas Terapi</th>
                                    <th>Blibli</th>
                                    <th>Bukalapak</th>
                                    <th>Elevenia</th>
                                    <th>Olx</th>
                                    <th>jdid</th>
                                    <th>Shopee</th>
                                    <th>Tokopedia</th>
                                    <th>Lazada</th>
                                    <th width="20%">Jumlah Link</th>
                                </tr>
                            </thead>
                            <tbody id="siber-data">
                            <?php $no=0; ?>
                            @foreach($TD as $take)
                            <?php $no++; ?>
                                <tr>
                                    <td>
                                    {{$no}}
                                    </td>
                                    <td>
                                    {{$take->content}}
                                    </td>
                                    <td>
                                    {{$take->TC}}
                                    </td>
                                    <td>
                                    <?php
                                    $sho1 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('marketplace.id_marketplace','=',1)
                                    ->where('keywords.therapy_class','=',$take->TC)
                                    ->get();
                                    echo count($sho1);
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                    $sho2 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('marketplace.id_marketplace','=',2)
                                    ->where('keywords.therapy_class','=',$take->TC)
                                    ->get();
                                    echo count($sho2);
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                    $sho3 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('marketplace.id_marketplace','=',3)
                                    ->where('keywords.therapy_class','=',$take->TC)
                                    ->get();
                                    echo count($sho3);
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                    $sho4 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('marketplace.id_marketplace','=',4)
                                    ->where('keywords.therapy_class','=',$take->TC)
                                    ->get();
                                    echo count($sho4);
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                    $sho5 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('marketplace.id_marketplace','=',5)
                                    ->where('keywords.therapy_class','=',$take->TC)
                                    ->get();
                                    echo count($sho5);
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                    $sho6 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('marketplace.id_marketplace','=',6)
                                    ->where('keywords.therapy_class','=',$take->TC)
                                    ->get();
                                    echo count($sho6);
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                    $sho7 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('marketplace.id_marketplace','=',7)
                                    ->where('keywords.therapy_class','=',$take->TC)
                                    ->get();
                                    echo count($sho7);
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                    $sho8 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('marketplace.id_marketplace','=',8)
                                    ->where('keywords.therapy_class','=',$take->TC)
                                    ->get();
                                    echo count($sho8);
                                    ?>
                                    </td>
                                    <td>
                                    <?php
                                    $total = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('keywords.therapy_class','=',$take->TC)
                                    ->get();
                                    echo count($total);
                                    ?>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfooter>
                                <th colspan='3' class="text-center">
                                    Jumlah Link
                                </th>
                                <th>
                                    <?php
                                    $sho11 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('marketplace.id_marketplace','=',1)
                                    ->get();
                                    echo count($sho11);
                                    ?>
                                    </th>
                                    <th>
                                    <?php
                                    $sho22 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('marketplace.id_marketplace','=',2)
                                    ->get();
                                    echo count($sho22);
                                    ?>
                                    </th>
                                    <th>
                                    <?php
                                    $sho33 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('marketplace.id_marketplace','=',3)
                                    ->get();
                                    echo count($sho33);
                                    ?>
                                    </th>
                                    <th>
                                    <?php
                                    $sho44 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('marketplace.id_marketplace','=',4)
                                    ->get();
                                    echo count($sho44);
                                    ?>
                                    </th>
                                    <th>
                                    <?php
                                    $sho55 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('marketplace.id_marketplace','=',5)
                                    ->get();
                                    echo count($sho55);
                                    ?>
                                    </th>
                                    <th>
                                    <?php
                                    $sho66 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('marketplace.id_marketplace','=',6)
                                    ->get();
                                    echo count($sho66);
                                    ?>
                                    </th>
                                    <th>
                                    <?php
                                    $sho77 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('marketplace.id_marketplace','=',7)
                                    ->get();
                                    echo count($sho77);
                                    ?>
                                    </th>
                                    <th>
                                    <?php
                                    $sho88 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->where('marketplace.id_marketplace','=',8)
                                    ->get();
                                    echo count($sho88);
                                    ?>
                                    </th>
                                    <th>
                                    <?php
                                    $total2 = \DB::table('keywords')
                                    ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
                                    ->join('products','keywords.id_keywords','=','products.id_keywords')
                                    ->join('links','products.id_product','=','links.id_product')
                                    ->select('marketplace.id_marketplace as id6')
                                    ->where('links.id_status','=','2')
                                    ->get();
                                    echo count($total2);
                                    ?>
                                    </th>
                            </tfooter>
                        </table>
                   {{-- </div> --}}
               </div>
           </div>
       </div>
 </div>
 <form action="{{url('/reported-product/takedown')}}" method="post" id="form-tllapangan">
    @csrf

 </form>
  
@endsection
