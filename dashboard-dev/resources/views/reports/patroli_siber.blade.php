@extends('layouts.layout')

@section('title')
{{$title}}
@endsection

@section('pageTitle')
{{$title}}
@endsection

@section('js')
    <!-- <script src="{{pageJS('tllapangan-product.js')}}"></script> -->
    <script>$('table').DataTable();</script>
@endsection

@section('content')
 <div class="row">
       <div class="col-md-12">
           <h3>{{$title}}</h3>
           <hr>
       </div>
       
       <div class="col-md-12 mt-3">
           <div class="card">
               <div class="card-body">
                   {{-- <div class="table-responsive"> --}}
                       <table class="table table-striped datatable">
                            <thead class="bg-primary">
                                <tr>
                                    <th width="5%">No.</th>
                                    <th width="10%">Komoditas</th>
                                    <th>Product</th>
                                    <th width="10%">Harga</th>
                                    <th width="16%">Toko</th>
                                    <th width="5%">Marketplace</th>
                                    <th width="7%">Status</th>
                                    <th width="15%">PLGN</th>
                                </tr>
                            </thead>
                            <tbody id="product-list">
                                <?php $no=0; ?>
                                @foreach($patrol as $pt)
                                <?php $no++; ?>
                                <tr>
                                    <td>
                                        {{$no}}
                                    </td>
                                    <td>
                                        {{$pt->commodity}}
                                    </td>
                                    <td>
                                    <a href='{{ route("grab.show", $pt->id_link) }}' data-toggle="modal" data-target="#detail-modal{{$pt->id_link}}"  id="advance-search-button">{{$pt->name}}</a>
                                    <div class="modal fade" id="detail-modal{{$pt->id_link}}">
                                    <div class="modal-dialog modal-xl">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title">Informasi Produk</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body">
                                        <div class="row">
                                            <div class="col-6">
                                                <table class="table table-striped" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td>Marketplace</td>
                                                            <td>:</td>
                                                            <td>{{$pt->mp_name}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Crawled</td>
                                                            <td>:</td>
                                                            <td>{{$pt->crawled}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Status</td>
                                                            <td>:</td>
                                                            <td>{{$pt->status}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="3">TOKO</th>
                                                        </tr>
                                                        <tr>
                                                            <td>Toko</td>
                                                            <td>:</td>
                                                            <td>{{$pt->store_name}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Link</td>
                                                            <td>:</td>
                                                            <td>{{$pt->store_url}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Toko Desc</td>
                                                            <td>:</td>
                                                            <td>{{$pt->desc}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-6">
                                                <table class="table table-striped table-responsive">
                                                        <tr>
                                                            <th colspan="3">PRODUK</th>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                                <div style="max-width: 250px">
                                                                    <img 
                                                                        id="product-picture" 
                                                                        class="d-block" 
                                                                        width="250"
                                                                        height="250"
                                                                        src="{{$pt->pic}}" 
                                                                        onerror="this.onerror=null;this.src='{{url('img/noimage.png')}}';" alt="Photo">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nama</td>
                                                            <td>:</td>
                                                            <td>{{$pt->store_name}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Harga</td>
                                                            <td>:</td>
                                                            <td>Rp. {{number_format($pt->price,2,',','.')}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Terjual</td>
                                                            <td>:</td>
                                                            <td>{{$pt->sold}}</td>
                                                       </tr>
                                                        <tr>
                                                            <td>Stok</td>
                                                            <td>:</td>
                                                            <td>{{$pt->stok}}</td>
                                                       </tr>
                                                        <tr>
                                                            <td>Rating</td>
                                                            <td>:</td>
                                                            <td>{{$pt->rating}}</td>
                                                       </tr>
                                                        <tr>
                                                            <td>Link</td>
                                                            <td>:</td>
                                                            <td>
                                                            <a href='{{$pt->store_url}}' target='__blank' title='Click untuk kunjungi'>Kunjungi</a>
                                                            </td>
                                                       </tr>
                                                        <tr>
                                                            <td>Deskripsi</td>
                                                            <td>:</td>
                                                            <td>{{$pt->description}}</td>
                                                       </tr>
                                                </table>
                                            </div>
                                        </div>
                                        </div>
                                        {{-- <div class="modal-footer float-right">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
                                        <button type="button" class="btn btn-primary">KEPASTIAN PELANGGAN</button>
                                        </div> --}}
                                    </div>
                                    <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                    <!-- /.modal -->

                                    </td>
                                    <td>
                                       Rp. {{number_format($pt->price,2,',','.')}}
                                    </td>
                                    <td>
                                    <a href='{{$pt->store_url}}' target='__blank' title='Click untuk kunjungi'>{{$pt->store_name}}</a>
                                    </td>
                                    <td>
                                    <img src='/img/{{$pt->mp_logo}}' class='rounded mx-auto d-block' style="width:45px" onerror="replcaceImageWithHTML(this, '$pt->mp_logo')"/>
                                    </td>
                                    <td>
                                        {{$pt->status}}
                                    </td>
                                    <td>   
                                        {{$pt->offense}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                   {{-- </div> --}}
                @include('utilities.detailModal')
               </div>
           </div>
       </div>
 </div>
 <!--
 <form action="{{url('/reported-product/takedown')}}" method="post" id="form-tllapangan">
    @csrf

 </form>
-->
@endsection
