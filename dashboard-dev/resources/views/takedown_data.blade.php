@extends('layouts.layout')

@section('title')
    Takedown Products
@endsection

@section('pageTitle')
    Takedown Products
@endsection

@section('js')
    <script src="{{pageJS('takedown-product.js')}}"></script>
@endsection

@section('content')
 <div class="row">
       <div class="col-md-12">
           <h3>Takedown Products List</h3>
           <hr>
       </div>
       <div class="col-md-12 ml-2">
           <div class="row">
                <div class="col-md-1 mt-2 mr-5">
                    <div class="input-group">
                        <div class="input-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <i class="fas fa-download"></i> Export Data
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" id="pilih" href="" onclick="return false">Pilih (0)</a>
                                <a class="dropdown-item" target="_blank" data-count="{{$takedownCount}}" id="btn-export-takedown-data" href="{{$linkExport}}">Takedown Products({{$takedownCount}})</a>
                                <a class="dropdown-item" id="custom-export" onclick="return false" href="">Custom Export</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-md-10 mt-2 mr-5">
                    <div class="row">
                        <a href="{{route('import-takedown-form')}}" class="btn btn-default mr-3"><i class="fas fa-upload"></i> Import Data</a>
                        <a class="btn btn-default" target="_blank" href="{{asset(config('app.takedown_import_file'))}}"><i class="fas fa-file-excel"></i>  Import Template</a>
                    </div>
                </div> -->
           </div>
       </div>
       <div class="col-md-12 mt-3">
           <div class="card">
               <div class="card-body">
                  <div class="table-responsive"> 
                       <table class="table table-striped datatable">
                            <thead class="bg-primary">
                                <tr>
                                    <th width="5%">Periode</th>
                                    <th width="5%">Commodity</th>
                                    <th width="">Product</th>
                                    <th width="10%">Store</th>
                                    <th width="10%">Marketplace</th>
                                    <th width="8%">Last Visit</th>
                                    <th width="5%">Status</th>
                                    <th width="5%">PLGN</th>
                                    <th width="8%">User</th>
                                    <th width="15%">Lokasi</th>
                                    <th width="5%">#</th>
                                </tr>
                            </thead>
                            <tbody id="product-list">
                                <tr>
                                    <td id="loading" colspan="10" class="text-center">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                   </div> 
               </div>
           </div>
       </div>
 </div>
  @include('forms.form_advance_search')
  @include('forms.form_export_modal')
  @include('utilities.detailModal')
  @include('utilities.filterHomeModal')
  @include('utilities.modal_file_import')
@endsection
