@extends('layouts.layout')

@section('title')
    TL Lapangan Products
@endsection

@section('pageTitle')
    TL Lapangan Products
@endsection

@section('js')
    <script src="{{pageJS('tllapangan-product.js')}}"></script>
@endsection

@section('content')
 <div class="row">
       <div class="col-md-12">
           <h3>TL Lapangan Products List</h3>
           <hr>
       </div>
       <div class="col-md-12 ml-2">
           <div class="row">
               <div class="input-group col-12">
                    <div class="input-group-prepend mr-2 py-0">
                        <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fas fa-download"></i> Export Data
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" id="pilih" onclick="return false" href="">Pilih (0)</a>
                            <a class="dropdown-item" target="_blank" data-count="{{$tllapanganCount}}" id="btn-export-tllapangan" href="{{$linkExport}}">TL Lapangan Products({{$tllapanganCount}})</a>
                            <a class="dropdown-item" id="custom-export" onclick="return false" href="">Custom Export</a>
                        </div>
                    </div>
                    <!--    <button class="btn btn-warning text-light mr-2 mt-2 takedown-feedback" data-btn="feedback" id="btn-feedback">Feedback(0)</button>
                    <button class="btn btn-danger mr-2 mt-2" id="btn-takedown">Takedown (0)</button> -->
                     <button class="btn btn-success mr-2 mt-2" id="btn-arsip">Arsipkan (0)</button>   
                </div>
           </div>
       </div>
       <div class="col-md-12 mt-3">
           <div class="card">
               <div class="card-body">
                   {{-- <div class="table-responsive"> --}}
                       <table class="table table-striped datatable">
                            <thead class="bg-primary">
                                <tr>
                                    <th width="5%">No.</th>
                                    <th width="15%">Komoditas</th>
                                    <th>Product</th>
                                    <th width="7%">Harga</th>
                                    <th width="16%">Toko</th>
                                    <th width="5%">Marketplace</th>
                                    <th width="10%">Status</th>
                                    <th width="7%">PLGN</th>
                                    <th width="5%">#</th>
                                    <th class="d-none"></th>
                                </tr>
                            </thead>
                            <tbody id="product-list">
                                <tr>
                                    <td id="loading" colspan="9" class="text-center">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                   {{-- </div> --}}
               </div>
           </div>
       </div>
 </div>
 <form action="{{url('/reported-product/takedown')}}" method="post" id="form-tllapangan">
    @csrf

 </form>
  @include('forms.form_advance_search')
  @include('forms.form_export_modal')
  @include('utilities.detailModal')
  @include('utilities.filterHomeModal')
@endsection
