@extends('layouts.layout')

@section('title')
    Draft Takedown Products
@endsection

@section('pageTitle')
    Draft Takedown Products
@endsection
@section('js')
    {!! $dataTable->scripts() !!}
@endsection

@section('content')
 <div class="row">
       <div class="col-md-12">
           <h3>Daftar Takedown</h3>
           <hr>
       </div>

       <div class="col-sm-2">
           <div class="card">
                <div class="card-body">
                <p class="font-weight-lighter">Nama Petugas<br/><strong>{{auth()->user()->name}}</strong></p>
                {{-- <p class="font-weight-lighter">Jenis Laporan<br/><strong>{{$report->jenis_laporan}}</strong></p> --}}
                {{-- <p class="font-weight-lighter">Periode<br/><strong>{{date("M Y", strtotime($report->tgl_submit))}}</strong></p> --}}
                {{-- <p class="font-weight-lighter">Unit Kerja<br/><strong>{{$report->jenis_laporan}}</strong></p> --}}
                </div>
            </div>
       </div>

       <div class="col-sm-10">
           <div class="card">
               <div class="card-body">
                    <div class="table-responsive"> 
                        {!! $dataTable->table() !!}

                        @if(is_null($reportedProducts->submitted_at))
                            <a 
                                href="{{ route('reported-product.draft-takedown.submit', request('reportCode')) }}" 
                                id="submit" 
                                class="btn btn-info mr-2 mt-2 btn-add"
                            >
                                Submit
                            </a>
                        @endif
                    </div> 
               </div>
           </div>
       </div>
 </div>

 <div class="modal fade" id="add-new-product-modal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambahkan Produk</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped">
                    <thead>
                        <th>No</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                        </tr>
                    </tbody>
                </table>
                <div class="d-flex flex-row justify-content-end">
                    <input type="reset" class="btn btn-default mr-3" id="reset-filter" value="RESET" />
                    <button type="button" class="btn btn-primary" id="btn-terapkan">TERAPKAN</button>
                </div>
            </div>
        </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
