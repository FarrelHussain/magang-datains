@extends('layouts.layout')

@section('title')
    Commodity Master
@endsection 

@section('js')
    <script src="{{pageJS('commodity.js')}}"></script>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h3>Commodity Master</h3>
                <hr>
            </div>
            {{-- <div class="col-12">
                <a href="{{url('keyword/create')}}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> TAMBAH</a>
            </div> --}}
            <div class="col-12 mt-2">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped datatable">
                            <thead class="bg-primary">
                                <tr>
                                    <td width='5%'>No.</td>
                                    <td>Komoditas</td>
                                    <td width='10%'>#</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
<div class="modal fade" id="modal-add-commodity" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form action="{{route('commodity.store')}}" data-store="{{route('commodity.store')}}" class="w-100" id="form-commodity" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-add-commodity-title">Tambah Komoditas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Nama Komoditas</label>
                        <input type="text" name="commodity" id="commodity" required maxlength="255" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(() => {
        $('#form-commodity').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
        }
        });
        $('#modal-add-commodity').on('hide.bs.modal', () => {
            $('#form-commodity').attr('action', $('#form-commodity').attr('data-store'));
            $('#modal-add-commodity-title').html("Tambah Komoditas");
            $('input[name=commodity]').val('');
            $('#edit-equipment').remove();
        })
    })
</script>
@endsection
