@extends('layouts.layout')

@section('title')
    Rekomendasi Takedown
@endsection

@section('pageTitle')
    Rekomendasi Takedown
@endsection

@section('js')
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
@endsection

@section('content')
 <div class="row">
    
       <div class="col-md-12">
            @if(!is_null(request('reportCode')) or !is_null(request('reportCodeVerified')))
                @if(\Str::contains(url()->current(), request('reportCodeVerified')) and \Str::contains(url()->current(), request('reportCode')))
                    <a
                        href="{{ \Str::replaceLast(request('reportCode'), '', url()->current()) }}" 
                        class="btn btn-sm btn-primary">Kembali</a>
                @elseif(\Str::contains(url()->current(), request('reportCodeVerified')))
                    <a href="{{ \Str::replaceLast(request('reportCodeVerified'), '', url()->current()) }}" class="btn btn-sm btn-primary">Kembali</a>
                @elseif(\Str::contains(url()->current(), request('reportCode')))
                    <a href="{{ \Str::replaceLast(request('reportCode'), '', url()->current()) }}" class="btn btn-sm btn-primary">Kembali</a>
                @endif
            @endif
            
           <h3>Rekomendasi Takedown</h3>
           <hr>
           
           @if(is_null(request('reportCode')) or is_null(request('reportCodeVerified')))
           <form action="/reports/saveFeedback" id="form-export" method="post">
                <div class="modal-body">
                    @csrf
                    
                    <div class="row">
                        
                                @if(is_stakeholder())
                        <div class="col-12">
                            <div class="form-group">
                                <label for="feedback">Feedback</label>
                                <input type="hidden" name="rcv" id="rcv" class="form-control" value='{{request('reportCodeVerified')}}'>
                                <input type="text" name="feedback" id="feedback" class="form-control" placeholder="Masukan Feedbacknya">
                                <input type='hidden' name='notadinas' value=''>  
                            </div>
                        </div>
                        <div class="col-12">
                             <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                             </div>
                        </div>
                                @elseif (is_petugas() || is_petugas_loka() || is_petugas_balai())
                        <div class="col-12">
                            <div class="form-group">
                                <label for="feedback">Feedback</label>
                                <input type="hidden" name="rcv" id="rcv" class="form-control" value='{{request('reportCodeVerified')}}'>
                                <input type="text" name="feedback" id="feedback" class="form-control" placeholder="Masukan Feedbacknya">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="notadinas">notadinas</label>
                                <input type="text" name="notadinas" id="feedback" class="form-control" placeholder="Masukan Notadinas">
                                
                            </div>
                        </div>
                        <div class="col-12">
                             <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                             </div>
                        </div>
                                @else
                                <input type='hidden' name='notadinas' value=''>
                                @endif
                        
                    </div>
                </div>
            </form>        
           @endif
       </div>

        @if(is_null(request('reportCode')) && is_null(request('reportCodeVerified')))
            <div class="col-md-12">
                <ul class="nav nav-pills">
                    <li class="nav-item">
                        <a class="nav-link {{ pill_active('status', 'semua', true) }}" href="{{ request()->url() }}">Semua</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ pill_active('status', 'menunggu') }}" href="{{ request()->url() }}?status=menunggu">Menunggu Persetujuan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ pill_active('status', 'disetujui') }}" href="{{ request()->url() }}?status=disetujui">Disetujui</a>
                    </li>
                </ul>
            </div>
        @endif

       <div class="col-md-12 mt-3">
           <div class="card">
               <div class="card-body">
                  <div class="table-responsive"> 
                    {!! $dataTable->table() !!}
                   </div> 
               </div>
           </div>
       </div>
 </div>

@include('forms.form_reject')
@include('draft-takedown.filter')
@endsection
