@extends('layouts.layout')

@section('title')
    Crawled Stores
@endsection

@section('js')
    <script src="{{pageJS('stores.js')}}"></script>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h3>Crawled Stores</h3>
                <hr>
            </div>
            {{-- <div class="col-12">
                <a href="{{url('keyword/create')}}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> TAMBAH</a>
            </div> --}}
            <div class="col-12 mt-2">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped datatable">
                            <thead class="bg-primary">
                                <tr>
                                    <td width='10%'>No.</td>
                                    <td>Toko</td>
                                    <td  align="center">Marketplace</td>
                                    <td >Total Produk</td>
                                    <td  align="center">Target</td>
                                    <td  align="center">Total Hasil</td>
                                    {{-- <td width='5%'>#</td> --}}
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <p class="font-weight-bold">Detail Keywords</p>
                            <table class="table table-striped" id="table-history">
                                <thead>
                                    <tr>
                                        <th>Kandungan</th>
                                        <th>Golongan</th>
                                        <th>Kelas Terapi</th>
                                        <th>Total Produk Marketplace</th>
                                        <th class="text-center">Target</th>
                                        <th class="text-center">Total Hasil</th>
                                        <th class="text-center">progress</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>-</td>
                                        <td class="text-center">-</td>
                                        <td class="text-center">-%</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-12 mt-3">
                            <p class="font-weight-bold">Toko Dengan Penjualan Terbanyak</p>
                            <table class="table table-striped" id="top-stores">
                                <thead>
                                    <tr>
                                        <th>Toko</th>
                                        <th class="text-center">Produk Terjual</th>
                                        <th class="text-center">Rating Toko</th>
                                        <th class="text-center">Terakhir Update</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>-</td>
                                        <td class="text-center">-</td>
                                        <td class="text-center">-</td>
                                        <td class="text-center">-</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('utilities.filterHomeModal')
    <script>
        $(() => {
            // $('#modal-detail').modal('show')
        })
    </script>
@endsection
