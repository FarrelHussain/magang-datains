<li class="nav-item dropdown {{Request::is('reported-product/*') ? 'active' : 'uri'}}">
    <a href="#" id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"> Blacklist Products </a>
    <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
        <li><a href="{{route('reported-product.rekomendasi-takedown')}}" class="dropdown-item">Rekomendasi Takedown</a></li>
        <li><a href="{{url('reported-product/takedown')}}" class="dropdown-item">Takedown </a></li>
        <li><a href="{{route('tllapangan')}}" class="dropdown-item">Tindak Lanjut Lapangan</a></li>
        <li><a href="{{route('guidance')}}" class="dropdown-item">Pembinaan</a></li>
        <li><a href="{{route('arsip')}}" class="dropdown-item">Arsip</a></li>
    </ul>
</li>
<li class="nav-item dropdown {{Request::is('reports/*') ? 'active' : 'uri'}}">
    <a href="#" id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"> Reports </a>
    <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
        <li><a href="{{url('reports/takedown')}}" class="dropdown-item"> Laporan Pengajuan Takedown </a></li>
        <li><a href="{{url('reports/laporan-siber')}}" class="dropdown-item"> Laporan Kegiatan Patroli Siber</a></li>
        <li><a href="{{url('reports/patrol')}}" class="dropdown-item"> Laporan Pengiriman Hasil Patroli Siber UPT </a></li>
        <li><a href="{{route('reports.monitoring')}}" class="dropdown-item">Laporan Monitoring dan Evaluasi</a></li>
        <li><a href="{{url('reports/feedback')}}" class="dropdown-item"> Feedback </a></li>
    </ul>
</li>