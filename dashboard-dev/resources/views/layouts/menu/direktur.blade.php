<li class="nav-item dropdown {{Request::is('reported-product/*') ? 'active' : 'uri'}}">
    <a href="#" id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"> Blacklist Products </a>
    <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
        <li><a href="{{route('reported-product.direktur.rekomendasi-takedown')}}" class="dropdown-item">Rekomendasi Takedown</a></li>
                                <li><a href="{{url('reported-product/takedown')}}" class="dropdown-item">Takedown </a></li>
    </ul>
</li>

<li class="nav-item dropdown {{Request::is('reports/*') ? 'active' : 'uri'}}">
    <a href="#" id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"> Reports </a>
    <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
        <li><a href="{{url('reports/takedown')}}" class="dropdown-item"> Laporan Pengajuan Takedown </a></li>
        <li><a href="{{url('reports/laporan-siber')}}" class="dropdown-item"> Laporan Kegiatan Patroli Siber</a></li>
        <li><a href="{{url('reports/patrol')}}" class="dropdown-item"> Laporan Pengiriman Hasil Patroli Siber UPT </a></li>
        <li><a href="{{url('reports/feedback')}}" class="dropdown-item"> Feedback </a></li>
    </ul>
</li>

<li class="nav-item dropdown {{Request::is('user-*') ? 'active' : 'uri'}}">
    <a href="#" id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"> Users </a>
    <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
        <li><a href="{{url('user-management')}}" class="dropdown-item"> Management User </a></li>
        <li><a href="{{url('user-activity')}}" class="dropdown-item"> User Activity </a></li>
    </ul>
</li>

