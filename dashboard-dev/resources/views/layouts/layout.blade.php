
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="csrf-token" content="{!! csrf_token() !!}">
        <meta name="base-url" content="{{url('')}}">
        <title>@yield('title') | {{config('app.name')}}</title>
        @include('utilities.include')

        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        @yield('head-script')
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <script>
            var _buildUrl = function(dt, action) {
                var url = dt.ajax.url() || '';
                var params = dt.ajax.params();
                params.action = action;

                if (url.indexOf('?') > -1) {
                    return url + '&' + $.param(params);
                }
                
                return url + '?' + $.param(params);
            };

            // let checkboxRowCount = 0;
        </script>
    </head>
<body class="hold-transition layout-top-nav" style="background-color: #fff !important">
    @include('utilities.loading')
    @include('utilities/alerts')
    <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand-md navbar-primary navbar-dark">
        
            <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button> 

            <div class="collapse navbar-collapse order-3" id="navbarCollapse">
                <!-- Left navbar links -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item {{setActive(Route::class, 'home', 'active', 'uri')}}">
                        <a href="{{url('home')}}" class="nav-link "> Home </a>
                    </li>

                    @if (!is_direktur() && !is_stakeholder() && !is_koordinator() && !is_kepala_balai() && !is_kepala_loka())
                        <li class="nav-item {{request()->is('grab*') ?  'active' : 'uri'}}">
                            <a href="{{url('grab')}}" class="nav-link"> Grab </a>
                        </li>
                        <li class="nav-item {{setActive(Route::class, 'crawling', 'active', 'uri')}}">
                            <a href="{{url('crawling')}}" class="nav-link"> Crawling </a>
                        </li>
                        <li class="nav-item dropdown {{Request::is('keyword/*') ? 'active' : 'uri'}}">
                            <a href="#" id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"> Keywords </a>
                            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                                <li><a href="{{url('keyword')}}" class="dropdown-item"> Keywords </a></li>
                                <li><a href="{{route('keyword.rekapitulasi')}}" class="dropdown-item"> Rekapitulasi </a></li>
                            </ul>
                        </li>
                    @endif

                    @if (is_admin())
                        <li class="nav-item {{setActive(Route::class, 'big-seller', 'active', 'uri')}}">
                            <a href="{{ route('big-seller') }}" class="nav-link"> Big Seller </a>
                        </li>
                        <li class="nav-item {{setActive(Route::class, 'commodity', 'active', 'uri')}}">
                            <a href="{{url('commodity')}}" class="nav-link"> Commodity </a>
                        </li>
                        <li class="nav-item dropdown {{Request::is('reported-product/*') ? 'active' : 'uri'}}">
                            <a href="#" id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"> Blacklist Products </a>
                            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                                <li><a href="{{url('reported-product/draft-takedown')}}" class="dropdown-item">Draft Takedown </a></li>
                                <li><a href="{{route('reported-product.rekomendasi-takedown')}}" class="dropdown-item">Rekomendasi Takedown (koordinator)</a></li>
                                <li><a href="{{route('reported-product.direktur.rekomendasi-takedown')}}" class="dropdown-item">Rekomendasi Takedown (direktur)</a></li>
                                <li><a href="{{url('reported-product/takedown')}}" class="dropdown-item">Takedown </a></li>
                                <li><a href="{{route('tllapangan')}}" class="dropdown-item">Tindak Lanjut Lapangan</a></li>
                                <li><a href="{{route('guidance')}}" class="dropdown-item">Pembinaan</a></li>
                                <li><a href="{{route('arsip')}}" class="dropdown-item">Arsip</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown {{Request::is('reports/*') ? 'active' : 'uri'}}">
                            <a href="#" id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"> Reports </a>
                            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                                <li><a href="{{url('reports/takedown')}}" class="dropdown-item"> Laporan Pengajuan Takedown </a></li>
                                <li><a href="{{url('reports/laporan-siber')}}" class="dropdown-item"> Laporan Kegiatan Patroli Siber</a></li>
                                <li><a href="{{url('reports/patrol')}}" class="dropdown-item"> Laporan Pengiriman Hasil Patroli Siber UPT </a></li>
                                <li><a href="{{route('reports.monitoring')}}" class="dropdown-item">Laporan Monitoring dan Evaluasi</a></li>
                                <li><a href="{{url('reports/feedback')}}" class="dropdown-item"> Feedback </a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown {{Request::is('user-*') ? 'active' : 'uri'}}">
                            <a href="#" id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"> Users </a>
                            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                                <li><a href="{{url('user-management')}}" class="dropdown-item"> Management User </a></li>
                                <li><a href="{{url('user-activity')}}" class="dropdown-item"> User Activity </a></li>
                            </ul>
                        </li>

                    @elseif (is_direktur() or is_kepala_balai())
                        @include('layouts.menu.direktur')
                    @elseif (is_koordinator() or is_koordinator_balai() or is_kepala_loka())
                        @include('layouts.menu.kepala')
                    @elseif (is_petugas() || is_petugas_loka() || is_petugas_balai())
                        @include('layouts.menu.petugas')
                    @else
                        <li class="nav-item dropdown {{Request::is('reports/*') ? 'active' : 'uri'}}">
                            <a href="#" id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"> Reports </a>
                            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                                <!-- <li><a href="{{url('reports/takedown')}}" class="dropdown-item"> Laporan Pengajuan Takedown </a></li> -->
                                <!-- <li><a href="{{url('reports/patrol')}}" class="dropdown-item"> Laporan Pengiriman Hasil Patroli Siber UPT </a></li> -->
                                <li><a href="{{url('reports/feedback')}}" class="dropdown-item"> Feedback </a></li>
                            </ul>
                        </li>

                    @endif


                </ul>
                <div class="">
                    <a class="btn btn-link text-white" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{Auth::user()->name}}
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <form action="{{ route('logout') }}" method="post">
                            @csrf
                            <button type="submit" class="dropdown-item">
                                <i class="fas fa-sign-out-alt mr-2"></i> Logout
                            </button>
                        </form>
                    </div>
                </div>
            </div>

    </nav>
    <!-- /.navbar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" id="">
        <!-- Content Header (Page header) -->
            @yield('content_header')
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content pt-3">
        <div class="container-fluid">
            <div class="row">
            <div class="col-12">
                @yield('content')
            </div>
            <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->

    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
        2020
        </div>
        <!-- Default to the left -->
        <strong><a href="#">Badan Pengawas Obat dan Makanan</a>.</strong>
    </footer>
    </div>
    <!-- ./wrapper -->
    @yield('js')
    <script>
        var year = new Date().getFullYear();
        $('.main-footer div').text(year)
    </script>
    </body>
</html>
