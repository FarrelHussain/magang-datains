@extends('layouts.layout')

@section('title')
    Big Seller
@endsection

@section('pageTitle')
Big Seller
@endsection

@section('js')
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.jsdelivr.net/g/mark.js(jquery.mark.min.js),datatables.mark.js"></script>
    <script src="/vendor/datatables/buttons.server-side.js"></script>

    {!! $dataTable->scripts() !!}
@endsection

@section('content')
 <div class="row">
    
       <div class="col-md-12">
            @if(!is_null(request('reportCode')) or !is_null(request('reportCodeVerified')))
                @if(\Str::contains(url()->current(), request('reportCodeVerified')) and \Str::contains(url()->current(), request('reportCode')))
                    <a
                        href="{{ \Str::replaceLast(request('reportCode'), '', url()->current()) }}" 
                        class="btn btn-sm btn-primary">Kembali</a>
                @elseif(\Str::contains(url()->current(), request('reportCodeVerified')))
                    <a href="{{ \Str::replaceLast(request('reportCodeVerified'), '', url()->current()) }}" class="btn btn-sm btn-primary">Kembali</a>
                @elseif(\Str::contains(url()->current(), request('reportCode')))
                    <a href="{{ \Str::replaceLast(request('reportCode'), '', url()->current()) }}" class="btn btn-sm btn-primary">Kembali</a>
                @endif
            @endif
            
           <h3>Big Seller</h3>
           <hr>
       </div>

       <div class="col-md-12 mt-3">
           <div class="card">
               <div class="card-body"> 
                    {!! $dataTable->table() !!}
               </div>
           </div>
       </div>
 </div>

@endsection
