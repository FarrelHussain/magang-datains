@extends('layouts.layout')

@section('title')
    Grab
@endsection

@section('pageTitle')
    Grab
@endsection

@section('js')
    <!-- <script src="{{pageJS('big-seller.js')}}"></script> -->
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h3>Big Seller</h3>
            <hr>
        </div>
        <div class="col-12">
            <div class="card">
                
				<form class="row" action="{{ url('big-seller') }}" method="post" id="form-grab" style="display: none" onsubmit="return false;">
            @csrf
            <input type="hidden" name="id_offense">
            <input type="hidden" name="note">
        </form>
        <div class="row">
            <!-- <div class="col-12 col-md-12 col-sm-12 mb-3">
                <div class="row">
                    <div class="col-md-12 mb-3 btn-store">
                        <button onclick="showOnly('blibli', this)" class="btn btn-primary mx-1 mt-3">BLIBLI (0)</button>
                        <button onclick="showOnly('bukalapak', this)" class="btn btn-primary mx-1 mt-3 ">BUKALAPAK (0)</button>
                        <button onclick="showOnly('elevenia', this)" class="btn btn-primary mx-1 mt-3 ">ELEVENIA (0)</button>
                        <button onclick="showOnly('olx', this)" class="btn btn-primary mx-1 mt-3 ">OLX (0)</button>
                        <button onclick="showOnly('jdid',this)" class="btn btn-primary mx-1 mt-3 ">JDID (0)</button>
                        <button onclick="showOnly('shopee',this)" class="btn btn-primary mx-1 mt-3 ">SHOPEE (0)</button>
                        <button onclick="showOnly('tokopedia', this)" class="btn btn-primary mx-1 mt-3 ">TOKOPEDIA (0)</button>
                        <button onclick="showOnly('lazada', this)" class="btn btn-primary mx-1 mt-3 ">LAZADA (0)</button>
                    </div>
                </div>
            </div> -->
            <div class="col-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        {{-- <div class="table-responsive"> --}}
                            <table class="table table-bordered table-striped datatable">
                                <thead class="bg-primary">
                                    <tr>
                                        <th>Toko</th>
                                        <th>Marketplace</th>
                                        <th width="">Total Produk</th>
                                        <th>Total Produk Terjual</th>
                                        <th>Total Dilihat</th>
                                        <th>Total Follower</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td id="loading" colspan="9" class="text-center">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        {{-- </div> --}}
                    </div>
                </div>
            </div>
        </div>
            @include('utilities.detailModal') 
                        
            </div>
        </div>
    </div>
</div>
@endsection
