@extends('layouts.layout')

@section('title')
    Monitoring Takedown
@endsection

@section('pageTitle')
    Monitoring Takedown
@endsection

@section('js')
    <script>
        const DATATABLE_URL = "{{ route('reports.monitoring.data') }}"
    </script>
    <script src="{{pageJS('monitoring-product.js')}}"></script>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 row justify-content-between">
            <div class="col-md-8 mb-3 btn-store">
                <h3>Laporan Monitoring dan Evaluasi</h3>
                <span>Periode: <span id='firstMonth'>{{ $firstOfMonth }}</span> - <span id="endMonth">{{ $endOfMonth }}</span></span>
                <hr>
            </div>

            <div class="col-md-4">
                <div class="d-flex justify-content-end">
                    <a href="###" data-toggle="modal" data-target="#home-filter-modal" class="btn btn-default btn-filter"><i class="fas fa-filter"></i> Filter Data</a>
                </div>
            </div>
        </div>

        <div class="col-md-12 mt-3">
            <div class="card">
                <div class="card-body">
                    {{-- <div class="table-responsive"> --}}
                        <table class="table table-striped datatable">
                            <thead class="bg-primary">
                                <tr>
                                    <th>No.</th>
                                    <th>Platform</th>
                                    <th>Crawling</th>
                                    <th>Aktif</th>
                                    <th>Takedown</th>
                                    <th>Presentase Tindak Lanjut</th>
                                </tr>
                            </thead>
                            <tbody id="product-list"></tbody>
                        </table>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>
  <!-- @include('forms.form_advance_search') -->
  @include('utilities.detailModal')
  @include('utilities.filterHomeModal')
@endsection