@extends('layouts.layout')

@section('title')
    Rekapitulasi Keyword
@endsection

@section('js')
    <script src="{{pageJS('rekapitulasi.js')}}"></script>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h3>Rekapitulasi Keywords</h3>
                <hr>
            </div>
            {{-- <div class="col-12">
                <a href="{{url('keyword/create')}}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> TAMBAH</a>
            </div> --}}
            @php
                $commodities = array_slice(array_keys($recapitulation[0]), 2);
            @endphp
            <div class="col-12 mt-2">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped datatable">
                            <thead class="bg-primary">
                                <tr>
                                    <td width='5%'>No.</td>
                                    <td>Marketplace</td>
                                    @foreach($commodities as $commodity)
                                        <td>{{ $commodity }}</td>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($recapitulation as $no => $recap)
                                    <tr>
                                        <td></td>
                                        <td>{{ $recap['name'] }}</td>
                                        @foreach($commodities as $commodity)
                                            <td>{{ $recap[$commodity] ?? 0 }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(() => {
            // $('#modal-detail').modal('show')
        })
    </script>
@endsection
