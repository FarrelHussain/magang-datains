
@inject('reportType', '\App\Models\CrawlingType')

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<div class="modal fade" id="filter-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Filter Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <label for="filter-report-type">Filter Berdasarkan Jenis Laporan</label>
                    <select class="form-control" id="filter-report-type">
                        <option value="" selected disabled hidden>Pilih jenis laporan</option>
                        @foreach($reportType->get() as $type)
                            <option value="{{ $type->type }}">{{ $type->type }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="daterange" class="mb-2" for="">Periode</label class="mb-1">
                    <input type="text" name="daterange" class="form-control" />
                </div>

                <div class="d-flex flex-row justify-content-end">
                    <button type="reset" class="btn btn-default mr-3">RESET</button>
                    <button type="submit" class="btn btn-primary">TERAPKAN</button>
                </div>
            </div>
        </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
      <!-- /.modal -->
<script defer>
    
</script>
