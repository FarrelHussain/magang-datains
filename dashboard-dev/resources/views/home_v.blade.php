@extends('layouts.layout')

@section('title')
    Home
@endsection

@section('js') 
  <script src="{{pageJS('home.js')}}"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/d3/4.2.2/d3.min.js'></script>
<script>
	// Seed data to populate the donut pie chart
var seedData = [
	<?php
	$datas = \DB::table('links')
    ->select(\DB::raw('count(links.id) as total'), 'marketplace.name', 'marketplace.id_marketplace as idm')            
    ->join('products', 'products.id_product', '=', 'links.id_product')            
    ->join('stores', 'stores.id_store', '=', 'products.id_store')            
    ->join('marketplace','marketplace.id_marketplace', '=', 'stores.id_marketplace')						
    ->groupBy('marketplace.name','marketplace.id_marketplace')						
    ->get();
	foreach($datas as $td){
	?>
  {
  "label": "{{$td->name}} {{$td->total}}",
  "value": "{{$td->total}}",
  "link": "#"
},
<?php } ?>
];

// Define size & radius of donut pie chart
var width = 310,
    height = 355,
    radius = Math.min(width, height) / 2;

// Define arc colours
var colour = d3.scaleOrdinal(d3.schemeCategory20);

// Define arc ranges
var arcText = d3.scaleOrdinal()
  .range([0, width]);

// Determine size of arcs
var arc = d3.arc()
  .innerRadius(radius - 65)
  .outerRadius(radius - 10);

// Create the donut pie chart layout
var pie = d3.pie()
  .value(function (d) { return d["value"]; })
  .sort(null);

// Append SVG attributes and append g to the SVG
var svg = d3.select("#donut-chart")
  .attr("width", width)
  .attr("height", height)
  .append("g")
    .attr("transform", "translate(" + radius + "," + radius + ")");

// Define inner circle
svg.append("circle")
  .attr("cx", 0)
  .attr("cy", 0)
  .attr("r", 80)
  .attr("fill", "#fff") ;

// Calculate SVG paths and fill in the colours
var g = svg.selectAll(".arc")
  .data(pie(seedData))
  .enter().append("g")
  .attr("class", "arc")
		
  // Make each arc clickable 
  .on("click", function(d, i) {
    window.location = seedData[i].link;
  });

	// Append the path to each g
	g.append("path")
  	.attr("d", arc)
  	.attr("fill", function(d, i) {
    	return colour(i);
  	});

	// Append text labels to each arc
	g.append("text")
  	.attr("transform", function(d) {
    	return "translate(" + arc.centroid(d) + ")";
  	})
  	.attr("dy", ".10em")
  	.style("text-anchor", "middle")
  	.attr("fill", "#000")
		.text(function(d,i) { return seedData[i].label; })
  
g.selectAll(".arc text").call(wrap, arcText.range([0, width]));

// Append text to the inner circle
svg.append("text")
  .attr("dy", "-0.5em")
  .style("text-anchor", "middle")
  .attr("class", "inner-circle")
  .attr("fill", "#36454f")
  .text(function(d) { return 'Distribusi Data'; });

svg.append("text")
  .attr("dy", "1.0em")
  .style("text-anchor", "middle")
  .attr("class", "inner-circle")
  .attr("fill", "#36454f")
  .text(function(d) { return 'Marketplace'; });

// Wrap function to handle labels with longer text
function wrap(text, width) {
  text.each(function() {
    var text = d3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.1, // ems
        y = text.attr("y"),
        dy = parseFloat(text.attr("dy")),
        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
    console.log("tspan: " + tspan);
    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(" "));
      if (tspan.node().getComputedTextLength() > 90) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
      }
    }
  });
}
</script>
@endsection

@section('content')
<div class="container-fluid">
	<!-- <div class="col-12">
		<h3>Dashboard BPOM Crawler</h3>
		<hr>
	</div> -->
	@if(is_direktur() or is_admin())
		<div class="row">
			<!-- ./col -->
			<div class="col-lg-3 col-6">
				@include('utilities.stats', [
					'status' => 'info',
					'value' => $total_product_crawled,
					'message' => 'Total Product',
					'url' => url("/grab")
				])
			</div>

			<!-- ./col -->
			<div class="col-lg-3 col-6">
			<!-- small box -->
				@include('utilities.stats', [
					'status' => 'success',
					'value' => $total_crawled,
					'message' => 'Total Active Product',
					'url' => url("/reported-product/tllapangan")
				])
			</div>

			<!-- ./col -->
			<div class="col-lg-3 col-6">
			<!-- small box -->
				@include('utilities.stats', [
					'status' => 'warning',
					'value' => $total_rekomendasi_takedown,
					'message' => 'Rekomendasi Takedown',
					'url' => url("/reported-product/takedown")
				])
			</div>
			
			<!-- ./col -->
			<div class="col-lg-3 col-6">
			<!-- small box -->
				@include('utilities.stats', [
					'status' => 'danger',
					'value' => $total_to_review_stackholder,
					'message' => 'Review Stackholder',
					'url' => url("/reported-product/takedown")
				])
			</div>
			<!-- ./col -->
		</div>

		<div class="row">
			<div class="col-md-10">
				<div class="mb-10">
					<!-- /.info-box-content -->
				</div>
			</div>
		</div>
		
		<div class="row pt-0">
			<div class="col-md-12">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-4">
						<div class="info-box h-75 shadow-none">
							<span class="info-box-icon bg-info elevation-1">{{ $total_product_takedown }}</span>

							<div class="info-box-content">
								<span class="font-weight-bold d-block">Takedown</span>
								<span class="">
									Number of products that have been takedown
								</span>
							</div>
							<!-- /.info-box-content -->
						</div>
					<!-- /.info-box -->
					</div>

					<!-- ./col -->
					<div class="col-12 col-sm-6 col-md-4">
						<div class="info-box h-75 shadow-none">
							<span class="info-box-icon bg-danger elevation-1">{{ $total_takedown_upt }}</span>

							<div class="info-box-content">
								<span class="font-weight-bold d-block">Data from UPT</span>
								<span class="">Number of products from UPT</span>
							</div>
							<!-- /.info-box-content -->
						</div>
					<!-- /.info-box -->
					</div>

					<!-- ./col -->
					<div class="col-12 col-sm-6 col-md-4">
						<div class="info-box h-75 shadow-none">
							<span class="info-box-icon bg-danger elevation-1">{{ $total_feedback }}</span>

							<div class="info-box-content">
								<span class="font-weight-bold d-block">Feedback</span>
								<span class="">Number of Feedback</span>
							</div>
							<!-- /.info-box-content -->
						</div>
					<!-- /.info-box -->
					</div>
					<!-- ./col -->
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-md-9">
				<div class="card shadow-none">
					<div class="card-body">
						<div class="text-center mb-5">
							<h4>Total Product</h4>
							<span class="text-secondary">Group by Commodity</span>
						</div>

						<dl class="row">
							@foreach($total_product_by_commodities as $commodity)
								@php
									if($commodity['total'] > 0 && $total_product_crawled > 0) {
										$percentage = $commodity['total'] / $total_product_crawled * 100;
									} else {
										$percentage = 0;
									}
								@endphp
								<dt class="col-sm-4 col-md-2">{{ $commodity['name'] }}</dt>
								<dd class="col-sm-8 col-md-10 p-1">
									<div class="progress bg-transparent {{ $percentage !== 0 ? 'position-relative' : '' }}">
										<div 
											class="progress-bar rounded" 
											role="progressbar" 
											aria-valuenow="{{ $commodity['total'] }}" 
											aria-valuemin="0" 
											aria-valuemax="{{ $total_crawled }}" 
											style="margin-right: {{ $percentage === 0 ? 0 : '10px' }}; width: {{ $percentage }}%; background-color: {{ random_color($commodity['name']) }};">
										</div>
										<span class="text-dark justify-content-center d-flex position-absolute w-100" >{{ $commodity['total'] }}</span>
									</div>
								</dd>
							@endforeach
						</dl>
						
					</div>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="card shadow-none">
					<div class="card-body">
						<div class="text-center mb-5">
							<h4>Distribution Data</h4>
							<span class="text-secondary">Per Marketplace</span>
						</div>									
						
						<div class="content">
  
						<svg id="donut-chart"></svg>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	@else
	<div class="card shadow-none">
		<div class="card-body pt-0 mt-3">
			<div class="row pt-0">
				<div class="col-md-12 ">
					<button class="btn btn-primary mr-2 mt-2 active">
						TOKOPEDIA
					</button>
					<button class="btn btn-primary mr-2 mt-2">
						BUKALAPAK
					</button>
						<button class="btn btn-primary mr-2 mt-2">
						SHOPEE
					</button>
					<button class="btn btn-primary mr-2 mt-2">
						OLX
					</button>
					<button class="btn btn-primary mr-2 mt-2">
						BLIBLI
					</button>
					<button class="btn btn-primary mr-2 mt-2">
						ELEVENIA
					</button>
					<button class="btn btn-primary mr-2 mt-2">
						JDID
					</button>
					<button class="btn btn-primary mr-2 mt-2">
						LAZADA
					</button>
				</div>
			</div>
			<div class="row mt-3">
				<div class="col-md-12 mt-3">
					<h3 class="table-title">Tokopedia</h3>
				</div>
				<div class="col-6 col-md-6 col-sm-12">
					<table class="table table-striped datatable">
						<thead class="bg-primary">
							<tr>
								<td>No</td>
								<td>Komoditas</td>
								<td>Total</td>
								<td>Terakhir</td>
								<td width="1%">Warna</td>
							</tr>
						</thead>
					</table>
				</div>
				<div class="col-md-6 mt-5">
					<div class="card shadow-none">
						<div class="card-body">
							<div id="chart"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif
</div>
@include('utilities.filterHomeModal')
@endsection
 