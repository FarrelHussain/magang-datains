import glob
import os
import mysql.connector
import argparse
import requests
from mysql.connector import Error

class DBHelper(object):
    def __init__(self, host, user, password, database):
        self.host = host
        self.user = user
        self.password = password
        self.database = database
        self.connect_db()

    def connect_db(self):
        self.db_connection = mysql.connector.connect(
            host = self.host,
            user = self.user,
            passwd = self.password,
            database = self.database,
            auth_plugin='mysql_native_password'
        )

    def get_cursor(self):
        try:
            self.db_connection.ping(reconnect=True, attempts=3, delay=5)
        except mysql.connector.Error as err:
            if err.errno == 2055:
                self.connect_db()
            pass
        finally:
            return self.db_connection.cursor(buffered=True, dictionary=True)

    def get_marketplace(self, id_marketplace):
        query = "SELECT * FROM marketplace WHERE id_marketplace = %s;"
        try:
            cursor = self.get_cursor()
            cursor.execute(query, (id_marketplace,))
            result = cursor.fetchone()
            return result
        except Exception as e:
            print(e)
        finally:
            cursor.close()
    
    def get_commodities(self, id_commodity):
        query = "SELECT * FROM commodities WHERE id_commodity = %s;"
        try:
            cursor = self.get_cursor()
            cursor.execute(query, (id_commodity,))
            result = cursor.fetchone()
            return result
        except Exception as e:
            print(e)
        finally:
            cursor.close()

    def get_data_product(self, id_product):
        query = "SELECT * FROM data_product WHERE id_product = %s;"
        try:
            cursor = self.get_cursor()
            cursor.execute(query, (id_product,))
            result = cursor.fetchone()
            return result
        except Exception as e:
            print(e)
        finally:
            cursor.close()

    def get_keywords(self, id_keywords):
        query = "SELECT * FROM keywords WHERE id_keywords = %s;"
        try:
            cursor = self.get_cursor()
            cursor.execute(query, (id_keywords,))
            result = cursor.fetchone()
            return result
        except Exception as e:
            print(e)
        finally:
            cursor.close()

    def get_links(self, link):
        query = "SELECT * FROM links WHERE link = %s;"
        try:
            cursor = self.get_cursor()
            cursor.execute(query, (link,))
            result = cursor.fetchone()
            return result
        except Exception as e:
            print(e)
        finally:
            cursor.close()

    def get_offenses(self, id_offense):
        query = "SELECT * FROM offenses WHERE id_offense = %s;"
        try:
            cursor = self.get_cursor()
            cursor.execute(query, (id_offense,))
            result = cursor.fetchone()
            return result
        except Exception as e:
            print(e)
        finally:
            cursor.close()

    def get_products(self, id_product):
        query = "SELECT * FROM products WHERE id_product = %s;"
        try:
            cursor = self.get_cursor()
            cursor.execute(query, (id_product,))
            result = cursor.fetchone()
            return result
        except Exception as e:
            print(e)
        finally:
            cursor.close()

    def get_reported_products(self, id_link):
        query = "SELECT * FROM reported_products WHERE id_link = %s;"
        try:
            cursor = self.get_cursor()
            cursor.execute(query, (id_link,))
            result = cursor.fetchone()
            return result
        except Exception as e:
            print(e)
        finally:
            cursor.close()

    def get_status(self, id_status):
        query = "SELECT * FROM status WHERE id_status = %s;"
        try:
            cursor = self.get_cursor()
            cursor.execute(query, (id_status,))
            result = cursor.fetchone()
            return result
        except Exception as e:
            print(e)
        finally:
            cursor.close()

    def get_stores(self, id_store):
        query = "SELECT * FROM stores WHERE id_store = %s;"
        try:
            cursor = self.get_cursor()
            cursor.execute(query, (id_store,))
            result = cursor.fetchone()
            return result
        except Exception as e:
            print(e)
        finally:
            cursor.close()

    def get_users(self, username):
        query = "SELECT * FROM users WHERE username = %s;"
        try:
            cursor = self.get_cursor()
            cursor.execute(query, (username,))
            result = cursor.fetchone()
            return result
        except Exception as e:
            print(e)
        finally:
            cursor.close()

    def query_select(self, query):
        try:
            cursor = self.get_cursor()
            cursor.execute(query)
            results = cursor.fetchall()
            return results
        except Exception as e:
            print(e)
        finally:
            cursor.close()

    def query_insert(self, query):
        print(query)
        try:
            cursor = self.get_cursor()
            cursor.execute(query)
            self.db_connection.commit()
            return cursor.lastrowid
        except Exception as e:
            print(e)
        finally:
            cursor.close()

    def query_update(self, query):
        print(query)
        try:
            cursor = self.get_cursor()
            cursor.execute(query)
            return self.db_connection.commit()
        except Exception as e:
            print(e)
        finally:
            cursor.close()

def get_link_monitored():
    try:
        reported_products = db1.query_select("""
            SELECT L.*
            FROM `reported_products` RP
            INNER JOIN links L on L.id = RP.id_link
            INNER JOIN products P on P.id_product = L.id_product
            WHERE RP.`id_status` = '1' AND P.screenshot != ""
            ORDER BY `id_reported_product`
            LIMIT 5
            """)
        for reported_product in reported_products:
            dlinks = db2.get_links(reported_product['link'])
            if not dlinks:
                olink = db1.get_links(reported_product['link'])
                oproduct = db1.get_products(olink['id_product'])
                ostore = db1.get_stores(oproduct['id_store'])
                okeyword = db1.get_keywords(oproduct['id_keywords'])
                ocommodity = db1.get_commodities(oproduct['id_commodity'])
                odata_product = db1.get_data_product(oproduct['id_product'])
                oreported_product = db1.get_reported_products(reported_product['id'])
                omarketplace = db1.get_marketplace(ostore['id_marketplace'])
                duser = db2.get_users('admin')

                dcommodity = db2.get_commodities(oproduct['id_commodity'])
                id_commodity = ocommodity['id_commodity']
                if not dcommodity:
                    id_commodity = db2.query_insert("""INSERT INTO commodities (id_commodity, commodity) VALUES ({}, '{}')""".format(id_commodity, ocommodity['commodity']))

                insert_stores = db2.query_insert("""INSERT INTO stores(id_marketplace, name, crawled_from) VALUES ({}, '{}', '{}')""".format(omarketplace['id_marketplace'], ostore['name'], ostore['crawled_from']))
                insert_keyword = db2.query_insert("""INSERT INTO keywords(keywords, id_marketplace, id_commodity, status, max_product, therapy_class) VALUES('{}', {}, {}, {}, {}, '{}')""".format(okeyword['keywords'], omarketplace['id_marketplace'], id_commodity, okeyword['status'], okeyword['max_product'], okeyword['therapy_class']))
                insert_products = db2.query_insert("INSERT INTO products (id_store, id_keywords, id_commodity, name) VALUES({}, {}, {}, '{}')".format(insert_stores, insert_keyword, id_commodity, oproduct['name']))
                insert_data_product = db2.query_insert("""INSERT INTO data_product(id_product, crawled, keywords) VALUES({},'{}','{}')""".format(insert_products, odata_product['crawled'], odata_product['keywords']))
                insert_links = db2.query_insert("INSERT INTO links(id_product, id_status, id_offense, id_user, link, inserted_by) VALUES({},{},{},{},'{}','{}')".format(insert_products, olink['id_status'], olink['id_offense'], duser['id'], olink['link'], olink['inserted_by']))
                insert_reported_product = db2.query_insert("INSERT INTO reported_products(id_user, id_link, id_status, initial, filename, periode, notes) VALUES({},{},{},'{}','{}','{}', 'dev sync')".format(duser['id'], insert_links,oreported_product['id_status'], oreported_product['initial'] if oreported_product['initial'] else "", oreported_product['filename'] if oreported_product['filename'] else "", oreported_product['periode']))
                print(oproduct)
                if oproduct['screenshot']:
                    upload_screenshot("https://dev.esmart.datains.id/storage/screenshot/"+str(oproduct['screenshot']), str(insert_products))
            else:
                dreported_product = db2.get_reported_products(dlinks['id'])
                oreported_product = db1.get_reported_products(reported_product['id'])

                if not dreported_product:
                    insert_reported_product = db2.query_insert("INSERT INTO reported_products(id_user, id_link, id_status, initial, filename, periode, notes) VALUES({},{},{},'{}','{}','{}', 'dev sync')".format(dlinks['id_user'], dlinks['id'], oreported_product['id_status'], "", "", oreported_product['periode']))
                else:
                    update_reported_product = db2.query_update("UPDATE reported_products SET id_status = {}".format(oreported_product['id_status']))

    except Error as e:
        print("Error", e)

def upload_screenshot(img_src, id_product):
    url = "http://localhost/bpom/dashboard/public/api/product/"+id_product
    # url = "https://simomon.pom.go.id/api/product/"+id_product

    rsrc = requests.get(img_src)
    payload = {}
    files = [
        ('screenshot', rsrc.content)
    ]
    headers= {}

    response = requests.request("POST", url, headers=headers, data = payload, files = files)
    print(response.text.encode('utf8'))


if __name__ == "__main__":
    db1 = DBHelper(host='192.168.10.115', user='mplace', password='MpLAce@123!', database='esmart_d4bpom_new_new')
    db2 = DBHelper(host='localhost', user='root', password='FGTukEJmObNJk5uz', database='esmart_d4bpom_new')
    # db2 = DBHelper(host='43.254.124.30', user='mplace', password='MpLAce@123!', database='esmart_d4bpom')

    get_link_monitored()