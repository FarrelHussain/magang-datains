<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Marketplace;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth\ForgotPasswordController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes(['register' => false]);

Route::middleware(['auth'])->group(function () {
    Route::group(['prefix' => 'grab'], function () { 
        Route::get('/', 'GrabController@index');
        Route::post('/{action}', 'GrabController@store');
        Route::get('/data',  'GrabController@getData');
        Route::get('/data/mp-product-total',  'GrabController@getMpProductTotal');
        Route::get('/export',  'GrabController@export');
        Route::get('/show/{id}', 'GrabController@getData')->name('grab.show');
        Route::get('/offense', 'GrabController@getOffense');
        Route::get('/crawling_type', 'GrabController@getCrawling_type');
        Route::get('/product-list', 'GrabController@product_list');
        Route::get('/product-list/data/{store}', 'GrabController@product_list_data');
    });
    Route::group(['prefix' => 'home'], function () {
        Route::get('/', 'HomeController@index');
        Route::get('/getdata', 'HomeController@getData');
    });
    Route::group(['prefix' => 'crawling', 'middleware' => 'admin'], function () {
        Route::get('/', 'CrawlingController@index');
        Route::post('/', 'CrawlingController@store');
        Route::post('/search-store', 'CrawlingController@searchStore')->name('search-store');
    });

    Route::group(['prefix' => 'crawling'], function () {
        Route::get('/', 'CrawlingController@index');
        Route::post('/', 'CrawlingController@store');
        Route::post('/Crawling_Type/data', 'Crawling_TypeController@data');
        Route::post('/search-store', 'CrawlingController@searchStore')->name('search-store');
    });

    Route::group(['prefix' => 'keyword'], function () {
        Route::get('/', 'KeywordController@index');
        Route::get('/data', 'KeywordController@data');
        Route::post('/detail/{keyword}', 'KeywordController@detail_keywords');
        Route::put('/activation/{todo}/{keyword}/{index}', 'KeywordController@activation');

        Route::get('/rekapitulasi', 'RekapitulasiController')->name('keyword.rekapitulasi');
    });

    Route::group(['prefix' => 'reported-product'], function () {
        Route::get('/', 'ReportedProductController@index');
        Route::get('/data', 'ReportedProductController@data');

        Route::get('/draft-takedown', 'DraftTakedownController@index')->name('reported-product.draft-takedown.index');
        Route::match(['get', 'post'], '/draft-takedown/import', 'DraftTakedownController@import')
            ->name('reported-product.draft-takedown.import');
        Route::get('/draft-takedown/{reportCode}', 'DraftTakedownController@show')->name('reported-product.draft-takedown.show');
        Route::get('/draft-takedown/{reportCode}/submit', 'DraftTakedownController@submit')->name('reported-product.draft-takedown.submit');

        Route::group(['middleware' => 'role:koordinator'], function() {
            Route::get('/rekomendasi-takedown', 'Koordinator\RekomendasiTakedown@index')
                ->name('reported-product.rekomendasi-takedown');
            Route::get('/rekomendasi-takedown/{reportCode}/submit', 'Koordinator\RekomendasiTakedown@submit')
                ->name('reported-product.rekomendasi-takedown.submit');
            Route::post('/rekomendasi-takedown/{reportCode}/reject', 'Koordinator\RekomendasiTakedown@reject')
                ->name('reported-product.rekomendasi-takedown.reject');
            Route::get('/rekomendasi-takedown/{reportCode}', 'Koordinator\RekomendasiTakedown@show')
                ->name('reported-product.rekomendasi-takedown.show');
        });

        Route::group(['middleware' => 'role:direktur'], function() {
            Route::get('/drekomendasi-takedown', 'Direktur\RekomendasiTakedown@index')
                ->name('reported-product.direktur.rekomendasi-takedown');
            Route::get('/drekomendasi-takedown/{reportCodeVerified}', 'Direktur\RekomendasiTakedown@showKoordinator')
                ->name('reported-product.direktur.rekomendasi-takedown.show');
            Route::get('/drekomendasi-takedown/{reportCodeVerified}/approve', 'Direktur\RekomendasiTakedown@approve')
                ->name('reported-product.direktur.rekomendasi-takedown.approve');
            Route::get('/drekomendasi-takedown/{reportCodeVerified}/{reportCode}', 'Direktur\RekomendasiTakedown@showPetugas')
                    ->name('reported-product.direktur.rekomendasi-takedown.show-petugas');
                    
        });
        
        Route::get('/draft-takedown-detail/{id}', 'ReportedProductController@draftTakedownDetail');

        Route::get('/show/{id}', 'ReportedProductController@data');
        Route::get('/data-monitoring', 'ReportedProductController@dataMonitoring');

        Route::get('/takedown', 'ReportedProductController@takedownData')->name('takedown-data');
        Route::get('/takedown/import', 'ReportedProductController@importForm')->name('import-takedown-form');
        Route::post('/takedown/import', 'ReportedProductController@takedownImport')->name('import-takedown-data');
        Route::post('/takedown/{action}', 'ReportedProductController@doAction')->name('takedown-do-action');

        Route::get('/tllapangan', 'ReportedProductController@tllapanganData')->name('tllapangan');
        Route::post('/tllapangan/{action}', 'ReportedProductController@doAction');

        Route::get('/arsip', 'ReportedProductController@arsipData')->name('arsip');
        Route::post('/arsip/{action}', 'ReportedProductController@doAction');

        Route::get('/pembinaan', 'ReportedProductController@guidanceData')->name('guidance');
        Route::get('/pembinaan/{action}', 'ReportedProductController@doAction')->name('guidance-do-action');
    });

    Route::group(['prefix' => 'reports', 'role:direktur,koordinator'], function () {
        Route::get('/takedown', 'ReportController@takedown')->name('takedown');
        Route::get('/takedown/{reportCode}', 'ReportController@show_takedown')->name('reports.takedown.show-takedown');

        Route::get('/patrol', 'ReportController@patrol')->name('patrol');
        Route::get('/laporan-siber', 'ReportController@lapor_patrol')->name('patrol-data');
        Route::get('/feedback', 'ReportController@feedback')->name('feedback-data');
        Route::post('/saveFeedback', 'ReportController@saveFeedback')->name('feedback-save');

        Route::get('/monitoring', 'MonitoringController@index')->name('reports.monitoring');
        Route::get('/monitoring/data', 'MonitoringController@data')->name('reports.monitoring.data');
    });

    Route::group(['prefix' => 'export'], function () {
        Route::any('/arsip', 'ExportData@arsip')->name('export-arsip');
        Route::any('/takedown', 'ExportData@takedown')->name('export-takedown');
        Route::any('/tllapangan', 'ExportData@tllapangan')->name('export-tllapangan');
        Route::any('/guidance', 'ExportData@guidance')->name('export-guidance');
    });
    
    Route::group(['middleware' => ['role:super_admin,direktur']], function() {
        Route::get('user-activity', 'UserActivityController@index');
        Route::get('user-activity/get-data', 'UserActivityController@getData');
        Route::resource('user-management', 'UserManagementController')->except(['show', 'update', 'edit']);
        Route::get('user-management/{user}', 'UserManagementController@show');
        Route::put('user-management/{user}', 'UserManagementController@update');
        Route::get('commodity/data', 'CommodityController@data');
        Route::resource('commodity', 'CommodityController')->except(['show', 'edit', 'destroy']);      
    });

    Route::get('big-seller', 'BigSellerController@index')->middleware('admin')->name('big-seller');
    Route::get('big-seller/{keyword}', 'BigSellerController@getByKeyword')->name('big-seller.get-detail-by-keyword');
    Route::get('big-seller/data',  'BigSellerController@getData');
    Route::get('big-seller/data/mp-product-total',  'BigSellerController@getMpProductTotal');

    Route::get('/download', 'DownloadController')->name('download');
});


Route::get('/marketplace', function () {
    return Marketplace::all();
});

Route::post('/get-config-commodity', function (Request $request) {
    $commodities = config('app.commodities');
    return response($commodities);
});
Route::get('forget-password', [ForgotPasswordController::class, 'showForgetPasswordForm'])->name('forget.password.get');
Route::post('forget-password', [ForgotPasswordController::class, 'submitForgetPasswordForm'])->name('forget.password.post'); 
Route::get('reset-password/{token}', [ForgotPasswordController::class, 'showResetPasswordForm'])->name('reset.password.get');
Route::post('reset-password', [ForgotPasswordController::class, 'submitResetPasswordForm'])->name('reset.password.post');
// Route::get('/test', 'GrabController@getData');
Auth::routes();
//Home
// Route::get('user-management', [App\Http\Controllers\UserManagementController::class, 'index'])->name('user');
// Route::get('user-management/{user}', 'UserManagementController@show')->name('user show');
// Route::get('user-management/create', 'UserManagementController@create')->name('user create');
// Route::post('user-management/store', 'UserManagementController@store')->name('user store');
// Route::put('user-management/{user}', 'UserManagementController@update')->name('user update'); 