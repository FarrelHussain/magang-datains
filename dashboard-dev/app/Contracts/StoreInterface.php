<?php

namespace App\Contracts;

interface StoreInterface
{
    public function save(array $data);
}
