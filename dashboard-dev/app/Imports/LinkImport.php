<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;

class LinkImport implements ToCollection, WithValidation, WithHeadingRow
{
    use Importable;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    private $data;

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function array(array $row)
    {
        return [
            'nama_platform' => $row['nama_platform'],
            'nama_penjual' => $row['nama_penjual'],
            'url' => $row['url'],
            'jenis_komoditas' => $row['jenis_komoditas'],
            'jenis_pelanggaran' => $row['jenis_pelanggaran'],
            'nama_produk' => $row['nama_produk'],
            'kandungan' => $row['kandungan'],
            'produsen' => $row['produsen'],
            'golongan' => $row['golongan'],
            'kelas_terapi' => $row['kelas_terapi'],
            'periode' => $row['periode'],
            'asal_produk' => $row['asal_produk'],
            'petugas' => $row['petugas'],
        ];
    }

    public function collection(Collection $rows)
    {
        $return = [];
        return $return;
    }

    // DOESN'T WORK!! (it's a bug according to the github project)
    public function rules(): array
    {
        return [
            'periode' => 'required|date|date_format:Y-m-d',
            'ecommerce' => 'required|string|max:255|exists:marketplace,name',
            'toko' => 'required|string|max:255',
            'produk' => 'nullable|string|max:255',
            'komoditas' => 'required|string|max:255|exists:commodities,commodity',
            'waktu_input' => 'nullable|date|date_format:Y-m-d',
            'pelanggaran' => 'required|string|max:255|exists:offenses,offense',
            'note' => 'nullable|string|max:1000',
            'rekomendasi' => 'required|string|max:255|exists:status,status',
            'url' => 'required|url|unique:links,link'
        ];
    }
}
