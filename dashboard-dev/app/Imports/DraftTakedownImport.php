<?php

namespace App\Imports;

use App\Repositories\DraftTakedownRepository;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class DraftTakedownImport implements ToCollection, WithBatchInserts, WithHeadingRow, WithValidation
{
    use Importable;

    public $draftTakedownRepository;
    public function __construct(DraftTakedownRepository $draftTakedownRepository)
    {
        $this->draftTakedownRepository = $draftTakedownRepository;
    }

    public function collection(Collection $rows)
    {
        Validator::make($rows->toArray(), $this->rules())->validate();

        foreach ($rows as $value) {
            # code...
            $this->draftTakedownRepository->save_import($value);
        }
    }

    /**
     * Tweak the data slightly before sending it to the validator
     * @param $data
     * @return mixed
     */
    public function prepareForValidation($data)
    {
        $data['periode'] = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($data['periode'])->format('Y-m-d');

        return $data;
    }

    public function rules(): array
    {
        return [
            '*.jenis_platform' => 'required',
            '*.nama_platform' => 'required',
            '*.nama_penjual' => 'required',
            '*.url' => 'required',
            '*.jenis_komoditas' => 'required',
            '*.jenis_pelanggaran' => Rule::in([
                'TIE',
                'PTK',
                'PP',
                'TIEBB',
                'TIEBKO',
                'RPKG',
                'PLL'
            ]),
            '*.nama_produk' => 'required',
            // '*.kandungan' => 'required',
            // '*.produsen' => 'required',
            // '*.golongan' => 'required',
            // '*.kelas_terapi' => 'required',
            '*.periode' => 'required',
            '*.rekomendasi' => 'required',
            '*.status' => 'required',
            '*.petugas' => 'required',
            '*.terjual' => 'required',
            '*.harga' => 'required',
            '*.wilayah' => 'required',
            '*.unit_kerja' => 'required',
            '*.jenis_laporan' => Rule::in([
                'Patber Obat Covid19',
                'Patber NPP, OOT, Misoprostol',
                'Patber dari Laporan ULPK',
                'Patber dari Laporan Kedeputian I,II,III',
                'Patber Semua Komoditas',
                'Patber Bulanan dari UPT',
                'Patber Nodin Rahasia'
            ]),
        ];
    }
    
    public function batchSize(): int
    {
        return 200;
    }
    
    public function chunkSize(): int
    {
        return 200;
    }
}
