<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;

class MacroServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $addRowNumber = "table.on( 'order.dt search.dt draw.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();";

        Builder::macro('addDatains', function () {
            $tableId = $this->tableAttributes['id'];
            $this->attributes['autoFill']['columns'] = true;

            $buttons = isset($this->attributes['buttons']) && count($this->attributes['buttons']) > 0 ? 'B' : '';
            $this->attributes['dom'] = '<"row d-flex"<"col-md-2 mt-2"l>
                <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"'.$buttons.'f<p>>>t
                <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                <"bg-transparent"r>';

            $this->attributes['initComplete'] = "function() {
                const table = window['".config('datatables-html.namespace', 'LaravelDataTables')."']['".$tableId."']

                $('thead').on('click', '.select-checkbox', function(e) {
                    const checkbox = $('th.select-checkbox');

                    const rowChecked = table.column(-1).checkboxes.selected().length;
                    
                    if($('th.select-checkbox').hasClass('selected')) {
                        table.rows().deselect();
                        $('#".$tableId."_delete').attr('disabled', 'disabled');
                    } else {
                        if(rowChecked > 0) {
                            table.rows().deselect();
                            $('#".$tableId."_delete').attr('disabled', 'disabled');
                        } else {
                            table.rows().select();
                            $(this).removeClass('selected');
                            $('#".$tableId."_delete').removeAttr('disabled');
                        }
                    }
                });

                $('tbody').on('click', '.select-checkbox', function(e) {
                    const cellIndex = e.target._DT_CellIndex.row;
                    const rowSelected = $(e.target.parentElement);
                    
                    if($(rowSelected).hasClass('selected')) {
                        console.log('ROW DE-SELECTED')
                        table.rows(cellIndex).deselect();
                    } else {
                        console.log('ROW SELECTED')
                        table.rows(cellIndex).select();
                    }

                    const rowChecked = $('tr.selected').length;
                    if(rowChecked > 0) {
                        $('#".$tableId."_delete').removeAttr('disabled');
                    } else {                
                        $('#".$tableId."_delete').attr('disabled', 'disabled');
                    }
                });

                $('.dataTables_filter label').replaceWith(function(){
                    return $(this).contents();
                });
                $('.dataTables_filter').addClass('mr-2');

                $('#".$tableId."_delete').attr('disabled', 'disabled')
            }";
            $this->attributes['language']['processing'] = '<div class="align-center text-center overlay-loading">
                    <div class="overlay-loading-content">
                       <img src="/img/dual_ring_loading.svg" />
                    </div>
                </div>';

            // $this->attributes['buttons'][] = Button::make('create')->text("<i class='fas fa-plus'></i>")->attr(['id' => $tableId."_create"])->toArray();
            // $this->attributes['buttons'][] = Button::make('delete')->text("<i class='far fa-trash-alt'></i>")->attr(['id' => $tableId."_delete"])->toArray();
            return $this;
        });

        Builder::macro('changeDom', function ($buttons = []) {
            $tableId = $this->tableAttributes['id'];
            $this->attributes['language']['processing'] = '<div class="align-center text-center overlay-loading">
                    <div class="overlay-loading-content">
                       <img src="/img/dual_ring_loading.svg" />
                    </div>
                </div>';

            
            $this->attributes['dom'] = '<"row d-flex"<"col-md-2 mt-2"l>
                <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"f<p>>>t
                <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                <"bg-transparent"r>';

            $this->attributes['initComplete'] = "function() {
                $('.dataTables_filter label').replaceWith(function(){
                    return $(this).contents();
                });
                $('.dataTables_filter').addClass('mr-2');

                $('#".$tableId."_delete').attr('disabled', 'disabled')
            }";

            return $this;
        });

        Builder::macro('addSubmit', function () {
            $tableId = $this->tableAttributes['id'];
            
            $attributes = [
                'title'          => 'No',
                'data'           => 'id',
                'name'           => '',
                'orderable'      => false,
                'searchable'     => false,
                'printable' => true,
                'width' => '10px',
            ];
        
            $this->collection->prepend(new Column($attributes));
            $this->collection->prepend(new Column($attributes));

            $this->attributes['language']['processing'] = '<div class="align-center text-center overlay-loading">
                    <div class="overlay-loading-content">
                       <img src="/img/dual_ring_loading.svg" />
                    </div>
                </div>';

            $this->attributes['dom'] = '<"row d-flex"<"col-md-2 mt-2"l>
                <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"<"mr-2"f><p>>>t
                <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                <"mt-3"B>
                <"bg-transparent"r>';

           /* $this->attributes['columnDefs'][] = [
                "targets" => 0,
                "orderable" => false,
                "className" => 'select-checkbox',
                "checkboxes" => [
                    "selectRow" => true
                ]
            ];*/

            $this->attributes['initComplete'] = "function() {
                const table = window['".config('datatables-html.namespace', 'LaravelDataTables')."']['".$tableId."'];
                const submitButton = $('#".$tableId."_submit');
                const rejectButton = $('#".$tableId."_reject');

                $('thead').on('click', '.select-checkbox', function(e) {
                    const selectAll = $('th input[type=checkbox]');
                    if(selectAll.is(':checked')) {
                        table.rows().select();
                        submitButton.removeAttr('disabled');
                        rejectButton.removeAttr('disabled');
                    } else {
                        table.rows().deselect();
                        submitButton.attr('disabled', 'disabled');
                        rejectButton.attr('disabled', 'disabled');
                    }
                });

                $('tbody').on('click', 'input[type=checkbox]', function(e) {
                    const cellIndex = e.target.parentElement._DT_CellIndex.row;
                    const checkboxes = table.column(0).checkboxes.selected().length;

                    if($(this).is(':checked')) {
                        table.rows(cellIndex).select();
                    } else {
                        table.rows(cellIndex).deselect();
                    }

                    if(checkboxes > 0) {
                        submitButton.removeAttr('disabled');
                        rejectButton.removeAttr('disabled');
                    } else {                
                        submitButton.attr('disabled', 'disabled');
                        rejectButton.attr('disabled', 'disabled');
                    }
                });
                
                $('.dataTables_filter label').replaceWith(function(){
                    return $(this).contents();
                });

                const checkboxes = table.column(0).checkboxes.selected().length;
                if(checkboxes === 0) {
                    submitButton.attr('disabled', 'disabled');
                    rejectButton.attr('disabled', 'disabled');
                }

                table.on( 'order.dt search.dt draw.dt', function () {
                    table.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            }";
            
            $this->attributes['buttons'][] = Button::make('submit')->attr(['id' => $tableId."_submit"])->toArray();
            $this->attributes['buttons'][] = Button::make('reject')->attr(['id' => $tableId."_reject", 'data-toggle' => 'modal', 'data-target' => '#reject-modal'])->toArray();

            return $this;
        });

        Builder::macro('addRowNumber', function () use ($addRowNumber) {
            $attributes = [
                'title'          => 'No',
                'data'           => 'id',
                'name'           => '',
                'orderable'      => false,
                'searchable'     => false,
                'printable' => true,
                'width' => '10px',
            ];
        
            $this->collection->prepend(new Column($attributes));
            return $this;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
