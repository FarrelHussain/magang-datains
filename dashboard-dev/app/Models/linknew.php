<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class linknew extends Model
{
    protected $table = "links";
    protected $fillable =['id_status','updated_at','id_user','id_offense','note','id_report'];
}