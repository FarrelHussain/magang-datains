<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table = 'stores';
    protected $primaryKey = 'id_store';
    protected $guarded = ['id_store'];
    public $timestamps = true;


    public function marketplace()
    {
        return $this->belongsTo('App\Models\Marketplace', 'id_marketplace');
    }

    public function produk()
    {
        return $this->hasMany('App\Models\Product', 'id_store', 'id_store');
    }

    public static function scopeGetBiggestSeller(Builder $query, $marketplace = null): Builder
    {
        $query->join('products', 'stores.id_store', '=', 'products.id_store')
        ->join('data_product', 'data_product.id_product', '=', 'products.id_product')
        ->join('marketplace', 'marketplace.id_marketplace', '=', 'stores.id_marketplace')
        ->join('keywords', 'keywords.id_keywords', '=', 'products.id_keywords')
        ->groupBy('stores.id_store')
        ->orderBy('sold_count', 'desc')
        ->selectRaw('
            keywords.keywords,
            stores.id_store,
            stores.name as store,
            marketplace.name as marketplace,
            COUNT(DISTINCT products.id_product) as product_total,
            MAX(data_product.view_count) as view_count,
            MAX(data_product.sold_count) as sold_count,
            CASE WHEN stores.followers IS NULL THEN 0 ELSE stores.followers END as followers_count
        ');

        if ($marketplace) {
            $query->where('marketplace.id_marketplace', $marketplace);
        }

        return $query;
    }
}
