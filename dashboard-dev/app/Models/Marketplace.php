<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Marketplace extends Model
{
    protected $table = 'marketplace';
    protected $primaryKey = 'id_marketplace';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function stores()
    {
        return $this->hasMany('App\Models\Store', 'id_marketplace');
    }

    public static function scopeGetAllRelations($q)
    {
        return
            $q
            ->join('stores', 'marketplace.id_marketplace', '=', 'stores.id_marketplace')
            ->join('products', 'stores.id_store', '=', 'products.id_store')
            ->join('data_product', 'products.id_product', '=', 'data_product.id_product')
            ->join('keywords', 'products.id_keywords', '=', 'keywords.id_keywords')
            ->join('commodities', 'products.id_commodity', '=', 'commodities.id_commodity');
    }

    public static function scopeHomeData($q, $marketplace = 'blibli')
    {
        $all_data = self::getAllRelations();

        return
            $all_data
            ->selectRaw(
                "
                COUNT(products.id_product) as total,
                max(data_product.crawled) as crawled_date,
                commodities.commodity, commodities.id_commodity

                "
            )
            ->when(is_petugas() || is_petugas_loka() || is_petugas_balai(),function($petugas)
            {
                $petugas->where('keywords.id_user','=',auth()->user()->id);
            }
            )
            ->groupBy('products.id_commodity')
            ->where('marketplace.name', $marketplace)
            ->orderBy('crawled_date', 'desc');
    }

    public function getLogoAttribute($value)
    {
        if ($value == null) {
            $value = 'toOccourAnErrorIfYouThinkThisIsNotEffectiveJustChangeIt.png';
        }
        $value = asset("img/$value");

        return $value;
    }

    public function scopeCount()
    {
        $select = is_admin() || is_direktur()
                    ? "sum(if(links.id_status = 1, 1, 0)) as total" 
                    : "sum(if(links.id_status = 1 AND links.id_user = ".auth()->user()->id.", 1, 0)) as total";
        
        return self::selectRaw("
                marketplace.name,
                $select
            ")
            ->leftJoin('stores', 'stores.id_marketplace', '=', 'marketplace.id_marketplace')
            ->leftJoin('products', 'products.id_store', '=', 'stores.id_store')
            ->leftJoin('links', 'links.id_product', '=', 'products.id_product')
            ->when(!is_admin() || !is_direktur(), function($query) {
                $query->leftJoin('users', 'users.id', '=', 'links.id_user');
            })
            ->whereNotIn('marketplace.name', ['twitter', 'facebook', 'instagram', 'youtube', 'tiktok'])
            ->groupBy('marketplace.name');
    }

    public function scopeOnlyMarketplace()
    {
        return self::where('marketplace.name', '!=', 'blanja')
            ->whereNotIn('marketplace.name', ['twitter', 'facebook', 'instagram', 'youtube', 'tiktok'])
            ->orderBy('name', 'asc');
    }
}
