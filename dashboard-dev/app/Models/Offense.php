<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offense extends Model
{
    use SoftDeletes;

    protected $table = 'offenses';
    protected $primaryKey = 'id_offense';

    protected $guarded = ['id_offense'];
    public $timestamps = true;
}
