<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrawlingHistory extends Model
{
    protected $table = 'crawling_histories';
    protected $guarded = ['id'];

    public function keywords()
    {
        $this->belongsTo(Keyword::class, 'id_keywords');
    }
}
