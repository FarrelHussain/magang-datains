<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'reports';
    protected $primaryKey = 'id_report';
    protected $guarded = ['id_report'];
    public $timestamps = true;
}
