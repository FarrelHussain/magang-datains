<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';
    protected $primaryKey = 'id_status';

    protected $guarded = ['id_status'];
    public $timestamps = true;


    public function links()
    {
        return $this->hasMany('App\Models\Link', 'id_status');
    }
}
