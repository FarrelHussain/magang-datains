<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ReportedProduct extends Model
{
    protected $table = 'reported_products';
    protected $primaryKey = 'id_reported_product';
    public $timestamps = true;
    protected $guarded = ['id_reported_product'];


    public function link()
    {
        return $this->belongsTo('App\Models\Link', 'id_link');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Status', 'id_status');
    }

    public function crawling_type()
    {
        return $this->belongsTo('App\Models\CrawlingType', 'report_type');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }

    public static function scopeGetFormatedData($q)
    {
        $data = self::with(['link.status', 'link.product.dataProduct', 'link.product.store.marketplace']);
        return $data;
    }

    public static function scopeGetReportedProducts()
    {
        return self::leftJoin('links', 'links.id', '=', 'reported_products.id_link')
            ->leftJoin('status as link_stat', 'links.id_status', '=', 'link_stat.id_status')
            ->leftJoin('status as reported_product_status', 'reported_products.id_status', '=', 'reported_product_status.id_status')
            ->leftJoin('products', 'links.id_product', '=', 'products.id_product')
            ->leftJoin('data_product', 'products.id_product', '=', 'data_product.id_product')
            ->leftJoin('keywords', 'keywords.id_keywords', '=', 'products.id_keywords')
            ->leftJoin('stores', 'stores.id_store', '=', 'products.id_store')
            ->leftJoin('marketplace', 'marketplace.id_marketplace', 'stores.id_marketplace')
            ->leftJoin('commodities', 'products.id_commodity', '=', 'commodities.id_commodity')
            ->leftJoin('offenses', 'offenses.id_offense', '=', 'links.id_offense')
            ->selectRaw(
                "
                links.link as product_link, links.id as link_id,
                reported_products.id_reported_product, reported_products.updated_at as last_visit,
                reported_product_status.status as report_status,
                products.id_product, products.name as product_name, products.description as product_desc, products.note as product_note, data_product.price as product_price,
                data_product.*, data_product.keywords as product_keywords,
                stores.name as store_name, stores.url as store_url, stores.description as store_desc, stores.*, marketplace.name as marketplace_name,
                keywords.keywords,
                links.created_at as link_date_create,
                links.updated_at as link_date_update,
                products.picture as product_picture,
                marketplace.*, marketplace.logo as mp_logo,
                commodities.commodity as commodity, offenses.offense, offenses.acronym, reported_products.periode,
                products.id_commodity, links.created_at = (SELECT min(created_at) from links ) as grab_start, links.created_at = (SELECT max(created_at) from links) as grab_end
            "
            )->where('links.id_status', config('app.status.takedown.id_status'));
    }

    public function scopeGetTakedownReports($q)
    {
        $result = $q->select('reported_products.submitted_at as tgs','crawling_type.type as jc',
            'status.status as status','reported_products.report_code as kode')
            ->join('links','links.id','=','reported_products.id_link')
            ->join('status','status.id_status','=','reported_products.id_status')
            ->join('crawling_type','id_crawling_type', '=', 'reported_products.report_type')
            ->orderBy('reported_products.id_reported_product','DESC');
        return $result;
    }

    public function scopeGetTakedownProducts($q, $id_report = null)
    {
        $result = $q->selectRaw("
            reported_products.periode, reported_products.id_reported_product,
            commodities.commodity,
            products.name as product_name,
            stores.name as store_name, reported_products.updated_at as last_visit, stores.url as store_url,
            stores.location as store_location,
            marketplace.name as mp_name, marketplace.logo as mp_logo,
            status.status,
            offenses.acronym,
            links.id as link_id,
            users.name as users_name
        ")
            ->leftJoin('status', 'reported_products.id_status', '=', 'status.id_status')
            ->leftJoin('links', 'reported_products.id_link', '=', 'links.id')
            ->leftJoin('offenses', 'links.id_offense', '=', 'offenses.id_offense')
            ->leftJoin('products', 'products.id_product', '=', 'links.id_product')
            ->leftJoin('commodities', 'products.id_commodity', '=', 'commodities.id_commodity')
            ->leftJoin('stores', 'stores.id_store', '=', 'products.id_store')
            ->leftJoin('marketplace', 'marketplace.id_marketplace', 'stores.id_marketplace')
            ->leftJoin('users', 'reported_products.id_user', '=', 'users.id')
            ->when(is_kepala_balai(), function($query) {
                $query->where('reported_products.approved_by', '=', auth()->user()->id)
                    ->where('users.id_upt_location', '=', auth()->user()->id_upt_location);
            })
            ->when(is_petugas() || is_petugas_loka() || is_petugas_balai(), function($query) {
                $query->whereNotNull('reported_products.approved_by')
                    ->where('reported_products.id_user', '=', auth()->user()->id);
            })
            ->when(is_koordinator_balai() || is_koordinator(), function($query) {
                $query->whereNotNull('reported_products.approved_by')
                    ->where('users.id_upt_location', '=', auth()->user()->id_upt_location);
            });

            return $result->where('links.id_status', '=', config('app.status.takedown.id_status'));
    }

    public function scopeGetDraftTakedown(Builder $q)
    {
        return $q->select('periode', 'crawling_type.type as report_type')
            ->join('crawling_type', 'reported_products.report_type', '=', 'crawling_type.id_crawling_type')
            ->where('id_status', '=', config('app.status.drafttakedown.id_status'))
            ->when(!is_admin(), function($query) {
                $query->where('id_user', '=', auth()->user()->id);
            })
            ->when(is_admin(), function($query) {
                $query->select('periode', 'crawling_type.type as report_type', 'users.name as proposed_by')
                    ->join('users', 'users.id', '=', 'reported_products.id_user');
            })
            ->groupBy('periode', 'report_type');
    }

    public static function scopeExport($q)
    {
        return self::leftJoin('links', 'links.id', '=', 'reported_products.id_link')
            ->leftJoin('status as status_link', 'status_link.id_status', '=', 'status_link.id_status')
            ->leftJoin('status as reported_product_status', 'reported_products.id_status', '=', 'reported_product_status.id_status')
            ->leftJoin('products', 'links.id_product', '=', 'products.id_product')
            // ->leftJoin("( SELECT manufacturer, id_product FROM data_product GROUP BY manufacturer, id_product ) data_product", 'products.id_product', '=', 'data_product.id_product')
            ->leftJoin(DB::raw("( SELECT manufacturer, id_product FROM data_product GROUP BY manufacturer, id_product ) as data_product"), 'products.id_product', '=', 'data_product.id_product')
            ->leftJoin('keywords', 'keywords.id_keywords', '=', 'products.id_keywords')
            ->leftJoin('stores', 'stores.id_store', '=', 'products.id_store')
            ->leftJoin('marketplace', 'marketplace.id_marketplace', 'stores.id_marketplace')
            ->leftJoin('commodities', 'products.id_commodity', '=', 'commodities.id_commodity')
            ->leftJoin('offenses', 'offenses.id_offense', '=', 'links.id_offense')
            ->leftJoin('users', 'reported_products.id_user', '=', 'users.id')
            ->selectRaw(
                "
                reported_products.id_reported_product as id_takedown, reported_products.initial,  marketplace.name as marketplace, stores.name as store, links.link as url, links.created_at as link_date_create,
                commodities.commodity, offenses.offense, case when keywords.keywords is NULL THEN products.name else keywords.keywords end as product_name, keywords.content as kandungan, keywords.group as golongan, keywords.therapy_class as kelas_terapi,
                status_link.status as recomendation, products.created_at as input_time, users.id as id_user, keywords.product_origin, reported_products.filename as file,
                reported_product_status.status as rp_status, products.screenshot as screenshot, reported_products.periode, data_product.manufacturer
            "
            );
    }

    public function scopeGetMonitoredProducts($q)
    {
        return $q->selectRaw("
            reported_products.periode, reported_products.id_reported_product,
            commodities.commodity,
            products.name as product_name,
            stores.name as store_name, reported_products.updated_at as last_visit, stores.url as store_url,
            marketplace.name as mp_name, marketplace.logo as mp_logo,
            status.status,
            offenses.acronym,
            links.id as link_id
        ")
            ->leftJoin('status', 'reported_products.id_status', '=', 'status.id_status')
            ->leftJoin('links', 'reported_products.id_link', '=', 'links.id')
            ->leftJoin('offenses', 'links.id_offense', '=', 'offenses.id_offense')
            ->leftJoin('products', 'products.id_product', '=', 'links.id_product')
            ->leftJoin('commodities', 'products.id_commodity', '=', 'commodities.id_commodity')
            ->leftJoin('stores', 'stores.id_store', '=', 'products.id_store')
            ->leftJoin('marketplace', 'marketplace.id_marketplace', 'stores.id_marketplace')
            ->where('reported_products.notes', '!=', 'null')
            ->where('links.id_status', '=', config('app.status.takedown.id_status'));
    }

    public function scopeGetPreviousQueueBy(Builder $q)
    {
        DB::statement("SET @queue:=0;");
        $queue = $q->select(DB::raw("@queue:=@queue+1 AS queue"), DB::raw("count(report_code)"))
            ->whereNotNull('report_code')
            ->where(DB::raw('DATE(periode)'), '=', date('Y-m-d'))
            ->groupBy(DB::raw('DATE(created_at)'), 'report_code')
            ->orderBy(DB::raw("queue"), 'desc')
            ->limit(1)
            ->get();

        if ($queue->count() === 0) {
            return 1;
        }
        return $queue[0]->queue + 1;
    }

    public function scopeChangeToSubmittedByReportCode(Builder $q, string $reportCode)
    {
        $q->where('report_code', '=', $reportCode)
            ->update([
                'submitted_at' => date('Y-m-d')
            ]);
    }
    
    public function scopeGetReportCode()
    {
        $lastProposedQueue = self::getPreviousQueueBy();
        $dataPosition = is_petugas() || is_petugas_loka() || is_petugas_balai() 
                ? 'A' : (is_koordinator() || is_koordinator_balai() || is_kepala_loka() ? 'B' : 'X');

        $submissionType = 'TD';
        return sprintf("%02d", auth()->user()->id)
            .'-'.date('Ymd')
            .'-'.sprintf("%02d", $lastProposedQueue)
            .'-'.$dataPosition.'-'
            .$submissionType;
    }

    public function scopeSetApprovedByKoordinator(Builder $q, string $reportCode)
    {
        $q->where('report_code', '=', $reportCode)
            ->update([
                    'report_code_verified' => self::getReportCode(),
                    'rejected_reason' => null
                ]);
    }

    public function scopeSetRejectedByKoordinator(Builder $q, string $reportCode)
    {
        $q->where('report_code', '=', $reportCode)
            ->update([
                    'submitted_at' => null,
                    'rejected_reason' => request()->message
                ]);
    }

    public function scopeSetApprovedByDirektur(Builder $q, string $reportCode)
    {
        Link::join('reported_products', 'reported_products.id_link', '=', 'links.id')
            ->where('reported_products.report_code_verified', '=', $reportCode)
            ->update([
                'links.id_status'   => config('app.status.takedown.id_status')
            ]);
        
        $q->where('report_code_verified', '=', $reportCode)
            ->update([
                    'approved_at' => date('Y-m-d'),
                    'approved_by' => auth()->user()->id,
                    'id_status'   => config('app.status.takedown.id_status')
            ]);
    }
}
