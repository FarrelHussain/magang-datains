<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataProduct extends Model
{
    protected $table = 'data_product';
    protected $primaryKey = 'id_data_product';
    public $timestamps = true;
    protected $dates = [
        'crawled',
    ];
    protected $guarded = ['id_data_product'];

    public function produk()
    {
        return $this->belongsTo('App\Models\Product', 'id_product');
    }

    public function getProductPictureAttribute()
    {
        $value = $this->product_picture;
        if ($value == null) {
            return asset("img/noimage.png");
        }
        return $value;
    }
}
