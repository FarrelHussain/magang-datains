<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Upt extends Model
{
    protected $guarded = ['id_upt_location'];
    protected $table = 'upt_locations';
    protected $primaryKey = 'id_upt_location';
    public $timestamps = true;
}
