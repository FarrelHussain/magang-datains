<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CrawlingType extends Model
{ 
    protected $table = 'crawling_type';
    protected $primaryKey = 'id_crawling_type';

    protected $guarded = ['id_crawling_type'];
    public $timestamps = true;
}
