<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Keyword extends Model
{
    protected $table = 'keywords';
    protected $primaryKey = 'id_keywords';
    protected $guarded = ['id_keywords'];
    public $timestamps = true;
    protected $casts = [
        'status' => 'boolean',
    ];

    public function marketplace()
    {
        return $this->belongsTo(Marketplace::class, 'id_marketplace');
    }
    public function commodity()
    {
        return $this->belongsTo(Commodity::class, 'id_commodity');
    }

    public function histories()
    {
        return $this->hasMany(CrawlingHistory::class, 'id_keywords');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'id_keywords');
    }

    public static function scopeWithDetail($q, $where = false)
    {
        $data = $q
            ->selectRaw('
                    COUNT(data_product.id_data_product) as total,
                    keywords.keywords as words,
                    (SELECT status from keywords where keywords in (words)) as active,
                    (SELECT keywords.id_keywords from keywords where keywords in (words)) as id,
                    commodities.commodity

                ')
            ->leftJoin('data_product', 'keywords.keywords', 'data_product.keywords')
            ->leftJoin('commodities', 'keywords.id_commodity', 'commodities.id_commodity');

        if ($where) {
            $data->whereRaw($where);
        }

        return $data->groupBy('keywords.keywords')->get();
    }

    public function scopeGetDetailKeywords($q, $where = false)
    {
        $data =  $q
            ->selectRaw("
                COUNT(products.id_product) as total,
                keywords.id_keywords as id,
                keywords.max_product, keywords.product_total,
                marketplace.name as marketplace_name,
                marketplace.logo as mp_logo,crawling_type.type as crawling_type,
                (SELECT keywords.keywords from keywords where keywords.id_keywords in (id)) as words,
                (SELECT keywords.status from keywords where keywords.id_keywords in (id)) as active,
                commodities.commodity
            ")
            ->leftJoin('products', 'products.id_keywords',  'keywords.id_keywords')
            ->leftJoin('marketplace', 'keywords.id_marketplace', 'marketplace.id_marketplace')
            ->leftJoin('commodities', 'keywords.id_commodity', 'commodities.id_commodity')
            ->leftJoin('crawling_type', 'keywords.id_crawling_type',  'crawling_type.id_crawling_type')
            ->where('status', '!=', '5')
            ->where('keywords.id_user', auth()->user()->id);

        if ($where) {
            $data->whereRaw($where);
        }

        if (request()->commodities) {
            $data->whereIn('keywords.id_commodity', request()->commodities);
        }

        if (request()->start_time) {
            $data->where('keywords.created_at', '>=', date(request()->start_time).' 00:00:00');
        }

        if (request()->end_time) {
            $data->where('keywords.created_at', '<=', date(request()->end_time).' 00:00:00');
        }

        if (request()->reported_product_status && request()->reported_product_status !== 'all') {
            $status = request()->reported_product_status === 'active' ? 1 : 0;
            $data->where('keywords.status', '=', $status);
        }
        
        return $data->groupBy('keywords.id_keywords');
    }

    public function getMaxProductAttribute($value)
    {
        if ($value == "-1") {
            $value = "Semua";
        }
        return $value;
    }

    public static function scopeTopStores($q, $id)
    {
        return $q->selectRaw(
            '
                stores.id_store as store,
                stores.name,
                stores.url,
                stores.last_update,
                stores.seller_rate,
                SUM(data_product.sold_count) as count,
                COUNT(products.id_product) as total
            '
        )
            ->leftJoin('products', 'keywords.id_keywords', 'products.id_keywords')
            ->leftJoin('data_product', 'products.id_product', 'data_product.id_product')
            ->leftJoin('stores', 'products.id_store', 'stores.id_store')
            ->orderBy('count', 'desc')
            ->where('keywords.id_keywords', $id)
            ->groupBy('stores.id_store')
            ->limit(3)->get();
    }

    public function getRecapitulation()
    {
        $commodities = Commodity::select('commodity as name', 'id_commodity')->get();
        $select = [];
        foreach ($commodities as $commodity) {
            if (is_admin()) {
                $select[] = "sum(if(c.id_commodity = $commodity->id_commodity, 1, 0)) as '$commodity->name'";
            } else {
                $select[] = "sum(if(c.id_commodity = $commodity->id_commodity and links.id_user = ".auth()->user()->id.", 1, 0)) as '$commodity->name'";
            }
        }

        $product_count = DB::raw("(
            SELECT 
                m.id_marketplace, 
                sum(if(links.id_status != 5, 1, 0)) as 'Total Produk Aktif', 
                sum(if(links.id_status = 5, 1, 0)) as 'Total Produk Tidak Aktif'
            FROM links 
                join products p on p.id_product = links.id_product 
                join keywords k on k.id_keywords = p.id_keywords
                join marketplace m on m.id_marketplace = k.id_marketplace
            GROUP BY k.id_marketplace) product_count"
        );

        if (!is_admin()) {
            $product_count = DB::raw("(
                SELECT 
                    m.id_marketplace, 
                    sum(if(links.id_status != 5 and links.id_user = ".auth()->user()->id.", 1, 0)) as 'Total Produk Aktif', 
                    sum(if(links.id_status = 5 and links.id_user = ".auth()->user()->id.", 1, 0)) as 'Total Produk Tidak Aktif'
                FROM links 
                    join products p on p.id_product = links.id_product 
                    join keywords k on k.id_keywords = p.id_keywords
                    join marketplace m on m.id_marketplace = k.id_marketplace
                GROUP BY k.id_marketplace) product_count"
            );
        }

        $commodity_count = DB::raw("(
            SELECT 
                m.id_marketplace,
                ".implode(',', $select)."
            
            FROM links
                join products p on p.id_product = links.id_product 
                join keywords k on k.id_keywords = p.id_keywords
                join commodities c on c.id_commodity = k.id_commodity
                join marketplace m on m.id_marketplace = k.id_marketplace
            GROUP BY k.id_marketplace
        ) commodity_count");
        
        return self::select('marketplace.name', 'commodity_count.*',  'product_count.*')
                ->rightjoin('marketplace', 'marketplace.id_marketplace', 'keywords.id_marketplace')
                ->leftjoin($product_count, function($join) {
                    $join->on('product_count.id_marketplace', '=', 'marketplace.id_marketplace');
                })
                ->leftjoin($commodity_count, function($join) {
                    $join->on('commodity_count.id_marketplace', '=', 'marketplace.id_marketplace');
                })
                ->whereNotIn('marketplace.name', ['twitter', 'facebook', 'instagram', 'youtube', 'tiktok'])
                ->groupBy('marketplace.name')
                ->get()
                ->toArray();
    }

    public function scopeGetTotalPatroliSiber()
    {
        $commodities = [config('app.commodities.medicine.id_commodity').",".config('app.commodities.nappza.id_commodity')];
        return self::select('keywords.content as content','keywords.therapy_class as TC',
            'marketplace.name as name','marketplace.id_marketplace as idm')
            ->join('marketplace','keywords.id_marketplace','=','marketplace.id_marketplace')
            ->join('products','keywords.id_keywords','=','products.id_keywords')
            ->join('links','products.id_product','=','links.id_product')
            ->where('links.id_status','=', config('app.status.takedown.id_status'))
            ->whereIn('keywords.id_commodity', $commodities)
            ->groupBY('keywords.content','keywords.therapy_class',
            'marketplace.name','marketplace.id_marketplace')
            ->get();
    }
}