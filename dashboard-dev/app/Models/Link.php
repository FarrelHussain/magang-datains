<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    public $timestamps = true;
    protected $guarded = ['id'];


    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'id_produk');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Status', 'id_status');
    }

    public function reported_product()
    {
        return $this->hasOne('App\Models\ReportedProduct', 'id_link');
    }

    public function scopeGetActiveLink($q)
    {
        return self::leftJoin('status', 'links.id_status', '=', 'status.id_status')
            ->leftJoin('products', 'links.id_product', '=', 'products.id_product')
            ->leftJoin('data_product', 'products.id_product', '=', 'data_product.id_product')
            ->leftJoin('keywords', 'keywords.id_keywords', '=', 'products.id_keywords')
            ->leftJoin('stores', 'stores.id_store', '=', 'products.id_store')
            ->leftJoin('marketplace', 'marketplace.id_marketplace', 'stores.id_marketplace')
            ->leftJoin('commodities', 'products.id_commodity', '=', 'commodities.id_commodity')
            //->leftJoin('reports', 'reports.id_report', '=', 'links.id_report')
            ->selectRaw(
                "
                links.link as product_link, links.id as link_id, links.created_at as sort_link,
                status.status as report_status,links.id_report as id_report,
                products.id_product, products.name as product_name, products.description as product_desc, products.note as product_note, data_product.price as product_price,
                data_product.*, data_product.keywords as product_keywords,
                stores.name as store_name, stores.url as store_url, stores.description as store_desc, stores.*, marketplace.name as marketplace_name,
                keywords.keywords, products.picture as product_picture, marketplace.*, marketplace.logo as mp_logo, commodities.*
            "
            );
    }
    public function scopeGetArsip($q)
    {
        return self::leftJoin('status', 'links.id_status', '=', 'status.id_status')
            ->leftJoin('products', 'links.id_product', '=', 'products.id_product')
            ->leftJoin('data_product', 'products.id_product', '=', 'data_product.id_product')
            ->leftJoin('keywords', 'keywords.id_keywords', '=', 'products.id_keywords')
            ->leftJoin('stores', 'stores.id_store', '=', 'products.id_store')
            ->leftJoin('marketplace', 'marketplace.id_marketplace', 'stores.id_marketplace')
            ->leftJoin('commodities', 'products.id_commodity', '=', 'commodities.id_commodity')
           // ->leftJoin('reports', 'reports.id_report', '=', 'links.id_report')
            ->selectRaw(
                "
                links.link as product_link, links.id as link_id, links.created_at as sort_link,
                status.status as report_status,links.id_report as id_report,
                products.id_product, products.name as product_name, products.description as product_desc, products.note as product_note, data_product.price as product_price,
                data_product.*, data_product.keywords as product_keywords,
                stores.name as store_name, stores.url as store_url, stores.description as store_desc, stores.*, marketplace.name as marketplace_name,
                keywords.keywords, products.picture as product_picture, marketplace.*, marketplace.logo as mp_logo, commodities.*
            "
            );
    }

    public function scopeGetMonitoring()
    {
        $commodities = request()->commodities;
        $select = "
            count(links.id_product) as crawling,
            sum(if(id_status = 1, 1, 0)) as active,
            sum(if(id_status = 2, 1, 0)) as takedown,
            round(sum(if(id_status = 2, 1, 0)) / count(links.id_product) * 100, 2) as percentage
        ";

        if (!is_admin()) {
            $select = "
                sum(if(links.id_user = ".auth()->user()->id.", 1, 0)) as crawling,
                sum(if(id_status = 1 and links.id_user = ".auth()->user()->id.", 1, 0)) as active,
                sum(if(id_status = 2 and links.id_user = ".auth()->user()->id.", 1, 0)) as takedown,
                round(sum(if(id_status = 2 and links.id_user = ".auth()->user()->id.", 1, 0)) / count(links.id_product) * 100, 2) as percentage
            ";
        }
        return self::selectRaw("marketplace.name,$select")
            ->join('products', 'products.id_product', '=', 'links.id_product')
            ->join('stores', 'stores.id_store', '=', 'products.id_store')
            ->rightJoin('marketplace', 'marketplace.id_marketplace', '=', 'stores.id_marketplace')
            ->when($commodities, function($query, $commodities) {
                $query->whereIn('products.id_commodity', explode(',', $commodities));
            })
            ->groupBy('marketplace.name');
    }
}
