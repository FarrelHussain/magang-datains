<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class feedback extends Model
{
    protected $table = "reported_products";
    protected $fillable =['report_feedback','official_memo','feedbacked_at'];
}
