<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'id_product';



    public function store()
    {
        return $this->belongsTo('App\Models\Store', 'id_store');
    }

    public function dataProduct()
    {
        return $this->hasOne('App\Models\DataProduct', 'id_product');
    }

    public function commodity()
    {
        return $this->belongsTo(Commodity::class, 'id_commodity');
    }

    public function link()
    {
        return $this->hasOne('App\Models\Link', 'id_product');
    }

    public function scopeTotal($query)
    {
        return $query->selectRaw('COUNT(id_product) as total_product')->groupBy('id_store');
    }

    public function keywords()
    {
        return $this->belongsTo('App\Models\Keyword', 'id_keywords');
    }

    public static function scopeGetReportedProducts_($q)
    {
        return self::with(['dataProduct', 'link.status', 'link.reported_product', 'store.marketplace']);
    }

    public function scopeGetArsip($q)
    {
        return self::leftJoin('links', 'products.id_product', '=', 'links.id_product')
            ->leftJoin('status', 'links.id_status', '=', 'status.id_status')
            ->leftJoin('data_product', 'products.id_product', '=', 'data_product.id_product')
            ->leftJoin('stores', 'stores.id_store', 'products.id_store')
            ->leftJoin('marketplace', 'marketplace.id_marketplace', 'stores.id_marketplace')
            ->leftJoin('offenses', 'offenses.id_offense', '=', 'links.id_offense')
            ->leftJoin('commodities', 'products.id_commodity', '=', 'commodities.id_commodity')
            ->selectRaw(
                "
                links.link as product_link, links.id as link_id,
                status.status as report_status,
                products.id_product, products.name as product_name, products.description as product_desc, products.note as product_note, data_product.price as product_price,
                data_product.sold_count,data_product.item_count, data_product.rating,data_product.keywords as product_keywords,
                stores.name as store_name, stores.url as store_url, stores.description as store_desc, marketplace.name as marketplace_name, stores.id_store,

                links.created_at as link_date_create,
                links.updated_at as link_date_update,
                products.picture as product_picture,
                marketplace.logo as mp_logo,
                commodities.commodity as commodity, offenses.offense, offenses.acronym,
                products.id_commodity, links.created_at = (SELECT min(created_at) from links ) as grab_start, links.created_at = (SELECT max(created_at) from links) as grab_end
            "
            );
    }

    public static function scopeGetGrabProducts($q)
    {
        return self::leftJoin('links', 'products.id_product', '=', 'links.id_product')
            ->leftJoin('status', 'links.id_status', '=', 'status.id_status')
            ->leftJoin('data_product', 'products.id_product', '=', 'data_product.id_product')
            ->leftJoin('stores', 'stores.id_store', 'products.id_store')
            ->leftJoin('marketplace', 'marketplace.id_marketplace', 'stores.id_marketplace')
            ->leftJoin('keywords', 'products.id_keywords', '=', 'keywords.id_keywords')
            ->leftJoin('commodities', 'products.id_commodity', '=', 'commodities.id_commodity')
            ->selectRaw(
                "
                links.link as product_link, links.id as link_id,
                status.status as report_status, products.name as product_name, products.description as product_desc, products.note as product_note, data_product.price as product_price, data_product.sold_count, data_product.item_count, data_product.rating, data_product.crawled, keywords.keywords as product_keywords,
                stores.name as store_name, stores.url as store_url, stores.description as store_desc, stores.id_store,marketplace.name as marketplace_name,
                products.picture as product_picture,
                marketplace.logo as mp_logo,
                commodities.commodity as commodity
            "
            );
    }

    public function scopeGrabProducts($query, Request $request)
    {
        $query->selectRaw(
            "
            products.id_product as prodid,
            products.name as product, products.id_commodity as cmdty_id, products.id_store as store_id,
            products.picture as picture,
            keywords.keywords,
            links.id as link_id,
            links.created_at as crawled,

            (SELECT stores.location FROM  stores WHERE id_store = store_id LIMIT 1) as store_location,
            (SELECT MAX(d.sold_count) FROM data_product d WHERE d.id_product = products.id_product LIMIT 1) as sold_count,
            (SELECT stores.id_marketplace FROM stores WHERE id_store = store_id LIMIT 1) as mp_id,
            (SELECT data_product.price FROM data_product WHERE id_data_product = (SELECT MAX(id_data_product) FROM data_product WHERE data_product.id_product = prodid)) as price,
            (SELECT commodity FROM commodities WHERE id_commodity = cmdty_id LIMIT 1) as commodity,
            (SELECT marketplace.logo FROM marketplace WHERE id_marketplace = (SELECT id_marketplace FROM marketplace WHERE marketplace.id_marketplace = mp_id LIMIT 1)) as logo,
            (SELECT marketplace.name FROM marketplace WHERE id_marketplace = (SELECT id_marketplace FROM marketplace WHERE marketplace.id_marketplace = mp_id LIMIT 1)) as mp
        "
        )
            ->leftJoin('stores', 'stores.id_store', '=', 'products.id_store')
            ->leftJoin('marketplace', 'stores.id_marketplace', '=', 'marketplace.id_marketplace')
            ->join('links', 'links.id_product', '=', 'products.id_product')
            ->leftJoin('keywords', 'keywords.id_keywords', '=', 'products.id_keywords')
            ->when($request->mp, function($query, $marketplace){
                if ($marketplace !== 'all') {
                    $query->where('marketplace.name', '=', $marketplace);
                }
            })
            ->when($request->s_store, function ($q) use ($request) {
                $q->whereRaw("(SELECT stores.name FROM stores WHERE id_store = products.id_store LIMIT 1) LIKE '%{$request->s_store}%'");
            })
            ->when($request->s_product_name, function ($q) use ($request) {
                $q->where('products.name', 'LIKE', "%{$request->s_product_name}%");
            })
            ->when($request->s_content, function ($q) use ($request) {
                $q->where('keywords.content', 'LIKE', "%{$request->s_content}%");
            })
            ->when($request->s_therapy_class, function ($q) use ($request) {
                $q->where('keywords.therapy_class', 'LIKE', "%{$request->s_therapy_class}%");
            })
            ->when($request->s_group, function ($q) use ($request) {
                $q->where('keywords.group', 'LIKE', "%{$request->s_group}%");
            })
            ->when($request->s_keywords, function ($q) use ($request) {
                $q->where('keywords.keywords', 'LIKE', "%{$request->s_keywords}%");
            })
            ->when($request->s_commodity, function ($q) use ($request) {
                $q->where('products.id_commodity', '=', "{$request->s_commodity}");
            })
            ->when($request->s_location, function ($q) use ($request) {
                $q->whereRaw("(SELECT stores.location FROM stores WHERE id_store = products.id_store LIMIT 1) = '{$request->s_location}'");
            })
            ->when($request->s_price_min, function ($q) use ($request) {
                $q->whereRaw("(SELECT data_product.price FROM data_product WHERE id_product = products.id_product LIMIT 1) >= {$request->s_price_min}");
            })
            ->when($request->s_price_max, function ($q) use ($request) {
                $q->whereRaw("(SELECT data_product.price FROM data_product WHERE id_product = products.id_product LIMIT 1) <= {$request->s_price_max}");
            })
            ->when($request->s_marketplace, function ($q) use ($request) {
                $mp = $request->s_marketplace;
                $q->whereRaw("(SELECT stores.id_marketplace FROM stores WHERE id_store = products.id_store LIMIT 1) = {$mp}");
            })
            ->when($request->start_time && $request->end_time, function($q) use ($request) {
                $dateFrom =  date_create($request->start_time)->format('Y-m-d H:i:s');
                $dateEnd = $request->start_time ?  date_add(date_create($request->end_time), date_interval_create_from_date_string("1 day"))->format('Y-m-d') : '3000-10-01 00:00:00';
                $q->whereBetween('links.created_at', [$dateFrom, $dateEnd]);
            })
            ->when($request->commodities, function($query) use ($request) {
                $query->whereIn('products.id_commodity', $request->commodities);
            })
            ->where('links.id_status', '=', config('app.status.active.id_status'))
            ->when(!is_admin(), function($query) {
                $query->where('keywords.id_user', '=', auth()->user()->id);
            });
        return $query;
    }



    public function scopeCountMpProduct($q)
    {
        return $q->selectRaw("
            count('products.id_product') as total
            ")
            ->join('stores', 'stores.id_store', '=', 'products.id_store')
            ->join('links', 'links.id_product', '=', 'products.id_product')
            ->join('keywords', 'keywords.id_keywords', '=', 'products.id_keywords')
            ->rightJoin('marketplace', 'stores.id_marketplace', '=', 'marketplace.id_marketplace')
            ->where('links.id_status', '=', config('app.status.active.id_status'))
            ->when(!is_admin(), function($query) {
                $query->where('keywords.id_user', '=', auth()->user()->id);
            })
            ->where('marketplace.name','<>','blanja');
    }

    public static function scopeExport($q)
    {
        return self::leftJoin('links', 'products.id_product', '=', 'links.id_product')
            ->leftJoin('status', 'links.id_status', '=', 'status.id_status')
            ->leftJoin('data_product', 'products.id_product', '=', 'data_product.id_product')
            ->leftJoin('stores', 'stores.id_store', 'products.id_store')
            ->leftJoin('marketplace', 'marketplace.id_marketplace', 'stores.id_marketplace')
            ->leftJoin('offenses', 'offenses.id_offense', '=', 'links.id_offense')
            ->leftJoin('commodities', 'products.id_commodity', '=', 'commodities.id_commodity')
            ->leftJoin('users', 'links.id_user', '=', 'users.id')
            ->leftJoin('keywords', 'products.id_keywords', '=', 'keywords.id_keywords')
            ->selectRaw(
                "
                marketplace.name as marketplace, links.created_at as periode_start, stores.name as store, links.link as url,
                status.status as recomendation, products.screenshot as screenshot, data_product.crawled as input_time, keywords.product_origin, keywords.content as kandungan, keywords.group as golongan, keywords.therapy_class as kelas_terapi, keywords.keywords as product_name,
                commodities.commodity, offenses.offense,  users.id as id_user, data_product.manufacturer
            "
            );
    }

    public function scopeGetTotalProductByCommodity(Builder $query): array {
        return $query->selectRaw('commodities.commodity as name, count(products.id_product) as total')
            ->rightJoin('commodities', 'commodities.id_commodity', '=', 'products.id_commodity')
            ->orderBy('commodities.commodity', 'asc')
            ->groupBy('commodities.commodity')
            ->get()
            ->toArray();
    }

    public function scopeGetTotalProductCrawled(Builder $query)
    {
        return $query->get()->count();
    }

    public function scopeGetTotalCrawled() {
        $marketplace = Marketplace::count()->get()->toArray();
        return array_sum(array_column($marketplace, 'total'));
    }

    public function scopeStats(Builder $query, int $status): int {
        return $query->selectRaw('count(links.id) as total')
            ->join('links', 'links.id_product', '=', 'products.id_product')
            ->join('reported_products', 'reported_products.id_link', '=', 'links.id')
            ->when($status, function($query) use ($status) {
                switch ($status) {
                    case ''.config('app.status.takedown.id_status').'':
                        $query->where('links.id_status', '=', 2);
                        break;
                    case 90:
                        $query->where('reported_products.id_status', config('app.status.takedown.id_status'));
                        $query->whereNull('reported_products.report_feedback');
                        $query->whereNull('reported_products.feedbacked_at');
                        break;
                    case 91:
                        $query->whereNotNull('reported_products.report_feedback');
                        $query->whereNotNull('reported_products.feedbacked_at');
                        break;
                    case 92:
                        $idUpts = Upt::selectRaw('id_upt_location')->where('name', '!=', 'Nasional')->get();
                        $idUsers = User::selectRaw('id')->whereIn('id_upt_location', $idUpts)->get();
                        $query->whereIn('reported_products.id_user',$idUsers);
                        $query->whereNotNull('reported_products.approved_at');
                        $query->whereNotNull('reported_products.approved_by');
                        break;
                    case 10:
                        $query->where('reported_products.id_status', $status);
                        break;
                    case ''.config('app.status.drafttakedown.id_status').'':
                        $query->where('reported_products.id_status', $status);
                        $query->whereNotNull('reported_products.submitted_at');
                    default:
                        $query->where('reported_products.id_status', $status);
                }
            })
            ->get()
            ->first()
            ->total;
    }
}
