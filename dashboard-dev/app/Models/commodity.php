<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

class Commodity extends Model
{
    use LogsActivity;

    protected $guarded = ['id_commodity'];
    protected $table = 'commodities';
    protected $primaryKey = 'id_commodity';
    public $timestamps = true;

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->properties = [
            "ip_address" => request()->ip()
        ];
        $activity->description = "log.${eventName}";
    }

    public static function getCommoditiesName()
    {
        $commodities = Cache::remember('commodities', 3600, function() {
            return self::select('commodity as name', 'id_commodity')
                    ->orderBy('commodity', 'asc')
                    ->get()
                    ->toArray();
        });

        return $commodities;
    }
}
