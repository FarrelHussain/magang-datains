<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cratype extends Model
{
    protected $table = "reports";
    protected $fillable =['kode_report','id_user','id_status','jenis_laporan','tgl_submit','notadinas','tgl_approve','feedback','tgl_feedback'];
}
 