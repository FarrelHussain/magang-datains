<?php 

namespace App\Repositories;

use App\Models\DataProduct;

class DataProductRepository
{
    public $dataProduct;
    public function __construct(DataProduct $dataProduct)
    {
        $this->dataProduct = $dataProduct;    
    }

    public function save(array $data)
    {
        $dataProduct = $this->dataProduct->firstOrCreate($data);
        
        return $dataProduct->id_data_product;
    }
}
