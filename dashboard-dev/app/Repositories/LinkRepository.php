<?php

namespace App\Repositories;

use App\Models\Link;

class LinkRepository
{
    public $link;
    public function __construct(Link $link)
    {
        $this->link = $link;
    }

    public function save(array $data)
    {
        $link = $this->findOneBy(['link' => $data['link']]);

        if (!is_null($link)) {
            return $link;
        }

        $link = new $this->link;
        
        foreach ($data as $key => $value) {
            $link->{$key} = $value;
        }

        $link->save();

        return $link;
    }

    public function findOneBy(array $where, $limit = null)
    {
        return $this->link->where($where)->limit($limit)->get()->first();
    }
}
