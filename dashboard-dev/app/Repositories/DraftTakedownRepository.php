<?php

namespace App\Repositories;

use App\Models\Commodity;
use App\Models\CrawlingType;
use App\Models\Offense;
use App\Models\ReportedProduct;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use InvalidArgumentException;

class DraftTakedownRepository 
{
    private $storeRepository;
    private $marketplaceRepository;
    private $productRepository;
    private $dataProductRepository;
    private $linkRepository;
    private $reportedProductRepository;

    public function __construct(
        ReportedProductRepository $reportedProductRepository, 
        StoreRepository $storeRepository, 
        MarketplaceRepository $marketplaceRepository, 
        ProductRepository $productRepository,
        DataProductRepository $dataProductRepository,
        LinkRepository $linkRepository
    )
    {
        $this->reportedProductRepository = $reportedProductRepository;
        $this->storeRepository = $storeRepository;
        $this->marketplaceRepository = $marketplaceRepository;
        $this->productRepository = $productRepository;
        $this->dataProductRepository = $dataProductRepository;
        $this->linkRepository = $linkRepository;
    }

    public function save_import(Collection $data)
    {
        DB::transaction(function() use ($data) {
            $marketplace = $this->marketplaceRepository->findOneBy(['name' => $data->get('nama_platform')]);
            $storeUrl = $this->storeRepository->buildUrl($data->get('nama_platform'), $data->get('nama_penjual'));
            $store = $this->storeRepository->save([
                'id_marketplace' => $marketplace->id_marketplace,
                'url' => $storeUrl,
                'name' => $data->get('nama_penjual'),
            ]);

            $commodity = Commodity::where(['commodity' => $data->get('jenis_komoditas')])->get()->first();
            $product = $this->productRepository->save([
                'id_store' => $store->id_store,
                'id_keywords' => NULL,
                'id_commodity' => $commodity->id_commodity,
                'name' =>  $data->get('nama_produk'),
                'url' => $data->get('url')
            ]);

            $this->dataProductRepository->save([
                'id_product' => $product->id_product,
                'price' => $data->get('harga'),
            ]);

            $offenseType = Offense::where(['acronym' => $data->get('jenis_pelanggaran')])->get()->first();
            $link = $this->linkRepository->save([
                'link' => $data->get('url'),
                'id_product' => $product->id_product,
                'id_user' => auth()->user()->id,
                'id_status' => config('app.status.drafttakedown.id_status'),
                'inserted_by' => 'Import',
                'id_offense' => $offenseType->id_offense
            ]);

            $reportType = CrawlingType::where(['type' => $data->get('jenis_laporan')])->get()->first();
            if (is_null($reportType)) {
                throw new InvalidArgumentException("Jenis Laporan '".$data->get('jenis_laporan')."' tidak tersedia", 1);
            }

            $this->reportedProductRepository->save([
                'id_user' => auth()->user()->id,
                'id_link' => $link->id,
                'id_status' => config('app.status.drafttakedown.id_status'),
                'periode' => $data->get('periode'),
                'report_code' => ReportedProduct::getReportCode(),
                'report_type' => !is_null($reportType) ? $reportType->id_crawling_type : null,
            ]);

        });
    }
}
