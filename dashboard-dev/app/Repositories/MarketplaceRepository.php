<?php 

namespace App\Repositories;

use App\Models\Marketplace;

class MarketplaceRepository
{
    public $marketplace;
    public function __construct(Marketplace $marketplace)
    {
        $this->marketplace = $marketplace;
    }

    public function save()
    {
        $marketplace = new $this->marketplace;
    }

    public function udpate()
    {

    }

    public function findBy(array $where, $limit = null)
    {
        return $this->marketplace->where($where)->limit($limit)->get();
    }

    public function findOneBy(array $where, $limit = null)
    {
        return $this->marketplace->where($where)->limit($limit)->get()->first();
    }
}
