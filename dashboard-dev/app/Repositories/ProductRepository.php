<?php 

namespace App\Repositories;

use App\Models\Product;

class ProductRepository
{
    public $product;
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function save(array $data)
    {
        $product = $this->findOneBy(['url' => $data['url']]);

        if (!is_null($product)) {
            return $product;
        }

        $product = new $this->product;
        
        foreach ($data as $key => $value) {
            $product->{$key} = $value;
        }

        $product->save();

        return $product;
    }


    public function findOneBy(array $where, $limit = null)
    {
        return $this->product->where($where)->limit($limit)->get()->first();
    }

}
