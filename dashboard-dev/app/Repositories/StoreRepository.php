<?php


namespace App\Repositories;

use App\Models\Store;

class StoreRepository
{
    const BLIBLI = 'blibli';
    const BUKALAPAK = 'bukalapak';
    const ELEVENIA = 'elevenia';
    const JDID = 'jdid';
    const LAZADA = 'lazada';
    const TOKOPEDIA = 'tokopedia';
    const SHOPEE = 'shopee';
    const OLX = 'olx';
    const TWITTER = 'twitter';
    const FACEBOOK = 'facebook';
    const INSTAGRAM = 'instagram';
    const YOUTUBE = 'youtube';
    const TIKTOK = 'tiktok';

    public $store;
    public function __construct(Store $store)
    {
        $this->store = $store;
    }

    public function save(array $data)
    {
        $store = $this->findOneBy(['name' => $data['name'], 'url' => $data['url']]);

        if (!is_null($store)) {
            return $store;
        }

        $store = new $this->store;
        
        foreach ($data as $key => $value) {
            $store->{$key} = $value;
        }

        $store->save();

        return $store;
    }

    public function update()
    {
        
    }

    public function findOneBy(array $where, $limit = null)
    {
        return $this->store->where($where)->limit($limit)->get()->first();
    }

    public function buildUrl(string $marketplace, string $store)
    {
        switch (strtolower($marketplace)) {
            case strtolower(self::BLIBLI):
                return "$store";
            case strtolower(self::BUKALAPAK):
                return "https://www.bukalapak.com/u/$store";
            case strtolower(self::ELEVENIA):
                return "https://www.elevenia.co.id/store/$store";
            case strtolower(self::JDID):
                // https://www.jd.id/shop/Good-lucky_10006174.html
                return "$store";
            case strtolower(self::LAZADA):
                return "https://www.lazada.co.id/shop/$store";
            case strtolower(self::TOKOPEDIA):
                return "https://www.tokopedia.com/$store";
            case strtolower(self::OLX):
                return "https://www.olx.co.id/profile/$store";
            case strtolower(self::TWITTER):
                return "https://twitter.com/$store";
            case strtolower(self::FACEBOOK):
                return "https://www.facebook.com/$store";
            case strtolower(self::INSTAGRAM):
                return "https://www.instagram.com/$store";
            case strtolower(self::YOUTUBE):
                return "https://www.youtube.com/channel/$store";
            case strtolower(self::TIKTOK):
                return "https://www.tiktok.com/@$store";
        }
        // 
    }
}
