<?php

namespace App\Repositories;

use App\Models\CrawlingType;
use App\Models\ReportedProduct;
use Illuminate\Support\Facades\DB;

class ReportedProductRepository
{
    public $reportedProduct;
    public function __construct(ReportedProduct $reportedProduct)
    {
        $this->reportedProduct = $reportedProduct;
    }

    public function save(array $data)
    {
        $reportedProduct = $this->findOneBy(['id_link' => $data['id_link']]);

        if (!is_null($reportedProduct)) {
            return $reportedProduct;
        }

        $reportedProduct = new $this->reportedProduct;
        
        foreach ($data as $key => $value) {
            $reportedProduct->{$key} = $value;
        }

        $reportedProduct->save();

        return $reportedProduct;
    }

    public function findOneBy(array $where, $limit = null)
    {
        return $this->reportedProduct->where($where)->limit($limit)->get()->first();
    }
    
    public function get_takedown_submission()
    {
        return $this->reportedProduct
            ->select('reported_products.submitted_at as tgs','crawling_type.type as jc',
            'status.status as status','reported_products.report_code as kode')
            ->join('links','links.id','=','reported_products.id_link')
            ->join('status','status.id_status','=','reported_products.id_status')
            ->join('crawling_type','id_crawling_type', '=', 'reported_products.report_type')
            ->orderBy('reported_products.id_reported_product','DESC')
            ->get();
    }

    public function get_filter_data()
    {
        $crawlingType = CrawlingType::get();
        $status = array_values(config('app.status'));

        return [
            'crawling_type' => $crawlingType,
            'status' => $status
        ];
    }

    public function get_product_by_report_code(array $reportCode) {
        
        DB::statement("SET @number:=0;");
        $result = $this->reportedProduct
            ->select(
                DB::raw("@number:=@number+1 AS no"),
                'reported_products.report_code',
                'reported_products.periode as periode',
                'commodities.commodity as commodity_name', 
                DB::raw('CONCAT(products.name, "|", products.url)'),
                DB::raw('CONCAT(stores.name, "|",stores.url)'),
                'marketplace.name as marketplace_name',
                'crawling_type.type as report_type'
            )
            ->join('crawling_type', 'crawling_type.id_crawling_type', '=', 'reported_products.report_type')
            ->join('links', 'links.id', '=', 'reported_products.id_link')
            ->join('products', 'products.id_product', '=', 'links.id_product')
            ->join('stores', 'stores.id_store', '=', 'products.id_store')
            ->join('marketplace', 'marketplace.id_marketplace', '=', 'stores.id_marketplace')
            ->join('commodities', 'commodities.id_commodity', '=', 'products.id_commodity')
            ->whereIn('report_code', $reportCode)
            ->orderBy('reported_products.created_at', 'asc')
            ->get();

        return $result;
    }
}
