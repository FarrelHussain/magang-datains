<?php

namespace App\Http\Controllers;

use App\DataTables\BigSellerByKeywordDatatable;
use App\DataTables\BigSellerDatatable;
use App\Models\Commodity;
use Illuminate\Http\Request;
use App\Models\Link;
use App\Models\Marketplace;
use App\Models\Offense;
use App\Models\Product;
use App\Models\ReportedProduct;
use App\Models\Store;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\Facades\DataTables;

class BigSellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BigSellerDatatable $dataTable)
    {
        // $data['marketplaces'] = Marketplace::where('name', '<>','blanja')->get();
        // $data['offenses'] = Offense::get();
        // $data['commodities'] = Commodity::orderBy('commodity', 'asc')->get();
        // return view('big_seller', $data);

        return $dataTable->render('big-seller.index');
    }

    public function getByKeyword(BigSellerByKeywordDatatable $dataTable)
    {
        return $dataTable->render('big-seller.detail');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $action)
    {
        $request->validate([
            'id' => "required|min:1"
        ]);
        $response = ['success' => true, 'code' => 200, 'message'];
        $status = config('app.status');
        $status = isset($status[$action]) ? $status[$action] : abort(400);
        try {
            $updateLinkTo = [
                'id_status' => $status['id_status'],
                'updated_at' => date('Y-m-d H:i:s'),
                'id_user' => auth()->user()->id
            ];

            if (($status['id_status'] == config('app.status.tllapangan.id_status')) || ($status['id_status'] == config('app.status.takedown.id_status')) || ($status['id_status'] == config('app.status.guidance.id_status'))) {
                $request->validate([
                    'id_offense' => 'required|numeric'
                ]);
                $updateLinkTo['id_offense'] = $request->id_offense;
                $updateLinkTo['note'] = $request->note;
            }

            Link::whereIn('id', $request->id)->update($updateLinkTo);

            if ($status['id_status'] == config('app.status.takedown.id_status')) {

                $dataLink = [];
                foreach ($request->id as $key => $value) {
                    $dataLink[] = [
                        'id_link' => $value,
                        'created_at' => date('Y-m-d H:i:s'),
                        'id_user' => auth()->user()->id,
                        'id_status' => config('app.status.active.id_status'),
                        'periode' => date('Y-m-d H:i:s'),
                    ];
                }
                ReportedProduct::insert($dataLink);
            }

            $response['message'] = "Proses $action berhasil dilakukan!";
        } catch (Exception $e) {
            $response = ['success' => false, 'code' => 500, 'message' => $e->getMessage()];
        }
        return response($response, $response['code']);
    }

    public function getData(Request $request)
    {
        if (!$request->ajax()) {
            return abort(400);
        }

        $store = Store::GetBiggestSeller($request->input('mp'));
        return DataTables::eloquent($store)
            ->editColumn('store', function($query) {
                return ucwords($query->store);
            })
            ->editColumn('marketplace', function($query) {
                $marketplace = $query->marketplace;
                return "<img"
                    ." src=".asset('img/'.$marketplace.'.png')
                    ." class='rounded mx-auto d-block'"
                    ." style='max-width:45px'"
                    ." onerror='replcaceImageWithHTML(this, '${marketplace}')' />";
            })
            ->rawColumns(['marketplace'])
            ->toJson();
    }

    public function getOffense(Request $request)
    {
        $key = $request->session()->token();
        if ($request->has($key) and ($request->input($key) === $key) and $request->ajax()) {
            $offenses = Offense::get();
            return response(['code' => 200, 'success' => true, 'message' => $offenses], 200);
        }
    }

    private function setAdvanceSearch(Builder $data, Request $request)
    {


        return $data;
    }

    public function product_list(Request $request)
    {
        $store = Store::where('id_store', $request->store)->firstOrFail();

        return view('product_list', compact('store'));
    }

    public function product_list_data(Request $request, Store $store)
    {
        if ($request->ajax()) {
            $store = Product::where('id_store', $store->id_store)
                ->leftJoin('data_product', 'data_product.id_product', 'products.id_product')
                ->leftJoin('keywords', 'keywords.id_keywords', 'products.id_keywords')
                ->leftJoin('commodities', 'commodities.id_commodity', 'products.id_commodity')
                ->leftJoin('links', 'links.id_product', 'products.id_product')
                ->leftJoin('status', 'status.id_status', 'links.id_status')
                ->selectRaw('products.name, data_product.price, data_product.item_count, commodities.commodity, keywords.keywords, links.link, status.status');
            return DataTables::eloquent($store)
                ->addIndexColumn()
                ->toJson();
        }
    }

    public function getMpProductTotal(Request $request)
    {
        if ($request->getTotalMarketplace == true) {
            $data = [];
            $mp = Marketplace::get();
            foreach ($mp as $key => $value) {
                $count = Product::countMpProduct()->where('marketplace.id_marketplace', $value->id_marketplace)->where('marketplace.name','<>','blanja')->first();
                $data[$key] = ['total' => $count['total'], 'marketplace' => $value->name, 'id_marketplace' => $value->id_marketplace];
            }
            return response()->json(['data' => $data], 200);
        } else {
            abort(400, "INVALID");
        }
    }
}
