<?php

namespace App\Http\Controllers;

use App\Models\Commodity;
use App\Models\Keyword;
use App\Models\KeywordsRecordView;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class KeywordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['commodities'] = Commodity::orderBy('commodity', 'asc')->get();
        return view('keyword_v', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forms.form_keyword');
    }

    public function data(Request $request)
    {
        $data = Keyword::getDetailKeywords();
        // dd($data->toSql());
        return DataTables::eloquent($data)
            ->addIndexColumn()
            ->order(function ($q) use ($request) {
                switch ($request->order[0]['column']) {
                    case '0':
                        $q->orderBy('keywords.created_at', 'desc')->orderBy('keywords.id_keywords', 'desc');
                        break;
                    case '1':
                        $q->orderBy('keywords.keywords', $request->order[0]['dir']);
                        break;
                    case '2':
                        $q->orderBy('commodities.commodity', $request->order[0]['dir']);
                        break;
                    case '3':
                        $q->orderBy('marketplace.name', $request->order[0]['dir']);
                        break;
                    case '4':
                        $q->orderBy('keywords.product_total', $request->order[0]['dir']);
                        break;
                    case '5':
                        $q->orderBy('keywords.max_product', $request->order[0]['dir']);
                        break;
                    case '6':
                        $q->orderBy('total', $request->order[0]['dir']);
                        break;
                    case '7':
                        $q->orderBy('keywords.status', $request->order[0]['dir']);
                        break;
                    default:
                        $q->orderBy('keywords.created_at', 'desc')->orderBy('keywords.id_keywords', 'desc');
                        break;
                }
            })
            ->toJson();
    }

    public function activation(Request $request, $todo, Keyword $keyword, $index)
    {
        $response = [
            'success' => false,
            'message' => 'Yeaa di prank!!',
            'code' => 500
        ];

        if ($request->ajax()) {
            switch ($todo) {
                case 'activate':
                    try {
                        $client = new Client();
                        $keywordsToSend = Keyword::with(['marketplace', 'commodity'])->where('id_keywords', $keyword->id_keywords)->first();
                        $response = $client->request('GET', config('app.crawler_url')."/active/".$keywordsToSend->marketplace->id_marketplace."/".$keyword->id_keywords);
                        $keyword->status = true;
                        $keyword->save();
                        $keyword = Keyword::getDetailKeywords('keywords.id_keywords = ' . $keyword->id_keywords)->first();
                        $keyword->DT_RowIndex = $index;
                        $response = [
                            'success' => true,
                            'message' => 'Berhasil mengaktifkan keyword ',
                            'code' => 200,
                            'data' => $keyword
                        ];
                                        
                        activity_log('log.activate', [
                            'keyword' => $keywordsToSend->keywords
                        ]);
                    } catch (\Exception $th) {
                        $response['message'] = $th->getMessage();
                    }
                    break;
                case 'deactivate':
                    try {
                        $client = new Client();
                        $keywordsToSend = Keyword::with(['marketplace', 'commodity'])->where('id_keywords', $keyword->id_keywords)->first();
                        $response = $client->request('GET', config('app.crawler_url')."/deactivate/".$keywordsToSend->marketplace->id_marketplace."/".$keyword->id_keywords);
                        $keyword->status = false;
                        $keyword->save();
                        $keyword = Keyword::getDetailKeywords('keywords.id_keywords = ' . $keyword->id_keywords)->first();
                        $keyword->DT_RowIndex = $index;
                        $response = [
                            'success' => true,
                            'message' => 'Berhasil menonaktifkan keyword',
                            'code' => 200,
                            'data' => $keyword
                        ];
                                  
                        activity_log('log.deactivate', [
                            'keyword' => $keyword->words
                        ]);
                    } catch (\Exception $th) {
                        $response['message'] = $th->getMessage();
                    }
                    break;
                default:

                    break;
            }
            return response($response, $response['code']);
        }
    }

    public function detail_keywords(Request $request, Keyword $keyword)
    {
        $history = KeywordsRecordView::where('id_keywords', $keyword->id_keywords)
            ->orderBy('created_at', 'desc')
            ->orderBy('updated_at', 'desc')
            ->first();
        $top_stores  = Keyword::topStores($keyword->id_keywords);

        $data = [
            'history' => $history,
            'top_stores' => $top_stores
        ];
        return response()->json($data);
    }

    public function rekapitulasi()
    {
        /** @todo
         * halaman rekapitulasi keywords
         */
        $data['commodities'] = Commodity::orderBy('commodity', 'asc')->get();
        return view('keyword_v', $data);
    }
}
