<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'screenshot' => 'required|image',
        ]);
        $filename = 'SS_' . $product->id_product  . "." . $request->file('screenshot')->extension();

        try {
            Storage::delete(storage_path("app/public/screenshot/$product->screenshot"));
            $request->file('screenshot')->move(storage_path('app/public/screenshot'), $filename);
            $product->screenshot = $filename;
            $product->updated_at = date('Y-m-d H:i:s');
            $product->save();
            $response = ['code' => 200, 'success' => true, 'message' => "Update success!"];
        } catch (Exception $e) {
            $response = ['code' => 500, 'success' => false, 'message' => $e->getMessage()];
        }

        return response($response, $response['code']);
    }

    public function add_screenshot(Request $request, Product $product)
    {
        $request->validate([
            'screenshot' => 'required|image',
        ]);
        $filename = 'PS_' . $product->id_product  . "." . $request->file('screenshot')->extension();

        try {
            Storage::delete(storage_path("app/public/screenshot/$product->screenshot"));
            $request->file('screenshot')->move(storage_path('app/public/screenshot'), $filename);
            $product->c_screenshot = $filename;
            $product->updated_at = date('Y-m-d H:i:s');
            $product->save();
            $response = ['code' => 200, 'success' => true, 'message' => "Update success!"];
        } catch (Exception $e) {
            $response = ['code' => 500, 'success' => false, 'message' => $e->getMessage()];
        }

        return response($response, $response['code']);
    }
}
