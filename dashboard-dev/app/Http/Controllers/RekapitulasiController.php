<?php

namespace App\Http\Controllers;

use App\Models\Commodity;
use App\Models\Keyword;

class RekapitulasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Commodity $commodity, Keyword $keyword)
    {
        $recapitulation = $keyword->getRecapitulation();

        return view('rekapitulasi', compact('recapitulation'));
    }
}
