<?php

namespace App\Http\Controllers;

use App\Models\UserActivity;
use Illuminate\Http\Request;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Models\Activity as ActivityModel;
use Yajra\DataTables\Facades\DataTables;

class UserActivityController extends Controller
{
    public function index()
    {
        return view('user_activity');
    }

    public function getData(Request $request)
    {
        if (!$request->ajax()) {
            abort(404);
        }

        $data = ActivityModel::select('*');
        return DataTables::eloquent($data)
            ->addIndexColumn()
            ->editColumn('description', function(Activity $activity) {
                return ucfirst(__($activity->description, [
                    'id'    => $activity->subject_id,
                    'keyword' => isset($activity->properties['keyword']) ? $activity->properties['keyword'] : '',
                    'name'  => isset($activity->properties['page']) ? $activity->properties['page'] : '',
                    'user'  => !is_null($activity->causer_id) ? $activity->causer->name : '',
                    'task' => isset($activity->properties['task']) ? $activity->properties['task'] : ''
                ]));
            })
            ->editColumn('username', function(Activity $activity) {
                if (is_null($activity->causer_id)) {
                    return '';
                }

                return $activity->causer->name;
            })
            ->editColumn('ip_address', function(Activity $activity) {
                return $activity->properties['ip_address'];
            })
            ->editColumn('timestamp', function(Activity $activity) {
                return $activity->created_at;
            })
            ->orderColumn('timestamp', function ($query, $order) {
                $query->orderBy('created_at', $order);
            })
            ->orderColumn('username', function ($query, $order) {
                $query->orderBy('causer_id', $order);
            })
            ->toJson();
    }
}
