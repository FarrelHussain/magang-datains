<?php

namespace App\Http\Controllers\Direktur;

use App\DataTables\Koordinator\RekomendasiTakedownDetailDatatable;
use App\DataTables\Direktur\RekomendasiTakedownByKoordinatorDatatable;
use App\DataTables\Direktur\RekomendasiTakedownDatatable;
use App\Http\Controllers\Controller;
use App\Models\ReportedProduct;

class RekomendasiTakedown extends Controller
{
    public function index(RekomendasiTakedownDatatable $dataTable)
    {
        return $dataTable->render('rekomendasi_takedown');
    }

    public function showKoordinator(string $reportCodeVerified, RekomendasiTakedownByKoordinatorDatatable $dataTable)
    {
        $reportedProducts = ReportedProduct::where('report_code_verified', '=', $reportCodeVerified)->first();
        return $dataTable->render('rekomendasi_takedown', compact('reportedProducts'));
    }

    public function showPetugas(RekomendasiTakedownDetailDatatable $dataTable)
    {
        return $dataTable->render('rekomendasi_takedown');
    }

    public function approve(string $reportCodeVerified)
    {
        ReportedProduct::setApprovedByDirektur($reportCodeVerified);
        return redirect(route('reported-product.direktur.rekomendasi-takedown'));
    }
}
