<?php

namespace App\Http\Controllers;

use App\DataTables\Direktur\RekomendasiTakedownDatatable;
use App\DataTables\ReportTakedownSubmissionDatatable;
use App\DataTables\ReportTakedownSubmissionDetailDatatable;
use App\Imports\LinkImport;
use App\Models\Commodity;
use App\Models\DataProduct;
use App\Models\Keyword;
use App\Models\Link;
use App\Models\Marketplace;
use App\Models\Offense;
use App\Models\Product;
use App\Models\ReportedProduct;
use App\Models\Store;
use App\Services\ReportedProductService;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ReportController extends Controller
{
    public $reportedProductService;
    public function __construct(ReportedProductService $reportedProductService)
    {
        $this->reportedProductService = $reportedProductService;
    }

    public function takedown(ReportTakedownSubmissionDatatable $dataTable)
    {   
        $filterData = $this->reportedProductService->get_filter_data();
        // return view('reports.pengajuan_takedown',['TD'=>$data]);

        return $dataTable->render('reports.pengajuan_takedown', compact('filterData'));
    }

    public function show_takedown(RekomendasiTakedownDatatable $dataTable)
    {   
        return $dataTable->render('rekomendasi_takedown');
    }

    public function patrol()
    {
        /** @todo
         * halaman report patrol
         */
        $patrol = \DB::table('reported_products')
        ->leftJoin('status', 'reported_products.id_status', '=', 'status.id_status')
        ->leftJoin('links', 'reported_products.id_link', '=', 'links.id')
        ->leftJoin('offenses', 'links.id_offense', '=', 'offenses.id_offense')
        ->leftJoin('products', 'products.id_product', '=', 'links.id_product')
        ->leftJoin('commodities', 'products.id_commodity', '=', 'commodities.id_commodity')
        ->leftJoin('stores', 'stores.id_store', '=', 'products.id_store')
        ->leftJoin('marketplace', 'marketplace.id_marketplace', 'stores.id_marketplace')
        ->leftJoin('data_product', 'data_product.id_product', 'products.id_product')
        ->select('commodities.commodity as commodity','products.name as name','stores.name as store_name','stores.url as store_url',
                'marketplace.logo as mp_logo','marketplace.name as mp_name','status.status','data_product.price as price','offenses.offense'
                ,'links.id as id_link','data_product.crawled as crawled','stores.description as desc','products.picture as pic',
                'data_product.sold_count as sold','data_product.item_count as stok','data_product.rating as rating','products.description as description')
        ->where('links.id_status', '=', config('app.status.takedown.id_status'))
        ->get();
			
		return view('reports.patroli_siber',['patrol'=>$patrol,
        'title' => 'Laporan Pengiriman Hasil Patroli Siber UPT'
        ]);
    }

    public function lapor_patrol()
    {
        /** @todo
         * halaman report kegitan patrol
         */
        $TD = Keyword::getTotalPatroliSiber();
			
		return view('reports.laporan_siber',['TD'=>$TD,
        'title' => 'Laporan Kegiatan Patroli Siber'
        ]);
    }

    public function feedback()
    {
        /** @todo
         * halaman report feedback
         
        $marketplaces = Marketplace::OnlyMarketplace()->get();
        $offenses = Offense::get();
        $commodities = Commodity::orderBy('commodity', 'asc')->get();
        $countProduct = Product::getArsip()->where('status.id_status', '=', config('app.status.guidance.id_status'))->count();
        $linkExport = url("export/guidance");
        $locations = Store::select('location')->whereNotNull('location')->groupBy('location')->get();
        return view('guidance_data', [
            'guidanceCount' => $countProduct,
            'commodities' => $commodities,
            'linkExport' => $linkExport,
            'marketplaces' => $marketplaces,
            'offenses' => $offenses,
            'locations' => $locations
        ]);
        */
        $feedback = \DB::table('reported_products')
        ->join('crawling_type','id_crawling_type', '=', 'reported_products.report_type')
        ->join('users','reported_products.approved_by','=','users.id')
        ->select('reported_products.report_code_verified as rcv','reported_products.periode as periode',
        'crawling_type.type as crawling_type','users.name as name')
        ->where('reported_products.approved_at', '>', 0)
        ->groupBy('reported_products.report_code_verified')
        ->get();
			
		return view('guidance_data',['feedback'=>$feedback,
        'title' => 'Feedback'
        ]);
    }

    public function saveFeedback(Request $request)
    {
        /** @todo
         * Save feedback
        */
                            $reg = new \App\Models\feedback;
							\DB::table('reported_products')->where('report_code_verified',$request->rcv)->update([
							  'report_feedback' => $request->feedback,
							  'official_memo' => $request->notadinas,
                              'feedbacked_at' => date('Y-m-d H:i:s')
							   ]);
        return redirect('/reports/feedback')->withSuccess('success.');
    }
}
