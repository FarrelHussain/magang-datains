<?php

namespace App\Http\Controllers;

use App\Models\Commodity;
use Illuminate\Http\Request;
use App\Models\Link;
use App\Models\Marketplace;
use App\Models\Offense;
use App\Models\CrawlingType;
use App\Models\Cratype;
use App\Models\Product;
use App\Models\ReportedProduct;
use App\Models\Store;
use Exception;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class GrabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['marketplaces'] = Marketplace::OnlyMarketplace()->get();
        $data['offenses'] = Offense::get();
        $data['crawling_type'] = CrawlingType::get();
        $data['commodities'] = Commodity::orderBy('commodity', 'asc')->get();
        $data['locations'] = Store::select('location')->whereNotNull('location')->groupBy('location')->get();
        $data['product_count'] = Marketplace::count()->get();

        return view('grab_v', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $action)
    {
        $request->validate([
            'id' => "required|min:1",
            'id_offense' => 'required|numeric'
        ]);
        $response = ['success' => true, 'code' => 200, 'message'];
        $status = config('app.status');
        $status = isset($status[$action]) ? $status[$action] : abort(400);

        try {
            $updateLinkTo = [
                'id_status' => $status['id_status'],
                'updated_at' => date('Y-m-d H:i:s'),
                'id_user' => auth()->user()->id,
                'id_offense' => $request->id_offense,
                'note' => $request->note
            ];
            Link::whereIn('id', $request->id)->update($updateLinkTo);

            $dataLink = []; 
            
            $reportCode = ReportedProduct::getReportCode();
            foreach ($request->id as $value) {
                $dataLink[] = [
                    'id_link' => $value,
                    'created_at' => date('Y-m-d H:i:s'),
                    'id_user' => auth()->user()->id,
                    'id_status' => $status['id_status'],
                    'periode' => date('Y-m-d H:i:s'),
                    'report_type' => $request->id_crawling_type,
                    'report_code' => $reportCode
                ];
            }
            ReportedProduct::insert($dataLink);

            $response['message'] = "Proses $action berhasil dilakukan!";
        } catch (Exception $e) {
            $response = ['success' => false, 'code' => 500, 'message' => $e->getMessage()];
        }
        return response($response, $response['code']);
        
    }

    public function getData(Request $request, $id = false)
    {
        if (!$request->ajax()) {
            return abort(400);
        }

        if ($id) {
            $data = Product::getGrabProducts()->where('links.id', $id)->first();
            $data->product_desc = rawurldecode($data->product_desc);
            
            return Cache::remember($request->fullUrl(), 3600, function() use ($data) {
                return response($data);
            });
        }

        $reportedProducts = Product::grabProducts($request);
        return DataTables::eloquent($reportedProducts)
            ->editColumn('product', function($product) {
                return view('utilities.grab_product', [
                            'title' => $product->product,
                            'link_id' => $product->link_id,
                            'picture' => $product->picture
                        ])->render();
            })
            ->rawColumns(['product'])
            ->toJson();
    }

    public function getOffense(Request $request)
    {
        $key = $request->session()->token();
        if ($request->has($key) and ($request->input($key) === $key) and $request->ajax()) {
            $offenses = Offense::get();
            return response(['code' => 200, 'success' => true, 'message' => $offenses], 200);
        }
    }

    public function getCrawling_type(Request $request)
    {
        $key = $request->session()->token();
        if ($request->has($key) and ($request->input($key) === $key) and $request->ajax()) {
            $crawling_type = CrawlingType::get();
            return response(['code' => 200, 'success' => true, 'message' => $crawling_type], 200);
        }
    }

    public function product_list(Request $request)
    {
        $store = Store::where('id_store', $request->store)->firstOrFail();

        return view('product_list', compact('store'));
    }

    public function product_list_data(Request $request, Store $store)
    {
        if ($request->ajax()) {
            $store = Product::where('id_store', $store->id_store)
                ->leftJoin('data_product', 'data_product.id_product', 'products.id_product')
                ->leftJoin('keywords', 'keywords.id_keywords', 'products.id_keywords')
                ->leftJoin('commodities', 'commodities.id_commodity', 'products.id_commodity')
                ->leftJoin('links', 'links.id_product', 'products.id_product')
                ->leftJoin('status', 'status.id_status', 'links.id_status')
                ->selectRaw('products.name, data_product.price, data_product.item_count, commodities.commodity, keywords.keywords, links.link, status.status');
            return DataTables::eloquent($store)
                ->addIndexColumn()
                ->toJson();
        }
    }

    public function getMpProductTotal(Request $request)
    {
        if ($request->getTotalMarketplace == true) {
            $data = [];
            $mp = Marketplace::get();
            foreach ($mp as $key => $value) {
                $count = Product::countMpProduct()->where('marketplace.id_marketplace', $value->id_marketplace)->where('marketplace.name','<>','blanja')->first();
                $data[$key] = ['total' => $count['total'], 'marketplace' => $value->name, 'id_marketplace' => $value->id_marketplace];
            }
            return response()->json(['data' => $data], 200);
        } else {
            abort(400, "INVALID");
        }
    }
}
