<?php

namespace App\Http\Controllers;

use App\Models\Commodity;
use App\Models\Marketplace;
use App\Models\Keyword;
use App\Models\Product;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->response = ['success' => true, 'code' => 200];
    }

    public function index()
    {
        $data['commodities'] = Commodity::select('commodity')->orderBy('commodity', 'asc')->get();
        $data['total_product_by_commodities'] = Product::getTotalProductByCommodity();
        $data['total_product_crawled'] = Product::getTotalProductCrawled();

        $data['total_crawled'] = Product::getTotalCrawled();;
        $data['total_field'] = Product::stats(config('app.status.tllapangan.id_status'));

        $data['total_active_takedown'] = Product::stats(config('app.status.takedown.id_status'));
        $data['total_rekomendasi_takedown'] = Product::stats(config('app.status.drafttakedown.id_status'));
        $data['total_to_review_stackholder'] = Product::stats(90);
        $data['total_feedback'] = Product::stats(91);
        $data['total_takedown_upt'] = Product::stats(92);

        $data['total_product_takedown'] = Product::stats(config('app.status.inactive.id_status'));

        // dd($data);
        
        return view('home_v', $data);
    }

    public function getData(Request $request)
    {
        if ($request->ajax() and $request->has('mp') and null != $request->input('mp')) {

            $dateFrom = null != $request->input('time_start') ? date_create($request->time_start)->format('Y-m-d H:i:s') : '1995-06-19 00:00:00';
            $dateEnd = null != $request->input('time_end') ? date_add(date_create($request->time_end), date_interval_create_from_date_string("1 day"))->format('Y-m-d') : '2200-06-19';
            $commodities = null != $request->input('commodities') ? explode(",", $request->commodities) : false;

            $data = Marketplace::homeData($request->mp)->whereBetween("data_product.crawled", [$dateFrom, $dateEnd]);
            $data = $commodities ? $data->whereIn('products.id_commodity', $commodities) : $data;
            //dd($data->get());
            return DataTables::eloquent($data)
                ->addIndexColumn()
                ->with([
                    'commodities' => Commodity::get()
                ])
                ->addColumn('color', '#{{substr(md5($commodity.$id_commodity), 0, 6)}}')
                ->toJson();
        }
    }
}
