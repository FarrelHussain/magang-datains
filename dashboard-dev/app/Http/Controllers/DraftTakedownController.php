<?php

namespace App\Http\Controllers;

use App\DataTables\DraftTakedown;
use App\DataTables\DraftTakedownDetailDataTable;
use App\Services\DraftTakedownService;
use App\Models\ReportedProduct;
use Exception;
use Illuminate\Http\Request;

class DraftTakedownController extends Controller
{
    public $draftTakedownService;
    
    public function __construct(DraftTakedownService $draftTakedownService)
    {
        $this->draftTakedownService = $draftTakedownService;
    }

    public function index(DraftTakedown $dataTable)
    {
        return $dataTable->render('draft_takedown');
    }

    public function show(string $reportCode, DraftTakedownDetailDataTable $dataTable)
    {
        try {
            $reportedProducts = ReportedProduct::where('report_code', request('reportCode'))->firstOrFail();
            return $dataTable->render('takedown_list', compact('reportedProducts'));
        } catch (\Throwable $th) {
            abort(404);
        }
    }

    public function submit(string $reportCode)
    {
        ReportedProduct::changeToSubmittedByReportCode($reportCode);

        return redirect(route('reported-product.draft-takedown.index'));
    }

    public function import(Request $request)
    {
        if ($request->method() === 'POST') {
            try {
                $this->draftTakedownService->import($request->all());
            } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                report($e);
                return redirect()->back()->withErrors($e->errors());
            } catch (\Illuminate\Validation\ValidationException $e) {
                report($e);
                return redirect()->back()->withErrors($e->errors());
            }
        }

        return view('forms.form_import');
    }
}
