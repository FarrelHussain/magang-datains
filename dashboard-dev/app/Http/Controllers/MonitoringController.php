<?php

namespace App\Http\Controllers;

use App\Models\Commodity;
use App\Models\Link;
use App\Models\Marketplace;
use App\Models\Offense;
use App\Models\Store;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Yajra\DataTables\Facades\DataTables;

class MonitoringController extends Controller
{
    public function index()
    {
        $links  = Link::getMonitoring();
        $marketplaces = Marketplace::OnlyMarketplace()->get();
        $offenses = Offense::get();
        $commodities = Commodity::orderBy('commodity', 'asc')->get();
        $locations = $this->_getLocations();

        $startDate = Carbon::now();
        $firstOfMonth = Carbon::parse($startDate->firstOfMonth())->format('d F Y');  
        $endOfMonth = Carbon::parse($startDate->endOfMonth())->format('d F Y');  
        return view('monitoring_data', [
            'commodities' => $commodities,
            'marketplaces' => $marketplaces,
            'offenses' => $offenses,
            'locations' => $locations,
            'links' => $links,
            'firstOfMonth' => $firstOfMonth,
            'endOfMonth' => $endOfMonth
        ]);
    }

    public function data()
    {
        $data  = Link::getMonitoring();        
        return DataTables::eloquent($data)
            ->editColumn('name', function($query) {
                return view('utilities.marketplace', ['name' => $query->name])->render();
            })
            ->editColumn('percentage', function($query){
                return is_null($query->percentage) ? "0.00%" : $query->percentage."%";
            })
            ->rawColumns(['name'])
            ->toJson();
    }

    private function _getLocations()
    {
        return Cache::remember('locations', 3600, function(){
            return Store::select('location')->whereNotNull('location')->groupBy('location')->get();
        });
    }
}
