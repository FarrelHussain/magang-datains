<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DownloadController extends Controller
{
    public function __invoke(Request $request)
    {
        $filePath = public_path($request->q);
        return response()->download($filePath);
    }
}
