<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Crawling_TypeController extends Controller
{
    public function data(Request $request)
    {
        $data = Crawling_Type::orderBy('created_at', 'desc');
        // $$data = collect($data);
        return DataTables::eloquent($data)
            ->addIndexColumn()
            ->order(function ($q) use ($request) {
                switch ($request->order[0]['column']) {
                    case '0':
                        $q->orderBy('crawling_type.type', 'asc');
                        break;
                    case '1':
                        $q->orderBy('crawling_type.type', $request->order[0]['dir']);
                        break;
                    default:

                        break;
                }
            })
            ->toJson();
    }
}
