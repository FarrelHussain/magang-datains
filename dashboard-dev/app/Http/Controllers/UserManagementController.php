<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Upt;
use App\User;
use DataTables;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('operator_data') and $request->operator_data == $request->session()->token()) {
            return DataTables::eloquent(User::operator()->orderBy('id', 'desc'))->addIndexColumn()->toJson();
        }
        return view('userManagement');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('role_code', '!=', config('app.roles.super_admin')['role_code'])->get();
        $upt_locations = Upt::get();
        return view('forms.form_user', ['roles' => $roles, 'upt_locations' => $upt_locations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = ['message' => 'Berhasil menambah data user', 'alert' => 'success'];
        $request->validate([
            'name' => 'required',
            'username' => 'required|min:4|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'role_code' => 'required|numeric|exists:roles,role_code',
            'id_upt_location' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);
        $data = [
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'role_code' => $request->role_code,
            'id_upt_location' => $request->id_upt_location,
            'password' => Hash::make($request->password)
        ];

        try {
            User::create($data);
        } catch (Exception $e) {
            $response = ['message' => $e->getMessage(), 'alert' => 'error'];
        }

        return redirect('user-management')->with($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request, User $user)
    {
        //dd($user);
        $data['data'] = $user;
        $data['roles'] = Role::where('role_code', '!=', config('app.roles.super_admin')['role_code'])->get();
        $data['upt_locations'] = Upt::get();
        return view('forms.form_user', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required',
            'username' => [
                'required',
                Rule::unique('users')->ignore($user)
            ],
            'email' => [
                'required',
                Rule::unique('users')->ignore($user)
            ],
            'role_code' => 'required|numeric|exists:roles,role_code',
            'password' => 'nullable|min:6|string'
        ]);

        $data = ['message' => 'Berhasil merubah data.', 'alert' => 'success'];
        try {
            $user->name = $request->name;
            $user->username = $request->username;
            $user->email = $request->email;
            $user->role_code = $request->role_code;
            $user->id_upt_location = $request->id_upt_location;
            $user->password = $request->password ? Hash::make($request->password) : $user->password;
            $user->save();
        } catch (Exception $e) {
            $data = ['message' => $e->getMessage(), 'alert' => 'error'];
        }

        return redirect('user-management')->with($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user)
    {
        $user = User::where('id', $user)->firstOrFail();

        $data = ['message' => 'Berhasil menghapus data.', 'alert' => 'success'];
        try {
            $user->delete();
        } catch (Exception $e) {
            $data = ['message' => $e->getMessage(), 'alert' => 'success'];
        }

        return redirect('user-management')->with($data);
    }
}
