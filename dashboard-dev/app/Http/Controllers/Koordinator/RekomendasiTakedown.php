<?php

namespace App\Http\Controllers\Koordinator;

use App\DataTables\Koordinator\RekomendasiTakedownDatatable;
use App\DataTables\Koordinator\RekomendasiTakedownDetailDatatable;
use App\Http\Controllers\Controller;
use App\Models\ReportedProduct;

class RekomendasiTakedown extends Controller
{
    public function index(RekomendasiTakedownDatatable $dataTable)
    {
        return $dataTable->render('rekomendasi_takedown');
    }

    public function show(string $reportCode, RekomendasiTakedownDetailDatatable $dataTable)
    {
        try {
            $reportedProducts = ReportedProduct::where('report_code', request('reportCode'))->firstOrFail();
            return $dataTable->render('rekomendasi_takedown', compact('reportedProducts'));
        } catch (\Throwable $th) {
            abort(404);
        }
    }
}
