<?php

namespace App\Http\Controllers;

use App\Models\Commodity;
use App\Models\Store;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class StoreController extends Controller
{
    public function index()
    {
        $data['commodities'] = Commodity::orderBy('commodity', 'asc')->get();
        return view('store_v', $data);
    }

    public function data(Request $request)
    {
        $data = Store::selectRaw('
            marketplace.name as mp,
            marketplace.logo as mp_logo,
            stores.name as store,
            stores.id_store as store_id,
            stores.total_product,
            stores.target,
            count("products.id_product") as total_result
            ')
            ->leftJoin('marketplace', 'stores.id_marketplace', '=', 'marketplace.id_marketplace')
            ->leftJoin('products', 'stores.id_store', '=', 'products.id_store')
            ->where('crawled_from', 1)
            ->groupBy('stores.id_store');

        return DataTables::eloquent($data)
            ->addIndexColumn()
            ->toJson();
    }
}
