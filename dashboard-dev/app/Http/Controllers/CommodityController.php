<?php

namespace App\Http\Controllers;

use App\Models\Commodity;
use Illuminate\Http\Request;
use DataTables;

class CommodityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('commodity');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'commodity' => 'required|max:255|string'
        ]);

        $data = [
            'commodity' => $request->commodity,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        try {
            Commodity::create($data);
            $response = [
                'message' => 'Berhasil menambah komoditas',
                'alert' => 'success'
            ];
        } catch (\Exception $e) {
            $response = [
                'message' => $e->getMessage(),
                'alert' => 'error'
            ];
        }
        return redirect()->route('commodity.index')->with($response);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Commodity $commodity)
    {
        $request->validate([
            'commodity' => 'required|string|max:255'
        ]);

        try {
            $name = $commodity->commodity;
            $commodity->commodity = $request->commodity;
            $commodity->save();
            $response = [
                'message' => "Berhasil merubah komoditas {$name}",
                'alert' => 'success'
            ];
        } catch (\Exception $e) {
            $response = [
                'message' => $e->getMessage(),
                'alert' => 'error'
            ];
        }
        return redirect()->route('commodity.index')->with($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy(Commodity $commodity)
    // {
    //     try {
    //         $name = $commodity->commodity;
    //         $commodity->delete();
    //         $response = [
    //             'message' => "Berhasil menghapus komoditas {$name}",
    //             'alert' => 'success'
    //         ];
    //     } catch (\Exception $e) {
    //         $response = [
    //             'message' =>  $e->getMessage(),
    //             'alert' => 'error'
    //         ];
    //     }
    //     return redirect()->route('commodity.index')->with($response);
    // }

    public function data(Request $request)
    {
        $data = Commodity::orderBy('created_at', 'desc');
        // $$data = collect($data);
        return DataTables::eloquent($data)
            ->addIndexColumn()
            ->order(function ($q) use ($request) {
                switch ($request->order[0]['column']) {
                    case '0':
                        $q->orderBy('commodities.commodity', 'asc');
                        break;
                    case '1':
                        $q->orderBy('commodities.commodity', $request->order[0]['dir']);
                        break;
                    default:

                        break;
                }
            })
            ->toJson();
    }
}
