<?php

namespace App\Http\Controllers;

use App\Exports\ReportedProductExport;
use App\Models\Product;
use App\Models\ReportedProduct;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExportData extends Controller
{
    public function arsip(Request $request)
    {
        $data = Product::export()->where('links.id_status', config('app.status.arsip.id_status'));
        // return ($data->toSql());
        if ($request->has('mp') and null != $request->mp) {
            $data = $data->whereIn('links.id',  $request->mp);
        }
        // dd($request->input());
        $data = $this->setCustomExport($data, $request);
        //dd($data->toSql());
        $data = $data->get();

        $export = [];
        foreach ($data as $key => $value) {
            $export[] = [
                'No' => $key + 1,
                'ID Takedown' => null,
                'Jenis Platform' => 'Ecommerce',
                'Nama Platform' => ucfirst($value->marketplace),
                'Nama Pemilik' => $value->store,
                "URL" => $value->url,
                "Komoditas" => $value->commodity,
                "Jenis Pelanggaran" => $value->offense,
                "Nama Produk" => $value->product_name,
                "Produsen" => $value->manufacturer,
                "Kandungan" => $value->kandungan,
                'Golongan' => $value->golongan,
                'Kelas Terapi' => $value->kelas_terapi,
                'Periode' =>  $value->periode_start,
                'Rekomendasi' => $value->recomendation,
                'File' => null,
                'Status' => $value->recomendation,
                'Waktu Input' => $value->input_time ? date_create($value->input_time)->format('d-m-Y') : date_create($value->link_date_create)->format('d-m-Y'),
                // 'ID User' => $value->id_user,
                'Asal Produk' => $value->product_origin,
                'Screenshot' => $value->screenshot != null ? asset("storage/screenshot/$value->screenshot") : '',
                "Petugas" => $value->initial != null ? $value->initial : "-",
                "Lokasi Wilayah Pelapak" => $value->id_store != null ? $value->initial : "-"
            ];
        }
        
        activity_log('log.export', [
            'task' => config('app.status.arsip.status')
        ]);
        return Excel::download(new ReportedProductExport($export), 'Arsip export ' . date('d m Y') . ' .xlsx');
    }

    public function takedown(Request $request)
    {
        $data = ReportedProduct::export()->where('status_link.id_status', config('app.status.takedown.id_status'));
        if ($request->has('mp') and null != $request->mp and is_array($request->mp)) {
            $data = $data->whereIn('links.id', $request->mp);
        }
        $data = $this->setCustomExport($data, $request);
        if ($request->periode) {
            $month = explode('-', $request->periode);
            $data = $data->whereMonth('periode', $month[1]);
            $data = $data->whereYear('periode', $month[0]);
        }
        $data = $data->get();
        $export = [];
        foreach ($data as $key => $value) {
            $export[] = [
                'No' => $key + 1,
                'ID Takedown' => $value->id_takedown,
                'Jenis Platform' => 'Ecommerce',
                'Nama Platform' => ucfirst($value->marketplace),
                'Nama Pemilik' => $value->store,
                "URL" => $value->url,
                "Komoditas" => $value->commodity,
                "Jenis Pelanggaran" => $value->offense,
                "Nama Produk" => $value->product_name,
                "Produsen" => $value->manufacturer,
                "Kandungan" => $value->kandungan,
                'Golongan' => $value->golongan,
                'Kelas Terapi' => $value->kelas_terapi,
                'Periode' => $value->periode ? date_create($value->periode)->format('M Y') : '',
                'Rekomendasi' => $value->recomendation,
                'File' => $value->file,
                'Status' => $value->rp_status,
                'Waktu Input' => $value->input_time ? date_create($value->input_time)->format('d-m-Y') : date_create($value->link_date_create)->format('d-m-Y'),
                // 'ID User' => $value->id_user,
                'Asal Produk' => $value->product_origin,
                'Screenshot' => $value->screenshot != null ? asset("storage/screenshot/$value->screenshot") : '',
                "Petugas" => $value->initial != null ? $value->initial : "-",
                "Lokasi Wilayah Pelapak" => $value->initial != null ? $value->initial : "-"
            ];
        }

        activity_log('log.export', [
            'task' => config('app.status.takedown.status')
        ]);
        return Excel::download(new ReportedProductExport($export, true), 'Takedown export ' . date('d m Y') . ' .xlsx');
    }

    public function tllapangan(Request $request)
    {
        $data = Product::export()->where('links.id_status', config('app.status.tllapangan.id_status'));
        if ($request->has('mp') and null != $request->mp) {
            $data = $data->whereIn('links.id', $request->mp);
        }

        $data = $this->setCustomExport($data, $request);
        $data = $data->get();

        $export = [];
        foreach ($data as $key => $value) {
            $export[] = [
                'No' => $key + 1,
                'ID Takedown' => null,
                'Jenis Platform' => 'Ecommerce',
                'Nama Platform' => ucfirst($value->marketplace),
                'Nama Pemilik' => $value->store,
                "URL" => $value->url,
                "Komoditas" => $value->commodity,
                "Jenis Pelanggaran" => $value->offense,
                "Nama Produk" => $value->product_name,
                "Produsen" => $value->manufacturer,
                "Kandungan" => $value->kandungan,
                'Golongan' => $value->golongan,
                'Kelas Terapi' => $value->kelas_terapi,
                'Periode' => '',
                'Rekomendasi' => $value->recomendation,
                'File' => $value->file,
                'Status' => $value->recomendation,
                'Waktu Input' => $value->input_time ? date_create($value->input_time)->format('d-m-Y') : date_create($value->link_date_create)->format('d-m-Y'),
                // 'ID User' => $value->id_user,
                'Asal Produk' => $value->product_origin,
                'Screenshot' => $value->screenshot != null ? asset("storage/screenshot/$value->screenshot") : '',
                "Petugas" => $value->initial != null ? $value->initial : "-",
                "Lokasi Wilayah Pelapak" => $value->initial != null ? $value->initial : "-"
            ];
        }

        activity_log('log.export', [
            'task' => config('app.status.tllapangan.status')
        ]);
        return Excel::download(new ReportedProductExport($export), 'TL Lapangan export ' . date('d m Y') . ' .xlsx');
    }

    public function guidance(Request $request)
    {
        $data = Product::export()->where('links.id_status', config('app.status.guidance.id_status'));
        if ($request->has('mp') and null != $request->mp) {
            $data = $data->whereIn('links.id', $request->mp);
        }

        $data = $this->setCustomExport($data, $request);
        $data = $data->get();

        $export = [];
        foreach ($data as $key => $value) {
            $export[] = [
                'No' => $key + 1,
                'ID Takedown' => null,
                'Jenis Platform' => 'Ecommerce',
                'Nama Platform' => ucfirst($value->marketplace),
                'Nama Pemilik' => $value->store,
                "URL" => $value->url,
                "Komoditas" => $value->commodity,
                "Jenis Pelanggaran" => $value->offense,
                "Nama Produk" => $value->product_name,
                "Produsen" => $value->manufacturer,
                "Kandungan" => $value->kandungan,
                'Golongan' => $value->golongan,
                'Kelas Terapi' => $value->kelas_terapi,
                'Periode' => '',
                'Rekomendasi' => $value->recomendation,
                'File' => $value->file,
                'Status' => $value->recomendation,
                'Waktu Input' => $value->input_time ? date_create($value->input_time)->format('d-m-Y') : date_create($value->link_date_create)->format('d-m-Y'),
                // 'ID User' => $value->id_user,
                'Asal Produk' => $value->product_origin,
                'Screenshot' => $value->screenshot != null ? asset("storage/screenshot/$value->screenshot") : '',
                "Petugas" => $value->initial != null ? $value->initial : "-",
                "Lokasi Wilayah Pelapak" => $value->initial != null ? $value->initial : "-"
            ];
        }

        activity_log('log.export', [
            'task' => config('app.status.guidance.status')
        ]);
        return Excel::download(new ReportedProductExport($export), 'Pembinaan ' . date('d m Y') . ' .xlsx');
    }

    private function setCustomExport(Builder $data, Request $request)
    {
        if ($request->product_name) $data->where('products.name', "LIKE", "%{$request->product_name}%");
        if ($request->content) $data->where('keywords.content', $request->content);
        if ($request->therapy_class) $data->where('keywords.therapy_class', "LIKE", "%{$request->therapy_class}%");
        if ($request->group) $data->where('keywords.group', "LIKE", "%{$request->group}%");
        if ($request->store) $data->where('stores.name', "LIKE", "%{$request->store}%");
        if ($request->marketplace) $data->where('marketplace.id_marketplace', $request->marketplace);
        if ($request->offense) $data->where('links.id_offense', $request->offense);
        if ($request->commodity) $data->where('products.id_commodity', $request->commodity);

        return $data;
    }
}
