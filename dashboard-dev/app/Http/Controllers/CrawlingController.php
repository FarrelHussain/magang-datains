<?php

namespace App\Http\Controllers;

use App\Http\Traits\AdminAuth;
use App\Models\Commodity;
use App\Models\Marketplace;
use App\Models\Upt;
use App\Models\Store;
use App\Models\Keyword;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CrawlingController extends Controller
{
    use AdminAuth;

    public function index()
    {
        $data['commodities'] = Commodity::orderBy('commodity', 'asc')->get();
        $data['marketplaces'] = Marketplace::OnlyMarketplace()->get();
        //$data['crawling_type'] = Crawling_Type::select('id_crawling_type', 'type')->orderBy('Type', 'asc')->get();
        $upt_locations = Upt::select('working_area')->where('id_upt_location', auth()->user()->id_upt_location)->get();
        if (count($upt_locations) ==0 ) {
            $upt_locations = Upt::select('working_area')->where('name', 'Nasional')->get();
        }
        $data['upt_location'] = explode(', ',$upt_locations[0]->working_area); 
        return view('crawling_v', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'query' => 'required|string|max:255',
            'marketplace' => 'required|min:1',
            'commodity' => 'required|numeric',
            'product_origin' => 'string|max:255',
            'content' => 'string|max:255|nullable',
            'therapy_class' => 'string|max:255|nullable',
            'group' => 'string|max:255|nullable',
            'crawl_by' => 'required|string|in:stores,keywords',
            'locations' => 'required|min:1'
        ]);

        $location = '*';
        if (!is_admin() && !is_petugas()) {
            $location = join(',', $request->locations);
        }

        $data = [
            'marketplaces' => join(',', $request->marketplace),
            'query' => $request->input('query'),
            'max' => (isset($request->quantity) and (NULL != $request->quantity)) ? $request->quantity : -1,
            'commodity' => $request->commodity,
            'product_origin' => $request->product_origin ? $request->product_origin : 'local',
            'content' => $request->content,
            'therapy_class' => $request->therapy_class,
            'group' => $request->group,
            'crawl_by' => $request->crawl_by,
            'location' => $location,
            'created_by' => auth()->user()->id
        ];
        $insertData = array();
        $username = [];
        foreach ($request->marketplace as $key => $value) {
            $idMarketplace = (int)Marketplace::select('id_marketplace')->where('name', $value)->first()['id_marketplace'];
            // keyword validations
            $keyword = Keyword::select('users.name')
            ->where('keywords', $request->input('query'))
            ->where('id_marketplace', $idMarketplace)
            ->join('users', 'users.id', 'keywords.id_user')
            ->first();

            if (!isset($keyword) ) {
                array_push($insertData, array(
                    'id_marketplace' => $idMarketplace,
                    'id_commodity' => (int)$request->commodity,
                    'id_user' => auth()->user()->id,
                    'id_crawling_type' => $request->crawling_type,
                    'keywords' => $request->input('query'),
                    'status' => config('app.status.active.id_status'),
                    'max_product' => (isset($request->quantity) and (NULL != $request->quantity)) ? $request->quantity : -1,
                    'note' => '',
                    'content' => $request->content,
                    'therapy_class' => $request->therapy_class,
                    'group' => '',
                    'product_origin' => $request->product_origin ? $request->product_origin : 'local',
                    'locations' => $location,
                ));
            } else {
                $username[] = $keyword['name'];
            }
        }

        if (count($username) > 0) {
            $data = [
                'alert' => 'error',
                'message' => 'Keyword '.$request->input('query').' sudah pernah di crawling oleh '.implode(', ',array_unique($username))
            ];

            return redirect('crawling')->with($data);
        }
        
        $client = new Client;
        try {
            $ids = [];

            foreach($insertData as $insertedData)
            {
                $ids[] = Keyword::insertGetId($insertedData);
            }
            $data['id_keywords'] = implode(',',$ids);

            $response = $client->request('POST', config('app.crawler_url'), [
                'form_params' => $data
            ]);
            $body = json_decode((string) $response->getBody());
            if ($body->success) {
                $data = [
                    'alert' => 'success',
                    'message' => 'Crawling berhasil dan sedang dilakukan. Click menu Grab untuk melihat hasilnya secara berkala'
                ];
            } else {
                $data = [
                    'alert' => 'error',
                    'message' => $body->message
                ];
            }

            activity_log('log.crawl', [
                'keyword' => $request->input('query')
            ]);
        } catch (ClientException $e) {
            $data = [
                'code' => $e->getCode(),
                'message' => "Tell sysadmin error code is " . $e->getCode(),
                'alert' => 'error'
            ];
        }

        return redirect('crawling')->with($data);
    }

    public function searchStore(Request $request)
    {
        $request->validate([
            'search' => 'required|string'
        ]);
        $stores = Store::selectRaw('stores.url as id, CONCAT(stores.name, " | ", marketplace.name) as text')
            ->leftJoin('marketplace', 'stores.id_marketplace', '=', 'marketplace.id_marketplace')
            ->where('stores.name', 'LIKE', "%{$request->search}%")
            ->where('stores.url', '!=', null)
            ->paginate(20);
        return response()->json($stores);
    }
}
