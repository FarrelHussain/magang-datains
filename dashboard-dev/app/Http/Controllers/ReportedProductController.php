<?php

namespace App\Http\Controllers;

use App\Imports\LinkImport;
use App\Models\Commodity;
use App\Models\DataProduct;
use App\Models\Keyword;
use App\Models\Link;
use App\Models\Marketplace;
use App\Models\Offense;
use App\Models\Product;
use App\Models\ReportedProduct;
use App\Models\Store;
use App\Models\Upt;
use App\Models\Status;
use App\Models\Report;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ReportedProductController extends Controller
{
    public function takedownData()
    {
        $marketplaces = Marketplace::OnlyMarketplace()->get();
        $offenses = Offense::get();
        $commodities = Commodity::orderBy('commodity', 'asc')->get();
        $countProduct = ReportedProduct::getReportedProducts()->count();
        $locations = Store::select('location')->whereNotNull('location')->groupBy('location')->get();
        $linkExport = url("export/takedown");
        return view('takedown_data', [
            'takedownCount' => $countProduct,
            'commodities' => $commodities,
            'linkExport' => $linkExport,
            'marketplaces' => $marketplaces,
            'offenses' => $offenses,
            'locations' => $locations
        ]);
    }

    public function draftTakedownData()
    {
        $marketplaces = Marketplace::OnlyMarketplace()->get();
        $offenses = Offense::get();
        $commodities = Commodity::orderBy('commodity', 'asc')->get();
        $countProduct = ReportedProduct::getReportedProducts()->count();
        $locations = Store::select('location')->whereNotNull('location')->groupBy('location')->get();
        $linkExport = url("export/takedown");
        if (isset($_GET['id'])) {
            $report = Report::where('id_report','=', $_GET['id'])->first();
            if (!isset($report)) {abort(404);}

            return view('takedown_list', [
                'takedownCount' => $countProduct,
                'commodities' => $commodities,
                'linkExport' => $linkExport,
                'marketplaces' => $marketplaces,
                'offenses' => $offenses,
                'locations' => $locations,
                'report' => $report
            ]);
        }
        return view('draft_takedown', [
            'takedownCount' => $countProduct,
            'commodities' => $commodities,
            'linkExport' => $linkExport,
            'marketplaces' => $marketplaces,
            'offenses' => $offenses,
            'locations' => $locations
        ]);
    }

    public function draftTakedownDetail()
    {
        $marketplaces = Marketplace::OnlyMarketplace()->get();
        $offenses = Offense::get();
        $commodities = Commodity::orderBy('commodity', 'asc')->get();
        $countProduct = ReportedProduct::getReportedProducts()->count();
        $locations = Store::select('location')->whereNotNull('location')->groupBy('location')->get();
        $linkExport = url("export/takedown");
        return view('takedown_list', [
            'takedownCount' => $countProduct,
            'commodities' => $commodities,
            'linkExport' => $linkExport,
            'marketplaces' => $marketplaces,
            'offenses' => $offenses,
            'locations' => $locations
        ]);
    }

    public function tllapanganData()
    {
        $marketplaces = Marketplace::OnlyMarketplace()->get();
        $offenses = Offense::get();
        $commodities = Commodity::orderBy('commodity', 'asc')->get();
        $countProduct = Product::getArsip()->where('status.id_status', '=', config('app.status.tllapangan.id_status'))->count();
        $linkExport = url("export/tllapangan");
        $locations = Store::select('location')->whereNotNull('location')->groupBy('location')->get();
        return view('tllapangan_data', [
            'tllapanganCount' => $countProduct,
            'commodities' => $commodities,
            'linkExport' => $linkExport,
            'marketplaces' => $marketplaces,
            'offenses' => $offenses,
            'locations' => $locations
        ]);
    }

    public function guidanceData()
    {
       /* $marketplaces = Marketplace::get();
        $offenses = Offense::get();
        $commodities = Commodity::orderBy('commodity', 'asc')->get();
        $countProduct = Product::getArsip()->where('status.id_status', '=', config('app.status.guidance.id_status'))->count();
        $linkExport = url("export/guidance");
        $locations = Store::select('location')->whereNotNull('location')->groupBy('location')->get();
        return view('guidance_data', [
            'guidanceCount' => $countProduct,
            'commodities' => $commodities,
            'linkExport' => $linkExport,
            'marketplaces' => $marketplaces,
            'offenses' => $offenses,
            'locations' => $locations
        ]);
        */
        $feedback = \DB::table('reported_products')
        ->leftJoin('links', 'reported_products.id_link', '=', 'links.id')
        ->leftJoin('products', 'products.id_product', '=', 'links.id_product')
        ->leftJoin('commodities', 'products.id_commodity', '=', 'commodities.id_commodity')
        ->leftjoin('keywords','commodities.id_commodity','=','keywords.id_commodity')
        ->leftjoin('crawling_type','crawling_type.id_crawling_type','=','keywords.id_crawling_type')
        ->leftJoin('data_product', 'data_product.id_product', 'products.id_product')
        ->leftjoin('users','reported_products.approved_by','=','users.id')
        ->select('reported_products.report_code_verified as rcv','reported_products.periode as periode',
                'crawling_type.type as crawling_type','users.name as name')
        ->where('approved_at', '>', '0')
        ->get();
			
		return view('guidance_data',['feedback'=>$feedback,
        'title' => 'Laporan Pembinaan'
        ]);
    }

    public function arsipData()
    {
        $marketplaces = Marketplace::OnlyMarketplace()->get();
        $commodities = Commodity::orderBy('commodity', 'asc')->get();
        $offenses = Offense::get();
        $countProduct = Product::getArsip()->where('status.id_status', '=', config('app.status.arsip.id_status'))->count();
        $linkExport = route('export-arsip');
        $locations = Store::select('location')->whereNotNull('location')->groupBy('location')->get();
        return view('arsip_data', [
            'arsipCount' => $countProduct,
            'commodities' => $commodities,
            'linkExport' => $linkExport,
            'marketplaces' => $marketplaces,
            'offenses' => $offenses,
            'locations' => $locations
        ]);
    }

    public function doAction(Request $request, $action)
    {
        $request->validate([
            'id' => "required|min:1"
        ]);

        $status = config('app.status');
        $status = isset($status[$action]) ? $status[$action] : abort(400);
        $response = ['success' => true, 'code' => 200, 'message'];

        try {
            Link::whereIn('id', $request->id)
                ->update(['id_status' => $status['id_status'], 'updated_at' => date('Y-m-d H:i:s')]);
            if ($status['id_status'] == config('app.status.takedown.id_status')) {
                $dataLink = [];
                foreach ($request->id as $key => $value) {
                    $dataLink[] = [
                        'id_link' => $value,
                        'created_at' => date('Y-m-d H:i:s'),
                        'periode' => date('Y-m-d H:i:s'),
                        'id_user' => auth()->user()->id,
                        'id_status' => config('app.status.active.id_status'),
                    ];
                }
                ReportedProduct::insert($dataLink);
            }
            $response['message'] = "Proses " . $status['status'] . " berhasil dilakukan!";
        } catch (Exception $e) {
            $response = ['success' => false, 'code' => 500, 'message' => $e->getMessage()];
        }
        return response($response, $response['code']);
    }

    public function data(Request $request, $id = false)
    {
        $data = $request->data;
        $status = config('app.status');

        if ($id) {
            if ((null != $data) and (($data == strtolower(config('app.status.arsip.status'))) || ($data == config('app.status.tllapangan.status')) || ($data == 'guidance'))) {
                $data = strtolower(str_replace(" ", "", $data));
                $status = isset($status[$data]) ? $status[$data] : abort(400);
                $data = Product::getArsip()->where('status.id_status', '=', $status['id_status'])->where('products.id_product', '=', $id);
            } else if ((null != $data) and ($data == strtolower(config('app.status.takedown.status')))) {
                $data = ReportedProduct::getReportedProducts()->where('reported_products.id_reported_product', $id);
            } else {
                return response()->json(['message' => "data tidak ditemukan atau request tidak benar", 'title' => 'error'], 400);
            }

            return response($data->first());
        }

        $status = isset($status[$data]) ? $status[$data] : abort(400);


        if ($status['id_status'] == config('app.status.arsip.id_status') || $status['id_status'] == config('app.status.tllapangan.id_status') || $status['id_status'] == config('app.status.guidance.id_status')) {
            $data = Product::getArsip()->where('status.id_status', '=', $status['id_status']);
        } else {
            $data = ReportedProduct::getTakedownProducts();
            if ($request->input('filter_periode') and strpos($request->input('filter_periode'), '-')) {
                $filter_periode = explode("-", $request->filter_periode);
                $data = $data->whereMonth('reported_products.periode', $filter_periode[1])->whereYear('reported_products.periode', $filter_periode[0]);
            }
        }


        if (null != $request->input('commodities')) {
            $commodities = explode(",", $request->commodities);
            array_push($commodities, 999999);
            $data = $data->whereIn('products.id_commodity', $commodities);
        };

        if ($request->input('link_status')) {
            $rps = $request->link_status;
            $link_status = config("app.status.{$rps}.id_status", false);
            if ($link_status) {
                $data = $data->where('reported_products.id_status', $link_status);
            }
        }
        
        $data = $this->setAdvanceSearch($data, $request);
        return DataTables::eloquent($data)
            ->editColumn('mp_logo', '{{asset("img/" . $mp_logo)}}')
            ->addIndexColumn()
            ->toJson();
    }

    public function draft(Request $request, $id = false)
    {
        $data = $request->data;
        $status = config('app.status');

        if ($id) {
            if ((null != $data) and (($data == strtolower(config('app.status.arsip.status'))) || ($data == config('app.status.tllapangan.status')) || ($data == 'guidance'))) {
                $data = strtolower(str_replace(" ", "", $data));
                $status = isset($status[$data]) ? $status[$data] : abort(400);
                $data = Product::getArsip()->where('status.id_status', '=', $status['id_status'])->where('products.id_product', '=', $id);
            } else if ((null != $data) and ($data == strtolower(config('app.status.takedown.status')))) {
                $data = ReportedProduct::getReportedProducts()->where('reported_products.id_reported_product', $id);
            } else {
                return response()->json(['message' => "data tidak ditemukan atau request tidak benar", 'title' => 'error'], 400);
            }

            return response($data->first());
        }

        $status = isset($status[$data]) ? $status[$data] : abort(400);


        if ($status['id_status'] == config('app.status.arsip.id_status') || $status['id_status'] == config('app.status.tllapangan.id_status') || $status['id_status'] == config('app.status.guidance.id_status')) {
            $data = Product::getArsip()->where('status.id_status', '=', $status['id_status']);
        } else {
            if ($status['id_status'] == config('app.status.drafttakedown.id_status')) {
                $data = ReportedProduct::getDraftTakedown();
                $data = $this->setAdvanceSearch($data, $request);
                return DataTables::eloquent($data)
                    ->addIndexColumn()
                    ->toJson();
            } else {
                $data = ReportedProduct::getTakedownProducts($request->id);
                if ($request->input('filter_periode') and strpos($request->input('filter_periode'), '-')) {
                    $filter_periode = explode("-", $request->filter_periode);
                    $data = $data->whereMonth('reported_products.periode', $filter_periode[1])->whereYear('reported_products.periode', $filter_periode[0]);
                }
            }
        }


        if (null != $request->input('commodities')) {
            $commodities = explode(",", $request->commodities);
            array_push($commodities, 999999);
            $data = $data->whereIn('products.id_commodity', $commodities);
        };

        if ($request->input('link_status')) {
            $rps = $request->link_status;
            $link_status = config("app.status.{$rps}.id_status", false);
            if ($link_status) {
                $data = $data->where('reported_products.id_status', $link_status);
            }
        }
        
        $data = $this->setAdvanceSearch($data, $request);
        return DataTables::eloquent($data)
            ->editColumn('mp_logo', '{{asset("img/" . $mp_logo)}}')
            ->addIndexColumn()
            ->toJson();
    }

    public function importForm()
    {
        $unit_kerja = Upt::get();
        $status = Status::whereIn('status', array('Takedown', 'Profilling'))->get();
        return view('forms.form_import', ['unit_kerja' => $unit_kerja, 'rekomendasi' => $status]);
    }

    public function takedownImport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'takedown_file' => 'required|file|mimes:xlsx,xls',
            //'initial' => 'required|string|max:255',
            // 'filename' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->first();
            return redirect()->route('takedown-data')->with(['message' => $errors, 'alert' => "error"]);
        }

        $file = $request->file('takedown_file');
        $request->filename = Str::slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
        $filename = "{$request->filename}_" . time() . "." . $file->extension();
        $file->move(storage_path('app/public/imports'), $filename);

        $data = Excel::toArray(new LinkImport, storage_path('app/public/imports') . "/" . $filename);
        Storage::delete(storage_path('app/public/imports/') . $filename);

        $i = 1;
        $errorDataCount = 0;
        $errorDataRow = [];
        $successDataRow = [];
        $successDataCount = 0;
        foreach ($data[0] as $row) {
            $input = $this->inputData($row, $request);
            if (!$input->success) {
                $errorDataCount += 1;
                $errorDataRow[] = (object) ['index' => $i, 'message' => $input->message];
            } else {
                $successDataCount += 1;
                $successDataRow[] = (object) ['index' => $i, 'message' => $input->message];
            }
            $i++;
        }
        $res['result'] =  [
            'error' => (object) ['total' => $errorDataCount, 'data' => $errorDataRow],
            'success' => (object) ['total' => $successDataCount, 'data' => $successDataRow],
        ];
        $unit_kerja = Upt::get();
        $res['unit_kerja'] = $unit_kerja;
        $res['rekomendasi'] = Status::whereIn('status', array('Takedown', 'Profilling'))->get();
        return view('forms.form_import', $res);
    }

    protected function inputData($row, $request)
    {
        $response = [
            'success' => true,
            'message' => "Berhasil"
        ];
        if (isset($row['url']) and (null != $row['url'])) {
            $link = Link::where('link', $row['url'])->first();
            if ($link and $link->id_status != config('app.status.takedown.id_status')) {
                try {
                    if (isset($row['periode']) and (null != $row['periode']) and (strpos($row['periode'], '-') || strpos($row['periode'], ' '))) {
                        $d = $row['periode'];
                        $date = strpos($d, '-') ? date_create_from_format('d-M-Y', "01-{$d}") : date_create_from_format('d M Y', "01 {$d}");

                        if (!$date) {
                            return (object) ['success' => false, 'message' => 'Data periode tidak valid format harus (Dec-2020 / Dec 2020). Atau terdapat whitespace di belakang maupun di depan data.'];
                        }
                    } else {
                        return (object) ['success' => false, 'message' => 'Data periode tidak valid format harus (Dec-2020 / Dec 2020)'];
                    }
                    $link->id_status = config('app.status.takedown.id_status');
                    $link->save();
                    ReportedProduct::insert([
                        'id_user' => auth()->user()->id,
                        'id_link' => $link->id,
                        'id_status' => config('app.status.active.id_status'),
                        'created_at' => date('Y-m-d H:i:s'),
                        'initial' => $request->initial ? $request->initial : $row['petugas'],
                        'filename' => $request->filename,
                        'periode' => $date->format('Y-m-d')
                    ]);
                    $response = ['success' => true, 'message' => "Berhasil update data!"];
                } catch (\Exception $e) {
                    $response = ['success' => false, 'message' => "Gagal update data!"];
                }
                return (object) $response;
            }
            if ($link == null) {
                if (!isset($row['nama_penjual']) || $row['nama_penjual'] == null) {
                    return (object) ['success' => false, 'message' => 'Data toko / nama penjual kosong'];
                }

                $mp = [];
                if (!isset($row['nama_platform']) || (null == $row['nama_platform'])) {
                    return (object) ['success' => false, 'message' => "Data nama platform kosong"];
                } else if (!$mp = Marketplace::where('name', ucfirst($row['nama_platform']))->first()) {
                    return (object) ['success' => false, 'message' => "nama platform tidak ditemukan di dalam database."];
                }

                $commodity = [];
                if (!isset($row['jenis_komoditas']) || (null == $row['jenis_komoditas'])) {
                    return (object) ['success' => false, 'message' => "Data jenis komoditas kosong."];
                } elseif (!$commodity = Commodity::where('commodity', $row['jenis_komoditas'])->first()) {
                    return (object) ['success' => false, 'message' => "Komoditas tidak ditemukan di dalam database."];
                }
                $offense = [];
                if (!isset($row['jenis_pelanggaran']) || (null == $row['jenis_pelanggaran'])) {
                    return (object) ['success' => false, 'message' => "Data jenis pelanggaran kosong."];
                } else if (
                    !$offense = Offense::where('offense', "LIKE", "%{$row['jenis_pelanggaran']}%")
                        ->orWhere('acronym', 'LIKE', "%{$row['jenis_pelanggaran']}%")
                        ->orWhere('id_offense', $row['jenis_pelanggaran'])
                        ->first()
                ) {
                    return (object) ['success' => false, 'message' => "Jenis pelanggaran tidak ditemukan di dalam database."];
                }
                //$rekomendasi = Status::where('status', $row['status'])->first();
                if (!isset($row['nama_produk']) || (null == $row['nama_produk'])) {
                    return (object) ['success' => false, 'message' => "Data nama produk kosong."];
                }
                $date = null;
                if (isset($row['periode']) and (null != $row['periode']) and (strpos($row['periode'], '-') || strpos($row['periode'], ' '))) {

                    $d = $row['periode'];
                    $date = strpos($d, '-') ? date_create_from_format('d-M-Y', "01-{$d}") : date_create_from_format('d M Y', "01 {$d}");

                    if (!$date) {
                        return (object) ['success' => false, 'message' => 'Data periode tidak valid format harus (Dec-2020 / Dec 2020). Atau terdapat whitespace di belakang maupun di depan data.'];
                    }
                } else {
                    return (object) ['success' => false, 'message' => 'Data periode tidak valid format harus (Dec-2020 / Dec 2020)'];
                }
                $data = (object) [
                    'mp' => $mp,
                    'commodity' => $commodity,
                    'offense' => $offense,
                    // 'recommmendation' => $rekomendasi,
                    'periode' => $date->format('Y-m-d')
                ];

                try {
                    DB::transaction(function () use ($data, $row, $request) {
                        Store::firstOrCreate(
                            ['name' => $row['nama_penjual'], 'id_marketplace' => $data->mp->id_marketplace],
                            [
                                'name' => $row['nama_penjual'],
                                'id_marketplace' => $data->mp->id_marketplace,
                                'location' => $row['wilayah']
                            ]
                        );
                        Keyword::firstOrCreate(
                            ['keywords' => $row['nama_produk'], 'id_marketplace' => $data->mp->id_marketplace, 'id_commodity' => $data->commodity->id_commodity],
                            [
                                'keywords' => $row['nama_produk'],
                                'id_marketplace' => $data->mp->id_marketplace,
                                'id_commodity' => $data->commodity->id_commodity,
                                'status' => 0,
                                'max_product' => -1,
                                'content' => $row['kandungan'],
                                'therapy_class' => $row['kelas_terapi'],
                                'group' => $row['golongan'],
                            ]
                        );

                        $store = Store::where([
                            'name' => $row['nama_penjual'],
                            'id_marketplace' => $data->mp->id_marketplace
                        ])->first();
                        $keywords = Keyword::where(['keywords' => $row['nama_produk'], 'id_marketplace' => $data->mp->id_marketplace, 'id_commodity' => $data->commodity->id_commodity,])->first();

                        $product = Product::insertGetId([
                            'id_store' => $store->id_store,
                            'id_keywords' => $keywords->id_keywords,
                            'id_commodity' => $data->commodity->id_commodity,
                            'name' => $row['nama_produk'],
                        ]);
                        //$input_time = isset($row['tanggal_input']) ? date_create_from_format('d/m/Y', $row['tanggal_input'])->format('Y-m-d') : false;
                        DataProduct::create([
                            'id_product' => $product,
                            'crawled' => $data->periode,
                            'keywords' => $keywords->keywords,
                            'manufacturer' => isset($row['produsen']) ? $row['produsen'] : null,
                            'sold_count' => $row['terjual'],
                            'price' => $row['harga']
                        ]);
                        $id_status = isset($request->rekomendasi) ? getStatusId($request->rekomendasi) : getStatusId($row['rekomendasi']);
                        $link = Link::insertGetId([
                            'id_product' => $product,
                            'id_status' => $id_status,
                            'id_offense' => $data->offense->id_offense,
                            'id_user' => auth()->user()->id,
                            'link' => $row['url'],
                            'inserted_by' => auth()->user()->id,
                            'created_at' => date('Y-m-d H:i:s')
                        ]);
                        ReportedProduct::create([
                            'id_user' => auth()->user()->id,
                            'id_link' => $link,
                            'id_status' => config('app.status.active.id_status'),
                            'created_at' => date('Y-m-d H:i:s'),
                            'initial' => $request->initial,
                            'filename' => $request->filename,
                            'periode' => $data->periode,
                        ]);
                    });
                    return (object) ['success' => true, "message" => "Berhasil input baru"];
                } catch (\Exception $th) {

                    return (object) ['success' => false, 'message' => $th->getMessage()];
                }
            } else {
                return (object) ['success' => false, 'message' => "Link sudah ada!"];
            }
        } else {
            return (object) ['success' => false, 'message' => "Data link kosong"];
        }
    }

    private function setAdvanceSearch(Builder $data, Request $request)
    {
        if ($request->s_product_name) $data = $data->where('products.name', 'LIKE', "%{$request->s_product_name}%");
        if ($request->s_content) $data = $data->where('keywords.content', 'LIKE', "%{$request->s_content}%");
        if ($request->s_therapy_class) $data = $data->where('keywords.therapy_class', 'LIKE', "%{$request->s_therapy_class}%");
        if ($request->s_group) $data = $data->where('keywords.group', 'LIKE', "%{$request->s_group}%");
        if ($request->s_store) $data = $data->where('stores.name', 'LIKE', "%{$request->s_store}%");
        if ($request->s_offense) $data = $data->where('links.id_offense', 'LIKE', "%{$request->s_offense}%");
        if ($request->s_commodity) $data = $data->where('products.id_commodity', 'LIKE', "%{$request->s_commodity}%");
        if ($request->s_marketplace) $data = $data->where('stores.id_marketplace', 'LIKE', "%{$request->s_marketplace}%");

        return $data;
    }

    public function monitoringData()
    {
        $marketplaces = Marketplace::OnlyMarketplace()->get();
        $offenses = Offense::get();
        $commodities = Commodity::orderBy('commodity', 'asc')->get();
        $countProduct = ReportedProduct::getReportedProducts()->count();
        // $linkExport = url("export/takedown");
        return view('monitoring_data', [
            'takedownCount' => $countProduct,
            'commodities' => $commodities,
            // 'linkExport' => $linkExport,
            'marketplaces' => $marketplaces,
            'offenses' => $offenses
        ]);
    }

    public function dataMonitoring(Request $request, $id = false)
    {
        $data = $request->data;
        $status = config('app.status');

        if ($id) {
            if ((null != $data) and (($data == strtolower(config('app.status.arsip.status'))) || ($data == config('app.status.tllapangan.status')) || ($data == 'guidance'))) {
                $data = strtolower(str_replace(" ", "", $data));
                $status = isset($status[$data]) ? $status[$data] : abort(400);
                $data = Product::getArsip()->where('status.id_status', '=', $status['id_status'])->where('products.id_product', '=', $id);
            } else if ((null != $data) and ($data == strtolower(config('app.status.takedown.status')))) {
                $data = ReportedProduct::getReportedProducts()->where('reported_products.id_reported_product', $id);
            } else {
                return response()->json(['message' => "data tidak ditemukan atau request tidak benar", 'title' => 'error'], 400);
            }

            return response($data->first());
        }

        $status = isset($status[$data]) ? $status[$data] : abort(400);


        if ($status['id_status'] == config('app.status.arsip.id_status') || $status['id_status'] == config('app.status.tllapangan.id_status') || $status['id_status'] == config('app.status.guidance.id_status')) {
            $data = Product::getArsip()->where('status.id_status', '=', $status['id_status']);
        } else {
            $data = ReportedProduct::GetMonitoredProducts();
            if ($request->input('filter_periode') and strpos($request->input('filter_periode'), '-')) {
                $filter_periode = explode("-", $request->filter_periode);
                $data = $data->whereMonth('reported_products.periode', $filter_periode[1])->whereYear('reported_products.periode', $filter_periode[0]);
            }
        }


        if (null != $request->input('commodities')) {
            $commodities = explode(",", $request->commodities);
            array_push($commodities, 999999);
            $data = $data->whereIn('products.id_commodity', $commodities);
        };

        if ($request->input('link_status')) {
            $rps = $request->link_status;
            $link_status = config("app.status.{$rps}.id_status", false);
            if ($link_status) {
                $data = $data->where('reported_products.id_status', $link_status);
            }
        }

        $data = $this->setAdvanceSearch($data, $request);

        return DataTables::eloquent($data)
            ->editColumn('mp_logo', '{{asset("img/" . $mp_logo)}}')
            ->addIndexColumn()
            ->toJson();
    }
}
