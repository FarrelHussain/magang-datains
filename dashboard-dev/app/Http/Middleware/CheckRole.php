<?php

namespace App\Http\Middleware;
use App\Http\Traits\AdminAuth;

use Closure;

class CheckRole
{
    use AdminAuth;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, string ...$roles)
    {
        $configRoles = array_keys(config('app.roles'));
        if (!!array_diff($roles, $configRoles) && !is_admin()) {
            abort(403);
        }
        
        return $next($request);
    }
}
