<?php

namespace App\Http\Middleware;

use App\Http\Traits\AdminAuth as TraitsAdminAuth;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminAuth
{
    use TraitsAdminAuth;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->is_admin()) {
            return redirect('/home');
        }

        return $next($request);
    }
}
