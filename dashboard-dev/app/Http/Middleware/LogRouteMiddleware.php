<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LogRouteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $user = auth()->user();

        if($user && !$request->ajax()) {
            activity()
                ->withProperties([
                    'ip_address' => $request->ip(),
                    'page'  => $request->url()
                ])
                ->causedBy($user->id)
                ->log('log.navigate');
        }
        return $response;
    }
}
