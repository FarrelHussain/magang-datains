<?php

namespace App\Http\Traits;

use App\Models\Role;
use Illuminate\Support\Facades\Auth;

/**
 * FOr admin authorizing
 */
trait AdminAuth
{
    public function is_admin()
    {
        $roles = config('app.roles');
        $admin_role = $roles['super_admin'];

        $superAdmin = Role::where('role_code', $admin_role['role_code'])->first();

        return Auth::user()->role_code === $superAdmin->role_code;
    }

    public function is_operator()
    {
        $roles = config('app.roles');
        $operator_role = $roles['operator'];

        $operator = Role::where('role_code', $operator_role['role_code'])->first();

        return Auth::user()->role_code === $operator->role_code;
    }

    public function is_direktur()
    {
        $roles = config('app.roles');
        $direktur_role = $roles['direktur'];

        $direktur = Role::where('role_code', $direktur_role['role_code'])->first();

        return Auth::user()->role_code === $direktur->role_code;
    }
 
    public function is_koordinator()
    {
        $roles = config('app.roles');
        $koordinator_role = $roles['koordinator'];

        $koordinator = Role::where('role_code', $koordinator_role['role_code'])->first();

        return Auth::user()->role_code === $koordinator->role_code;
    }

    public function is_petugas()
    {
        $roles = config('app.roles');
        $petugas_role = $roles['petugas'];

        $petugas = Role::where('role_code', $petugas_role['role_code'])->first();

        return Auth::user()->role_code === $petugas->role_code;
    }

    public function is_petugas_balai()
    {
        $roles = config('app.roles');
        $petugas_balai_role = $roles['petugas_balai'];

        $petugas_balai = Role::where('role_code', $petugas_balai_role['role_code'])->first();

        return Auth::user()->role_code === $petugas_balai->role_code;
    }

    public function is_stakeholder()
    {
        $roles = config('app.roles');
        $stakeholder_role = $roles['stakeholder'];

        $stakeholder = Role::where('role_code', $stakeholder_role['role_code'])->first();

        return Auth::user()->role_code === $stakeholder->role_code;
    }

    public function is_koordinator_balai()
    {
        $roles = config('app.roles');
        $koordinator_balai_role = $roles['koordinator_balai'];

        $koordinator_balai = Role::where('role_code', $koordinator_balai_role['role_code'])->first();

        return Auth::user()->role_code === $koordinator_balai->role_code;
    }

    public function is_kepala_balai()
    {
        $roles = config('app.roles');
        $kepala_balai_role = $roles['kepala_balai'];

        $kepala_balai = Role::where('role_code', $kepala_balai_role['role_code'])->first();

        return Auth::user()->role_code === $kepala_balai->role_code;
    }

    public function is_petugas_loka()
    {
        $roles = config('app.roles');
        $petugas_loka_role = $roles['petugas_loka'];

        $petugas_loka = Role::where('role_code', $petugas_loka_role['role_code'])->first();

        return Auth::user()->role_code === $petugas_loka->role_code;
    }

    public function is_kepala_loka()
    {
        $roles = config('app.roles');
        $role = $roles['kepala_loka'];

        $role = Role::where('role_code', $role['role_code'])->first();

        return Auth::user()->role_code === $role->role_code;
    }

}