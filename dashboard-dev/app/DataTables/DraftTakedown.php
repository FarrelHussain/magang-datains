<?php

namespace App\DataTables;

use App\Exports\DraftTakedownExport;
use App\Models\ReportedProduct;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class DraftTakedown extends DataTable
{
    protected $action = ['export'];

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('checkbox', '<input type="checkbox" data-id="{{ $checkbox }}" />')
            ->editColumn('periode', function($query) {
                return date('d-m-Y', strtotime($query->periode));
            })
            ->editColumn('status', function($query) {
                if (is_null($query->status)) {
                    if (!is_null($query->rejected_reason)) {
                        return '<span class="badge badge-danger">Ditolak</span>';
                    }
                    return '<span class="badge badge-warning">Belum disubmit</span>';
                }

                if ($query->id_status === config('app.status.takedown.id_status')) {
                    return '<span class="badge badge-success">Disetujui</span>';
                }
                return '<span class="badge badge-success">Sudah disubmit</span>';
            })
            ->editColumn('id_draft_takedown', function($query) {
                return '<a href="'.route('reported-product.draft-takedown.show', $query->id_draft_takedown).'">'.$query->id_draft_takedown.'</a>';
            })
            ->addColumn('action', function(ReportedProduct $query) {
                return view('utilities.action', [
                    'show' => [
                        'url' => route('reported-product.draft-takedown.show', $query->id_draft_takedown),
                        'text' => 'Detail'
                    ]
                ])->render();
            })
            ->filterColumn('reported_products.id_user', function($query, $proposedBy) {
                $query->where('users.name', 'like', "%{$proposedBy}%");
            })
            ->filterColumn('report_type', function($query, $reportType) {
                $query->where('crawling_type.type', 'like', "%{$reportType}%");
            })
            ->filterColumn('periode', function($query, $periode) {
                $dateRange = explode('/', $periode);
                if (count($dateRange) > 1) {
                    $startDate = $dateRange[0];
                    $endDate = $dateRange[1];
                    $query->whereDate('reported_products.periode','<=', $endDate)
                          ->whereDate('reported_products.periode','>=', $startDate);
                }
            })
            ->rawColumns(['action', 'status', 'checkbox', 'id_draft_takedown']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ReportedProduct $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ReportedProduct $model)
    {
        $select = [
            'report_code as id_draft_takedown', 
            'report_code as checkbox', 
            'id_status',
            'periode', 
            'crawling_type.type as report_type',
            'submitted_at as status',
            'rejected_reason',
            'reported_products.created_at'
        ];
        return $model->newQuery()
            ->select(...$select)
            ->join('crawling_type', 'reported_products.report_type', '=', 'crawling_type.id_crawling_type')
            ->when(request()->status, function($query) {
                $status = request()->status;
                if ($status === 'disetujui') {
                    $query->where('id_status', '=', config('app.status.takedown.id_status'));
                }

                if ($status === 'ditolak') {
                    $query
                        ->where('id_status', '=', config('app.status.drafttakedown.id_status'))
                        ->whereNotNull('rejected_reason');
                }

                if ($status === 'menunggu') {
                    $query
                        ->where('id_status', '=', config('app.status.drafttakedown.id_status'))
                        ->whereNotNull('submitted_at');
                }

                if ($status === 'belum-disubmit') {
                    $query
                        ->where('id_status', '=', config('app.status.drafttakedown.id_status'))
                        ->whereNull('submitted_at');
                }
                
            })
            ->when(!is_admin(), function($query) {
                $query->where('id_user', '=', auth()->user()->id);
            })
            ->when(is_admin(), function($query) use ($select) {
                $query->select('users.name as proposed_by', ...$select)
                    ->join('users', 'users.id', '=', 'reported_products.id_user');
            })
            ->groupBy(DB::raw('DATE(periode)'), 'report_type', 'report_code');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('drafttakedowndatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction([
                        'width' => 10
                    ])
                    ->orderBy(3)
                    ->parameters([
                        'mark' => true,
                        'language' => [
                            'processing' => '<div class="align-center text-center overlay-loading">
                                <div class="overlay-loading-content">
                                <img src="/img/dual_ring_loading.svg" />
                                </div>
                            </div>'
                        ],
                    ])
                    ->dom('<"row d-flex"<"col-md-2 mt-2"l>
                    <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"<"mr-2"B><"mr-2"f><p>>>t
                    <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                    <"bg-transparent"r>')
                    ->initComplete("function() {
                        const table = window['".config('datatables-html.namespace', 'LaravelDataTables')."']['drafttakedowndatatable-table'];
                        $('.dataTables_filter label').replaceWith(function(){
                            return $(this).contents();
                        });

                        table.on( 'order.dt search.dt draw.dt', function () {
                            table.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                cell.innerHTML = i+1;
                            } );
                        } ).draw();


                        let startDate = null;
                        let endDate = null;
                        const dateRangeFilter = $('input[name=daterange]');
                        dateRangeFilter.daterangepicker({
                            'autoApply': true,
                            maxDate: moment().startOf('hour').add(32, 'hour'),
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                            },
                            alwaysShowCalendars: true
                        });

                        dateRangeFilter.on('apply.daterangepicker', function(ev, picker) {
                            startDate = picker.startDate.format('YYYY-MM-DD');
                            endDate = picker.endDate.format('YYYY-MM-DD');
                        });

                        dateRangeFilter.on('cancel.daterangepicker', function(ev, picker) {
                            $(this).val('');
                        });

                        const filterModal = $('#filter-modal');
                        filterModal.on('click','button[type=submit]', function(){
                            const status = $('#filter-status').val()
                            if(status) {
                                table.columns(5).search(status).draw();
                            }

                            const reportType = $('#filter-report-type').val();
                            if(reportType) {
                                table.columns(4).search(reportType).draw();
                            }

                            if (startDate && endDate) {
                                table.columns(3).search(startDate + '/' + endDate).draw()
                            }

                            filterModal.modal('toggle');
                         });
                         
                         // Reset Button
                        filterModal.on('click', 'button[type=reset]', function(){
                            console.log('reset');
                            $('#filter-status').val('')
                            $('#filter-report-type').val('')
                            dateRangeFilter.val('');

                            table.columns([4,2,3]).search('').draw();
                            filterModal.modal('toggle');
                        });

                        let checkboxRowCount = 0;
                        const tableHeadCheckbox = $('#dataTablesCheckbox');
                        tableHeadCheckbox.on('click', function() {
                            const isCheckedAll = this.checked;
                            $('tbody tr').find('td:first input[type=checkbox]:not(:disabled)').each(function() {
                                var checkedStatus = this.checked;

                                if(isCheckedAll && checkedStatus) {
                                    // Jika select semua dan statusnya sudah di ceklist
                                    // Biarkan
                                } else {
                                    $(this).prop('checked', !checkedStatus);
                                    if(checkedStatus) {
                                        checkboxRowCount -= 1;
                                    } else {
                                        checkboxRowCount += 1;
                                    }
                                }
                            });
                        })

                        const checkboxRow = $('tbody tr').find('td:first input[type=checkbox]:not(:disabled)')
                        checkboxRow.on('click', function() {
                            var checkedStatus = this.checked;

                            if(checkedStatus) {
                                checkboxRowCount += 1;
                                if(checkboxRowCount === checkboxRow.length) {
                                    tableHeadCheckbox.prop('indeterminate', false);
                                    tableHeadCheckbox.prop('checked', true);
                                } else {
                                    tableHeadCheckbox.prop('indeterminate', true);
                                }
                            } else {
                                checkboxRowCount -= 1;
                                if(checkboxRowCount < checkboxRow.length) {
                                    tableHeadCheckbox.prop('checked', false);
                                    tableHeadCheckbox.prop('indeterminate', true);
                                    
                                    if(checkboxRowCount === 0) {
                                        tableHeadCheckbox.prop('indeterminate', false);
                                    }
                                }
                            }
                        });
                    }")
                    ->buttons(
                        Button::make('import'),
                        Button::make('excel'),
                        Button::make('filter')
                            ->addClass('btn-sm')
                            ->attr([
                                'data-toggle' => 'modal',
                                'data-target' => '#filter-modal'
                            ])
                    )
                    ->addCheckbox([], true);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns = [
            Column::computed('')
                ->content('')
                ->title('No')
                ->width(10)
                ->addClass('text-center'),
            Column::make('reported_products.report_code')
                ->title('Kode')
                ->width(150)
                ->data('id_draft_takedown'),
            Column::make('periode')->title('Periode')->width('10%'),
            Column::make('report_type')->title('Jenis Laporan'),
            Column::make('submitted_at')
                ->title('Status')
                ->data('status'),
            Column::make('created_at')
                ->title('')
                ->hidden(),
        ];

        if (is_admin()) {
            $columns[] = Column::make('reported_products.id_user')
                ->data('proposed_by')
                ->title('Dilaporkan Oleh')
                ->width('15%');
        }
        return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'DraftTakedown_' . date('YmdHis');
    }

    public function excel()
    {
        $selectedCode = explode(',', request()->selected);
    
        return (new DraftTakedownExport($selectedCode))->download($this->filename().'.xlsx');
    }
}
