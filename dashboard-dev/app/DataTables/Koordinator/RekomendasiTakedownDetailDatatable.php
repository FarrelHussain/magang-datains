<?php

namespace App\DataTables\Koordinator;

use App\Models\ReportedProduct;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class RekomendasiTakedownDetailDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('periode', function($query) {
                return date('d-m-Y', strtotime($query->periode));
            })
            ->editColumn('marketplace', function($query) {
                return view('utilities.marketplace', ['name' => $query->marketplace]);
            })
            ->editColumn('product', '{{ ucwords($product) }}')
            ->editColumn('store', '{{ ucwords($store) }}');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ReportedProduct $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ReportedProduct $model)
    {
        $select = [
            'links.id as DT_RowIndex',
            'reported_products.periode',
            'commodities.commodity as commodity',
            'products.name as product',
            'stores.name as store',
            'marketplace.name as marketplace',
            'reported_products.report_feedback as report_feedback',
            'products.updated_at as last_visit', 
            'reported_products.submitted_at'
        ];

        return $model->newQuery()
                ->select(...$select)
                ->join('links', 'links.id', '=', 'reported_products.id_link')
                ->join('products', 'products.id_product', '=', 'links.id_product')
                ->join('stores', 'stores.id_store', '=', 'products.id_store')
                ->join('marketplace', 'marketplace.id_marketplace', '=', 'stores.id_marketplace')
                ->join('commodities', 'commodities.id_commodity', '=', 'products.id_commodity')
                ->where('report_code', '=', request('reportCode'));
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('rekomendasitakedowndetaildatatable-table')
                    ->columns($this->getColumns())
                    ->addIndex()
                    ->columnDefs([
                        [
                            "targets" => -1,
                            "visible" => false
                        ]
                    ])
                    ->minifiedAjax()
                    ->addDatains();
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('periode'),
            Column::make('commodity')
                ->name('commodities.commodity')
                ->title('Komoditas'),
            Column::make('product')
                ->name('products.name')
                ->title('Product'),
            Column::make('store')
                ->name('stores.name')
                ->title('Store'),
            Column::make('marketplace')
                ->name('marketplace.name')
                ->addClass('text-center'),
            Column::make('report_feedback')
                    ->addClass('text-center')
                    ->name('reported_products.report_feedback')
                    ->title('Feedback'),
            Column::make('last_visit')
                ->addClass('text-center')
                ->name('products.updated_at')
                ->title('Last Visit'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Koordinator/RekomendasiTakedownDetail_' . date('YmdHis');
    }
}
