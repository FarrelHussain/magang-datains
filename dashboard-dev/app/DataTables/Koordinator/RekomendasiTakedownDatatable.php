<?php

namespace App\DataTables\Koordinator;

use App\Models\ReportedProduct;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class RekomendasiTakedownDatatable extends DataTable
{
    private $tableId = 'rekomendasitakedowndatatable-table';
    protected $actions = ['approve', 'reject'];

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
                ->eloquent($query)
                ->editColumn('checkbox', function($query) {
                    if (is_null($query->status)) {
                        if (!is_null($query->rejected_reason)) {
                            return '<input type="checkbox" data-id="'.$query->report_code.'" />';
                        }

                        return '<input type="checkbox" data-id="'.$query->report_code.'" />';
                    }
    
                    if ($query->id_status === config('app.status.takedown.id_status')) {
                        return '';
                    }
                    
                    return '';
                })
                ->editColumn('report_code', '<a href="{{ route("reported-product.rekomendasi-takedown.show", $report_code) }}">{{ $report_code }}</a>')
                ->editColumn('periode', function($query) {
                    return date('d-m-Y', strtotime($query->periode));
                })
                ->editColumn('status', function($query) {
                    if (!is_null($query->status)) {
                        return 'Sudah Disetujui';
                    }
                    return 'Menunggu Persetujuan';
                })
                ->addColumn('action', function(ReportedProduct $query) {
                    return view('utilities.action', [
                        'show' => [
                            'url' => route('reported-product.rekomendasi-takedown.show', $query->report_code),
                            'text' => 'Detail'
                        ]
                    ])->render();
                })
                ->filterColumn('reported_products.id_user', function($query, $proposedBy) {
                    $query->where('users.name', 'like', "%{$proposedBy}%");
                })
                ->filterColumn('report_type', function($query, $reportType) {
                    $query->where('crawling_type.type', 'like', "%{$reportType}%");
                })
                ->filterColumn('periode', function($query, $periode) {
                    $dateRange = explode('/', $periode);
                    if (count($dateRange) > 1) {
                        $startDate = $dateRange[0];
                        $endDate = $dateRange[1];
                        $query->whereDate('reported_products.periode','<=', $endDate)
                              ->whereDate('reported_products.periode','>=', $startDate);
                    }
                })
                ->rawColumns(['report_code', 'action', 'checkbox']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ReportedProduct $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ReportedProduct $model)
    {
        $select = [
            'report_code', 
            'periode', 
            'crawling_type.type as report_type',
            'submitted_at as is_submitted',
            'report_code as id',
            'report_code_verified as status'
        ];

        return $model->newQuery()
                    ->select(...$select)
                    ->join('crawling_type', 'reported_products.report_type', '=', 'crawling_type.id_crawling_type')
                    ->whereNotNull('submitted_at')
                    ->when(is_admin() || is_koordinator() || is_kepala_loka() || is_kepala_balai(), function($query) use ($select) {
                        $petugas_role_code = 0;

                        if (is_kepala_loka()) {
                            $petugas_role_code = config('app.roles.petugas_loka.role_code');
                        }

                        if (is_kepala_balai()) {
                            $petugas_role_code = config('app.roles.petugas_balai.role_code');
                        }

                        if (is_koordinator()) {
                            $petugas_role_code = config('app.roles.petugas.role_code');
                        }
                    
                        $query->select('users.name as proposed_by', ...$select)
                                ->join('users', 'users.id', '=', 'reported_products.id_user')
                                ->when(!is_admin(), function($query) use ($petugas_role_code) {
                                    $query->where('users.id_upt_location', '=', auth()->user()->id_upt_location)
                                        ->where('users.role_code', '=', $petugas_role_code);
                                });

                    })
                    ->when(request('status'), function($query, $status) {
                        if ($status === 'disetujui') {
                            $query->whereNotNull('report_code_verified');
                        }

                        if ($status === 'menunggu') {
                            $query->whereNull('report_code_verified');
                        }
                    })
                    ->groupBy(DB::raw('DATE(periode)'), 'report_type', 'report_code');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId($this->tableId)
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->orderBy(2)
                    ->addAction([
                        'width' => 10
                    ])
                    ->parameters([
                        'mark' => true,
                        'language' => [
                            'processing' => '<div class="align-center text-center overlay-loading">
                                <div class="overlay-loading-content">
                                <img src="/img/dual_ring_loading.svg" />
                                </div>
                            </div>'
                        ],
                    ])
                    ->dom('<"row d-flex"<"col-md-2 mt-2"l>
                    <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"<"mr-2"B><"mr-2"f><p>>>t
                    <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                    <"bg-transparent"r>')
                    ->initComplete("function() {
                        const table = window['".config('datatables-html.namespace', 'LaravelDataTables')."']['".$this->tableId."'];
                        $('.dataTables_filter label').replaceWith(function(){
                            return $(this).contents();
                        });

                        table.on( 'order.dt search.dt draw.dt', function () {
                            table.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                cell.innerHTML = i+1;
                            } );
                        } ).draw();

                        new $.fn.dataTable.Buttons(table, {
                            buttons: [
                                {
                                    className: 'btn btn-primary disabled',
                                    text: 'Setujui',
                                    attr:  {
                                        title: 'Approve',
                                        id: '".$this->tableId."_approve'
                                    },
                                    action: function ( e, dt, node, conf ) {
                                        const url = _buildUrl(dt, 'approve');
                                        const selected = [];
                                        const checkboxChecked = $('tbody tr').find('td:first input[type=checkbox]:checked');
                                        if (checkboxChecked.length > 0) {
                                            checkboxChecked.each(function() {
                                                selected.push($(this).data('id'));
                                            })
                                        } else {
                                            $('tbody tr').find('td:first input[type=checkbox]').each(function() {
                                                selected.push($(this).data('id'));
                                            });
                                        }

                                        $.ajax({
                                            type: 'GET',
                                            url: url + '&selected=' + selected,
                                            success: function (response) {
                                                dt.draw(false);
                                            },
                                            error: (xhr, status, error) => {
                                            }
                                        });
                                    }
                                },
                                {
                                    className: 'btn btn-danger disabled',
                                    text: 'Tolak',
                                    attr:  {
                                        title: 'Reject',
                                        id: '".$this->tableId."_reject'
                                    },
                                    action: function ( e, dt, node, conf ) {
                                        alert( 'Button 3 clicked on' );
                                    }
                                }
                            ]
                        });
                     
                        table.buttons(1, null ).container().appendTo(
                            table.table().container()
                        );

                        let startDate = null;
                        let endDate = null;
                        const dateRangeFilter = $('input[name=daterange]');
                        dateRangeFilter.daterangepicker({
                            'autoApply': true,
                            maxDate: moment().startOf('hour').add(32, 'hour'),
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                            },
                            alwaysShowCalendars: true
                        });

                        dateRangeFilter.on('apply.daterangepicker', function(ev, picker) {
                            startDate = picker.startDate.format('YYYY-MM-DD');
                            endDate = picker.endDate.format('YYYY-MM-DD');
                        });

                        dateRangeFilter.on('cancel.daterangepicker', function(ev, picker) {
                            $(this).val('');
                        });

                        const filterModal = $('#filter-modal');
                        filterModal.on('click','button[type=submit]', function(){
                            const status = $('#filter-status').val()
                            if(status) {
                                table.columns(5).search(status).draw();
                            }

                            const reportType = $('#filter-report-type').val();
                            if(reportType) {
                                table.columns(4).search(reportType).draw();
                            }

                            if (startDate && endDate) {
                                table.columns(3).search(startDate + '/' + endDate).draw()
                            }

                            filterModal.modal('toggle');
                         });
                         
                         // Reset Button
                        filterModal.on('click', 'button[type=reset]', function(){
                            console.log('reset');
                            $('#filter-status').val('')
                            $('#filter-report-type').val('')
                            dateRangeFilter.val('');

                            table.columns([4,2,3]).search('').draw();
                            filterModal.modal('toggle');
                        });

                        $('#".$this->tableId."_wrapper > :last-child').removeClass('btn-group')

                        let checkboxRowCount = 0;
                        const tableHeadCheckbox = $('#dataTablesCheckbox');

                        const handleClickCheckboxHead = function() {
                            const isCheckedAll = this.checked;
                            $('tbody tr').find('td:first input[type=checkbox]:not(:disabled)').each(function() {
                                var checkedStatus = this.checked;

                                if(isCheckedAll && checkedStatus) {
                                    // Jika select semua dan statusnya sudah di ceklist
                                    // Biarkan
                                } else {
                                    $(this).prop('checked', !checkedStatus);
                                    if(checkedStatus) {
                                        checkboxRowCount -= 1;
                                    } else {
                                        checkboxRowCount += 1;
                                    }
                                }
                            });

                            if(checkboxRowCount !== 0) {
                                $('#".$this->tableId."_approve, #".$this->tableId."_reject').removeClass('disabled');
                            } else {
                                $('#".$this->tableId."_approve, #".$this->tableId."_reject').addClass('disabled');
                            }
                        }
                        tableHeadCheckbox.on('click', handleClickCheckboxHead)

                        const checkboxRow = $('tbody tr').find('td:first input[type=checkbox]:not(:disabled)')
                        const handleClickCheckboxRow = function() {
                            var checkedStatus = this.checked;
                            console.log(checkboxRowCount)

                            if(checkedStatus) {
                                checkboxRowCount += 1;
                                if(checkboxRowCount === checkboxRow.length) {
                                    tableHeadCheckbox.prop('indeterminate', false);
                                    tableHeadCheckbox.prop('checked', true);
                                } else {
                                    tableHeadCheckbox.prop('indeterminate', true);
                                }
                            } else {
                                checkboxRowCount -= 1;
                                if(checkboxRowCount < checkboxRow.length) {
                                    tableHeadCheckbox.prop('checked', false);
                                    tableHeadCheckbox.prop('indeterminate', true);
                                    
                                    if(checkboxRowCount === 0) {
                                        tableHeadCheckbox.prop('indeterminate', false);
                                    }
                                }
                            }

                            if(checkboxRowCount !== 0) {
                                $('#".$this->tableId."_approve, #".$this->tableId."_reject').removeClass('disabled');
                            } else {
                                $('#".$this->tableId."_approve, #".$this->tableId."_reject').addClass('disabled');
                            }
                        }
                        checkboxRow.on('click', handleClickCheckboxRow);
                    }")
                    ->buttons(
                        Button::make('filter')
                            ->addClass('btn-sm')
                            ->attr([
                                'data-toggle' => 'modal',
                                'data-target' => '#filter-modal'
                            ])
                    )
                    ->addCheckbox([], true);
    }
    
    public function approve() 
    {
        $selectedReportCode = explode(',', request()->selected);
        foreach ($selectedReportCode as $reportCode) {
            ReportedProduct::setApprovedByKoordinator($reportCode);
        }

        return response('success');
    }
    
    public function reject(){
        $selectedReportCode = explode(',', request()->selected);

        foreach ($selectedReportCode as $reportCode) {
            ReportedProduct::setRejectedByKoordinator($reportCode);
        }

        return response('success');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns = [
            Column::computed('')
                ->content('')
                ->title('No')
                ->width(10)
                ->addClass('text-center'),
            Column::make('report_code')
                ->title('Kode')
                ->addClass('text-center')
                ->width('20%'),
            Column::make('periode')
                ->title('Periode')
                ->width('10%'),
            Column::make('report_type')
                ->title('Jenis Laporan'),
            Column::make('report_code_verified')
                ->data('status')
                ->title('Status'),
        ];

        if (is_admin() || is_kepala_loka() || is_koordinator() || is_kepala_balai()) {
            $columns[] = Column::make('reported_products.id_user')
                ->data('proposed_by')
                ->title('Dilaporkan Oleh')
                ->width('15%');;
        }
        return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Koordinator/RekomendasiTakedown_' . date('YmdHis');
    }
}
