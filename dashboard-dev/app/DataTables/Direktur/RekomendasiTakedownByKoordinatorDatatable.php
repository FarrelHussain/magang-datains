<?php

namespace App\DataTables\Direktur;

use App\Models\ReportedProduct;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class RekomendasiTakedownByKoordinatorDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('periode', function($query) {
                return date('d-m-Y', strtotime($query->periode));
            })
            ->addColumn('action', function(ReportedProduct $query) {
                return view('utilities.action', [
                    'show' => [
                        'url' => route('reported-product.direktur.rekomendasi-takedown.show-petugas', [
                            'reportCodeVerified' => request('reportCodeVerified'),
                            'reportCode' => $query->report_code
                        ]),
                        'text' => 'Detail'
                    ]
                ])->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ReportedProduct $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ReportedProduct $model)
    {
        $select = [
            'report_code', 
            'periode', 
            'crawling_type.type as report_type',
            'submitted_at as is_submitted'
        ];
        return $model->newQuery()
                    ->select(...$select)
                    ->join('crawling_type', 'reported_products.report_type', '=', 'crawling_type.id_crawling_type')
                    //->where('id_status', '=', config('app.status.drafttakedown.id_status'))
                    ->where('report_code_verified', '=', request('reportCodeVerified'))
                    ->when(is_admin() || is_direktur() || is_kepala_balai(), function($query) use ($select) {
                        $query->select('users.name as proposed_by', ...$select)
                            ->join('users', 'users.id', '=', 'reported_products.id_user');
                    })
                    ->groupBy(DB::raw('DATE(periode)'), 'report_code');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('rekomendasitakedowndetaildatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->orderBy(1)
                    ->addAction([
                        'width' => '5%'
                    ])
                    ->changeDom();
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('report_code')->title('Kode')->addClass('text-center')->width('20%'),
            Column::make('periode')->title('Periode'),
            Column::make('report_type')->title('Jenis Laporan'),
            Column::make('proposed_by')->title('Dilaporkan Oleh')->width('15%')
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'RekomendasiTakedownByKoordinatorDatatable_' . date('YmdHis');
    }
}
