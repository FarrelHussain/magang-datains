<?php

namespace App\DataTables\Direktur;

use App\Models\ReportedProduct;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class RekomendasiTakedownDatatable extends DataTable
{
    protected $actions = ['submit', 'reject'];

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
                ->eloquent($query)
                ->editColumn('approved_at', function($query) {
                    if (is_null($query->approved_at)) {
                        return 'Menunggu Persetujuan';
                    }

                    return 'Sudah Disetujui';
                })
                ->editColumn('periode', function($query) {
                    return date('d-m-Y', strtotime($query->periode));
                })
                ->addColumn('action', function(ReportedProduct $query) {
                    return view('utilities.action', [
                        'show' => [
                            'url' => route('reported-product.direktur.rekomendasi-takedown.show', $query->report_code),
                            'text' => 'Detail'
                        ]
                    ])->render();
                });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ReportedProduct $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ReportedProduct $model)
    {
        $select = [
            'report_code_verified as report_code', 
            'periode', 
            'crawling_type.type as report_type',
            'submitted_at as is_submitted',
            'report_code_verified as id',
            'approved_at'
        ];
        return $model->newQuery()
                    ->select(...$select)
                    ->join('crawling_type', 'reported_products.report_type', '=', 'crawling_type.id_crawling_type')
                    ->whereNotNull('submitted_at')
                    ->whereNotNull('report_code_verified')
                    ->when(is_admin() || is_direktur() || is_kepala_balai(), function($query) use ($select) {
                        $petugas_role_code = 0;

                        // if (is_kepala_loka()) {
                        //     $petugas_role_code = config('app.roles.petugas_loka.role_code');
                        // }

                        if (is_kepala_balai()) {
                            $petugas_role_code = config('app.roles.petugas_balai.role_code');
                        }

                        if (is_koordinator()) {
                            $petugas_role_code = config('app.roles.petugas.role_code');
                        }

                        $query->select('users.name as proposed_by', ...$select)
                            ->join('users', 'users.id', '=', 'reported_products.id_user')
                            ->when(!is_admin(), function($query) use ($petugas_role_code) {
                                $query->where('users.id_upt_location', '=', auth()->user()->id_upt_location)
                                    ->where('users.role_code', '=', $petugas_role_code);
                            });
                    })
                    ->when(request('status'), function($query, $status) {
                        if ($status === 'disetujui') {
                            $query->whereNotNull('approved_at');
                        }

                        if ($status === 'menunggu') {
                            $query->whereNull('approved_at');
                        }
                    })
                    ->groupBy(DB::raw('DATE(periode)'), 'report_code_verified');;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $tableId = 'direktur-rekomendasitakedowndatatable-table';
        return $this->builder()
                    ->setTableId($tableId)
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->orderBy(2)
                    ->addAction([
                        'width' => '5%'
                    ])
                    ->parameters([
                        'mark' => true,
                        'searchDelay' => 500,
                        'language' => [
                            'processing' => '<div class="align-center text-center overlay-loading">
                                <div class="overlay-loading-content">
                                <img src="/img/dual_ring_loading.svg" />
                                </div>
                            </div>'
                        ],
                        'dom' => '<"row d-flex"<"col-md-2 mt-2"l>
                        <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"<"mr-2"f><p>>>t
                        <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                        <"mt-3"B>
                        <"bg-transparent"r>',
                        'columnDefs' => [
                            [
                                "targets" => 0,
                                "orderable" => false,
                                "className" => 'select-checkbox',
                                "checkboxes" => [
                                    "selectRow" => true
                                ]
                            ]
                        ]
                    ])
                    ->initComplete("function() {
                        const table = window['".config('datatables-html.namespace', 'LaravelDataTables')."']['".$tableId."'];
                        const submitButton = $('#".$tableId."_submit');
                        const rejectButton = $('#".$tableId."_reject');
        
                        $('thead').on('click', '.select-checkbox', function(e) {
                            const selectAll = $('th input[type=checkbox]');
                            if(selectAll.is(':checked')) {
                                table.rows().select();
                                submitButton.removeAttr('disabled');
                                rejectButton.removeAttr('disabled');
                            } else {
                                table.rows().deselect();
                                submitButton.attr('disabled', 'disabled');
                                rejectButton.attr('disabled', 'disabled');
                            }
                        });
        
                        $('tbody').on('click', 'input[type=checkbox]', function(e) {
                            const cellIndex = e.target.parentElement._DT_CellIndex.row;
                            const checkboxes = table.column(0).checkboxes.selected().length;
        
                            if($(this).is(':checked')) {
                                table.rows(cellIndex).select();
                            } else {
                                table.rows(cellIndex).deselect();
                            }
        
                            if(checkboxes > 0) {
                                submitButton.removeAttr('disabled');
                                rejectButton.removeAttr('disabled');
                            } else {                
                                submitButton.attr('disabled', 'disabled');
                                rejectButton.attr('disabled', 'disabled');
                            }
                        });
                        
                        $('.dataTables_filter label').replaceWith(function(){
                            return $(this).contents();
                        });
        
                        const checkboxes = table.column(0).checkboxes.selected().length;
                        if(checkboxes === 0) {
                            submitButton.attr('disabled', 'disabled');
                            rejectButton.attr('disabled', 'disabled');
                        }
        
                        table.on( 'order.dt search.dt draw.dt', function () {
                            table.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                cell.innerHTML = i+1;
                            } );
                        } ).draw();
                    }")
                    ->buttons(
                        Button::make('submit')->attr(['id' => $tableId."_submit"]),
                        Button::make('reject')->attr(['id' => $tableId."_reject", 'data-toggle' => 'modal', 'data-target' => '#reject-modal']),
                    );
    }
    
    public function submit() 
    {
        $selectedReportCode = explode(',', request()->selected);
        foreach ($selectedReportCode as $reportCodeVerified) {
            ReportedProduct::setApprovedByDirektur($reportCodeVerified);
        }

        return response('success');
    }
    
    public function reject(){
        // NOT YET IMPLEMENTED
        // $selectedReportCode = explode(',', request()->selected);

        // foreach ($selectedReportCode as $reportCode) {
        //     ReportedProduct::setRejectedByKoordinator($reportCode);
        // }

        // return response('success');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns = [
            Column::make('report_code')
                ->searchable(false)
                ->orderable(false),
            Column::make('report_code')
                ->title('No')
                ->searchable(false)
                ->orderable(false)
                ->width('2%'),
            Column::make('report_code')
                ->title('Kode')
                ->addClass('text-center')
                ->width('15%'),
            Column::make('periode')->title('Periode')->width('10%'),
            Column::make('report_type')->title('Jenis Laporan'),
            Column::make('approved_at')->title('Status'),
            Column::make('users.name')
                ->data('proposed_by')
                ->title('Dilaporkan Oleh')
                ->width('15%')
        ];

        return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'RekomendasiTakedown_' . date('YmdHis');
    }
}
