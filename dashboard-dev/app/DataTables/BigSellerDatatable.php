<?php

namespace App\DataTables;

use App\Models\Keyword;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Str;

class BigSellerDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('keywords', function($query) {
                return "<a href='".route('big-seller.get-detail-by-keyword', Str::slug($query->keywords))."'>{$query->keywords}</a>";
            })
            ->editColumn('action', function($query) {
                return view('utilities.action', [
                    'show' => [
                        'url' => route('big-seller.get-detail-by-keyword', Str::slug($query->keywords)),
                        'text' => 'Detail'
                    ]
                ])->render();
            })
            ->rawColumns(['action', 'keywords']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Keyword $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Keyword $model)
    {
        return $model->newQuery()
            ->select('keywords', DB::raw('COUNT(products.id_product) as product_total'))
            ->join('products', 'products.id_keywords', '=', 'keywords.id_keywords')
            ->groupBy("keywords");
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('bigsellerdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->ajax([
                        'coba' => 's'
                    ])
                    ->parameters([
                        'mark' => true,
                        'searchDelay' => 500,
                        'language' => [
                            'processing' => '<div class="align-center text-center overlay-loading">
                                <div class="overlay-loading-content">
                                <img src="/img/dual_ring_loading.svg" />
                                </div>
                            </div>'
                        ],
                        'dom' => '<"row d-flex"<"col-md-2 mt-2"l>
                        <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"<"mr-2"f><p>>>t
                        <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                        <"bg-transparent"r>'
                    ])
                    ->orderBy(1)
                    ->initComplete("function() {
                        $('.dataTables_filter label').replaceWith(function(){
                            return $(this).contents();
                        });
                        const table = window['".config('datatables-html.namespace', 'LaravelDataTables')."']['bigsellerdatatable-table'];
                        table.on( 'order.dt search.dt draw.dt', function () {
                            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                cell.innerHTML = i+1;
                            } );
                        } ).draw();
                    }");
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('')
                ->content('')
                ->title('No')
                ->width(10)
                ->addClass('text-center'),
            Column::make('keywords'),
            Column::make('product_total')
                ->width(100)
                ->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(20)
                ->addClass('text-center')
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'BigSeller_' . date('YmdHis');
    }
}
