<?php

namespace App\DataTables;

use App\Models\Link;
use App\Models\ReportedProduct;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class DraftTakedownDetailDataTable extends DataTable
{
    protected $actions = ['delete'];

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $datatable = datatables()->eloquent($query);
        return $datatable
            ->makeHidden([-1])
            ->editColumn('periode', function($query) {
                return date('d-m-Y', strtotime($query->periode));
            })
            ->editColumn('marketplace', function($query) {
                return view('utilities.marketplace', ['name' => $query->marketplace]);
            })
            ->editColumn('product', '{{ ucwords($product) }}')
            ->editColumn('store', '{{ ucwords($store) }}');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ReportedProduct $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ReportedProduct $model)
    {
        $select = [
            'links.id as DT_RowIndex',
            'reported_products.periode',
            'commodities.commodity as commodity',
            'products.name as product',
            'stores.name as store',
            'marketplace.name as marketplace',
            'products.updated_at as last_visit', 
            'reported_products.submitted_at'
        ];

        return $model->newQuery()
                ->select(...$select)
                ->join('links', 'links.id', '=', 'reported_products.id_link')
                ->join('products', 'products.id_product', '=', 'links.id_product')
                ->join('stores', 'stores.id_store', '=', 'products.id_store')
                ->join('marketplace', 'marketplace.id_marketplace', '=', 'stores.id_marketplace')
                ->join('commodities', 'commodities.id_commodity', '=', 'products.id_commodity')
                ->where('report_code', '=', request('reportCode'))
                ->when(!is_admin(), function($query) {
                    $query->where('reported_products.id_user', '=', auth()->user()->id);
                })
                ->when(is_admin(), function($query) use ($select) {
                    $query->select('users.name as proposed_by', ...$select)
                        ->join('users', 'users.id', '=', 'reported_products.id_user');
                });
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $model = app(ReportedProduct::class);
        $builder = $this->builder();
        $query = $this->query($model)->first();
        
        if (!is_null($query) && is_null($query->submitted_at)) {
            $builder
                ->buttons(
                    Button::make('create')
                        ->text("<i class='fas fa-plus'></i>")
                        ->attr([
                            'id' => "drafttakedowndetail-table_create",
                            'data-toggle' => 'modal',
                            'data-target' => '#add-new-product-modal'
                        ]),
                    Button::make('delete')
                        ->text("<i class='far fa-trash-alt'></i>")
                        ->attr(['id' => "drafttakedowndetail-table_delete"])
                )
                ->columnDefs([
                    [
                        "targets" => -1,
                        "orderable" => false,
                        "className" => 'select-checkbox',
                        "checkboxes" => [
                            "selectRow" => true
                        ]
                    ]
                ]);
        } else {
            $builder
                ->columnDefs([
                    [
                        "targets" => -1,
                        "visible" => false
                    ]
                ]);
        }

        return $builder
                    ->setTableId('drafttakedowndetail-table')
                    ->columns($this->getColumns())
                    ->addDatains()
                    ->addIndex()
                    ->minifiedAjax();
    }
    
    public function delete(){
        $updateLinkTo = [
            'id_status' => config('app.status')['active']['id_status'],
            'updated_at' => date('Y-m-d H:i:s'),
            'id_user' => auth()->user()->id,
            'id_offense' => null,
            'note' => null
        ];
        Link::whereIn('id', explode(',', request()->selected))->update($updateLinkTo);

        ReportedProduct::whereIn('id_link', explode(',', request()->selected))->delete();
        return response('sss');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('periode'),
            Column::make('commodity')
                ->name('commodities.commodity')
                ->title('Komoditas'),
            Column::make('product')
                ->name('products.name')
                ->title('Product'),
            Column::make('store')
                ->name('stores.name')
                ->title('Store'),
            Column::make('marketplace')
                ->name('marketplace.name')
                ->addClass('text-center'),
            Column::make('last_visit')
                ->addClass('text-center')
                ->name('products.updated_at')
                ->title('Last Visit'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'DraftTakedownDetail_' . date('YmdHis');
    }
}
