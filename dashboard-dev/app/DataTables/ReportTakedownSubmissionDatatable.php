<?php

namespace App\DataTables;

use App\Models\ReportedProduct;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ReportTakedownSubmissionDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('periode', '{{ date("d-m-Y", strtotime($periode)) }}')
            ->editColumn('created_at', '{{ date("d-m-Y", strtotime($created_at)) }}')
            ->editColumn('submitted_at', '{{ date("d-m-Y", strtotime($submitted_at)) }}')
            ->filterColumn('status.status', function($query, $idStatus) {
                if (intval($idStatus) === 0) {
                    $query->where('status.status', 'like', "%$idStatus%");
                } else {
                    $query->where('status.id_status', '=', $idStatus);
                }
            })
            ->filterColumn('periode', function($query, $periode) {
                $dateRange = explode('/', $periode);
                if (count($dateRange) > 1) {
                    $startDate = $dateRange[0];
                    $endDate = $dateRange[1];
                    $query->whereDate('reported_products.periode','<=', $endDate)
                          ->whereDate('reported_products.periode','>=', $startDate);
                }
            })
            ->addColumn('action', function(ReportedProduct $query) {
                return view('utilities.action', [
                    'show' => [
                        'url' => route('reports.takedown.show-takedown', $query->report_code),
                        'text' => 'Detail'
                    ]
                ])->render();
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ReportedProduct $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ReportedProduct $model)
    {
        return $model->newQuery()
            ->select(
                'reported_products.id_reported_product as id',
                'reported_products.submitted_at as submitted_at',
                'crawling_type.type as report_type',
                'status.status as status',
                'reported_products.report_code as report_code',
                'reported_products.periode',
                'reported_products.created_at',
            )
            ->join('links','links.id','=','reported_products.id_link')
            ->join('status','status.id_status','=','reported_products.id_status')
            ->join('crawling_type','id_crawling_type', '=', 'reported_products.report_type')
            ->groupBy('reported_products.report_code');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('reporttakedownsubmissiondatatable-table')
                    ->columns($this->getColumns())
                    ->autoFill(true)
                    ->minifiedAjax()
                    ->orderBy(1)
                    ->parameters([
                        'mark' => true,
                        'searchDelay' => 500,
                        'language' => [
                            'processing' => '<div class="align-center text-center overlay-loading">
                                <div class="overlay-loading-content">
                                <img src="/img/dual_ring_loading.svg" />
                                </div>
                            </div>'
                        ],
                        'dom' => '<"row d-flex"<"col-md-2 mt-2"l>
                        <"col-md-10 d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-end"<"mr-2"B><"mr-2"f><p>>>t
                        <"bottom d-flex flex-column flex-md-row align-items-center justify-content-between"ip>
                        <"bg-transparent"r>'
                    ])
                    ->initComplete("function() {
                        $('.dataTables_filter label').replaceWith(function(){
                            return $(this).contents();
                        });
                        const table = window['".config('datatables-html.namespace', 'LaravelDataTables')."']['reporttakedownsubmissiondatatable-table'];
                        table.on( 'order.dt search.dt draw.dt', function () {
                            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                cell.innerHTML = i+1;
                            } );
                        } ).draw();
        
                        let startDate = null;
                        let endDate = null;
                        const filterModal = $('#filter-data-reports');
                        const dateRangeFilter = $('input[name=daterange]');
                        filterModal.on('click','button[type=submit]', function(){
                            filterModal.modal('toggle');

                            const status = $('#filter-status').val()
                            if(status) {
                                table.columns(4).search(status).draw();
                            }

                            if (startDate && endDate) {
                                table.columns(2).search(startDate + '/' + endDate).draw()
                            }
                         });
        
                         // Reset Button
                        filterModal.on('click','button[type=reset]', function(){
                            const status = $('#filter-status').val('')
                            dateRangeFilter.val('');

                            table.columns([4,2]).search('').draw();
                            filterModal.modal('toggle');
                        });

                        dateRangeFilter.daterangepicker({
                            'autoApply': true,
                            maxDate: moment().startOf('hour').add(32, 'hour'),
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                            },
                            alwaysShowCalendars: true
                        });

                        dateRangeFilter.on('apply.daterangepicker', function(ev, picker) {
                            startDate = picker.startDate.format('YYYY-MM-DD');
                            endDate = picker.endDate.format('YYYY-MM-DD');
                        });

                        dateRangeFilter.on('cancel.daterangepicker', function(ev, picker) {
                            $(this).val('');
                        });
                    }")
                    ->buttons(
                        Button::make('filter')
                            ->addClass('btn-sm')
                            ->attr([
                                'data-toggle' => 'modal',
                                'data-target' => '#filter-data-reports'
                            ])
                    )
                    ->addAction([
                        'width' => '8%'
                    ])
                    ->addRowNumber();
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('report_code')->title('Nomor Pengajuan'),
            Column::make('periode')->width('10%')->addClass('text-center'),
            Column::make('crawling_type.type')
                ->data('report_type')
                ->title('Jenis Crawling')
                ->addClass('text-center')
                ->width('10%'),
            Column::make('status.status')
                ->data('status')
                ->title('Status')
                ->width('8%'),
            Column::make('created_at')
                ->title('Tanggal Dibuat')->width('12%')->addClass('text-center'),
            Column::make('submitted_at')
                ->title('Tanggal Surat Pengajuan')->width('12%')->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ReportTakedownSubmission_' . date('YmdHis');
    }
}
