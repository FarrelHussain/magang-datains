<?php

namespace App\Exports;

use App\Models\ReportedProduct;
use App\Services\ReportedProductService;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\Hyperlink;

class DraftTakedownExport implements FromCollection, WithProperties, WithHeadings, ShouldAutoSize, WithStyles, WithEvents
{
    use Exportable;

    private $_selected;
    private $reportedProductService;

    public function __construct($selected = [])
    {
        $this->_selected = $selected;
        $this->reportedProductService = app(ReportedProductService::class);
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if (count($this->_selected) > 0) {
            return $this->reportedProductService->export_by_report_code($this->_selected);
        }

        return ReportedProduct::all();
    }
    
    public function prepareRows($rows): array
    {
        return array_map(function ($row) {
            $row->periode = date('Y-m-d', strtotime($row->periode));
            $row->marketplace_name = ucwords($row->marketplace_name);

            return $row;
        }, $rows);
    }

    public function headings(): array
    {
        return [
            'No',
            'Kode',
            'Periode',
            'Komoditas',
            'Produk',
            'Nama Toko',
            'Marketplace',
            'Jenis Laporan'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                /** @var Worksheet $sheet */
                foreach ($event->sheet->getColumnIterator('F') as $row) {
                    foreach ($row->getCellIterator() as $key => $cell) {
                        if ($key > 1) {
                            if (str_contains($cell->getValue(), '://')) {
                                $value = explode('|', $cell->getValue());
                                $cell->setValue($value[0]);
                                $cell->setHyperlink(new Hyperlink($value[1], $value[0]));

                                $event->sheet->getStyle($cell->getCoordinate())->applyFromArray([
                                    'font' => [
                                        'color' => ['rgb' => '0000FF'],
                                        'underline' => 'single'
                                    ]
                                ]);
                            }
                        }
                    }
                }

                /** @var Worksheet $sheet */
                foreach ($event->sheet->getColumnIterator('E') as $row) {
                    foreach ($row->getCellIterator() as $key => $cell) {
                        if ($key > 1) {
                            if (str_contains($cell->getValue(), '://')) {
                                $value = explode('|', $cell->getValue());
                                $cell->setValue($value[0]);
                                $cell->setHyperlink(new Hyperlink($value[1], $value[0]));

                                $event->sheet->getStyle($cell->getCoordinate())->applyFromArray([
                                    'font' => [
                                        'color' => ['rgb' => '0000FF'],
                                        'underline' => 'single'
                                    ]
                                ]);
                            }
                        }
                    }
                }
            },
        ];
    }
    
    public function styles(Worksheet $sheet)
    {
        return [
            1    => [
                'font' => ['bold' => true, 'size' => 14],
                'fill' => [
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                    'startColor' => ['argb' => '42c3ff']
                ]
            ],
        ];
    }

    public function properties(): array
    {
        return [
            'creator'        => 'Datains',
            'lastModifiedBy' => 'Datains',
            'title'          => 'Exported Draft Takedown',
            'description'    => 'Draft Takedown',
            'subject'        => 'Draft Takedown',
            'keywords'       => 'draft,takedown,datains',
            'category'       => 'Draft Takedown',
            'company'        => 'BPOM',
        ];
    }
}
