<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;

class ReportedProductExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    private $data;
    private $petugas;
    /**
     * @return \Illuminate\Support\Collection
     */

    public function __construct($data, $petugas = false)
    {
        $this->data = $data;
        $this->petugas = $petugas;
    }

    public function collection()
    {

        $data = $this->data;

        $data = collect($data);
        //dd($data);

        return $data;
    }

    public function headings(): array
    {
        $hedaer = [
            'No',
            'ID Takedown',
            'Jenis Platform',
            'Nama Platform',
            'Nama Pemilik',
            "URL",
            "Komoditas",
            "Jenis Pelanggaran",
            "Nama Produk",
            "Produsen",
            "Kandungan",
            'Golongan',
            'Kelas',
            'Periode',
            'Rekomendasi',
            'File',
            'Status',
            'Waktu Input',
            'Asal Produk',
            'Screenshot',
            'Petugas',
            'Lokasi Wilayah Pelapak',
        ];
        /*
        if ($this->petugas) {
            $hedaer[] = "Petugas";
        }
        */
        return $hedaer;
    }

    public function registerEvents(): array
    {
        $cellrange = 'A1:V1';
        if ($this->petugas) {
            $cellrange = 'A1:V1';
        }

        return [
            AfterSheet::class =>
            function (AfterSheet $event) use ($cellrange) {
                $styles = [
                    'font' => ['bold' => false, 'size' => 12],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => ['argb' => '42c3ff']
                    ]
                ];
                $event->sheet->getDelegate()->getStyle($cellrange)->applyFromArray($styles);
            }
        ];
    }
}
