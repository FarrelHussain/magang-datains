<?php

function getMarketplace($marketplaceId = null)
{
    $marketplace = \App\Models\Marketplace::where('id_marketplace', '=', $marketplaceId)->get();
    return $marketplace[0]->name;
}

function getStatusId($txtStatus = null)
{
    $id_status = \App\Models\Status::select('id_status')->where('status', $txtStatus)->first()['id_status'];
    if(isset($id_status)){
        return $id_status;
    }
    return 0;
}