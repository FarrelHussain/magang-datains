<?php

use Illuminate\Support\Facades\Auth;

if (!function_exists('is_admin')) {
    function is_admin() {
        return Auth::user()->is_admin();
    }
}

if (!function_exists('is_operator')) {
    function is_operator() {
        return Auth::user()->is_operator();
    }
}

if (!function_exists('is_admin')) {
    function is_admin() {
        return Auth::user()->is_admin();
    }
}

if (!function_exists('is_direktur')) {
    function is_direktur() {
        return Auth::user()->is_direktur();
    }
}

if (!function_exists('is_koordinator')) {
    function is_koordinator() {
        return Auth::user()->is_koordinator();
    }
}

if (!function_exists('is_petugas')) {
    function is_petugas() {
        return Auth::user()->is_petugas();
    }
}

if (!function_exists('is_petugas_balai')) {
    function is_petugas_balai() {
        return Auth::user()->is_petugas_balai();
    }
}

if (!function_exists('is_stakeholder')) {
    function is_stakeholder() {
        return Auth::user()->is_stakeholder();
    }
}

if (!function_exists('is_koordinator_balai')) {
    function is_koordinator_balai() {
        return Auth::user()->is_koordinator_balai();
    }
}

if (!function_exists('is_kepala_balai')) {
    function is_kepala_balai() {
        return Auth::user()->is_kepala_balai();
    }
}
if (!function_exists('is_petugas_loka')) {
    function is_petugas_loka() {
        return Auth::user()->is_petugas_loka();
    }
}
if (!function_exists('is_kepala_loka')) {
    function is_kepala_loka() {
        return Auth::user()->is_kepala_loka();
    }
}