<?php

if (!function_exists('activity_log')) {
    function activity_log(string $logName, array $properties = []) {
        $params = array_merge([
            'ip_address' => request()->ip(),
            'page'  => request()->url()
        ], $properties);
        
        activity()
            ->withProperties($params)
            ->causedBy(auth()->id())
            ->log($logName);
    }
}

if (!function_exists('invert_color')) {
    function invert_color(string $hex, bool $bw = false) {
        if (strpos($hex, '#')) {
            $hex = substr($hex, 1);
        }

        // convert 3-digit hex to 6-digits.
        if (strlen($hex) === 3) {
            $hex = $hex[0] + $hex[0] + $hex[1] + $hex[1] + $hex[2] + $hex[2];
        }

        // 6 + 1 (#)
        if (strlen($hex) !== 7) {
            throw new Exception("Invalid HEX '$hex' color.");
        }

        $r = intval(substr($hex, 0, 2), 16);
        $g = intval(substr($hex, 2, 4), 16);
        $b = intval(substr($hex,4, 6), 16);
        if ($bw) {
            // http://stackoverflow.com/a/3943023/112731
            return ($r * 0.299 + $g * 0.587 + $b * 0.114) > 186
                ? '#000000'
                : '#FFFFFF';
        }
        // invert color components
        $r = strval(255 - $r);
        $g = strval(255 - $g);
        $b = strval(255 - $b);
        // pad each with zeros and return
        return "#" . str_pad($r, 3) . str_pad($g, 3) . str_pad($b, 3);
    }
}

if (!function_exists('random_color')) {
    function random_color(string $name) {
        return '#'.substr(md5($name), 0, 6);
    }
}