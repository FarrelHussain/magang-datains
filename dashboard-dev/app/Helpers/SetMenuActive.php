<?php
function setActive($route, $menu, $kind = 'active', $uri = false)
{
    $active = '';
    $r = Route::currentRouteName($route);
    if ($uri) {
        $r = Route::current($route)->uri;
    }
    if (is_array($menu)) {
        foreach ($menu as $m => $item) {
            if ($r === $item) {
                $active = $kind;
            }
        }
    } else if ($r === $menu) {
        $active = $kind;
    }
    return $active;
}

if (!function_exists('pageJS')) {
    function pageJS($to = null)
    {
        return asset('js/pages/' . $to);
    }
}

if (!function_exists('pill_active')) {
    function pill_active($index, $status, $home = false)
    {
        $query = request()->query();
        if (count($query) === 0 && $home) {
            return 'active';
        }
        if (isset($query[$index])) {
           if ($query[$index] === $status) {
               return 'active';
           }
        }
    }
}