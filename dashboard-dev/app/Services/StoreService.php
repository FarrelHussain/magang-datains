<?php 

namespace App\Services;

use App\Contracts\StoreInterface;
use App\Repositories\MarketplaceRepository;
use App\Repositories\StoreRepository;

class StoreService implements StoreInterface
{
    public $storeRepository;
    public $marketplaceRepository;
    public function __construct(StoreRepository $storeRepository, MarketplaceRepository $marketplaceRepository)
    {
        $this->storeRepository = $storeRepository;
        $this->marketplaceRepository = $marketplaceRepository;
    }

    public function save(array $data)
    {
        $data['id_marketplace'] = $this->marketplaceRepository
                                        ->findOneBy(['name' => $data['nama_platform']])
                                        ->id_marketplace;
        dd($data);
        return $this->storeRepository->save($data);
    }
}
