<?php

namespace App\Services;

use App\DataTables\DraftTakedown;
use App\Imports\DraftTakedownImport;
use App\Repositories\DraftTakedownRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class DraftTakedownService
{
    public $draftTakedownRepository;
    public function __construct(DraftTakedownRepository $draftTakedownRepository)
    {
        $this->draftTakedownRepository = $draftTakedownRepository;
    }

    public function import($import_data)
    {
        Validator::make($import_data, [
            'takedown_file' => 'required|file|mimes:xlsx,xls',
        ])->validate();

        $file = request()->file('takedown_file');
        $filename = Str::slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME))."-";
        $filename .= time()."-DTD-";
        $filename .= sprintf("%02d", auth()->user()->id).".xlsx";
        
        $file = $file->move(storage_path('app/public/imports'), $filename);

        app(DraftTakedownImport::class)->import($file->getPathname(), null, \Maatwebsite\Excel\Excel::XLSX);
    }
}
