<?php 

namespace App\Services;

use App\Contracts\StoreInterface;
use App\Repositories\ReportedProductRepository;
use App\Repositories\MarketplaceRepository;
use App\Repositories\StoreRepository;

class ReportedProductService
{
    public $reportedProductRepository;
    public function __construct(
        ReportedProductRepository $reportedProductRepository
    ) {
        $this->reportedProductRepository = $reportedProductRepository;
    }

    public function save(array $data)
    {
        return $this->reportedProductRepository->save($data);
    }

    public function get_takedown_submission()
    {
        return $this->reportedProductRepository->get_takedown_submission();
    }

    public function get_filter_data()
    {
        return $this->reportedProductRepository->get_filter_data();
    }

    public function export_by_report_code(array $reportCode)
    {
        return $this->reportedProductRepository->get_product_by_report_code($reportCode);
    }
}
