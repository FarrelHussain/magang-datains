<?php 

namespace App\Services;

use App\Contracts\MarketplaceInterface;
use App\Repositories\MarketplaceRepository;

class StoreService implements MarketplaceInterface
{
    public $marketplaceRepository;
    public function __construct(MarketplaceRepository $marketplaceRepository)
    {
        $this->marketplaceRepository = $marketplaceRepository;
    }
    public function save()
    {

    }

    public function update()
    {

    }

    public function get_marketplace_by_name(string $marketplace_name)
    {
        return $this->marketplaceRepository->findBy(['name' => $marketplace_name]);
    }
}
