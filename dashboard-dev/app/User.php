<?php

namespace App;

use App\Http\Traits\AdminAuth;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable, AdminAuth, LogsActivity;
    
    protected static $logFillable = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'role_code','id_upt_location'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->properties = [
            "ip_address" => request()->ip()
        ];
        $activity->description = "log.${eventName}";
    }

    public static function scopeOperator($q)
    {
        $admin = config('app.roles.super_admin');
        return self::where('role_code', '!=', $admin['role_code'])
        ->select('users.*', DB::raw('(CASE WHEN upt_locations.name = "Nasional" THEN "Pusat" ELSE upt_locations.name END) AS location'))
        ->join('upt_locations', 'upt_locations.id_upt_location', 'users.id_upt_location')
        ->orderBy('users.created_at','DESC')
        ->with('role');

        $direktur = config('app.roles.direktur');
        return self::where('role_code', '!=', $direktur['role_code'])->with('role');
    }

    public function role()
    {
        return $this->hasOne('App\Models\Role', 'role_code', 'role_code');
    }
}
